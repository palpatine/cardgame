﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using NUnit.Framework;

namespace Assets.Tests.TestTools
{
    internal class GameStateBuilder : IGameStateProvider
    {
        private bool _isPlatformDeckCreated;

        public UpdateNotifier UpdateNotifier { get; } = new UpdateNotifier();

        public GameState GameState { get; }

        public GameSettings GameSettings { get; }

        public GameStateBuilder()
        {
            GameState = new GameState(UpdateNotifier);
            GameSettings = new GameSettings();
        }

        public PlatformCardSettings GivenPlatformSettings()
        {
            if (_isPlatformDeckCreated)
            {
                Assert.Fail("Platform cards deck was already created");
            }

            var card = new PlatformCardSettings();
            GameSettings.PlatformDeck = new List<PlatformCardSettings>(GameSettings.PlatformDeck) { card };

            card.Name = "Test Platform " + GameSettings.PlatformDeck.Count();
            card.CountInDeck = 1;
            return card;
        }

        public GameStateBuilder GivenMapLevel(PlatformCard[] platforms, bool isLocked = false)
        {
            Assert.IsTrue(platforms.Any());
            var level = new GameLevel(UpdateNotifier)
            {
                IsLocked = isLocked
            };
            foreach (var item in platforms)
            {
                level.Platforms.Add(item);
            }

            GameState.Levels.Add(level);
            return this;
        }

        public PlatformCardBulder UsingPlatform(PlatformCard card) => new PlatformCardBulder(card);

        public Character GivenCharacter()
        {
            var character = new Character(UpdateNotifier);
            GameState.Units.Add(character);
            return character;
        }

        public FoeSettings GivenFoeSettings()
        {
            var foe = new FoeSettings
            {
                Name = "foe: " + GameSettings.Foes.Count()
            };
            GameSettings.Foes = new List<FoeSettings>(GameSettings.Foes) { foe };
            return foe;
        }

        public Foe GivenFoe(FoeSettings foeSettings = null)
        {
            foeSettings = foeSettings ?? GivenFoeSettings();
            var foe = new Foe(UpdateNotifier, foeSettings);
            GameState.Units.Add(foe);
            return foe;
        }

        public GameStateBuilder CreatePlatforsDeck()
        {
            _isPlatformDeckCreated = true;
            var cards = GameSettings.PlatformDeck.SelectMany(x => Enumerable.Range(0, x.CountInDeck).Select(i => new PlatformCard(UpdateNotifier, x)))
                .ToArray();

            GameState.PlatformCards = new Deck<PlatformCard>(cards);
            return this;
        }

        public ObstacleSettings GivenObstacleSettings()
        {
            var settings = new ObstacleSettings
            {
                Name = "Obstacle" + GameSettings.Obstacles.Count()
            };
            GameSettings.Obstacles = new List<ObstacleSettings>(GameSettings.Obstacles) { settings };
            return settings;
        }

        public GameStateBuilder CreateUntisOrder(params IUnit[] units)
        {
            CollectionAssert.AreEquivalent(GameState.Units, units);
            foreach (var item in units)
            {
                GameState.UnitsOrder.Add(item);
            }
            return this;
        }

        public GameStateBuilder SetCurrentUnit(IUnit unit)
        {
            Assert.IsTrue(GameState.Units.Contains(unit));
            Assert.IsTrue(GameState.UnitsOrder.Contains(unit));
            GameState.CurrentUnit = unit;
            return this;
        }

        public ItemSettings GetItemSettings(params string[] kinds)
        {
            var item = new ItemSettings
            {
                Name = "item" + GameSettings.Items.Count(),
                Kinds = kinds
            };
            GameSettings.Items = new List<ItemSettings>(GameSettings.Items) { item };
            return item;
        }

        public Item GetItem(ItemSettings itemSettings)
        {
            var item = new Item(UpdateNotifier)
            {
                Settings = itemSettings
            };

            return item;
        }
    }
}
