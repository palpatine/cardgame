﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.GameLogic.Tools.Notification;

namespace Assets.Tests
{
    class TestNotifyOnChnage : INotifyOnChange
    {
        public IUpdateNotifier UpdateNotifier { get; set; }

        public event Action<ChangeNotification> PropertyChanged;

        public void Trigger(ChangeNotification notification)
        {
            PropertyChanged?.Invoke(notification);
        }
    }
}
