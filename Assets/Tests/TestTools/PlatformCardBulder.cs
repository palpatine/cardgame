﻿using System;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using NUnit.Framework;

namespace Assets.Tests.TestTools
{
    internal class PlatformCardBulder
    {
        private PlatformCard _card;

        public PlatformCardBulder(PlatformCard card) => _card = card;

        public PlatformCardBulder PlaceUnit(IUnit unit, bool isToTheLeftOfObstacle = true, bool isFacingLeft = false)
        {
            if (isToTheLeftOfObstacle)
            {
                unit.Location.UnitId = _card.UnitsToLeftOfObstacle.Count;
                _card.UnitsToLeftOfObstacle.Add(unit);
            }
            else
            {
                if (_card.Obstacle == null)
                {
                    Assert.Fail("Platform must have obstacle");
                }

                unit.Location.UnitId = _card.UnitsToRightOfObstacle.Count;
                _card.UnitsToRightOfObstacle.Add(unit);
            }

            unit.Location.IsToTheLeftOfObstacle = isToTheLeftOfObstacle;
            unit.Location.IsFacingLeft = isFacingLeft;
            unit.Location.PlatformCard = _card;
            unit.Location.CurrentLevelId = _card.Level;

            return this;
        }

        public PlatformCardBulder PlaceObstacle(ObstacleSettings obstacleSettings)
        {
            var obstacle = new Obstacle { Location = _card, Settings = obstacleSettings };
            _card.Obstacle = obstacle;
            return this;
        }
    }
}
