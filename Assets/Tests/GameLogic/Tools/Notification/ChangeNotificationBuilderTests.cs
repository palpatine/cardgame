﻿using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using NUnit.Framework;

namespace Assets.Tests.Tools.Notification
{
    public class ChangeNotificationBuilderTests
    {
        private class TestTarget : TestNotifyOnChnage
        {
            public int Property { get; set; }
            public object Property2 { get; internal set; }
        }

        private class ParentTestTarget : TestNotifyOnChnage
        {
            public TestTarget Target { get; set; }
        }

        [Test]
        public void ShouldTriggerDirectOnChange()
        {
            var updateNotifier = new UpdateNotifier();
            var target = new TestTarget();
            var builder = new ChangeNotificationBuilder<TestTarget>(
                updateNotifier,
                target);

            var wasTriggered = false;
            builder.OnChange(x => x.Property)
                .React(() => wasTriggered = true);

            target.Trigger(new ChangeNotification { PropertyName = "Property" });

            Assert.IsTrue(wasTriggered);
        }

        [Test]
        public void ShouldNotTriggerDirectOnChangeIsDisposed()
        {
            var updateNotifier = new UpdateNotifier();
            var target = new TestTarget();
            var builder = new ChangeNotificationBuilder<TestTarget>(
                updateNotifier,
                target);

            var wasTriggered = false;
            builder.OnChange(x => x.Property)
                .React(() => wasTriggered = true)
                .Dispose();

            target.Trigger(new ChangeNotification { PropertyName = "Property" });

            Assert.IsFalse(wasTriggered);
        }

        [Test]
        public void ShouldTriggerIndirectOnChange()
        {
            var target = new TestTarget();
            var parent = new ParentTestTarget
            {
                Target = target
            };
            var updateNotifier = new UpdateNotifier();

            var builder = new ChangeNotificationBuilder<ParentTestTarget>(
                updateNotifier,
                parent);

            var wasTriggered = false;
            builder.TrackValue(x => x.Target)
                .OnChange(x => x.Property)
                .React(() => wasTriggered = true);

            target.Trigger(new ChangeNotification { PropertyName = "Property" });

            Assert.IsTrue(wasTriggered);
        }


        [Test]
        public void ShouldTriggerIndirectOnChangeWithInitalNull()
        {
            var target = new TestTarget();
            var parent = new ParentTestTarget();
            var updateNotifier = new UpdateNotifier();

            var builder = new ChangeNotificationBuilder<ParentTestTarget>(
                updateNotifier,
                parent);

            var wasTriggered = false;
            builder.TrackValue(x => x.Target)
                .OnChange(x => x.Property)
                .React(() => wasTriggered = true);

            parent.Target = target;
            parent.Trigger(new ChangeNotification
            {
                PropertyName = "Target",
                NewValue = target
            });

            Assert.IsTrue(wasTriggered);
            wasTriggered = false;
            updateNotifier.Update();
            target.Trigger(new ChangeNotification { PropertyName = "Property" });

            Assert.IsTrue(wasTriggered);
        }

        [Test]
        public void ShouldNotTriggerIndirectOnChangeWithInitalNullIfDisposed()
        {
            var target = new TestTarget();
            var parent = new ParentTestTarget();
            var updateNotifier = new UpdateNotifier();

            var builder = new ChangeNotificationBuilder<ParentTestTarget>(
                updateNotifier,
                parent);

            var wasTriggered = false;
            builder.TrackValue(x => x.Target)
                .OnChange(x => x.Property)
                .React(() => wasTriggered = true)
                .Dispose();

            parent.Target = target;
            parent.Trigger(new ChangeNotification
            {
                PropertyName = "Target",
                NewValue = target
            });

            Assert.IsFalse(wasTriggered);
            wasTriggered = false;
            updateNotifier.Update();
            target.Trigger(new ChangeNotification { PropertyName = "Property" });

            Assert.IsFalse(wasTriggered);
        }

        [Test]
        public void ShouldTriggerOnChangeWithAlternativeProperties()
        {
            var target = new TestTarget();
            var updateNotifier = new UpdateNotifier();

            var builder = new ChangeNotificationBuilder<TestTarget>(
                updateNotifier,
                target);

            var wasTriggered = false;
            builder
                .OnChange(x => x.Property)
                .Or(x => x.Property2)
                .React(() => wasTriggered = true);

            target.Trigger(new ChangeNotification { PropertyName = "Property" });
            Assert.IsTrue(wasTriggered);
            wasTriggered = false;
            target.Trigger(new ChangeNotification { PropertyName = "Property2" });
            Assert.IsTrue(wasTriggered);
        }


    }
}