﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class TestValidator<T> : ISettingsValidator<T>
    {
        private readonly bool valid;

        public TestValidator(bool valid) => this.valid = valid;

        public IEnumerable<string> Validate(
                        GameSettings gameSettings,
                        IEnumerable<(string parentName, T item)> items, string parentKind)
            => valid ? Enumerable.Empty<string>() : items.Select((x, i) => $"{typeof(T).Name}|{parentKind}|{x.parentName}|{i}");
    }

    public class TestSettingsValidationManager : ISettingsValidationManager
    {
        private readonly Dictionary<Type, bool> valid;
        private List<bool> _wasSelectorsContextDisposed = new List<bool>();
        private List<IEnumerable<string>> _selectorsRegistrations = new List<IEnumerable<string>>();

        public TestSettingsValidationManager(Dictionary<Type, bool> valid) => this.valid = valid;

        public Dictionary<string, int> Parameters { get; } = new Dictionary<string, int>();
        public List<string> VerifiedSelectors { get; } = new List<string>();

        public int GetParameterUsage(object parent, string name)
        {
            Assert.IsTrue(Parameters.ContainsKey(name));
            return Parameters[name];
        }

        public IDisposable RegisterDeclaredParameters(object parent, IEnumerable<string> selectors)
        {
            _selectorsRegistrations.Add(selectors);
            var id = _wasSelectorsContextDisposed.Count();
            _wasSelectorsContextDisposed.Add(false);
            return new Disposer(() => { _wasSelectorsContextDisposed[id] = true; });
        }

        public IEnumerable<string> Validate<T>(
                        GameSettings gameSettings,
                        IEnumerable<(string parentName, T item)> items, string parentKind)
        {
            if (items.Any(x => x.item == null))
            {
                Assert.Fail("Null value is not allowed in items item. " + typeof(T).Name);
            }

            return valid.ContainsKey(typeof(T)) && valid[typeof(T)] ? Enumerable.Empty<string>() : items.Select((x, i) => $"{typeof(T).Name}|{parentKind}|{x.parentName}|{i}");
        }

        public IEnumerable<string> Validate<T>(GameSettings gameSettings, string parentName, T item, string parentKind)
        {
            if (item == null)
            {
                return Enumerable.Empty<string>();
            }

            return Validate(gameSettings, new[] { (parentName, item) }, parentKind);
        }

        public IEnumerable<string> Validate<T>(GameSettings gameSettings)
            => valid.ContainsKey(typeof(T)) && valid[typeof(T)] ? Enumerable.Empty<string>() : new[] { typeof(T).Name };

        public bool VerifyParameter(string selected)
        {
            VerifiedSelectors.Add(selected);
            return Parameters.ContainsKey(selected);
        }

        internal void AssertSelectorRegistrationAndRelease(int registrationid, string selectorName)
        {
            Assert.IsTrue(_selectorsRegistrations[registrationid].Contains(selectorName));
            Assert.IsTrue(_wasSelectorsContextDisposed[registrationid]);
        }
    }
}
