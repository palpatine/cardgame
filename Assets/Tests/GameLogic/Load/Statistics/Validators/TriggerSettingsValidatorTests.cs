﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class TriggerSettingsValidatorTests
    {
        private static TriggerSettingsValidator GetValidator()
            => new TriggerSettingsValidator(
                new TestSettingsValidationManager(
                    new Dictionary<Type, bool> { }));

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, TriggerSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequreNameToBeDefinedFromGivenSetOfTriggers()
        {
            var values = new[]{
                ("v1", new TriggerSettings{ Name = Constants.Triggers.BeforeEffectApplied }),
                ("v2", new TriggerSettings{ Name = "wrong name" }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequreConsumableGanedTriggerToDefineKind()
        {
            var values = new[]{
                ("v1", new TriggerSettings{ Name = Constants.Triggers.ConsumableGained }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldRequreConsumableGanedTriggerToDefineKindAsExistingItem()
        {
            var values = new[]{
                ("v1", new TriggerSettings{ Name = Constants.Triggers.ConsumableGained, Kind = "consumable item" }),
                ("v2", new TriggerSettings{ Name = Constants.Triggers.ConsumableGained, Kind = "item" }),
                ("v3", new TriggerSettings{ Name = Constants.Triggers.ConsumableGained, Kind = "not item" }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings
                {
                    Items = new[]
                    {
                        new ItemSettings
                        {
                            Name = "consumable item",
                            Kinds = new []{ Constants.ItemKinds.Consumable }
                        },
                        new ItemSettings
                        {
                            Name = "item",
                        }
                    }
                }, 
                values, 
                "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
