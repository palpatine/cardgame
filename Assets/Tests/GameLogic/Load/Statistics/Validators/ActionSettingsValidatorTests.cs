﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class ActionSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private ActionSettingsValidator GetValidator(
            bool isEffectSettingsValid = true,
            bool IsSelectorSettingsValid = false,
            bool IsConditionSettingsValid = false,
            bool isEquipmentSelectorValid = false,
            bool isRangeSettingsValid = true)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(EffectSettings),isEffectSettingsValid },
                                    { typeof(SelectorSettings), IsSelectorSettingsValid },
                                    { typeof(ConditionSettings),IsConditionSettingsValid },
                                    { typeof(EquipmentSelector), isEquipmentSelectorValid },
                                    { typeof(RangeSettings),isRangeSettingsValid }
                                });
            return new ActionSettingsValidator(_settingsValidationManager);
        }

        private static ActionSettings GetValidItem(Action<ActionSettings> modify = null)
        {
            var item = new ActionSettings
            {
                Effects = new[] { new EffectSettings() },
                Kinds = new[] { "kind" },
                Range = new RangeSettings()
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ActionSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfEffectSettings()
        {
            var values = new[]{
                ("v1", GetValidItem()),
            };
            var validator = GetValidator(isEffectSettingsValid: false);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfEquipmentSettings()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Import = new EquipmentSelector())),
            };

            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EquipmentSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfSelectorSettings()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Selectors = new[] { new SelectorSettings { Name = "selector" } })),
            };
            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(SelectorSettings)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfConditionSettings()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Condition = new ConditionSettings())),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ConditionSettings)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfMinAndMaxRangeSettings()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.MinRange = new RangeSettings{ Name = Constants.Range.Short })),
            };
            var validator = GetValidator(isRangeSettingsValid: false);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRegisterSelectorWithManager()
        {
            var values = new[]{
                ("v1", GetValidItem(x => {
                    x.Selectors = new[] { new SelectorSettings { Name = "selector" }  };
                }))
            };

            var validator = GetValidator(IsSelectorSettingsValid: true);
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            _settingsValidationManager.AssertSelectorRegistrationAndRelease(0, "selector");
        }

        [Test]
        public void ShouldReportErrorIfSelectorIsNotInUse()
        {
            var values = new[]{
                ("v1", GetValidItem(x => {
                    x.Selectors = new[] { new SelectorSettings { Name = "selector" }  };
                }))
            };
            var validator = GetValidator(IsSelectorSettingsValid: true);
            _settingsValidationManager.Parameters.Add("selector", 0);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireActionToHaveAnEffect()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Effects = Enumerable.Empty<EffectSettings>())),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAtLeastOneKind()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Kinds = Enumerable.Empty<string>())),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireMaximalRangeToBeSet()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Range = null)),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireMaximalRangeToBeGreaterThanMinimalRange()
        {
            var values = new[]{
                ("v1", GetValidItem(x =>
                {
                    x.Range = new RangeSettings{ Name = Constants.Range.Self };
                    x.MinRange = new RangeSettings{ Name = Constants.Range.Medium };
                })),
                ("v2", GetValidItem(x =>
                {
                    x.Range = new RangeSettings{ Name = Constants.Range.Medium };
                    x.MinRange = new RangeSettings{ Name = Constants.Range.Short, Value = 2 };
                }))
            };

            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldVerifyIfRequiredAmmoIsAmmo()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Ammo = "arrow")),
                ("v2", GetValidItem(x => x.Ammo = "sword")),
                ("v3", GetValidItem(x => x.Ammo = "sticks")),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings
                {
                    Items = new[]
                        {
                            new ItemSettings{ Name = "arrow", Kinds = new[]{ Constants.ItemKinds.Ammo }},
                            new ItemSettings{ Name = "sword", Kinds = new[]{ Constants.ItemKinds.Weapon }}
                        }
                },
                values,
                "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
