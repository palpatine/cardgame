﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class DangerSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private DangerSettingsValidator GetValidator(
            bool isConditionSettingsValid = false,
            bool isActionSettingsValid = false)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(ConditionSettings), isConditionSettingsValid },
                                    { typeof(ActionSettings), isActionSettingsValid }
                                });
            return new DangerSettingsValidator(_settingsValidationManager);
        }

        private static DangerSettings GetItem(Action<DangerSettings> modify = null)
        {
            var item = new DangerSettings
            {
                Name = Guid.NewGuid().ToString(),
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequireAbilityToBeNamed()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Name = null),
                 GetItem(x => x.Name = ""),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Dangers = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAbilityNamedToBeUnique()
        {
            var values = new[]{
                 GetItem(x => x.Name = "name"),
                 GetItem(x => x.Name = "name"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Dangers = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfDangerEffectSettings()
        {
            var values = new[]{
                 GetItem( x => x.Effects = new[]{ new DangerEffectSettings { Effect = new EffectSettings() } }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Dangers = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Danger effect|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfRewardSettings()
        {
            var values = new[]{
                 GetItem( x => x.Reward = new[] { new EffectSettings() }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Dangers = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Danger reward|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfPlatformSelector()
        {
            var values = new[]{
                 GetItem( x => x.Effects = new[]{ new DangerEffectSettings { PlatformSelector = new PlatformSelector() } }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Dangers = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(PlatformSelector)}|Danger|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireFoeToBeDefined()
        {
            var values = new[]{
                 GetItem( x => x.Effects = new[]{ new DangerEffectSettings { Foe = "foe" } }),
                 GetItem( x => x.Effects = new[]{ new DangerEffectSettings { Foe = "not foe" } }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Dangers = values, Foes = new[] { new FoeSettings { Name = "foe" } } });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireReplacementCardToBeDefined()
        {
            var values = new[]{
                 GetItem( x => x.Effects = new[]{ new DangerEffectSettings { Replacement = "card" } }),
                 GetItem( x => x.Effects = new[]{ new DangerEffectSettings { Replacement = "not card" } }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Dangers = values, PlatformDeck = new[] { new PlatformCardSettings { Name = "card" } } });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
