﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{

    public class ResourceSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private ResourceSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new ResourceSettingsValidator(_settingsValidationManager);
        }

        private static ResourceSettings GetItem(Action<ResourceSettings> modify = null)
        {
            var item = new ResourceSettings
            {
                Name = "Item",
                Count = 5
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ResourceSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequireItemToBeDefined()
        {
            var values = new[]{
                ("v1", GetItem()),
                ("v2", GetItem(x => x.Name = "not item"))
            };

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(GetGameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireCountToBePositive()
        {
            var values = new[]{
                ("v1", GetItem()),
                ("v2", GetItem(x => x.Count = 0)),
                ("v3", GetItem(x => x.Count = -7))
            };

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(GetGameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        private static GameSettings GetGameSettings() => new GameSettings { Items = new[] { new ItemSettings { Name = "Item" } } };
    }
}
