﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class FeatureSelectorValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private static GameSettings GetGameSettings() => new GameSettings { PlatformPlaces = new[] { new PlatformPlaceSettings { Name = "Place" } } };

        private FeatureSelectorValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });

            return new FeatureSelectorValidator(_settingsValidationManager);
        }

        private static FeatureSelector GetItem(Action<FeatureSelector> modify = null)
        {
            var item = new FeatureSelector
            {
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, FeatureSelector)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequireFeatureToBeDefined()
        {
            var values = new[]{
                 ("v0",GetItem( x => x.Name = "cave")),
                 ("v1",GetItem( x => x.Name = null)),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings(),
                values,
                "ParentKind");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
