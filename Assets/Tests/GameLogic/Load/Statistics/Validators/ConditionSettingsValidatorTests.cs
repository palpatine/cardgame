﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class ConditionSettingsValidatorTests
    {
        private ConditionSettingsValidator GetValidator(
            bool isConditionValueValid = true)
                => new ConditionSettingsValidator(
                    new TestSettingsValidationManager(
                        new Dictionary<Type, bool> {
                            { typeof(ConditionValueSettings), isConditionValueValid } }));

        private ConditionSettings GetValidItem(Action<ConditionSettings> modify = null)
        {
            var item = new ConditionSettings
            {
                LeftValue = new ConditionValueSettings(),
                RightValue = new ConditionValueSettings(),
                Compare = Constants.Compare.GreaterThen
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ConditionSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfConditionValueSettings()
        {
            var values = new[]{
                ("v1", GetValidItem()),
            };
            var validator = GetValidator(isConditionValueValid: false);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ConditionValueSettings)}|PARENT_KIND|v1|0", errors.First(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ConditionValueSettings)}|PARENT_KIND|v1|1", errors.Last(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAllFieldsToBeSet()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.LeftValue = null)),
                ("v3", GetValidItem(x => x.RightValue = null)),
                ("v2", GetValidItem(x => x.Compare = null)),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(3, errors.Count(), string.Join(Environment.NewLine, errors));

        }
    }
}
