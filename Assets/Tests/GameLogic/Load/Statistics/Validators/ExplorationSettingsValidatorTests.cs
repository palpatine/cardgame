﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class ExplorationSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private static GameSettings GetGameSettings() => new GameSettings { PlatformPlaces = new[] { new PlatformPlaceSettings { Name = "Place" } } };

        private ExplorationSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });

            return new ExplorationSettingsValidator(_settingsValidationManager);
        }

        private static ExplorationSettings GetItem(Action<ExplorationSettings> modify = null)
        {
            var item = new ExplorationSettings
            {
                Place = "Place",
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ExplorationSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfAmbushSettings()
        {
            var values = new[]{
                ("v1", GetItem()),
                ("v1", GetItem(x=>x.Ambushes = new[] { new AmbushSettings() })),
            };
            var validator = GetValidator();
            var errors = validator.Validate(GetGameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(AmbushSettings)}|PARENT_KIND|v1 in Exploration|0", errors.Last(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequirePlaceToBeDefined()
        {
            var values = new[]{
                ("v1", GetItem()),
                ("v2", GetItem(x => x.Place = "not place"))
            };

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(GetGameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

    }
}
