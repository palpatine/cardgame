﻿using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Load.Settings.Validators;
using NUnit.Framework;
using System;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class EquipmentSelectorValidatorTests
    {
        private static EquipmentSelectorValidator GetValidator()
                => new EquipmentSelectorValidator();

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, EquipmentSelector)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldVerifyKindToBeOnOfAllowedValues()
        {
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings()
                {
                    EquipableItemKinds = new[] { "sword", "ring" },
                },
                new[]
                    {
                        ("v1", new EquipmentSelector { Kind = "wand", Property=Constants.EquipmentSelectorProperties.Item}),
                        ("v1", new EquipmentSelector { Kind = "sword", Property=Constants.EquipmentSelectorProperties.Item}),
                        ("v1", new EquipmentSelector { Kind = "ring", Property=Constants.EquipmentSelectorProperties.Item})
                    },
                    "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldVerifyPropertyIsOneOfAllowedValues()
        {
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings()
                {
                    EquipableItemKinds = new[] { "sword", "ring" },
                },
                new[]
                    {
                        ("v1", new EquipmentSelector { Kind = "ring", Property = Constants.EquipmentSelectorProperties.Item }),
                        ("v1", new EquipmentSelector { Kind = "sword", Property = Constants.EquipmentSelectorProperties.Item }),
                        ("v1", new EquipmentSelector { Kind = "ring", Property = "thing" })
                    },
                    "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireBothKindAndPropertyValuesToBeSet()
        {
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings()
                {
                    EquipableItemKinds = new[] { "sword", "ring" },
                },
                new[]
                    {
                        ("v1", new EquipmentSelector { Kind = "ring", }),
                        ("v1", new EquipmentSelector { Property = Constants.EquipmentSelectorProperties.Item }),
                    },
                    "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
