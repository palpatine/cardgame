﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class AmbushSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private AmbushSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new AmbushSettingsValidator(_settingsValidationManager);
        }

        private static AmbushSettings GetItem(Action<AmbushSettings> modify = null)
        {
            var item = new AmbushSettings
            {
                Foe = "Foe",
                Count = 5
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, AmbushSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequireFoeToBeDefined()
        {
            var values = new[] {
                ("v1", GetItem()),
                ("v2", GetItem(x => x.Foe = "not foe"))
            };

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(GetGameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireCountToBePositive()
        {
            var values = new[] {
                ("v1", GetItem()),
                ("v2", GetItem(x => x.Count = 0)),
                ("v3", GetItem(x => x.Count = -7))
            };

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(GetGameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        private static GameSettings GetGameSettings() => new GameSettings { Foes = new[] { new FoeSettings { Name = "Foe" } } };
    }
}
