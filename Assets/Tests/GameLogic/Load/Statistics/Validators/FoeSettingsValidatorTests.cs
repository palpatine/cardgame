﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class FoeSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private FoeSettingsValidator GetValidator(bool isSpawnSettingsValid = false, bool isActionSettingsValid = true)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(SpawnSettings), isSpawnSettingsValid },
                                    { typeof(ActionSettings), isActionSettingsValid }
                                });
            return new FoeSettingsValidator(_settingsValidationManager);
        }

        private static FoeSettings GetItem(Action<FoeSettings> modify = null)
        {
            var item = new FoeSettings
            {
                Name = Guid.NewGuid().ToString(),
                Actions = new[] { new ActionSettings() },
                Count = 1
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfActionsSettings()
        {
            var values = new[]{
                 GetItem(),
            };
            var validator = GetValidator(isActionSettingsValid: false);
            var errors = validator.Validate(new GameSettings { Foes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ActionSettings)}|Foe|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAtLeastOnActionSetting()
        {
            var values = new[]{
                 GetItem(x=>x.Actions= Enumerable.Empty<ActionSettings>()),
            };
            var validator = GetValidator(isActionSettingsValid: false);
            var errors = validator.Validate(new GameSettings { Foes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfSpawnSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Spawn = new SpawnSettings()),
            };
            var validator = GetValidator(isSpawnSettingsValid: false);
            var errors = validator.Validate(new GameSettings { Foes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(SpawnSettings)}|Foe|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireSpawnSettingsOrCountToBeSet()
        {
            var values = new[]{
                 GetItem(x => { x.Spawn = null; x.Count = 0; }),
            };
            var validator = GetValidator(isSpawnSettingsValid: false);
            var errors = validator.Validate(new GameSettings { Foes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldVerifyAbilitiesToBeDefined()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Abilities = new []{ "ability" }; }),
                 GetItem(x => { x.Abilities = new []{ "not ability" }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Foes = values, Abilities = new[] { new AbilitySettings { Name = "ability" } } });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireFoeToBeNamed()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Name = null),
                 GetItem(x => x.Name = ""),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Foes = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireFoeNamedToBeUnique()
        {
            var values = new[]{
                 GetItem(x => x.Name = "name"),
                 GetItem(x => x.Name = "name"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Foes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
