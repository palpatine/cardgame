﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class PlatformCardSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private PlatformCardSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new PlatformCardSettingsValidator(_settingsValidationManager);
        }

        private static PlatformCardSettings GetItem(Action<PlatformCardSettings> modify = null)
        {
            var item = new PlatformCardSettings
            {
                Name = Guid.NewGuid().ToString()
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }


        [Test]
        public void ShouldDelegateValidationOfExplorationSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Exploration = new ExplorationSettings(); }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { PlatformDeck = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ExplorationSettings)}|Platform card|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfEntranceEffect()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.EntranceEffects = new[]{ new EffectSettings() }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { PlatformDeck = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Platform card|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldDelegateValidationOfResourceSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Resources = new[]{ new ResourceSettings() }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { PlatformDeck = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ResourceSettings)}|Platform card|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequirePlatformToBeNamed()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Name = null),
                 GetItem(x => x.Name = ""),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { PlatformDeck = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequirePlatformNamedToBeUnique()
        {
            var values = new[]{
                 GetItem(x => x.Name = "name"),
                 GetItem(x => x.Name = "name"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { PlatformDeck = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireCountInDeckToBeNonnegative()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.CountInDeck = -1),
                 GetItem(x => x.CountInDeck = 1),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { PlatformDeck = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireFeatureToBeDefined()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Feature = "feature"),
                 GetItem(x => x.Feature = "not feature"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { PlatformDeck = values, Features = new[] { new FeaturesSettings { Name = "feature" } } });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
