﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class AbilitySettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private AbilitySettingsValidator GetValidator(
            bool isConditionSettingsValid = false,
            bool isActionSettingsValid = false)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(ConditionSettings), isConditionSettingsValid },
                                    { typeof(ActionSettings), isActionSettingsValid }
                                });
            return new AbilitySettingsValidator(_settingsValidationManager);
        }

        private static AbilitySettings GetItem(Action<AbilitySettings> modify = null)
        {
            var item = new AbilitySettings
            {
                Name = Guid.NewGuid().ToString(),
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequireAbilityToBeNamed()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Name = null),
                 GetItem(x => x.Name = ""),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Abilities = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAbilityNamedToBeUnique()
        {
            var values = new[]{
                 GetItem(x => x.Name = "name"),
                 GetItem(x => x.Name = "name"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Abilities = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfConditionSettings()
        {
            var values = new[]{
                 GetItem( x => x.Condition = new ConditionSettings()),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Abilities = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ConditionSettings)}|Ability|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfEffectSettings()
        {
            var values = new[]{
                 GetItem( x => x.Effects = new[] { new EffectSettings() }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Abilities = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Ability|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfTriggerSettings()
        {
            var values = new[]{
                 GetItem( x => x.Trigger = new TriggerSettings()),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Abilities = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(TriggerSettings)}|Ability|{values[0].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }
    }
}
