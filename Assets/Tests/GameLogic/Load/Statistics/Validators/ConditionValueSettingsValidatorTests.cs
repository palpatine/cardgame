﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class ConditionValueSettingsValidatorTests
    {
        private static ConditionValueSettingsValidator GetValidatorWithAllDelegatingValidatorsSetToValid()
            => GetValidator(true, true, true, true, true, true);

        private static ConditionValueSettingsValidator GetValidator(
            bool isValueSettingsValid = false,
            bool IsConditionSettingsValid = false,
            bool isEquipmentSelectorValid = false,
            bool isTargetSelectorValid = false,
            bool isStatisticReferenceValid = false,
            bool isActionTraceFilterValid = false)
        {
            var settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(ValueSettings),isValueSettingsValid },
                                    { typeof(ConditionSettings),IsConditionSettingsValid },
                                    { typeof(EquipmentSelector), isEquipmentSelectorValid },
                                    { typeof(TargetSelector), isTargetSelectorValid },
                                    { typeof(StatisticReference), isStatisticReferenceValid },
                                    { typeof(ActionTraceFilter), isActionTraceFilterValid },
                                });
            settingsValidationManager.Parameters.Add("param", 1);
            return new ConditionValueSettingsValidator(
         settingsValidationManager);
        }

        private static ConditionValueSettings GetItem(Action<ConditionValueSettings> modify = null)
        {
            var item = new ConditionValueSettings
            {
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ConditionValueSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfEquipmentSettings()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Source = new TargetSelector();
                    x.Equipment = new EquipmentSelector();
                })),
            };

            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EquipmentSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldDelegateValidationOfTargetSettings()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Source = new TargetSelector();
                })),
            };

            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(TargetSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfStatisticRefernceSettingsInStatisticProperty()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Source = new TargetSelector();
                    x.Statistic = new StatisticReference();
                })),
            };

            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(StatisticReference)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldDelegateValidationOfStatisticRefernceSettingsInStatisticsChangeProperty()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Source = new TargetSelector();
                    x.StatisticsChange = new StatisticReference();
                })),
            };

            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(StatisticReference)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfValueSettings()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Values = new [] { new ValueSettings() };
                })),
            };

            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ValueSettings)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfConditionSettings()
        {
            var values = new[]{
                ("v1", GetItem(x => x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    })),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ConditionSettings)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldRequireAtLeastOneSetOfPropertiesToBeSet()
        {
            var values = new[]{
                ("v1", new ConditionValueSettings { }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfSelectedParameterIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.Selected = "param")),
                ("v1", GetItem(x => {
                    x.Selected = "param";
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Selected = "param";
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Selected = "param";
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Value = "1";
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Selected = "param";
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Selected = "param";
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Selected = "param";
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Selected = "param";
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(8, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfValueIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.Value = "1")),
                ("v1", GetItem(x => {
                    x.Value = "1";
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Value = "1";
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Value = "1";
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Value = "1";
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Value = "1";
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Value = "1";
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Value = "1";
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Value = "1";
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(8, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfSOurceAndStatisticChangeAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v1", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v6", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Value = "1";
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfSOurceAndStatisticAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v1", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v6", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Value = "1";
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawOnlyEquipmentStatisticAndStatisticChangeToBeSetIfSourceIsSetIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.Source = new TargetSelector())),
                ("v1", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Value = "1";
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(5, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfActionTraceFilterIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.PreviousAction = new ActionTraceFilter())),
                ("v1", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Value = "1";
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(8, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfEquipmentAndSourceAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v1", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Value = "1";
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v6", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfValuesAndOperationAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
                ("v1", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Value = "1";
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(8, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfGroupIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x =>
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    })),
                ("v1", GetItem(x => {
                    x.Value = "1";
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                })),
                ("v2", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Group = new ConditionGroupSettings
                    {
                        Conditions = new[]
                        {
                            new ConditionSettings()
                        }
                    };
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(8, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
