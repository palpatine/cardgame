﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Model.Trace;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{

    public class ActionTraceFilterValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private ActionTraceFilterValidator GetValidator(
            bool isTargetSelectroValid = false)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(TargetSelector),isTargetSelectroValid },
                                });
            return new ActionTraceFilterValidator(_settingsValidationManager);
        }

        private static ActionTraceFilter GetValidItem(Action<ActionTraceFilter> modify = null)
        {
            var item = new ActionTraceFilter
            {
                Action = Constants.TraceActions.Acted,
                Get = typeof(ActionTrace)
                .GetProperties()
                .Select(x => x.Name)
                .First()
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ActionTraceFilter)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfTargetSelector()
        {
            var values = new[]{
                ("v1", GetValidItem(x => x.Target = new TargetSelector())),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(TargetSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireValidAction()
        {
            var values = new[]{
                ("v1", GetValidItem()),
                ("v2", GetValidItem(x => x.Action = "invalid")),
                ("v3", GetValidItem(x => x.Action = null)),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireValidProperty()
        {
            var values = new[]{
                ("v1", GetValidItem()),
                ("v2", GetValidItem(x => x.Get = "invalid")),
                ("v3", GetValidItem(x => x.Get = null)),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
