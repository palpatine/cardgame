﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class SpawnSettingsValidatorTests
    {
        private static SpawnSettingsValidator GetValidatorWithAllDelegatingValidatorsSetToValid()
          => GetValidator(true, true);

        private static SpawnSettingsValidator GetValidator(
           bool isFeatureSelectorValid = false,
           bool isPlatformSelectorValid = true)
                => new SpawnSettingsValidator(
                    new TestSettingsValidationManager(
                    new Dictionary<Type, bool>
                    {
                        { typeof(FeatureSelector), isFeatureSelectorValid },
                        { typeof(PlatformSelector), isPlatformSelectorValid },
                    }));

        private SpawnSettings GetItem(Action<SpawnSettings> modify = null)
        {
            var item = new SpawnSettings
            {
                CountDelta = 1,
                PlatformSelector = new PlatformSelector()
            };
            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, SpawnSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfPlatformSelector()
        {
            var values = new[]{
                ("v1", GetItem()),
            };
            var validator = GetValidator(isPlatformSelectorValid: false);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(PlatformSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfFeatureSelector()
        {
            var values = new[]{
                ("v1", GetItem(x => { x.PlatformSelector = null; x.FeatureSelector = new FeatureSelector(); } )),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(FeatureSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfStatisticReferenceSettings()
        {
            var values = new[]{
                ("v1", GetItem(x => { x.Delta = 3; x.CountDelta=null; x.Statistic = new StatisticReference(); } )),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(StatisticReference)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }
    }
}
