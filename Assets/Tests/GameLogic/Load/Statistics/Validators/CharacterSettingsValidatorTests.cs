﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class CharacterSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private CharacterSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new CharacterSettingsValidator(_settingsValidationManager);
        }

        private static CharacterSettings GetItem(Action<CharacterSettings> modify = null)
        {
            var item = new CharacterSettings
            {
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRequireEquipmentItmesToBeDefined()
        {
            var settings = GetItem(x => x.Equipment = new[] { "item", "not item" });

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(
                new GameSettings { Items = new[] { new ItemSettings { Name = "item" } }, Character = settings });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAbilitiesToBeDefined()
        {
            var settings = GetItem(x => x.Abilities = new[] { "ability", "not ability" });

            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(
                new GameSettings { Abilities = new[] { new AbilitySettings { Name = "ability" } }, Character = settings });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
