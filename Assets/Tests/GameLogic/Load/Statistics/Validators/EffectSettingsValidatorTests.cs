﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{

    public class EffectSettingsValidatorTests
    {
        private static TestSettingsValidationManager _settingsValidationManager;

        private static EffectSettingsValidator GetValidatorWithAllDelegatingValidatorsSetToValid()
           => GetValidator(true, true, true, true, true);

        private static EffectSettingsValidator GetValidator(
           bool isActionSettingValid = false,
           bool isValueSettingsValid = false,
            bool isTargetSelectorValid = true,
            bool isActionTraceFilterValid = false,
            bool isStatisticReferenceSettingsValid = false,
            bool isEquipmentSelectorValid = false)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                        { typeof(ActionSettings), isActionSettingValid },
                        { typeof(ValueSettings), isValueSettingsValid },
                        { typeof(TargetSelector), isTargetSelectorValid },
                        { typeof(ActionTraceFilter), isActionTraceFilterValid },
                        { typeof(StatisticReference), isStatisticReferenceSettingsValid },
                        { typeof(EquipmentSelector), isEquipmentSelectorValid }
                                });
            return new EffectSettingsValidator(_settingsValidationManager);
        }

        private EffectSettings GetItem(Action<EffectSettings> modify = null)
        {
            var item = new EffectSettings { Target = new TargetSelector() };
            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, EffectSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfActionSettings()
        {
            var values = new[]{
                ("v1", GetItem(x => x.EndTurn = true )),
                ("v2", GetItem(x => x.Action = new ActionSettings() ))
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ActionSettings)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfConditionSettings()
        {
            var values = new[]{
                ("v1", GetItem(x => x.EndTurn = true )),
                ("v2", GetItem(x => { x.Condition = new ConditionSettings(); x.EndTurn = true; }))
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ConditionSettings)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireParameterToBeDefinedIfItIsUsedInForDelta()
        {
            var values = new[]
            {
                ("v1", GetItem(x =>
                {
                    x.ParameterForDelta = "param";
                    x.RuneCard = true;
                }))
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireParametersToBeDefinedOnEffectIfSelectedAbilityRequiresOne()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Ability = "Ability";
                    x.Parameters = new[]{ new ParameterSettings{ Name = "param1" } };
                })),
                ("v2", GetItem(x =>
                {
                    x.Ability = "Ability";
                }))
            };

            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings
            {
                Abilities = new[] { new AbilitySettings
                    {
                        Name = "Ability",
                        Parameters = new [] { "param1" }
                    }
                }
            }, values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireDefinedParametersToBeRequiredBySelectedAbility()
        {
            var values = new[]{
                ("v1", GetItem(x =>
                {
                    x.Ability = "Ability";
                })),
                ("v2", GetItem(x =>
                {
                    x.Ability = "Ability";
                    x.Parameters = new[]{ new ParameterSettings{ Name = "param2" } };
                }))
            };

            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings
            {
                Abilities = new[] { new AbilitySettings
                    {
                        Name = "Ability",
                    }
                }
            }, values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfStatisticReferenceSettings()
        {
            var values = new[] {
                ("v1", GetItem(x => x.EndTurn = true)),
                ("v2", GetItem(x => {x.Statistic = new StatisticReference(); x.Delta = "1"; }))
            };
            var validator = GetValidator(isValueSettingsValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(StatisticReference)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfValueSettings()
        {
            var values = new[] {
                ("v1", GetItem(x => x.EndTurn = true)),
                ("v2", GetItem(x => {x.Statistic = new StatisticReference(); x.Delta = "1"; }))
            };
            var validator = GetValidator(isStatisticReferenceSettingsValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ValueSettings)}|PARENT_KIND|v2 effect delta|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfTargetSelector()
        {
            var values = new[]{
                ("v1", GetItem(x => x.EndTurn = true))
            };
            var validator = GetValidator(isTargetSelectorValid: false);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(TargetSelector)}|PARENT_KIND|v1|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAbilityToBeDefined()
        {
            var values = new[]{
                ("v1", new EffectSettings { Target = new TargetSelector(), Ability = "Ability" }),
                ("v2", new EffectSettings { Target = new TargetSelector(), Ability = "NotAbility" }),
            };
            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings { Abilities = new[] { new AbilitySettings { Name = "Ability" } } }, values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireEquipmentToBeDefined()
        {
            var values = new[]{
                ("v1", new EffectSettings { Target = new TargetSelector(), Equipment = "Sword", Delta="1" }),
                ("v2", new EffectSettings { Target = new TargetSelector(), Equipment = "NotSword", Delta="1" }),
            };
            var validator = GetValidator(isTargetSelectorValid: true, isValueSettingsValid: true);
            var errors = validator.Validate(new GameSettings { Items = new[] { new ItemSettings { Name = "Sword" } } }, values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenStatisticAndDeltaIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x => { x.Statistic = new StatisticReference(); x.Delta = "1"; });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Equipment = "Sword";  })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.ParameterForDelta = "param"; })), // additional error for not defined parameter
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(12, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenStatisticAndParameterForDeltaIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.Statistic = new StatisticReference();
                    x.ParameterForDelta = "param";
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Equipment = "Sword";  })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            _settingsValidationManager.Parameters.Add("param", 1);
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(10, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenRuneCardAndDeltaIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x => { x.RuneCard = true; x.Delta = "1"; });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Equipment = "Sword";  })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.ParameterForDelta = "param"; })), // additional error for not defined parameter
                ("v7", GetTemplate(x => { x.Statistic = new StatisticReference(); })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(12, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenRuneCardAndParameterForDeltaIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.RuneCard = true;
                    x.ParameterForDelta = "param";
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Equipment = "Sword";  })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.Statistic = new StatisticReference(); })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            _settingsValidationManager.Parameters.Add("param", 1);
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(10, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenEqupmentAndDeltaIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.Equipment = "Sword";
                    x.Delta = "1";
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference();  })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.ParameterForDelta = "1"; })), // additional error for not defined parameter
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(12, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenEqupmentAndParameterForDeltaIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.Equipment = "Sword";
                    x.ParameterForDelta = "param";
                });
                modify?.Invoke(item);
                return item;
            }

            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            _settingsValidationManager.Parameters.Add("param", 1);
            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference();  })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
            };
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(10, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenAbilityIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.Ability = "Ability";
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Equipment = "Sword"; x.Delta = "1"; })),
                ("v2", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference(); x.Delta = "1"; })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),//valid
                ("v10", GetTemplate(x => { x.Duration = 1; })),//valid
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(8, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenActionIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.Action = new ActionSettings();
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Equipment = "Sword"; x.Delta = "1"; })),
                ("v2", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference(); x.Delta = "1"; })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.EndTurn = true; })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(11, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenEndTurnIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.EndTurn = true;
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Equipment = "Sword"; x.Delta = "1"; })),
                ("v2", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference(); x.Delta = "1"; })),
                ("v4", GetTemplate(x => { x.Invulnerability = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(11, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenInvulnerabilityIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.Invulnerability = true;
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Equipment = "Sword"; x.Delta = "1"; })),
                ("v2", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference(); x.Delta = "1"; })),
                ("v4", GetTemplate(x => { x.EndTurn = true; })),
                ("v5", GetTemplate(x => { x.NoRestoration = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(11, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllowAnyOtherEffectPropertyToBeSetWhenNoRestorationIsSet()
        {
            EffectSettings GetTemplate(Action<EffectSettings> modify = null)
            {
                var item = GetItem(x =>
                {
                    x.NoRestoration = true;
                });
                modify?.Invoke(item);
                return item;
            }

            var values = new[]{
                ("v0", GetTemplate()),
                ("v1", GetTemplate(x => { x.Equipment = "Sword"; x.Delta = "1"; })),
                ("v2", GetTemplate(x => { x.Ability = "Ability"; })),
                ("v3", GetTemplate(x => { x.Statistic = new StatisticReference(); x.Delta = "1"; })),
                ("v4", GetTemplate(x => { x.EndTurn = true; })),
                ("v5", GetTemplate(x => { x.Invulnerability = true; })),
                ("v6", GetTemplate(x => { x.Delta = "1"; })),
                ("v7", GetTemplate(x => { x.RuneCard = true; })),
                ("v8", GetTemplate(x => { x.Action = new ActionSettings(); })),
                ("v9", GetTemplate(x => { x.UseCount = 1; })),
                ("v10", GetTemplate(x => { x.Duration = 1; })),
                ("v11", GetTemplate(x => {
                    x.Parameters = new [] { new ParameterSettings{ Name = "param" } }; })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(
                GetSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(11, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        private static GameSettings GetSettings() => new GameSettings
        {
            Abilities = new[] { new AbilitySettings { Name = "Ability" } },
            Items = new[] { new ItemSettings { Name = "Sword" } }
        };
    }
}
