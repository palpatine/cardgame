﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Load.Settings.Validators;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class StatisticReferenceValidatorTests
    {
        private static StatisticReferenceValidator GetValidator()
                   => new StatisticReferenceValidator();

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, StatisticReference)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldVerifyIfStatisticNameIsDefined()
        {
            var values = new[]{
                ("v1", new StatisticReference {
                    Value = Constants.StatisitcValues.Current }),
                ("v2", new StatisticReference {
                    Name = Constants.Statisitcs.Attack,
                    Value = Constants.StatisitcValues.Current }),
                ("v3", new StatisticReference {
                    Name = "fake",
                    Value = Constants.StatisitcValues.Current }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldVerifyIfStatisticValueIsDefined()
        {
            var values = new[]{
                ("v1", new StatisticReference {
                    Name = Constants.Statisitcs.Attack }),
                ("v2", new StatisticReference {
                    Name = Constants.Statisitcs.Attack,
                    Value = Constants.StatisitcValues.Current }),
                ("v3", new StatisticReference {
                    Name = Constants.Statisitcs.Block,
                    Value = "fake" }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldVerifyIfRegenerationValueIsApplicableOnlyToRegenerableStatistics()
        {
            var values = new[]{
                ("v1", new StatisticReference {
                    Name = Constants.RegenerableStatistics.First(),
                    Value = Constants.StatisitcValues.Regeneration }),
                ("v2", new StatisticReference {
                    Name = Constants.ReplenishableStatistics.First(),
                    Value = Constants.StatisitcValues.Regeneration }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldVerifyIfMaximumValueIsApplicableOnlyToRegenerableOrReplenishableStatistics()
        {
            var values = new[]{
                ("v1", new StatisticReference {
                    Name = Constants.RegenerableStatistics.First(),
                    Value = Constants.StatisitcValues.Maximum }),
                ("v2", new StatisticReference {
                    Name = Constants.ReplenishableStatistics.First(),
                    Value = Constants.StatisitcValues.Maximum }),
                ("v3", new StatisticReference {
                    Name = Constants.Statisitcs.Damage,
                    Value = Constants.StatisitcValues.Maximum }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
