﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{

    public class PlatformSelectorValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private PlatformSelectorValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new PlatformSelectorValidator(_settingsValidationManager);
        }

        private static PlatformSelector GetItem(Action<PlatformSelector> modify = null)
        {
            var item = new PlatformSelector
            {
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, PlatformSelector)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequirePlatformKindToBeDefined()
        {
            var values = new[]{
                ("v1", GetItem(x => x.Kind = "platform kind")),
                ("v2", GetItem(x => x.Kind = "not platform kind")),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { PlatformDeck = new[] { new PlatformCardSettings { Kinds = new[] { "platform kind" } } } },
                values,
                "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequirePlatformNameToBeDefined()
        {
            var values = new[]{
                ("v1", GetItem(x => x.Name = "platform name")),
                ("v2", GetItem(x => x.Name = "not platform name")),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { PlatformDeck = new[] { new PlatformCardSettings { Name = "platform name" } } },
                values,
                "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireObstacleNameToBeDefined()
        {
            var values = new[]{
                ("v1", GetItem(x => x.Obstacle = "Obstacle name")),
                ("v2", GetItem(x => x.Obstacle = "not Obstacle name")),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Obstacles = new[] { new ObstacleSettings { Name = "Obstacle name" } } },
                values,
                "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
