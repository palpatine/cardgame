﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class ValueSettingsValidatorTests
    {
        private static ValueSettingsValidator GetValidatorWithAllDelegatingValidatorsSetToValid()
           => GetValidator(true, true, true, true, true);

        private static ValueSettingsValidator GetValidator(
            bool isValueSettingsValid = false,
            bool isTargetSelectorValid = false,
            bool isActionTraceFilterValid = false,
            bool isStatisticReferenceSettingsValid = false,
            bool isEquipmentSelectorValid = false)
        {
            var settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(ValueSettings), isValueSettingsValid },
                                    { typeof(TargetSelector), isTargetSelectorValid },
                                    { typeof(ActionTraceFilter), isActionTraceFilterValid },
                                    { typeof(StatisticReference), isStatisticReferenceSettingsValid },
                                    { typeof(EquipmentSelector), isEquipmentSelectorValid }
                                });
            settingsValidationManager.Parameters.Add("param", 1);
            return new ValueSettingsValidator(
         settingsValidationManager);
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, ValueSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfTargetSelectors()
        {
            var values = new[]{
                ("v1", new ValueSettings { Value = "1" }),
                ("v2", new ValueSettings{  Source = new TargetSelector() })
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(TargetSelector)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfStatisticReferenceSettings()
        {
            var values = new[]{
                ("v1", new ValueSettings { Value = "1" }),
                ("v2", new ValueSettings{  Statistic = new StatisticReference(), Source = new TargetSelector() })
            };
            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(StatisticReference)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfEquipmentSelector()
        {
            var values = new[]{
                ("v1", new ValueSettings { Value = "1" }),
                ("v2", new ValueSettings{  Equipment = new EquipmentSelector(), Source = new TargetSelector() })
            };
            var validator = GetValidator(isTargetSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EquipmentSelector)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfPreviousAction()
        {
            var values = new[]{
                ("v1", new ValueSettings { Value = "1" }),
                ("v2", new ValueSettings{  PreviousAction = new ActionTraceFilter() })
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ActionTraceFilter)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldValidateInnerValues()
        {
            var values = new[]{
                ("v1", new ValueSettings { Value = "1" }),
                ("v2", new ValueSettings { Operation = "min",  Values = new []{ new ValueSettings() } })
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ValueSettings)}|PARENT_KIND|v2|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldValidateIfOperationIsDefined()
        {
            var values = new[]{
                ("v1", new ValueSettings { Operation = "min", Values = new[]{ new ValueSettings { Value = "1" } } }),
                ("v2", new ValueSettings{  Operation = "OO", Values = new[]{ new ValueSettings { Value = "1" } } })
            };
            var validator = GetValidator(isValueSettingsValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireBothValuesAndOperationToBeSetAtTheSameTime()
        {
            var values = new[]
            {
                ("v1", new ValueSettings { Values = new[]{ new ValueSettings { Value = "1" } } }),
                ("v2", new ValueSettings { Operation = "min" })
            };
            var validator = GetValidator(isValueSettingsValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireSourcePropertyForSomeValues()
        {
            var values = new[]
            {
                ("v1", new ValueSettings { Statistic = new StatisticReference() }),
                ("v2", new ValueSettings { StatisticsChange = new StatisticReference() }),
                ("v3", new ValueSettings { Equipment = new EquipmentSelector() })
            };
            var validator = GetValidator(isStatisticReferenceSettingsValid: true, isEquipmentSelectorValid: true);
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(3, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAtLeastOneSetOfPropertiesToBeSet()
        {
            var values = new[]{
                ("v1", new ValueSettings { }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        private static ValueSettings GetItem(Action<ValueSettings> modify = null)
        {
            var item = new ValueSettings
            {
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfSelectedParameterIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.Selected = "param")),
                ("v2", GetItem(x => {
                    x.Selected = "param";
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Selected = "param";
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Value = "1";
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Selected = "param";
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Selected = "param";
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Selected = "param";
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Selected = "param";
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfValueIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.Value = "1")),
                ("v2", GetItem(x => {
                    x.Value = "1";
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Value = "1";
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Value = "1";
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Value = "1";
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Value = "1";
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Value = "1";
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Value = "1";
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfSOurceAndStatisticChangeAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v2", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v6", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Value = "1";
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(6, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfSOurceAndStatisticAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v2", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v6", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Value = "1";
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(6, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawOnlyEquipmentStatisticAndStatisticChangeToBeSetIfSourceIsSetIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.Source = new TargetSelector())),
                ("v2", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Value = "1";
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(4, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfActionTraceFilterIsSet()
        {
            var values = new[]{
                ("v0", GetItem(x => x.PreviousAction = new ActionTraceFilter())),
                ("v2", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Value = "1";
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.PreviousAction = new ActionTraceFilter();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfEquipmentAndSourceAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v2", GetItem(x => {
                    x.Value = "1";
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.Selected = "param";
                })),
                ("v6", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(6, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldNotAllawAnyOtherDataPropertyToBeSetIfValuesAndOperationAreSet()
        {
            var values = new[]{
                ("v0", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
                ("v2", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Equipment = new EquipmentSelector();
                    x.Source = new TargetSelector();
                })),
                ("v3", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.PreviousAction = new ActionTraceFilter();
                })),
                ("v4", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Selected = "param";
                })),
                ("v5", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Source = new TargetSelector();
                })),
                ("v6", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.Statistic = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v7", GetItem(x => {
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                    x.StatisticsChange = new StatisticReference();
                    x.Source = new TargetSelector();
                })),
                ("v8", GetItem(x => {
                    x.Value = "1";
                    x.Values = new [] { new ValueSettings() };
                    x.Operation = Constants.ValuesOperations.Max;
                })),
            };
            var validator = GetValidatorWithAllDelegatingValidatorsSetToValid();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(7, errors.Count(), string.Join(Environment.NewLine, errors));
        }

    }
}
