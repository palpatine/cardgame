﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class ObstacleSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private ObstacleSettingsValidator GetValidator(
            bool isRemediesSettingsValid = false)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(RemediesSettings), isRemediesSettingsValid }
                                });
            return new ObstacleSettingsValidator(_settingsValidationManager);
        }

        private static ObstacleSettings GetItem(Action<ObstacleSettings> modify = null)
        {
            var item = new ObstacleSettings
            {
                Name = Guid.NewGuid().ToString(),
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldRequireNameToBeNotEmpty()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Name = null; }),
                 GetItem(x => { x.Name = ""; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Obstacles = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireNameToBeUnique()
        {
            var values = new[]{
                 GetItem(x => { x.Name = "name"; }),
                 GetItem(x => { x.Name = "name"; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Obstacles = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
        [Test]
        public void ShouldDelegateValidationOfEffectSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Effects = new [] { new EffectSettings() }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Obstacles = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Obstacle|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfRemediesSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Remedies =  new RemediesSettings(); }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Obstacles = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(RemediesSettings)}|Obstacle|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireObstacleToNotReferenceItselfInRemedies()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Remedies =  new RemediesSettings{ Obstacle = x.Name }; }),
            };
            var validator = GetValidator(isRemediesSettingsValid: true);
            var errors = validator.Validate(new GameSettings { Obstacles = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
