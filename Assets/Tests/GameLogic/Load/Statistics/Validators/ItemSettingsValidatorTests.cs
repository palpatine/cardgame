﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{

    public class ItemSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private ItemSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new ItemSettingsValidator(_settingsValidationManager);
        }

        private static ItemSettings GetItem(Action<ItemSettings> modify = null)
        {
            var item = new ItemSettings
            {
                Name = Guid.NewGuid().ToString(),
                Kinds = new[] { "weapon" }
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfActionsSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Actions = new []{ new ActionSettings() }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Items = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ActionSettings)}|Item|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldDelegateValidationOfEffectSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Effects = new []{ new EffectSettings() }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Items = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Item|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireValueToBePositive()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Value = 4; }),
                 GetItem(x => { x.Value = 0; }),
                 GetItem(x => { x.Value = -3; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Items = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireItemToBeNamed()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Name = null),
                 GetItem(x => x.Name = ""),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Items = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireItemNamedToBeUnique()
        {
            var values = new[]{
                 GetItem(x => x.Name = "name"),
                 GetItem(x => x.Name = "name"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Items = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireKindsToBeDefined()
        {
            var values = new[]{
                 GetItem(x => x.Kinds = new[]{ Constants.ItemKinds.Ammo }),
                 GetItem(x => x.Kinds = new[]{ "not kind" }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Items = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
