﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class RemediesSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private RemediesSettingsValidator GetValidator(
            bool isTargetSelectroValid = false)
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                    { typeof(TargetSelector),isTargetSelectroValid },
                                });
            return new RemediesSettingsValidator(_settingsValidationManager);
        }

        private static RemediesSettings GetItem(Action<RemediesSettings> modify = null)
        {
            var item = new RemediesSettings
            {
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), Enumerable.Empty<(string, RemediesSettings)>(), "PARENT_KIND");
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfEffectSettings()
        {
            var values = new[]{
                ("v1", GetItem(x => x.Effect =  new EffectSettings() )),
            };

            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings(), values, "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|PARENT_KIND|v1 in Remedies|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireObstacleToBeDefined()
        {
            var values = new[]{
                ("v0", GetItem()),
                ("v1", GetItem(x => x.Obstacle = "obstacle" )),
                ("v2", GetItem(x => x.Obstacle = "not obstacle" )),
            };

            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings { Obstacles = new[] { new ObstacleSettings { Name = "obstacle" } } },
                values,
                "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void ShouldRequireActionToBeDefined()
        {
            var values = new[]{
                ("v0", GetItem()),
                ("v1", GetItem(x => x.Action = Constants.Actions.Harvest )),
                ("v2", GetItem(x => x.Obstacle = "not action" )),
            };

            var validator = GetValidator();
            var errors = validator.Validate(
                new GameSettings(),
                values,
                "PARENT_KIND");
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
