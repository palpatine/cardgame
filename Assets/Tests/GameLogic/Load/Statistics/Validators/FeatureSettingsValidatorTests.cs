﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class FeatureSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private FeatureSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new FeatureSettingsValidator(_settingsValidationManager);
        }

        private static GameSettings GetGameSettings(FeaturesSettings[] values)
            => new GameSettings { Features = values, PlatformDeck = new[] { new PlatformCardSettings { Kinds = new[] { "Kind" } } } };

        private static FeaturesSettings GetItem(Action<FeaturesSettings> modify = null)
        {
            var item = new FeaturesSettings
            {
                Name = Guid.NewGuid().ToString(),
                TriggeringPlatformKind = "Kind"
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfResourceSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Resource = new ResourceSettings(); }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(GetGameSettings(values));
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(ResourceSettings)}|Feature|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequirePlatformToBeNamed()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => x.Name = null),
                 GetItem(x => x.Name = ""),
            };
            var validator = GetValidator();
            var errors = validator.Validate(GetGameSettings(values));
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequirePlatformNamedToBeUnique()
        {
            var values = new[]{
                 GetItem(x => x.Name = "name"),
                 GetItem(x => x.Name = "name"),
            };
            var validator = GetValidator();
            var errors = validator.Validate(
                GetGameSettings(values));
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireTriggeringPlatformKindToBeDefined()
        {
            var values = new[]{
                GetItem(x => x.TriggeringPlatformKind = "not kind"),
                GetItem(x => x.TriggeringPlatformKind = null)
            };
            var validator = GetValidator();
            _settingsValidationManager.Parameters.Add("selector", 1);
            var errors = validator.Validate(GetGameSettings(values));
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
