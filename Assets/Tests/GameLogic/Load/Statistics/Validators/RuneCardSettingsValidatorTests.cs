﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Load.Settings.Validators;
using Assets.GameLogic.Settings;
using NUnit.Framework;
namespace Assets.Tests.GameLogic.Load.Statistics.Validators
{
    public class RuneCardSettingsValidatorTests
    {
        private TestSettingsValidationManager _settingsValidationManager;

        private RuneCardSettingsValidator GetValidator()
        {
            _settingsValidationManager = new TestSettingsValidationManager(
                                new Dictionary<Type, bool>
                                {
                                });
            return new RuneCardSettingsValidator(_settingsValidationManager);
        }

        private static RuneCardSettings GetItem(Action<RuneCardSettings> modify = null)
        {
            var item = new RuneCardSettings
            {
                Name = Guid.NewGuid().ToString(),
                Decks = new[] { Constants.Decks.Magic },
                Value = 1
            };

            modify?.Invoke(item);
            return item;
        }

        [Test]
        public void ShouldRetrunEmptyListIfNoValuesToValidateArePased()
        {
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings());
            CollectionAssert.IsEmpty(errors);
        }

        [Test]
        public void ShouldDelegateValidationOfEffectSettings()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Effects = new [] { new EffectSettings() }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
            Assert.AreEqual($"{nameof(EffectSettings)}|Rune card|{values[1].Name}|0", errors.Single(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireCountInDeckToBeNonnegative()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.CountInDeck = -1; }),
                 GetItem(x => { x.CountInDeck = 3; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireCostToBeNonnegative()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Cost = -1; }),
                 GetItem(x => { x.Cost = 3; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireValueToBePositive()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Value = -1; }),
                 GetItem(x => { x.Value = 0; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireAtLeastOneDeck()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Decks = Enumerable.Empty<string>(); }),
                 GetItem(x => { x.Decks = new [] { Constants.Decks.Magic, Constants.Decks.Might }; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireNameToBeNotEmpty()
        {
            var values = new[]{
                 GetItem(),
                 GetItem(x => { x.Name = null; }),
                 GetItem(x => { x.Name = ""; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(2, errors.Count(), string.Join(Environment.NewLine, errors));
        }

        [Test]
        public void ShouldRequireNameToBeUnique()
        {
            var values = new[]{
                 GetItem(x => { x.Name = "name"; }),
                 GetItem(x => { x.Name = "name"; }),
            };
            var validator = GetValidator();
            var errors = validator.Validate(new GameSettings { Runes = values });
            Assert.AreEqual(1, errors.Count(), string.Join(Environment.NewLine, errors));
        }
    }
}
