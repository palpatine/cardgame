﻿using Assets.GameLogic.State;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Extensions
{

    public class LocationStateExtensionsTests_AllVisiblePlatfomrsToRight
    {
        [TestCase(null, true, true, 1)]    // _ A< _
        [TestCase(null, false, true, 1)]   // _ A> _
        [TestCase(false, false, true, 1)]  // _ A>o _
        [TestCase(false, false, false, 1)]  // _ oA> _
        [TestCase(false, true, false, 1)]  // _ oA< _
        [TestCase(false, true, true, 1)]  // _ A<o _
        [TestCase(true, false, true, 0)]  // _ A>O _
        [TestCase(true, false, false, 1)]  // _ OA> _
        [TestCase(true, true, false, 1)]  // _ OA< _
        [TestCase(true, true, true, 0)]  // _ A<O _
        public void ShouldConsiderStartingLocationWhileDeterminingResult(
            bool? obstacleIsBlockingVisibility,
            bool isFacingLeft,
            bool isToTheLeftOfObstacle,
            int expectedCount)
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[1];

            if (obstacleIsBlockingVisibility.HasValue)
            {
                var obstacleSettings = builder.GivenObstacleSettings();
                obstacleSettings.IsBlockingVisibility = obstacleIsBlockingVisibility.Value;
                builder
                    .UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings);
            }

            var location = new Location(builder.UpdateNotifier)
            {
                IsFacingLeft = isFacingLeft,
                IsToTheLeftOfObstacle = isToTheLeftOfObstacle,
                PlatformCard = actionCard
            };

            var cards = location.AllVisiblePlatfomrsToRight(builder.GameState);
            Assert.AreEqual(expectedCount, cards.Count);
        }
    }
}
