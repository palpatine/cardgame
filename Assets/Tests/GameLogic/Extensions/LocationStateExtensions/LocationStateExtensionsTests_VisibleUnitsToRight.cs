﻿using System.Linq;
using Assets.GameLogic.State;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Extensions
{
    public class LocationStateExtensionsTests_VisibleUnitsToRight
    {
        [TestCase(null, true, 1)]
        [TestCase(false, true, 4)]
        [TestCase(false, false, 2)]
        [TestCase(true, false, 2)]
        [TestCase(true, true, 1)]
        public void ShouldConsiderObstacleAndStartingLocationToGetVislibleUnits(
            bool? obstacleIsBlockingVisibility,
            bool isToTheLeftOfObstacle,
            int expectedCount)
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];

            builder
                    .UsingPlatform(actionCard)
                    .PlaceUnit(builder.GivenFoe())
                    .PlaceUnit(builder.GivenFoe());

            if (obstacleIsBlockingVisibility.HasValue)
            {
                var obstacleSettings = builder.GivenObstacleSettings();
                obstacleSettings.IsBlockingVisibility = obstacleIsBlockingVisibility.Value;
                builder
                    .UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings)
                    .PlaceUnit(builder.GivenFoe(), isToTheLeftOfObstacle: false)
                    .PlaceUnit(builder.GivenFoe(), isToTheLeftOfObstacle: false)
                    .PlaceUnit(builder.GivenFoe(), isToTheLeftOfObstacle: false);
            }

            var location = new Location(builder.UpdateNotifier)
            {
                IsToTheLeftOfObstacle = isToTheLeftOfObstacle,
                PlatformCard = actionCard,
                UnitId = 0
            };

            var units = location.VisibleUnitsToRight();
            Assert.AreEqual(expectedCount, units.Count);
            var expected = !isToTheLeftOfObstacle ?
                actionCard.UnitsToRightOfObstacle.Skip(1).Take(expectedCount) :
                actionCard.AllUnits.Skip(1).Take(expectedCount);
            CollectionAssert.AreEqual(expected, units);
        }
    }
}
