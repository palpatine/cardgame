﻿using System.Linq;
using Assets.GameLogic.State;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Extensions
{
    public class LocationStateExtensionsTests_VisibleUnitsToLeft
    {
        [TestCase(null, true, 2)]
        [TestCase(false, true, 2)]
        [TestCase(false, false, 3)]
        [TestCase(true, false, 1)]
        [TestCase(true, true, 2)]
        public void ShouldConsiderObstacleAndStartingLocationToGetVislibleUnits(
            bool? obstacleIsBlockingVisibility,
            bool isToTheLeftOfObstacle,
            int expectedCount)
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];

            builder
                    .UsingPlatform(actionCard)
                    .PlaceUnit(builder.GivenFoe())
                    .PlaceUnit(builder.GivenFoe());

            if (obstacleIsBlockingVisibility.HasValue)
            {
                var obstacleSettings = builder.GivenObstacleSettings();
                obstacleSettings.IsBlockingVisibility = obstacleIsBlockingVisibility.Value;
                builder
                    .UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings)
                    .PlaceUnit(builder.GivenFoe(), isToTheLeftOfObstacle: false);
            }

            var location = new Location(builder.UpdateNotifier)
            {
                IsToTheLeftOfObstacle = isToTheLeftOfObstacle,
                PlatformCard = actionCard,
                UnitId = 2
            };

            var units = location.VisibleUnitsToLeft();
            Assert.AreEqual(expectedCount, units.Count);
            var expected = obstacleIsBlockingVisibility == true && !isToTheLeftOfObstacle ?
                actionCard.UnitsToRightOfObstacle.Take(expectedCount).Reverse() :
                actionCard.AllUnits.Take(expectedCount).Reverse();
            CollectionAssert.AreEqual(expected, units);
        }
    }
}
