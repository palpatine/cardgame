﻿using System.Linq;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Extensions
{
    public class PlatformCardExtensionsTests_AllVisiblePlatfomrsToRight
    {
        [Test]
        public void ShouldReturnAllPlatformsToTheRightIfNoObstacleBlockingVisibilityisPresent()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms.First();

            var cards = actionCard.AllVisiblePlatfomrsToRight(builder.GameState);

            CollectionAssert.AreEqual(
                builder.GameState.Levels[0].Platforms.Skip(1),
                cards);
        }

        [Test]
        public void LastReturnedPlatformCanContainObstacleThatIsBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms.First();
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[3])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToRight(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Skip(1).Take(3).ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnAllPlatformsToTheRightIfObstacleIsNotBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms.First();
            var obstacleSettings = builder.GivenObstacleSettings();
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[3])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToRight(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Skip(1).ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnAllPlatformsIfNoObstacleIsPresentAndLevelIsLocked()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray(), isLocked: true);
            var actionCard = builder.GameState.Levels[0].Platforms[2];

            var cards = actionCard.AllVisiblePlatfomrsToRight(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Skip(3).Take(2).Concat(
                    builder.GameState.Levels[0].Platforms.Take(2))
                    .ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnAllPlatformsIfObstacleIsNotBlockingVisibilityAndLevelIsLocked()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray(), isLocked: true);
            var actionCard = builder.GameState.Levels[0].Platforms[3];
            var obstacleSettings = builder.GivenObstacleSettings();
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[1])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToRight(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Skip(4).Take(1).Concat(
                    builder.GameState.Levels[0].Platforms.Take(3))
                    .ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnCorrectPlatformsIfObstacleIsBlockingVisibilityAndLevelIsLocked()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray(), isLocked: true);
            var actionCard = builder.GameState.Levels[0].Platforms[3];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[1])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToRight(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Skip(4).Take(1).Concat(
                    builder.GameState.Levels[0].Platforms.Take(2))
                    .ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }
    }
}
