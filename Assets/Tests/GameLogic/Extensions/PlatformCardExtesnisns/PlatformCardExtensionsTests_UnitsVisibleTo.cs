﻿using System.Linq;
using Assets.GameLogic.State;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Extensions
{

    public class PlatformCardExtensionsTests_UnitsVisibleTo
    {
        [Test]
        public void ShouldReturnUnitsVisibleFromLeft()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            var platformBuilder = builder.UsingPlatform(actionCard)
                    .PlaceUnit(character)
                    .PlaceUnit(foe);

            var units = actionCard.UnitsVisibleTo(false);

            Assert.AreEqual(2, units.Count());
            Assert.AreSame(character, units.First());
            Assert.AreSame(foe, units.Last());
        }

        [Test]
        public void ShouldReturnUnitsVisibleFromLeftWithObstacleBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            var foe2 = builder.GivenFoe();
            var foe3 = builder.GivenFoe();
            var platformBuilder = builder.UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings)
                    .PlaceUnit(character)
                    .PlaceUnit(foe)
                    .PlaceUnit(foe2, isToTheLeftOfObstacle: false)
                    .PlaceUnit(foe3, isToTheLeftOfObstacle: false);

            var units = actionCard.UnitsVisibleTo(false);

            Assert.AreEqual(2, units.Count());
            Assert.AreSame(character, units.First());
            Assert.AreSame(foe, units.Last());
        }

        [Test]
        public void ShouldReturnUnitsVisibleFromLeftWithObstacleNotBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = false;
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            var foe2 = builder.GivenFoe();
            var foe3 = builder.GivenFoe();
            var platformBuilder = builder.UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings)
                    .PlaceUnit(character)
                    .PlaceUnit(foe)
                    .PlaceUnit(foe2, isToTheLeftOfObstacle: false)
                    .PlaceUnit(foe3, isToTheLeftOfObstacle: false);

            var units = actionCard.UnitsVisibleTo(false);

            Assert.AreEqual(4, units.Count());
            CollectionAssert.AreEqual(actionCard.AllUnits, units);
        }

        [Test]
        public void ShouldReturnUnitsVisibleFromRight()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            var platformBuilder = builder.UsingPlatform(actionCard)
                    .PlaceUnit(character)
                    .PlaceUnit(foe);

            var units = actionCard.UnitsVisibleTo(true);

            Assert.AreEqual(2, units.Count());
            Assert.AreSame(foe, units.First());
            Assert.AreSame(character, units.Last());
        }


        [Test]
        public void ShouldReturnUnitsVisibleFromRightWithObstacleBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            var foe2 = builder.GivenFoe();
            var foe3 = builder.GivenFoe();
            var platformBuilder = builder.UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings)
                    .PlaceUnit(character)
                    .PlaceUnit(foe)
                    .PlaceUnit(foe2, isToTheLeftOfObstacle: false)
                    .PlaceUnit(foe3, isToTheLeftOfObstacle: false);

            var units = actionCard.UnitsVisibleTo(true);

            Assert.AreEqual(2, units.Count());
            Assert.AreSame(foe3, units.First());
            Assert.AreSame(foe2, units.Last());
        }

        [Test]
        public void ShouldReturnUnitsVisibleFromRightWithObstacleNotBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms[0];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = false;
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            var foe2 = builder.GivenFoe();
            var foe3 = builder.GivenFoe();
            var platformBuilder = builder.UsingPlatform(actionCard)
                    .PlaceObstacle(obstacleSettings)
                    .PlaceUnit(character)
                    .PlaceUnit(foe)
                    .PlaceUnit(foe2, isToTheLeftOfObstacle: false)
                    .PlaceUnit(foe3, isToTheLeftOfObstacle: false);

            var units = actionCard.UnitsVisibleTo(true);

            Assert.AreEqual(4, units.Count());
            CollectionAssert.AreEqual(actionCard.AllUnits.Reverse(), units);
        }
    }
}
