﻿using System.Linq;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Extensions
{
    public class PlatformCardExtensionsTests_AllVisiblePlatfomrsToLeft
    {
        [Test]
        public void ShouldReturnAllPlatformsToTheLeftIfNoObstacleBlockingVisibilityisPresent()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms.Last();

            var cards = actionCard.AllVisiblePlatfomrsToLeft(builder.GameState);

            CollectionAssert.AreEqual(
                builder.GameState.Levels[0].Platforms.Reverse().Skip(1),
                cards);
        }

        [Test]
        public void LastReturnedPlatformCanContainObstacleThatIsBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms.Last();
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[1])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToLeft(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Skip(1).Reverse().Skip(1).ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnAllPlatformsToTheLeftIfObstacleIsNotBlockingVisibility()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actionCard = builder.GameState.Levels[0].Platforms.Last();
            var obstacleSettings = builder.GivenObstacleSettings();
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[1])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToLeft(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Reverse().Skip(1).ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnAllPlatformsIfNoObstacleIsPresentAndLevelIsLocked()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray(), isLocked: true);
            var actionCard = builder.GameState.Levels[0].Platforms[2];

            var cards = actionCard.AllVisiblePlatfomrsToLeft(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Take(2).Reverse().Concat(
                    builder.GameState.Levels[0].Platforms.Reverse().Take(2))
                    .ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnAllPlatformsIfObstacleIsNotBlockingVisibilityAndLevelIsLocked()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray(), isLocked: true);
            var actionCard = builder.GameState.Levels[0].Platforms[2];
            var obstacleSettings = builder.GivenObstacleSettings();
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[4])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToLeft(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Take(2).Reverse().Concat(
                    builder.GameState.Levels[0].Platforms.Reverse().Take(2))
                    .ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }

        [Test]
        public void ShouldReturnCorrectPlatformsIfObstacleIsBlockingVisibilityAndLevelIsLocked()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray(), isLocked: true);
            var actionCard = builder.GameState.Levels[0].Platforms[2];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            builder
                .UsingPlatform(builder.GameState.Levels[0].Platforms[4])
                .PlaceObstacle(obstacleSettings);

            var cards = actionCard.AllVisiblePlatfomrsToLeft(builder.GameState);

            var expected = builder.GameState.Levels[0].Platforms.Take(2).Reverse().Concat(
                    builder.GameState.Levels[0].Platforms.Reverse().Take(1))
                    .ToArray();
            CollectionAssert.AreEqual(expected, cards);
        }
    }
}
