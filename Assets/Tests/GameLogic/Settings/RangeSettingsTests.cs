﻿using System;
using Assets.GameLogic;
using Assets.GameLogic.Settings;
using NUnit.Framework;

namespace Assets.Tests.Settings
{
    public class RangeSettingsTests
    {
        const int Equal = 0;
        const int FirstIsGreater = 1;
        const int FirstIsLower = -1;

        [Test]
        public void ShouldRejectComparisionWithOtherObjects()
        {
            var target = new RangeSettings();
            Assert.Throws<InvalidOperationException>(() => target.CompareTo(""));
        }

        [TestCase(Constants.Range.Far, 4, Constants.Range.Medium, 6, FirstIsGreater)]
        [TestCase(Constants.Range.Far, 4, Constants.Range.Far, 4, Equal)]
        [TestCase(Constants.Range.Far, 4, Constants.Range.Far, 3, FirstIsGreater)]
        [TestCase(Constants.Range.Far, 4, Constants.Range.Far, 5, FirstIsLower)]
        [TestCase(Constants.Range.Far, 4, Constants.Range.Self, null, FirstIsGreater)]
        [TestCase(Constants.Range.Far, 4, Constants.Range.Short, 1, FirstIsGreater)]
        [TestCase(Constants.Range.Far, 4, Constants.Range.Short, 6, FirstIsGreater)]

        [TestCase(Constants.Range.Medium, 1, Constants.Range.Far, 3, FirstIsLower)]
        [TestCase(Constants.Range.Medium, 1, Constants.Range.Medium, 1, Equal)]
        [TestCase(Constants.Range.Medium, 1, Constants.Range.Medium, 2, FirstIsLower)]
        [TestCase(Constants.Range.Medium, 1, Constants.Range.Self, null, FirstIsGreater)]
        [TestCase(Constants.Range.Medium, 1, Constants.Range.Short, 1, FirstIsGreater)]
        [TestCase(Constants.Range.Medium, 1, Constants.Range.Short, 6, FirstIsGreater)]
        [TestCase(Constants.Range.Medium, 6, Constants.Range.Far, 3, FirstIsLower)]
        [TestCase(Constants.Range.Medium, 6, Constants.Range.Self, null, FirstIsGreater)]
        [TestCase(Constants.Range.Medium, 6, Constants.Range.Short, 1, FirstIsGreater)]
        [TestCase(Constants.Range.Medium, 6, Constants.Range.Short, 6, FirstIsGreater)]

        [TestCase(Constants.Range.Self, null, Constants.Range.Far, 3, FirstIsLower)]
        [TestCase(Constants.Range.Self, null, Constants.Range.Medium, 1, FirstIsLower)]
        [TestCase(Constants.Range.Self, null, Constants.Range.Self, null, Equal)]
        [TestCase(Constants.Range.Self, null, Constants.Range.Short, 1, FirstIsLower)]

        [TestCase(Constants.Range.Short, 1, Constants.Range.Far, 3, FirstIsLower)]
        [TestCase(Constants.Range.Short, 6, Constants.Range.Far, 3, FirstIsLower)]
        [TestCase(Constants.Range.Short, 1, Constants.Range.Medium, 1, FirstIsLower)]
        [TestCase(Constants.Range.Short, 6, Constants.Range.Medium, 1, FirstIsLower)]
        [TestCase(Constants.Range.Short, 1, Constants.Range.Self, null, FirstIsGreater)]
        [TestCase(Constants.Range.Short, 1, Constants.Range.Short, 6, FirstIsLower)]
        [TestCase(Constants.Range.Short, 1, Constants.Range.Short, 1, Equal)]
        [TestCase(Constants.Range.Short, 6, Constants.Range.Short, 1, FirstIsGreater)]
        public void ShouldCompareRanges(string targetName, int? targetValue, string subjectName, int? subjectValue, int expectedResult)
        {
            var target = new RangeSettings
            {
                Name = targetName,
                Value = targetValue
            };

            var subject = new RangeSettings
            {
                Name = subjectName,
                Value = subjectValue
            };

            var result = target.CompareTo(subject);
            Assert.AreEqual(expectedResult, result);
        }
    }
}