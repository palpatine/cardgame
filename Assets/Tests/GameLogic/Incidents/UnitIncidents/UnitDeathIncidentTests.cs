﻿using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Incidents.UnitIncidents;
using Assets.Tests.GameLogic.Activities;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Incidents.UnitIncidents
{
    public class UnitDeathIncidentTests
    {
        [Test]
        public void ShouldRemoveUnitThatDiedFromUnitsListAsync()
        {
            GivenState(out var builder, out var character);
            var endTurnAction = new EndTurnActivityStub();
            var subject = new UnitDeathIncident(builder, endTurnAction);

            subject.Hande(new UnitDeathIncidentData { Unit = character }).Wait();

            Assert.IsFalse(builder.GameState.Units.Any(x => x == character));
        }

        [Test]
        public void ShouldRemoveUnitFromUnitsOrderListAsync()
        {
            GivenState(out var builder, out var character);
            var endTurnAction = new EndTurnActivityStub();
            var subject = new UnitDeathIncident(builder, endTurnAction);

            subject.Hande(new UnitDeathIncidentData { Unit = character }).Wait();

            Assert.IsFalse(builder.GameState.UnitsOrder.Any(x => x == character));
        }

        [Test]
        public void ShouldUpdateLocationOfFollowingUnitsToTheLeftOfTheObstacleAsync()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var character = builder.GivenCharacter();
            var card = builder.GameState.PlatformCards.Cards.Single();
            builder.UsingPlatform(card)
                .PlaceUnit(character)
                .PlaceUnit(builder.GivenFoe())
                .PlaceUnit(builder.GivenFoe());
            var endTurnAction = new EndTurnActivityStub();
            var subject = new UnitDeathIncident(builder, endTurnAction);

            subject.Hande(new UnitDeathIncidentData { Unit = character }).Wait();

            Assert.IsFalse(card.UnitsToLeftOfObstacle.Any(x => x == character));
            foreach (var item in card.UnitsToLeftOfObstacle)
            {
                Assert.AreEqual(card.UnitsToLeftOfObstacle.IndexOf(item), item.Location.UnitId);
            }
        }

        [Test]
        public void ShouldUpdateLocationOfFollowingUnitsToTheRightOfTheObstacleAsync()
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var character = builder.GivenCharacter();
            var card = builder.GameState.PlatformCards.Cards.Single();
            builder.UsingPlatform(card)
                .PlaceObstacle(builder.GivenObstacleSettings())
                .PlaceUnit(character, false)
                .PlaceUnit(builder.GivenFoe(), false)
                .PlaceUnit(builder.GivenFoe(), false);
            var endTurnAction = new EndTurnActivityStub();
            var subject = new UnitDeathIncident(builder, endTurnAction);

            subject.Hande(new UnitDeathIncidentData { Unit = character }).Wait();

            Assert.IsFalse(card.UnitsToRightOfObstacle.Any(x => x == character));
            foreach (var item in card.UnitsToRightOfObstacle)
            {
                Assert.AreEqual(card.UnitsToRightOfObstacle.IndexOf(item), item.Location.UnitId);
            }
        }


        [Test]
        public void ShouldUpdateEndTurnOfFallenCharacterThatIsCurrentUnit()
        {
            var builder = new GameStateBuilder();
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck()
                .GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray())
                .CreateUntisOrder(foe, character)
                .SetCurrentUnit(character);

            var card = builder.GameState.PlatformCards.Cards.Single();
            builder.UsingPlatform(card)
                .PlaceObstacle(builder.GivenObstacleSettings())
                .PlaceUnit(character, false)
                .PlaceUnit(foe, false);

            var endTurnAction = new EndTurnActivityStub();
            var subject = new UnitDeathIncident(builder, endTurnAction);

            subject.Hande(new UnitDeathIncidentData { Unit = character }).Wait();

            Assert.AreEqual(0, endTurnAction.CurrentUnitIndex);
            Assert.IsTrue(endTurnAction.SuppressTurnRollover);
        }

        [Test]
        public void ShouldPlaceAllItemsCarriedByUintToPlatform()
        {
            var builder = new GameStateBuilder();
            var character = builder.GivenCharacter();
            var foe = builder.GivenFoe();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck()
                .GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray())
                .CreateUntisOrder(foe, character)
                .SetCurrentUnit(character);
            var item = builder.GetItem(builder.GetItemSettings());
            character.Inventory.Add(item);
            var secondItem = builder.GetItem(builder.GetItemSettings(Constants.ItemKinds.Weapon));
            character.Equipment.Add(Constants.ItemKinds.Weapon, secondItem);
            var card = builder.GameState.PlatformCards.Cards.Single();
            builder.UsingPlatform(card)
                .PlaceObstacle(builder.GivenObstacleSettings())
                .PlaceUnit(character, false)
                .PlaceUnit(foe, false);

            var endTurnAction = new EndTurnActivityStub();
            var subject = new UnitDeathIncident(builder, endTurnAction);

            subject.Hande(new UnitDeathIncidentData { Unit = character }).Wait();

            CollectionAssert.AreEquivalent(new[] { item }, card.Items);
            Assert.IsTrue(endTurnAction.SuppressTurnRollover);
        }


        private static void GivenState(out GameStateBuilder builder, out Assets.GameLogic.State.Character character)
        {
            builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            character = builder.GivenCharacter();
            builder.UsingPlatform(builder.GameState.PlatformCards.Cards.Single())
                .PlaceUnit(character);
        }
    }
}
