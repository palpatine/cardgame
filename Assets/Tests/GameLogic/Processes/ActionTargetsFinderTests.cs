﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Processes;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.Tests.TestTools;
using NUnit.Framework;

namespace Assets.Tests.GameLogic.Processes
{
    public class ActionTargetsFinderTests
    {
        // actor facing foe A>F
        [TestCase(false, Constants.Range.Short, 1, 0, 0, 0, 1)]
        [TestCase(false, Constants.Range.Short, 2, 0, 0, 0, 1)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 0, 0, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 0, 0, 1)]
        // actor facing foe separated by one character A>CF
        [TestCase(false, Constants.Range.Short, 1, 0, 0, 1, 0)]
        [TestCase(false, Constants.Range.Short, 2, 0, 0, 1, 1)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 0, 1, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 0, 1, 1)]
        // actor facing foe separated by two characters A>CCF
        [TestCase(false, Constants.Range.Short, 2, 0, 0, 2, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 0, 2, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 0, 2, 1)]
        // actor facing foe on next platform A>  F
        [TestCase(false, Constants.Range.Short, 1, 0, 1, 0, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 1, 0, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 1, 0, 1)]
        // actor facing foe on next platform with another chanracter A>  CF
        [TestCase(false, Constants.Range.Short, 1, 0, 1, 1, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 1, 1, 0)]
        [TestCase(false, Constants.Range.Medium, 2, 0, 1, 1, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 1, 1, 1)]
        // actor facing foe on second platform down range A>  _  F
        [TestCase(false, Constants.Range.Short, 1, 0, 2, 0, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 2, 0, 0)]
        [TestCase(false, Constants.Range.Far, 3, 0, 2, 0, 1)]
        // actor facing foe on second platform down range with another chanracter A>  _  CF
        [TestCase(false, Constants.Range.Far, 3, 0, 2, 1, 0)]
        // actor facing away from foe to the map edge | A
        [TestCase(true, Constants.Range.Short, 1, 0, 0, 0, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 1, 0, 0)]
        [TestCase(true, Constants.Range.Far, 3, 1, 2, 0, 0)]
        // actor facing map edge | _ A<
        [TestCase(true, Constants.Range.Medium, 1, 1, 2, 0, 0)]
        [TestCase(true, Constants.Range.Far, 3, 1, 2, 0, 0)]
        // actor facing map edge | _ _ A<
        [TestCase(true, Constants.Range.Far, 3, 2, 3, 0, 0)]
        public void ShouldFindTargetToTheRight(
            bool isCharacterFacingLeft,
            string rangeName,
            int rangeValue,
            int actorCardId,
            int actionCardId,
            int additionalCharactersCount,
            int expectedTargetSets)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[actorCardId];
            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard).PlaceUnit(character, isFacingLeft: isCharacterFacingLeft);
            var actionCard = builder.GameState.Levels[0].Platforms[actionCardId];
            var platformBuilder = builder.UsingPlatform(actionCard);
            for (var i = 0; i < additionalCharactersCount; i++)
            {
                var aditionalCharacter = builder.GivenCharacter();
                platformBuilder.PlaceUnit(aditionalCharacter);
            }
            var foe = builder.GivenFoe();
            platformBuilder.PlaceUnit(foe);

            var action = new ActionSettings
            {
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };
            var subject = new ActionTargetsFinder(
                builder);

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(expectedTargetSets, targets.Count());
            if (expectedTargetSets != 0)
            {
                Assert.AreEqual(1, targets.Single().Count());
                Assert.AreSame(foe, targets.Single().Single());
            }
        }

        // actor facing foe FA<
        [TestCase(true, Constants.Range.Short, 1, 0, 0, 0, 1)]
        [TestCase(true, Constants.Range.Short, 2, 0, 0, 0, 1)]
        [TestCase(true, Constants.Range.Medium, 2, 0, 0, 0, 1)]
        [TestCase(true, Constants.Range.Far, 3, 0, 0, 0, 1)]
        // actor facing foe separated by one character FCA<
        [TestCase(true, Constants.Range.Short, 1, 0, 0, 1, 0)]
        [TestCase(true, Constants.Range.Short, 2, 0, 0, 1, 1)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 0, 1, 1)]
        [TestCase(true, Constants.Range.Far, 3, 0, 0, 1, 1)]
        // actor facing foe separated by two characters FCCA<
        [TestCase(true, Constants.Range.Short, 2, 0, 0, 2, 0)]
        // actor facing foe on next platform F  A<
        [TestCase(true, Constants.Range.Short, 1, 0, 1, 0, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 1, 0, 1)]
        [TestCase(true, Constants.Range.Far, 3, 0, 1, 0, 1)]
        // actor facing foe on next platform with another chanracter FC A<
        [TestCase(true, Constants.Range.Short, 1, 0, 1, 1, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 1, 1, 0)]
        [TestCase(true, Constants.Range.Medium, 2, 0, 1, 1, 1)]
        [TestCase(true, Constants.Range.Far, 3, 0, 1, 1, 1)]
        // actor facing foe on second platform down range  F  _  A>
        [TestCase(true, Constants.Range.Short, 1, 0, 2, 0, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 2, 0, 0)]
        [TestCase(true, Constants.Range.Far, 3, 0, 2, 0, 1)]
        // actor facing foe on second platform down range with another chanracter FC  _  A<
        [TestCase(true, Constants.Range.Short, 1, 0, 2, 1, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 2, 1, 0)]
        [TestCase(true, Constants.Range.Far, 3, 0, 2, 1, 0)]
        // actor facing away from foe to the map edge A> |
        [TestCase(false, Constants.Range.Short, 1, 0, 0, 0, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 1, 0, 0)]
        [TestCase(false, Constants.Range.Far, 3, 1, 2, 0, 0)]
        // actor facing map edge A> _ |
        [TestCase(false, Constants.Range.Medium, 1, 1, 2, 0, 0)]
        [TestCase(false, Constants.Range.Far, 3, 1, 2, 0, 0)]
        // actor facing map edge A> _ _ |
        [TestCase(false, Constants.Range.Far, 3, 2, 3, 0, 0)]
        public void ShouldFindTargetToTheLeft(
           bool isCharacterFacingLeft,
           string rangeName,
           int rangeValue,
           int actorCardId,
           int actionCardId,
           int additionalCharactersCount,
           int expectedTargetSets)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[4 - actorCardId];
            var actionCard = builder.GameState.Levels[0].Platforms[4 - actionCardId];
            var platformBuilder = builder.UsingPlatform(actionCard);
            var foe = builder.GivenFoe();
            platformBuilder.PlaceUnit(foe);
            for (var i = 0; i < additionalCharactersCount; i++)
            {
                var aditionalCharacter = builder.GivenCharacter();
                platformBuilder.PlaceUnit(aditionalCharacter);
            }
            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard).PlaceUnit(character, isFacingLeft: isCharacterFacingLeft);

            var action = new ActionSettings
            {
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };
            var subject = new ActionTargetsFinder(
                builder);

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(expectedTargetSets, targets.Count());
            if (expectedTargetSets != 0)
            {
                Assert.AreEqual(1, targets.Single().Count());
                Assert.AreSame(foe, targets.Single().Single());
            }
        }

        // actor facing foe next to him A>F
        [TestCase(false, Constants.Range.Short, 1, 0, 0, 0, 1)]
        [TestCase(false, Constants.Range.Short, 2, 0, 0, 0, 1)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 0, 0, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 0, 0, 1)]
        // actor facing away form foe next to him and facing map edge | A<F
        [TestCase(true, Constants.Range.Short, 1, 0, 0, 0, 0)]
        // actor facing foe separated by one more character A>CF
        [TestCase(false, Constants.Range.Short, 1, 0, 0, 1, 1)]
        [TestCase(false, Constants.Range.Short, 2, 0, 0, 1, 1)]
        // actor facing foe separated by two more character A>CF
        [TestCase(false, Constants.Range.Short, 2, 0, 0, 2, 1)]
        // actor facing foe on next platform  A>  F
        [TestCase(false, Constants.Range.Short, 2, 0, 1, 0, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 0, 1, 0, 1)]
        [TestCase(false, Constants.Range.Far, 3, 0, 1, 0, 1)]
        // actor facing foe on next platform separated by one more character  A->  CF
        [TestCase(false, Constants.Range.Medium, 1, 0, 1, 1, 1)]
        [TestCase(false, Constants.Range.Medium, 2, 0, 1, 1, 1)]
        // actor facing foe on platform behind one empty platform A>  _  F
        [TestCase(false, Constants.Range.Medium, 1, 0, 2, 0, 0)]
        [TestCase(false, Constants.Range.Far, 3, 0, 2, 0, 1)]
        // actor facing away from foe on platform one empty platform A<  _  F
        [TestCase(true, Constants.Range.Far, 3, 0, 2, 0, 0)]
        // actor facing foe on platform behind one empty platform and one character adjecent to foe A>  _  CF
        [TestCase(false, Constants.Range.Far, 3, 0, 2, 1, 1)]
        // actor facing foe on platform behind two empty platforms and one character adjecent to foe A>  _  _  F
        [TestCase(false, Constants.Range.Far, 3, 0, 3, 0, 0)]
        // actor facing away from foe and map edge in various distances that are in range | A< , | _ A<, | _ _ A< 
        [TestCase(true, Constants.Range.Far, 3, 0, 3, 0, 0)]
        [TestCase(true, Constants.Range.Far, 3, 1, 3, 0, 0)]
        [TestCase(true, Constants.Range.Far, 3, 2, 3, 0, 0)]
        // actor facing away from foe and facing map edge in range | A<  F, | _ A<  F
        [TestCase(true, Constants.Range.Medium, 1, 0, 1, 0, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 1, 1, 0, 0)]
        public void ShouldFindAreaTargetToTheRight(
           bool isCharacterFacingLeft,
           string rangeName,
           int rangeValue,
           int actorCardId,
           int actionCardId,
           int additionalCharactersCount,
           int expectedTargetSets)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[actorCardId];
            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard).PlaceUnit(character, isFacingLeft: isCharacterFacingLeft);
            var actionCard = builder.GameState.Levels[0].Platforms[actionCardId];
            var platformBuilder = builder.UsingPlatform(actionCard);
            var aditionalCharacters = new List<Character>();
            for (var i = 0; i < additionalCharactersCount; i++)
            {
                var aditionalCharacter = builder.GivenCharacter();
                aditionalCharacters.Add(aditionalCharacter);
                platformBuilder.PlaceUnit(aditionalCharacter);
            }

            var foe = builder.GivenFoe();
            platformBuilder.PlaceUnit(foe);

            var action = new ActionSettings
            {
                AffectsArea = true,
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };
            var subject = new ActionTargetsFinder(
                builder);

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(expectedTargetSets, targets.Count());
            if (expectedTargetSets != 0)
            {
                CollectionAssert.AreEquivalent(aditionalCharacters.Cast<IUnit>().Concat(new[] { foe }).ToArray(), targets.Single());
            }
        }

        // actor facing foe next to him FA<
        [TestCase(true, Constants.Range.Short, 1, 0, 0, 0, 1)]
        [TestCase(true, Constants.Range.Short, 2, 0, 0, 0, 1)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 0, 0, 1)]
        [TestCase(true, Constants.Range.Far, 3, 0, 0, 0, 1)]
        // actor facing away form foe next to him and facing map edge FA> |
        [TestCase(false, Constants.Range.Short, 1, 0, 0, 0, 0)]
        // actor facing foe separated by one more character FCA<
        [TestCase(true, Constants.Range.Short, 1, 0, 0, 1, 1)]
        [TestCase(true, Constants.Range.Short, 2, 0, 0, 1, 1)]
        // actor facing foe separated by two more character FCA<
        [TestCase(true, Constants.Range.Short, 2, 0, 0, 2, 1)]
        // actor facing foe on next platform  F  A<
        [TestCase(true, Constants.Range.Short, 2, 0, 1, 0, 0)]
        [TestCase(true, Constants.Range.Medium, 1, 0, 1, 0, 1)]
        [TestCase(true, Constants.Range.Far, 3, 0, 1, 0, 1)]
        // actor facing foe on next platform separated by one more character  CF  A<
        [TestCase(true, Constants.Range.Medium, 1, 0, 1, 1, 1)]
        [TestCase(true, Constants.Range.Medium, 2, 0, 1, 1, 1)]
        // actor facing foe on platform behind one empty platform F  _  A<
        [TestCase(true, Constants.Range.Medium, 1, 0, 2, 0, 0)]
        [TestCase(true, Constants.Range.Far, 3, 0, 2, 0, 1)]
        // actor facing away from foe on platform one empty platform F  _  A>
        [TestCase(false, Constants.Range.Far, 3, 0, 2, 0, 0)]
        // actor facing foe on platform behind one empty platform and one character adjecent to foe FC  _  A<
        [TestCase(true, Constants.Range.Far, 3, 0, 2, 1, 1)]
        // actor facing foe on platform behind two empty platforms and one character adjecent to foe F  _  _  A<
        [TestCase(true, Constants.Range.Far, 3, 0, 3, 0, 0)]
        // actor facing away from foe and map edge in various distances that are in range | A< , | _ A<, | _ _ A< 
        [TestCase(false, Constants.Range.Far, 3, 0, 3, 0, 0)]
        [TestCase(false, Constants.Range.Far, 3, 1, 3, 0, 0)]
        [TestCase(false, Constants.Range.Far, 3, 2, 3, 0, 0)]
        // actor facing away from foe and facing map edge in range  A>  |, A>  _ |
        [TestCase(false, Constants.Range.Medium, 1, 0, 1, 0, 0)]
        [TestCase(false, Constants.Range.Medium, 1, 1, 1, 0, 0)]
        public void ShouldFindAreaTargetToTheLeft(
           bool isCharacterFacingLeft,
           string rangeName,
           int rangeValue,
           int actorCardId,
           int actionCardId,
           int additionalCharactersCount,
           int expectedTargetSets)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[4 - actorCardId];
            var actionCard = builder.GameState.Levels[0].Platforms[4 - actionCardId];
            var platformBuilder = builder.UsingPlatform(actionCard);
            var foe = builder.GivenFoe();
            platformBuilder.PlaceUnit(foe);

            var aditionalCharacters = new List<Character>();
            for (var i = 0; i < additionalCharactersCount; i++)
            {
                var aditionalCharacter = builder.GivenCharacter();
                aditionalCharacters.Add(aditionalCharacter);
                platformBuilder.PlaceUnit(aditionalCharacter);
            }

            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard).PlaceUnit(character, isFacingLeft: isCharacterFacingLeft);

            var action = new ActionSettings
            {
                AffectsArea = true,
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };
            var subject = new ActionTargetsFinder(
                builder);

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(expectedTargetSets, targets.Count());
            if (expectedTargetSets != 0)
            {
                CollectionAssert.AreEquivalent(aditionalCharacters.Cast<IUnit>().Concat(new[] { foe }).ToArray(), targets.Single());
            }
        }

        // actor facing foe FOA<
        [TestCase(Constants.Range.Short, 1, 0, 0, false)]
        [TestCase(Constants.Range.Short, 1, 0, 0, true)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, true)]
        [TestCase(Constants.Range.Far, 3, 0, 0, false)]
        [TestCase(Constants.Range.Far, 3, 0, 0, true)]
        // actor facing foe on next platform from behind obstacle F OA<
        [TestCase(Constants.Range.Medium, 1, 1, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 0, true)]
        [TestCase(Constants.Range.Far, 3, 1, 0, false)]
        [TestCase(Constants.Range.Far, 3, 1, 0, true)]
        // actor facing foe that is on next platform behind obstacle FO A<
        [TestCase(Constants.Range.Medium, 1, 1, 1, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 1, true)]
        [TestCase(Constants.Range.Far, 3, 1, 1, false)]
        [TestCase(Constants.Range.Far, 3, 1, 1, true)]
        // actor facing foe that is two platforms down range actor is behind obstacle F OA<
        [TestCase(Constants.Range.Far, 3, 2, 0, false)]
        [TestCase(Constants.Range.Far, 3, 2, 0, true)]
        // actor facing foe that is two platforms down range and obstacle is on the middle platform F O A<
        [TestCase(Constants.Range.Far, 3, 2, 1, false)]
        [TestCase(Constants.Range.Far, 3, 2, 1, true)]
        // actor facing foe that is two platforms down range behind obstacle FO _ A<
        [TestCase(Constants.Range.Far, 3, 2, 2, false)]
        [TestCase(Constants.Range.Far, 3, 2, 2, true)]
        public void ShouldNotFindTargetsInRangeBehindObstacleObscuringVisibilityToLeft(
           string rangeName,
           int rangeValue,
           int foeCardId,
           int obstacleCardId,
           bool affectsArea)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[4];
            var foeCard = builder.GameState.Levels[0].Platforms[4 - foeCardId];
            var obstacleCard = builder.GameState.Levels[0].Platforms[4 - obstacleCardId];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            builder
                .UsingPlatform(obstacleCard)
                .PlaceObstacle(obstacleSettings);

            var foe = builder.GivenFoe();
            builder
                .UsingPlatform(foeCard)
                .PlaceUnit(foe);

            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard)
                .PlaceUnit(character, isFacingLeft: true, isToTheLeftOfObstacle: actorCard.Obstacle == null);

            var subject = new ActionTargetsFinder(
             builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(0, targets.Count());
        }

        // actor facing foe FOA<
        [TestCase(Constants.Range.Short, 1, 0, 0, false)]
        [TestCase(Constants.Range.Short, 1, 0, 0, true)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, true)]
        [TestCase(Constants.Range.Far, 3, 0, 0, false)]
        [TestCase(Constants.Range.Far, 3, 0, 0, true)]
        // actor facing foe on next platform from behind obstacle A>O  F
        [TestCase(Constants.Range.Medium, 1, 1, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 0, true)]
        [TestCase(Constants.Range.Far, 3, 1, 0, false)]
        [TestCase(Constants.Range.Far, 3, 1, 0, true)]
        // actor facing foe that is on next platform behind obstacle A>  OF
        [TestCase(Constants.Range.Medium, 1, 1, 1, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 1, true)]
        [TestCase(Constants.Range.Far, 3, 1, 1, false)]
        [TestCase(Constants.Range.Far, 3, 1, 1, true)]
        // actor facing foe that is two platforms down range actor is behind obstacle A>O  F
        [TestCase(Constants.Range.Far, 3, 2, 0, false)]
        [TestCase(Constants.Range.Far, 3, 2, 0, true)]
        // actor facing foe that is two platforms down range and obstacle is on the middle platform A>  O  F
        [TestCase(Constants.Range.Far, 3, 2, 1, false)]
        [TestCase(Constants.Range.Far, 3, 2, 1, true)]
        // actor facing foe that is two platforms down range behind obstacle A>  _  OF
        [TestCase(Constants.Range.Far, 3, 2, 2, false)]
        [TestCase(Constants.Range.Far, 3, 2, 2, true)]
        public void ShouldNotFindTargetsInRangeBehindObstacleObscuringVisibilityToRight(
           string rangeName,
           int rangeValue,
           int foeCardId,
           int obstacleCardId,
           bool affectsArea)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[0];
            var foeCard = builder.GameState.Levels[0].Platforms[foeCardId];
            var obstacleCard = builder.GameState.Levels[0].Platforms[obstacleCardId];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = true;
            builder
                .UsingPlatform(obstacleCard)
                .PlaceObstacle(obstacleSettings);

            var foe = builder.GivenFoe();
            builder
                .UsingPlatform(foeCard)
                .PlaceUnit(foe, isToTheLeftOfObstacle: foeCard.Obstacle == null);

            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard)
                .PlaceUnit(character, isFacingLeft: false);

            var subject = new ActionTargetsFinder(
             builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(0, targets.Count());
        }

        // actor facing foe FOA<
        [TestCase(Constants.Range.Short, 1, 0, 0, false)]
        [TestCase(Constants.Range.Short, 1, 0, 0, true)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, true)]
        [TestCase(Constants.Range.Far, 3, 0, 0, false)]
        [TestCase(Constants.Range.Far, 3, 0, 0, true)]
        // actor facing foe on next platform from behind obstacle F OA<
        [TestCase(Constants.Range.Medium, 1, 1, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 0, true)]
        [TestCase(Constants.Range.Far, 3, 1, 0, false)]
        [TestCase(Constants.Range.Far, 3, 1, 0, true)]
        // actor facing foe that is on next platform behind obstacle FO A<
        [TestCase(Constants.Range.Medium, 1, 1, 1, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 1, true)]
        [TestCase(Constants.Range.Far, 3, 1, 1, false)]
        [TestCase(Constants.Range.Far, 3, 1, 1, true)]
        // actor facing foe that is two platforms down range actor is behind obstacle F OA<
        [TestCase(Constants.Range.Far, 3, 2, 0, false)]
        [TestCase(Constants.Range.Far, 3, 2, 0, true)]
        // actor facing foe that is two platforms down range and obstacle is on the middle platform F O A<
        [TestCase(Constants.Range.Far, 3, 2, 1, false)]
        [TestCase(Constants.Range.Far, 3, 2, 1, true)]
        // actor facing foe that is two platforms down range behind obstacle FO _ A<
        [TestCase(Constants.Range.Far, 3, 2, 2, false)]
        [TestCase(Constants.Range.Far, 3, 2, 2, true)]
        public void ShouldFindTargetsInRangeBehindObstacleNotObscuringVisibilityToLeft(
           string rangeName,
           int rangeValue,
           int foeCardId,
           int obstacleCardId,
           bool affectsArea)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[4];
            var foeCard = builder.GameState.Levels[0].Platforms[4 - foeCardId];
            var obstacleCard = builder.GameState.Levels[0].Platforms[4 - obstacleCardId];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = false;
            builder
                .UsingPlatform(obstacleCard)
                .PlaceObstacle(obstacleSettings);

            var foe = builder.GivenFoe();
            builder
                .UsingPlatform(foeCard)
                .PlaceUnit(foe);

            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard)
                .PlaceUnit(character, isFacingLeft: true, isToTheLeftOfObstacle: actorCard.Obstacle == null);

            var subject = new ActionTargetsFinder(
             builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(1, targets.Count());
        }

        // actor facing foe A>oF
        [TestCase(Constants.Range.Short, 1, 0, 0, false)]
        [TestCase(Constants.Range.Short, 1, 0, 0, true)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 0, 0, true)]
        [TestCase(Constants.Range.Far, 3, 0, 0, false)]
        [TestCase(Constants.Range.Far, 3, 0, 0, true)]
        // actor facing foe on next platform from behind obstacle A>o  F
        [TestCase(Constants.Range.Medium, 1, 1, 0, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 0, true)]
        [TestCase(Constants.Range.Far, 3, 1, 0, false)]
        [TestCase(Constants.Range.Far, 3, 1, 0, true)]
        // actor facing foe that is on next platform behind obstacle A>  oF
        [TestCase(Constants.Range.Medium, 1, 1, 1, false)]
        [TestCase(Constants.Range.Medium, 1, 1, 1, true)]
        [TestCase(Constants.Range.Far, 3, 1, 1, false)]
        [TestCase(Constants.Range.Far, 3, 1, 1, true)]
        // actor facing foe that is two platforms down range actor is behind obstacle A>o  F
        [TestCase(Constants.Range.Far, 3, 2, 0, false)]
        [TestCase(Constants.Range.Far, 3, 2, 0, true)]
        // actor facing foe that is two platforms down range and obstacle is on the middle platform A>  o  F
        [TestCase(Constants.Range.Far, 3, 2, 1, false)]
        [TestCase(Constants.Range.Far, 3, 2, 1, true)]
        // actor facing foe that is two platforms down range behind obstacle A>  _  oF
        [TestCase(Constants.Range.Far, 3, 2, 2, false)]
        [TestCase(Constants.Range.Far, 3, 2, 2, true)]
        public void ShouldFindTargetsInRangeBehindObstacleNotObscuringVisibilityToRight(
           string rangeName,
           int rangeValue,
           int foeCardId,
           int obstacleCardId,
           bool affectsArea)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[0];
            var foeCard = builder.GameState.Levels[0].Platforms[foeCardId];
            var obstacleCard = builder.GameState.Levels[0].Platforms[obstacleCardId];
            var obstacleSettings = builder.GivenObstacleSettings();
            obstacleSettings.IsBlockingVisibility = false;
            builder
                .UsingPlatform(obstacleCard)
                .PlaceObstacle(obstacleSettings);

            var foe = builder.GivenFoe();
            builder
                .UsingPlatform(foeCard)
                .PlaceUnit(foe, isToTheLeftOfObstacle: foeCard.Obstacle == null);

            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard)
                .PlaceUnit(character, isFacingLeft: false);

            var subject = new ActionTargetsFinder(
             builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                Range = new RangeSettings
                {
                    Name = rangeName,
                    Value = rangeValue
                }
            };

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(1, targets.Count());
        }

        [TestCase(1, 0, 0, 0, null, null, false, 1)] // A>FFF
        [TestCase(2, 0, 0, 0, null, null, false, 2)] // A>FFF
        [TestCase(2, 0, 0, 0, 0, false, false, 2)] // A>oFFF
        [TestCase(2, 0, 0, 0, 0, true, false, 1)] // A>FOFF
        [TestCase(2, 1, 0, 0, 0, true, false, 0)] // A>OFF F
        [TestCase(2, 0, 1, 2, null, null, false, 2)] // A>F F F
        [TestCase(2, 0, 1, 2, 1, false, false, 2)] // A>F oF F
        [TestCase(2, 0, 1, 2, 1, true, false, 1)] // A>F OF F
        [TestCase(1, 1, 1, 2, null, null, false, 1)] // A> FF F
        [TestCase(1, 2, 2, 2, null, null, false, 1)] // A> _ FFF

        [TestCase(1, 0, 0, 0, null, null, true, 1)] // A>FFF
        [TestCase(2, 0, 0, 0, null, null, true, 1)] // A>FFF
        [TestCase(2, 0, 0, 0, 0, false, true, 1)] // A>FoFFF
        [TestCase(2, 1, 0, 0, 0, false, true, 1)] // A>oFF  F
        [TestCase(2, 1, 1, 0, 0, false, true, 2)] // A>oF  FF
        [TestCase(2, 0, 0, 0, 0, true, true, 1)] // A>FOFF
        [TestCase(2, 1, 0, 0, 0, true, true, 0)] // A>OFF F
        [TestCase(2, 0, 1, 2, null, null, true, 2)] // A>F F F
        [TestCase(2, 0, 1, 2, 1, false, true, 2)] // A>F oF F
        [TestCase(2, 0, 1, 2, 1, true, true, 1)] // A>F OF F
        [TestCase(1, 1, 1, 2, null, null, true, 1)] // A> FF F
        [TestCase(1, 2, 2, 2, null, null, true, 1)] // A> _ FFF
        public void ShouldFindTrargetForFoeRangeToRight(
           int rangeValue,
           int foe1CardId,
           int foe2CardId,
           int foe3CardId,
           int? obstacleCardId,
           bool? isBlockingVisibility,
           bool affectsArea,
           int expectedSetsCount)
        {
            //arrange
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[0];
            var character = builder.GivenCharacter();
            builder.UsingPlatform(actorCard)
                .PlaceUnit(character, isFacingLeft: false);

            if (obstacleCardId.HasValue)
            {
                var obstacleCard = builder.GameState.Levels[0].Platforms[obstacleCardId.Value];
                var obstacleSettings = builder.GivenObstacleSettings();
                obstacleSettings.IsBlockingVisibility = isBlockingVisibility.Value;
                builder
                    .UsingPlatform(obstacleCard)
                    .PlaceObstacle(obstacleSettings);
            }

            var foe1Card = builder.GameState.Levels[0].Platforms[foe1CardId];
            var foe1 = builder.GivenFoe();
            builder
                .UsingPlatform(foe1Card)
                .PlaceUnit(foe1);

            var foe2Card = builder.GameState.Levels[0].Platforms[foe2CardId];
            var foe2 = builder.GivenFoe();
            builder
                .UsingPlatform(foe2Card)
                .PlaceUnit(foe2, isToTheLeftOfObstacle: foe2Card.Obstacle == null);

            var foe3Card = builder.GameState.Levels[0].Platforms[foe3CardId];
            var foe3 = builder.GivenFoe();
            builder
                .UsingPlatform(foe3Card)
                .PlaceUnit(foe3, isToTheLeftOfObstacle: foe3Card.Obstacle == null);


            var subject = new ActionTargetsFinder(
             builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                Range = new RangeSettings
                {
                    Name = Constants.Range.Foe,
                    Value = rangeValue
                }
            };

            //act
            var targets = subject.GetTargets(character, action);

            //assert
            Assert.AreEqual(expectedSetsCount, targets.Count());
        }

        [TestCase(false, 1, 2, false)]
        [TestCase(false, 2, 3, false)]
        [TestCase(false, 3, 4, false)]
        [TestCase(false, 4, 5, false)]
        [TestCase(false, 1, 5, false)]
        [TestCase(false, 2, 5, false)]
        [TestCase(true, 1, 2, false)]
        [TestCase(true, 2, 3, false)]
        [TestCase(true, 3, 4, false)]
        [TestCase(true, 4, 5, false)]
        [TestCase(true, 1, 5, false)]
        [TestCase(true, 2, 5, false)]

        [TestCase(false, 1, 2, true)]
        [TestCase(false, 2, 3, true)]
        [TestCase(false, 3, 4, true)]
        [TestCase(false, 4, 5, true)]
        [TestCase(false, 1, 5, true)]
        [TestCase(false, 2, 5, true)]
        [TestCase(true, 1, 2, true)]
        [TestCase(true, 2, 3, true)]
        [TestCase(true, 3, 4, true)]
        [TestCase(true, 4, 5, true)]
        [TestCase(true, 1, 5, true)]
        [TestCase(true, 2, 5, true)]
        public void ShouldEvaluateMinRangeForShortRange(
            bool isActorFacingLeft,
            int minRangeValue,
            int rangeValue,
            bool affectsArea)
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[0];
            var character = builder.GivenCharacter();

            var foes = new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                };

            if (isActorFacingLeft)
            {
                var platformBuilder = builder.UsingPlatform(actorCard);
                foreach (var item in foes)
                {
                    platformBuilder.PlaceUnit(item);
                }
                platformBuilder
                     .PlaceUnit(character, isFacingLeft: isActorFacingLeft);
            }
            else
            {
                var platformBuilder = builder.UsingPlatform(actorCard);
                platformBuilder
                     .PlaceUnit(character, isFacingLeft: isActorFacingLeft);
                foreach (var item in foes)
                {
                    platformBuilder.PlaceUnit(item);
                }
            }

            var subject = new ActionTargetsFinder(
            builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                MinRange = new RangeSettings
                {
                    Name = Constants.Range.Short,
                    Value = minRangeValue
                },
                Range = new RangeSettings
                {
                    Name = Constants.Range.Short,
                    Value = rangeValue
                }
            };

            //act
            var targetSets = subject.GetTargets(character, action);


            if (affectsArea)
            {
                Assert.AreEqual(1, targetSets.Count());
                if (isActorFacingLeft)
                {
                    var expected = foes.Reverse();
                    CollectionAssert.AreEqual(expected, targetSets.Single());
                }
                else
                {
                    var expected = foes;
                    CollectionAssert.AreEqual(expected, targetSets.Single());
                }
            }
            else
            {
                var targets = targetSets.SelectMany(x => x).ToArray();
                Assert.AreEqual(rangeValue - minRangeValue, targetSets.Count());
                if (isActorFacingLeft)
                {
                    var expected = foes.Reverse().Skip(minRangeValue).Take(rangeValue - minRangeValue);
                    CollectionAssert.AreEqual(expected, targets);
                }
                else
                {
                    var expected = foes.Skip(minRangeValue).Take(rangeValue - minRangeValue);
                    CollectionAssert.AreEqual(expected, targets);
                }
            }
        }


        [TestCase(false, Constants.Range.Short, 1, 1, false)]
        [TestCase(false, Constants.Range.Short, 2, 1, false)]
        [TestCase(false, Constants.Range.Short, 3, 1, false)]
        [TestCase(false, Constants.Range.Short, 4, 1, false)]
        [TestCase(false, Constants.Range.Short, 5, 1, false)]
        [TestCase(true, Constants.Range.Short, 1, 1, false)]
        [TestCase(true, Constants.Range.Short, 2, 1, false)]
        [TestCase(true, Constants.Range.Short, 3, 1, false)]
        [TestCase(true, Constants.Range.Short, 4, 1, false)]
        [TestCase(true, Constants.Range.Short, 5, 1, false)]

        [TestCase(false, Constants.Range.Short, 1, 1, true)]
        [TestCase(false, Constants.Range.Short, 2, 1, true)]
        [TestCase(false, Constants.Range.Short, 3, 1, true)]
        [TestCase(false, Constants.Range.Short, 4, 1, true)]
        [TestCase(false, Constants.Range.Short, 5, 1, true)]
        [TestCase(true, Constants.Range.Short, 1, 1, true)]
        [TestCase(true, Constants.Range.Short, 2, 1, true)]
        [TestCase(true, Constants.Range.Short, 3, 1, true)]
        [TestCase(true, Constants.Range.Short, 4, 1, true)]
        [TestCase(true, Constants.Range.Short, 5, 1, true)]

        [TestCase(false, Constants.Range.Medium, 1, 2, false)]
        [TestCase(false, Constants.Range.Medium, 2, 3, false)]
        [TestCase(false, Constants.Range.Medium, 3, 5, false)]
        [TestCase(false, Constants.Range.Medium, 4, 6, false)]
        [TestCase(false, Constants.Range.Medium, 5, 6, false)]
        [TestCase(true, Constants.Range.Medium, 1, 2, false)]
        [TestCase(true, Constants.Range.Medium, 2, 3, false)]
        [TestCase(true, Constants.Range.Medium, 3, 5, false)]
        [TestCase(true, Constants.Range.Medium, 4, 6, false)]
        [TestCase(true, Constants.Range.Medium, 5, 6, false)]

        [TestCase(false, Constants.Range.Medium, 1, 2, true)]
        [TestCase(false, Constants.Range.Medium, 2, 3, true)]
        [TestCase(false, Constants.Range.Medium, 3, 5, true)]
        [TestCase(false, Constants.Range.Medium, 4, 6, true)]
        [TestCase(false, Constants.Range.Medium, 5, 6, true)]
        [TestCase(true, Constants.Range.Medium, 1, 2, true)]
        [TestCase(true, Constants.Range.Medium, 2, 3, true)]
        [TestCase(true, Constants.Range.Medium, 3, 5, true)]
        [TestCase(true, Constants.Range.Medium, 4, 6, true)]
        [TestCase(true, Constants.Range.Medium, 5, 6, true)]

        public void ShouldEvaluateMinRangeForMediumRane(
            bool isActorFacingLeft,
            string minRangeName,
            int minRangeValue,
            int rangeValue,
            bool affectsArea)
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[isActorFacingLeft ? 1 : 0];
            var character = builder.GivenCharacter();

            var foesOnActorsCard = new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                };

            var foesOnNextCard = new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                };

            if (isActorFacingLeft)
            {
                var platformBuilder = builder.UsingPlatform(actorCard);
                foreach (var item in foesOnActorsCard)
                {
                    platformBuilder.PlaceUnit(item);
                }
                platformBuilder
                     .PlaceUnit(character, isFacingLeft: isActorFacingLeft);
            }
            else
            {
                var platformBuilder = builder.UsingPlatform(actorCard);
                platformBuilder
                     .PlaceUnit(character, isFacingLeft: isActorFacingLeft);
                foreach (var item in foesOnActorsCard)
                {
                    platformBuilder.PlaceUnit(item);
                }
            }

            var nextPlatformBuilder = builder.UsingPlatform(builder.GameState.Levels[0].Platforms[isActorFacingLeft ? 0 : 1]);
            foreach (var item in foesOnNextCard)
            {
                nextPlatformBuilder.PlaceUnit(item);
            }

            var subject = new ActionTargetsFinder(
            builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                MinRange = new RangeSettings
                {
                    Name = minRangeName,
                    Value = minRangeValue
                },
                Range = new RangeSettings
                {
                    Name = Constants.Range.Medium,
                    Value = rangeValue
                }
            };

            //act
            var targetSets = subject.GetTargets(character, action);

            if (affectsArea)
            {
                if (minRangeName == Constants.Range.Medium || minRangeValue == 5)
                {
                    Assert.AreEqual(1, targetSets.Count());
                    CollectionAssert.AreEquivalent(foesOnNextCard, targetSets.Single());
                }
                else
                {
                    Assert.AreEqual(2, targetSets.Count());
                    CollectionAssert.AreEquivalent(foesOnNextCard, targetSets.Last());
                    CollectionAssert.AreEquivalent(foesOnActorsCard, targetSets.First());
                }
            }
            else
            {
                var targets = targetSets.SelectMany(x => x).ToArray();

                if (minRangeName == Constants.Range.Medium)
                {
                    Assert.AreEqual(rangeValue - minRangeValue, targetSets.Count());
                    if (isActorFacingLeft)
                    {
                        var expected = foesOnNextCard.Reverse().Skip(minRangeValue).Take(rangeValue - minRangeValue);
                        CollectionAssert.AreEqual(expected, targets);
                    }
                    else
                    {
                        var expected = foesOnNextCard.Skip(minRangeValue).Take(rangeValue - minRangeValue);
                        CollectionAssert.AreEqual(expected, targets);
                    }
                }
                else
                {
                    Assert.AreEqual(rangeValue + 5 - minRangeValue, targetSets.Count());
                    if (isActorFacingLeft)
                    {
                        var expected = foesOnActorsCard.Reverse().Skip(minRangeValue).Concat(foesOnNextCard.Reverse().Take(rangeValue));
                        CollectionAssert.AreEqual(expected, targets);
                    }
                    else
                    {
                        var expected = foesOnActorsCard.Skip(minRangeValue).Concat(foesOnNextCard.Take(rangeValue));
                        CollectionAssert.AreEqual(expected, targets);
                    }
                }
            }
        }


        [TestCase(false, Constants.Range.Short, 1, 3, false)]
        [TestCase(false, Constants.Range.Short, 2, 3, false)]
        [TestCase(false, Constants.Range.Short, 3, 3, false)]
        [TestCase(false, Constants.Range.Short, 4, 3, false)]
        [TestCase(false, Constants.Range.Short, 5, 3, false)]
        [TestCase(true, Constants.Range.Short, 1, 3, false)]
        [TestCase(true, Constants.Range.Short, 2, 3, false)]
        [TestCase(true, Constants.Range.Short, 3, 3, false)]
        [TestCase(true, Constants.Range.Short, 4, 3, false)]
        [TestCase(true, Constants.Range.Short, 5, 3, false)]

        [TestCase(false, Constants.Range.Short, 1, 3, true)]
        [TestCase(false, Constants.Range.Short, 2, 3, true)]
        [TestCase(false, Constants.Range.Short, 3, 3, true)]
        [TestCase(false, Constants.Range.Short, 4, 3, true)]
        [TestCase(false, Constants.Range.Short, 5, 3, true)]
        [TestCase(true, Constants.Range.Short, 1, 3, true)]
        [TestCase(true, Constants.Range.Short, 2, 3, true)]
        [TestCase(true, Constants.Range.Short, 3, 3, true)]
        [TestCase(true, Constants.Range.Short, 4, 3, true)]
        [TestCase(true, Constants.Range.Short, 5, 3, true)]

        [TestCase(false, Constants.Range.Medium, 1, 3, false)]
        [TestCase(false, Constants.Range.Medium, 2, 3, false)]
        [TestCase(false, Constants.Range.Medium, 3, 3, false)]
        [TestCase(false, Constants.Range.Medium, 4, 3, false)]
        [TestCase(false, Constants.Range.Medium, 5, 3, false)]
        [TestCase(true, Constants.Range.Medium, 1, 3, false)]
        [TestCase(true, Constants.Range.Medium, 2, 3, false)]
        [TestCase(true, Constants.Range.Medium, 3, 3, false)]
        [TestCase(true, Constants.Range.Medium, 4, 3, false)]
        [TestCase(true, Constants.Range.Medium, 5, 3, false)]

        [TestCase(false, Constants.Range.Medium, 1, 3, true)]
        [TestCase(false, Constants.Range.Medium, 2, 3, true)]
        [TestCase(false, Constants.Range.Medium, 3, 3, true)]
        [TestCase(false, Constants.Range.Medium, 4, 3, true)]
        [TestCase(false, Constants.Range.Medium, 5, 3, true)]
        [TestCase(true, Constants.Range.Medium, 1, 3, true)]
        [TestCase(true, Constants.Range.Medium, 2, 3, true)]
        [TestCase(true, Constants.Range.Medium, 3, 3, true)]
        [TestCase(true, Constants.Range.Medium, 4, 3, true)]
        [TestCase(true, Constants.Range.Medium, 5, 3, true)]

        [TestCase(false, Constants.Range.Far, 3, 4, true)]
        [TestCase(true, Constants.Range.Far, 3, 4, true)]
        [TestCase(false, Constants.Range.Far, 3, 4, false)]
        [TestCase(true, Constants.Range.Far, 3, 4, false)]

        public void ShouldEvaluateMinRangeForFarRane(
            bool isActorFacingLeft,
            string minRangeName,
            int minRangeValue,
            int rangeValue,
            bool affectsArea)
        {
            var builder = new GameStateBuilder();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.GivenPlatformSettings();
            builder.CreatePlatforsDeck();
            builder.GivenMapLevel(builder.GameState.PlatformCards.Cards.ToArray());
            var actorCard = builder.GameState.Levels[0].Platforms[isActorFacingLeft ? 3 : 0];
            var character = builder.GivenCharacter();

            var foesOnActorsCard = new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                };
            var foesOnCards = new[] {
                new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                },
                new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                },
                new[]
                {
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe(),
                    builder.GivenFoe()
                }
            };

            if (isActorFacingLeft)
            {
                var platformBuilder = builder.UsingPlatform(actorCard);
                foreach (var item in foesOnActorsCard)
                {
                    platformBuilder.PlaceUnit(item);
                }
                platformBuilder
                     .PlaceUnit(character, isFacingLeft: isActorFacingLeft);

                for (var i = 3; i > 0; i--)
                {
                    var nextPlatformBuilder = builder.UsingPlatform(builder.GameState.Levels[0].Platforms[i - 1]);
                    foreach (var item in foesOnCards[3 - i])
                    {
                        nextPlatformBuilder.PlaceUnit(item);
                    }
                }
            }
            else
            {
                var platformBuilder = builder.UsingPlatform(actorCard);
                platformBuilder
                     .PlaceUnit(character, isFacingLeft: isActorFacingLeft);
                foreach (var item in foesOnActorsCard)
                {
                    platformBuilder.PlaceUnit(item);
                }

                for (var i = 1; i <= 3; i++)
                {
                    var nextPlatformBuilder = builder.UsingPlatform(builder.GameState.Levels[0].Platforms[i]);
                    foreach (var item in foesOnCards[i - 1])
                    {
                        nextPlatformBuilder.PlaceUnit(item);
                    }
                }
            }

            var subject = new ActionTargetsFinder(
            builder);

            var action = new ActionSettings
            {
                AffectsArea = affectsArea,
                MinRange = new RangeSettings
                {
                    Name = minRangeName,
                    Value = minRangeValue
                },
                Range = new RangeSettings
                {
                    Name = Constants.Range.Far,
                    Value = rangeValue
                }
            };

            //act
            var targetSets = subject.GetTargets(character, action).ToList();

            if (affectsArea)
            {
                var expectedSets = new List<Foe[]>();
                if (minRangeName == Constants.Range.Short && minRangeValue < 5)
                {
                    expectedSets.Add(foesOnActorsCard);
                }

                if (minRangeName == Constants.Range.Far)
                {
                    expectedSets.AddRange(foesOnCards.Skip(minRangeValue - 2).Take(rangeValue + 1 - minRangeValue));
                }
                else
                {
                    expectedSets.AddRange(foesOnCards.Take(rangeValue - 1));
                }

                Assert.AreEqual(expectedSets.Count(), targetSets.Count());

                for (var i = 0; i < expectedSets.Count(); i++)
                {
                    CollectionAssert.AreEquivalent(expectedSets[i], targetSets[i], i.ToString());
                }
            }
            else
            {
                var targets = targetSets.SelectMany(x => x).ToArray();
                if (minRangeName == Constants.Range.Far)
                {
                    var card = foesOnCards[minRangeValue - 2];
                    if (isActorFacingLeft)
                    {
                        var expected = card.Reverse().Skip(1)
                            .Concat(foesOnCards.Skip(minRangeValue - 2)
                                               .Take(rangeValue - minRangeValue - 2)
                                               .SelectMany(x => x.Reverse()))
                            .Concat(foesOnCards.Skip(minRangeValue - 1).First().Reverse().Take(1));

                        Assert.AreEqual(expected.Count(), targetSets.Count());
                        CollectionAssert.AreEqual(expected, targets);
                    }
                    else
                    {
                        var expected = card.Skip(1)
                            .Concat(foesOnCards.Skip(minRangeValue - 2)
                                               .Take(rangeValue - minRangeValue - 2)
                                               .SelectMany(x => x))
                            .Concat(foesOnCards.Skip(minRangeValue - 1).First().Take(1));

                        Assert.AreEqual(expected.Count(), targetSets.Count());
                        CollectionAssert.AreEqual(expected, targets);
                    }
                }
                else if (minRangeName == Constants.Range.Medium)
                {
                    var card = foesOnCards[0];
                    if (isActorFacingLeft)
                    {
                        var expected = card.Reverse().Skip(minRangeValue)
                            .Concat(foesOnCards.Skip(1).Take(rangeValue - 3).SelectMany(x => x.Reverse()))
                            .Concat(foesOnCards.Skip(rangeValue - 2).First().Reverse().Take(1));
                        Assert.AreEqual(expected.Count(), targetSets.Count());
                        CollectionAssert.AreEqual(expected, targets);
                    }
                    else
                    {
                        var expected = card.Skip(minRangeValue)
                           .Concat(foesOnCards.Skip(1).Take(rangeValue - 3).SelectMany(x => x))
                            .Concat(foesOnCards.Skip(rangeValue - 2).First().Take(1));
                        Assert.AreEqual(expected.Count(), targetSets.Count());
                        CollectionAssert.AreEqual(expected, targets);
                    }
                }
                else if (minRangeName == Constants.Range.Short)
                {
                    if (isActorFacingLeft)
                    {
                        var expected = foesOnActorsCard.Reverse().Skip(minRangeValue)
                            .Concat(foesOnCards.Take(rangeValue - 2).SelectMany(x => x.Reverse()))
                            .Concat(foesOnCards.Skip(rangeValue - 2).First().Reverse().Take(1));
                        Assert.AreEqual(expected.Count(), targetSets.Count());
                        CollectionAssert.AreEqual(expected, targets);
                    }
                    else
                    {
                        var expected = foesOnActorsCard.Skip(minRangeValue)
                           .Concat(foesOnCards.Take(rangeValue - 2).SelectMany(x => x))
                            .Concat(foesOnCards.Skip(rangeValue - 2).First().Take(1));
                        Assert.AreEqual(expected.Count(), targetSets.Count());
                        CollectionAssert.AreEqual(expected, targets);
                    }
                }
            }
        }
    }
}
