﻿using System;
using System.Threading.Tasks;
using Assets.GameLogic.Activities;

namespace Assets.Tests.GameLogic.Activities
{
    internal class EndTurnActivityStub : IEndTurnActivity
    {
        public string DisplayName => throw new NotImplementedException();

        public string Name => throw new NotImplementedException();

        public int? CurrentUnitIndex { get; private set; }
        public bool? SuppressTurnRollover { get; private set; }

        public bool CanExecute() => throw new NotImplementedException();
        public Task Execute() => throw new NotImplementedException();

        public Task Execute(int currentUnitIndex, bool suppressTurnRollover)
        {
            CurrentUnitIndex = currentUnitIndex;
            SuppressTurnRollover = suppressTurnRollover;
            return Task.CompletedTask;
        }
    }
}
