﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Load.Settings;
using Assets.GameLogic.Load.Settings.Validators;
using Zenject;

namespace Assets.GameLogic.Load
{
    public class LoadInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            InstallValidators();

            Container.Bind<IGameDataLoader>().To<GameDataLoader>().AsTransient();
            Container.Bind<ISettingsValidationManager>().To<SettingsValidationManager>().AsSingle();
        }

        private void InstallValidators()
        {
            var validators = new Type[] {
                typeof(AbilitySettingsValidator),
                typeof(ActionSettingsValidator),
                typeof(ActionTraceFilterValidator),
                typeof(AmbushSettingsValidator),
                typeof(CharacterSettingsValidator),
                typeof(ConditionSettingsValidator),
                typeof(ConditionValueSettingsValidator),
                typeof(DangerSettingsValidator),
                typeof(EffectSettingsValidator),
                typeof(EquipmentSelectorValidator),
                typeof(ExplorationSettingsValidator),
                typeof(FeatureSelectorValidator),
                typeof(FeatureSettingsValidator),
                typeof(FoeSettingsValidator),
                typeof(ItemSettingsValidator),
                typeof(ObstacleSettingsValidator),
                typeof(PlatformCardSettingsValidator),
                typeof(PlatformSelectorValidator),
                typeof(RangeSettingsValidator),
                typeof(RemediesSettingsValidator),
                typeof(ResourceSettingsValidator),
                typeof(RuneCardSettingsValidator),
                typeof(SelectorSettingsValidator),
                typeof(SpawnSettingsValidator),
                typeof(StatisticReferenceValidator),
                typeof(TargetSelectorValidator),
                typeof(TriggerSettingsValidator),
                typeof(ValueSettingsValidator),
            };

            Container.Bind(validators).AsTransient();

            Container.Bind<Func<IEnumerable<ISettingsValidator>>>().FromMethod(
                () => () => validators.Select(x => (ISettingsValidator)Container.Resolve(x)).ToArray()
            );
        }
    }
}
