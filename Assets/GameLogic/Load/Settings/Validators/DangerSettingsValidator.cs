﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class DangerSettingsValidator : IRootSettingsValidator<DangerSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public DangerSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();
            foreach (var item in gameSettings.Dangers
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Danger entry must have a name.");
            }

            foreach (var item in gameSettings.Dangers
                   .Where(x => !string.IsNullOrEmpty(x.Name))
                   .GroupBy(x => x.Name)
                   .Where(x => x.Count() > 1))
            {
                errors.Add($"Danger entry must have unique name '{item.Key}'.");
            }

            foreach (var danger in gameSettings.Dangers)
            {
                foreach (var effect in danger.Effects.Where(
                               x => x.Foe != null && gameSettings.Foes.All(z => z.Name != x.Foe)))
                {
                    errors.Add($"Danger entry '{danger.Name}' refers foe '{effect.Foe}' that is not defined.");
                }

                foreach (var effect in danger.Effects.Where(
                               x => x.Replacement != null && gameSettings.PlatformDeck.All(z => z.Name != x.Replacement)))
                {
                    errors.Add($"Danger entry '{danger.Name}' refers platform '{effect.Replacement}' that is not defined.");
                }

                var effects = danger.Effects.Where(
                               x => x.Effect != null)
                               .Select(x => (Name: danger.Name, x.Effect));

                errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, "Danger effect"));

                var platformSelectors = danger.Effects.Where(x => x.PlatformSelector != null)
                   .Select(x => (Name: danger.Name, x.PlatformSelector));

                errors.AddRange(_settingsValidationManager.Validate(
                    gameSettings,
                    platformSelectors,
                    "Danger"));
            }

            var rewards = gameSettings.Dangers
                           .SelectMany(x => x.Reward.Select(y => (Name: x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, rewards, "Danger reward"));

            return errors;
        }
    }
}