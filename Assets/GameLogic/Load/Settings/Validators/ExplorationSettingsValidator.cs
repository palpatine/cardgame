﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class ExplorationSettingsValidator : ISettingsValidator<ExplorationSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ExplorationSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, ExplorationSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();

            foreach (var (parentName, item) in items.Where(x => x.item.Place != null && gameSettings.PlatformPlaces.All(z => z.Name != x.item.Place)))
            {
                errors.Add($"{parentKind} entry '{parentName}' refers to platform place '{item.Place}' that is not defined.");
            }

            var sources = items
                .SelectMany(x => x.item.Ambushes.Select(z => (x.parentName + " in Exploration", z)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, sources, parentKind));

            return errors;

            //if (card.Exploration.Treasures != null)
            //{
            //    foreach (var treasure in card.Exploration.Treasures
            //        .Where(x => data.Treasures.All(z => z.Name != x.Name)))
            //    {
            //        errors.Add($"Platform card {card.Name} refers to treasure by name {treasure.Name} that is not defined.");
            //    }
            //}

            //if (card.Exploration.Treasures != null)
            //{
            //    foreach (var treasure in card.Exploration.Treasures
            //        .Where(x => data.Treasures.All(z => z.Kinds.Contains(x.Kind))))
            //    {
            //        errors.Add($"Platform card {card.Name} refers to treasure by name {treasure.Name} that is not defined.");
            //    }
            //}
        }
    }
}