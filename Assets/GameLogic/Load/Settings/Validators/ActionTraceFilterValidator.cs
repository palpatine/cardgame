﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Model.Trace;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class ActionTraceFilterValidator : ISettingsValidator<ActionTraceFilter>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ActionTraceFilterValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
           GameSettings gameSettings,
           IEnumerable<(string parentName, ActionTraceFilter item)> items,
           string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var actions = LoaderHelper.GetAllValues(typeof(Constants.TraceActions));
            var traceActionProperties = typeof(ActionTrace)
                .GetProperties().Select(x => x.Name).ToArray();
            foreach (var (parentName, item) in items)
            {
                if (!actions.Contains(item.Action))
                {
                    errors.Add($"{parentKind} entry '{parentName}' refers to past action '{item.Action}' that is not defined.");
                }

                if (string.IsNullOrEmpty(item.Get))
                {
                    errors.Add($"{parentKind} entry '{parentName}' refers to past action '{item.Action}' and does not specify which value to get.");
                }
                else if (!traceActionProperties.Contains(item.Get))
                {
                    errors.Add($"{parentKind} entry '{parentName}' refers to past action '{item.Action}' and does specify as value to get '{item.Get}' which is not defined.");
                }
            }

            var targets = items
                .Where(x => x.item.Target != null)
                .Select(x => (x.parentName, x.item.Target));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, targets, parentKind));

            return errors;
        }
    }
}