﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class TriggerSettingsValidator : ISettingsValidator<TriggerSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public TriggerSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
           GameSettings data,
           IEnumerable<(string parentName, TriggerSettings item)> items,
           string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var triggerKinds = LoaderHelper.GetAllValues(typeof(Constants.Triggers));
            foreach (var (parentName, trigger) in items)
            {
                if (!triggerKinds.Contains(trigger.Name))
                {
                    errors.Add($"{parentKind} entry '{parentName}' defines trigger '{trigger.Name}' that is not defined.");
                }

                if (trigger.Name == Constants.Triggers.ConsumableGained)
                {
                    if (trigger.Kind == null)
                    {
                        errors.Add($"{parentKind} entry '{parentName}' trigger of type '{trigger.Name}' must define kind as item name.");
                    }
                    else
                    {
                        var item = data.Items.SingleOrDefault(x => x.Name == trigger.Kind);
                        if (item == null || !item.Kinds.Contains(Constants.ItemKinds.Consumable))
                        {
                            errors.Add($"{parentKind} entry '{parentName}' trigger of type '{trigger.Name}' refers to item '{trigger.Kind}' that is not definied or is not {Constants.ItemKinds.Consumable}.");
                        }
                    }
                }
            }

            return errors;
        }

    }
}