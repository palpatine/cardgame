﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class ValueSettingsValidator : ValueSettingsValidatorBase, ISettingsValidator<ValueSettings>
    {
        public ValueSettingsValidator(ISettingsValidationManager settingsValidationManager) : base(settingsValidationManager) { }

        protected override void ValidatePropertyAssignmentSets(IEnumerable<(string parentName, ValueSettings item)> items, string parentKind, List<string> errors)
        {
            foreach (var (parentName, conditionValue) in items)
            {
                if (conditionValue.Value != null && (
                                conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Source != null
                                || conditionValue.Equipment != null
                                || conditionValue.Statistic != null
                                || conditionValue.Selected != null)
                    || (conditionValue.Source != null) && (
                                conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Selected != null)
                    || (conditionValue.Statistic != null) && (
                                conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || conditionValue.Source == null)
                    || conditionValue.Operation != null && (
                                conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || !conditionValue.Values.Any()
                                || conditionValue.Selected != null)
                    || conditionValue.Values.Any() && (
                                conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || conditionValue.Operation == null)
                    || conditionValue.StatisticsChange != null && (
                                conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || conditionValue.Source == null)
                    || conditionValue.Equipment != null && (
                                conditionValue.PreviousAction != null
                                || conditionValue.Selected != null
                                || conditionValue.Source == null)
                    || conditionValue.Selected != null && (
                                conditionValue.PreviousAction != null)
                    || (conditionValue.Value == null
                                && !conditionValue.Values.Any()
                                && conditionValue.Operation == null
                                && conditionValue.StatisticsChange == null
                                && conditionValue.PreviousAction == null
                                && conditionValue.Source == null
                                && conditionValue.Equipment == null
                                && conditionValue.Statistic == null
                                && conditionValue.Selected == null))
                {
                    errors.Add($@"{parentKind} entry '{parentName}' definies invalid value. Only one of following sets of properties can be set:
{nameof(ValueSettings.Value)} or
{nameof(ValueSettings.Source)} or
{nameof(ValueSettings.Selected)} or
{nameof(ValueSettings.Source)} and {nameof(ValueSettings.Equipment)} or
{nameof(ValueSettings.Source)} and {nameof(ValueSettings.Statistic)} or
{nameof(ValueSettings.Operation)} and {nameof(ValueSettings.Values)} or
{nameof(ValueSettings.StatisticsChange)} or
{nameof(ValueSettings.PreviousAction)}");
                }
            }
        }
    }
}