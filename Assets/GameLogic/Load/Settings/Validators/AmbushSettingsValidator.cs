﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class AmbushSettingsValidator : ISettingsValidator<AmbushSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public AmbushSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, AmbushSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();
            foreach (var (parentName, item) in items
                   .Where(x => gameSettings.Foes.All(z => z.Name != x.item.Foe)))
            {
                errors.Add($"{parentKind} entry '{parentName}' refers to foe '{item.Foe}' that is not defined.");
            }

            foreach (var (parentName, item) in items.Where(x => x.item.Count < 1))
            {
                errors.Add($"{parentKind} entry '{parentName}' refers to foe '{item.Foe}' and provides negative count.");
            }

            return errors;
        }
    }
}