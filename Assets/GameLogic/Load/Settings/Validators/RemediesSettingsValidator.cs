﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class RemediesSettingsValidator : ISettingsValidator<RemediesSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public RemediesSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, RemediesSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();

            foreach (var (parentName, item) in items
                   .Where(x => x.item.Obstacle != null
                   && gameSettings.Obstacles.All(z => z.Name != x.item.Obstacle)))
            {
                errors.Add($"{parentKind} entry '{parentName}' in remedies refers to obstacle '{item.Obstacle}' that is not defined.");
            }

            var effects = items.Where(x => x.item.Effect != null)
            .Select(x => (x.parentName + " in Remedies", x.item.Effect))
            .ToArray();

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, parentKind));

            var actions = LoaderHelper.GetAllValues(typeof(Constants.Actions));
            foreach (var (parentName, item) in items.Where(x => !string.IsNullOrWhiteSpace(x.item.Action) && !actions.Contains(x.item.Action)))
            {
                errors.Add($"{parentKind} entry '{parentName}' in remedies refers to action '{item.Action}' that is not defined.");
            }

            return errors;
        }
    }
}