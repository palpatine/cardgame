﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class FoeSettingsValidator : IRootSettingsValidator<FoeSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public FoeSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();
            foreach (var item in gameSettings.Foes
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Foe entry must have a name.");
            }

            foreach (var item in gameSettings.Foes
                   .Where(x => !string.IsNullOrEmpty(x.Name))
                   .GroupBy(x => x.Name)
                   .Where(x => x.Count() > 1))
            {
                errors.Add($"Foe entry must have unique name '{item.Key}'.");
            }

            var platformSelectors = gameSettings.Foes
                      .Where(x => x.Spawn != null && x.Spawn.PlatformSelector != null)
                      .Select(x => (x.Name, x.Spawn.PlatformSelector));

            errors.AddRange(_settingsValidationManager.Validate(
                gameSettings,
                platformSelectors,
                $"Foe"));

            foreach (var foe in gameSettings.Foes.Where(x => x.Actions == null || !x.Actions.Any()))
            {
                errors.Add($"Foe entry '{foe.Name}' must have an action.");
            }

            foreach (var foe in gameSettings.Foes.Where(x => x.Count < 0))
            {
                errors.Add($"Foe entry '{foe.Name}' must have nonnegative count.");
            }

            foreach (var foe in gameSettings.Foes.Where(x => x.Spawn == null && x.Count == 0))
            {
                errors.Add($"Foe entry '{foe.Name}' must have spawn or count defined.");
            }

            var actions = gameSettings.Foes
                           .SelectMany(x => x.Actions.Select(y => (Name: x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, actions, "Foe"));

            var spawn = gameSettings.Foes.Where(x => x.Spawn != null)
                           .Select(x => (x.Name, x.Spawn));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, spawn, "Foe"));


            foreach (var foe in gameSettings.Foes.Where(x => x.Abilities != null))
            {
                foreach (var (name, abilitySettings) in foe.Abilities.Select(
                                x => (x, gameSettings.Abilities.SingleOrDefault(z => z.Name == x))))
                {
                    if (abilitySettings == null)
                    {
                        errors.Add($"Foe settings '{foe.Name}' refers to ability '{name}' that is not defined.");
                    }
                    else if (abilitySettings.Parameters.Any())
                    {
                        errors.Add($"Character settings refers to ability '{name}' that defines parameters.");
                    }
                }
            }

            return errors;
        }
    }
}