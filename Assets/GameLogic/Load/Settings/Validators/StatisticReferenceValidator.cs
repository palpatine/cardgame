﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class StatisticReferenceValidator : ISettingsValidator<StatisticReference>
    {
        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, StatisticReference item)> statisticsReferences,
            string parentKind)
        {
            var errors = new List<string>();
            if (!statisticsReferences.Any())
            {
                return errors;
            }

            var statistics = LoaderHelper.GetAllValues(typeof(Constants.Statisitcs));
            var statisticValues = LoaderHelper.GetAllValues(typeof(Constants.StatisitcValues));

            foreach (var item in statisticsReferences
                .Where(x => !statistics.Contains(x.item.Name)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' refers statistic '{item.item.Name}' that is not defined.");
            }

            foreach (var item in statisticsReferences
                .Where(x => !statisticValues.Contains(x.item.Value)
                || x.item.Value == Constants.StatisitcValues.Maximum && !Constants.RegenerableStatistics.Contains(x.item.Name) && !Constants.ReplenishableStatistics.Contains(x.item.Name)
                || x.item.Value == Constants.StatisitcValues.Regeneration && !Constants.RegenerableStatistics.Contains(x.item.Name)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' refers statistic value '{item.item.Name}' that is not defined or not appropriate for statistic {item.item.Value}.");
            }

            return errors;
        }
    }
}