﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class ResourceSettingsValidator : ISettingsValidator<ResourceSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ResourceSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, ResourceSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();
            foreach (var (parentName, item) in items
                   .Where(x => gameSettings.Items.All(z => z.Name != x.item.Name)))
            {
                errors.Add($"{parentKind} entry '{parentName}' in resources refers to item '{item.Name}' that is not defined.");
            }

            foreach (var (parentName, item) in items.Where(x => x.item.Count < 1))
            {
                errors.Add($"{parentKind} entry '{parentName}' in resources refers to item '{item.Name}' and provides negative count.");
            }

            return errors;
        }
    }
}