﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class EquipmentSelectorValidator : ISettingsValidator<EquipmentSelector>
    {
        public IEnumerable<string> Validate(
                      GameSettings gameSettings,
                      IEnumerable<(string parentName, EquipmentSelector item)> items,
                      string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            foreach (var item in items.Where(x => !gameSettings.EquipableItemKinds.Contains(x.item.Kind)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' in equipment selector points to undefined equipement kind {item.item.Kind}.");
            }

            var values = LoaderHelper.GetAllValues(typeof(Constants.EquipmentSelectorProperties));
            foreach (var item in items.Where(x => !values.Contains(x.item.Property)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' in equipment selector points to undefined equipement kind {item.item.Kind}.");
            }

            return errors;
        }
    }
}