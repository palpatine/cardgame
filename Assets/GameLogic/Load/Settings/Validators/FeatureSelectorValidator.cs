﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class FeatureSelectorValidator : ISettingsValidator<FeatureSelector>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public FeatureSelectorValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, FeatureSelector item)> items,
            string parentKind)
        {
            var errors = new List<string>();
            var features = LoaderHelper.GetAllValues(typeof(Constants.Features));

            foreach (var item in items.Where(x => !features.Contains(x.item.Name)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' in feature selector refers to feature '{item.item.Name}' which is not defineid.");
            }

            return errors;
        }
    }
}