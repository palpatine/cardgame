﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class EffectSettingsValidator : ISettingsValidator<EffectSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public EffectSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, EffectSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var effectsWithStatistic = items.Where(
                               x => x.item.Statistic != null)
                               .Select(x => (Name: x.parentName, x.item.Statistic));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, effectsWithStatistic, parentKind));

            var conditions = items.Where(
                               x => x.item.Condition != null)
                               .Select(x => (Name: x.parentName, x.item.Condition));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, conditions, parentKind));

            foreach (var item in items.Where(
                               x => x.item.Statistic != null
                               && x.item.Delta == null
                               && x.item.ParameterForDelta == null))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' defines effect on statistic without providing delta.");
            }

            foreach (var (parentName, item) in items.Where(
                               x => x.item.ParameterForDelta != null))
            {
                if (!_settingsValidationManager.VerifyParameter(item.ParameterForDelta))
                {
                    errors.Add($"{parentKind} entry '{parentName}' defines effect with parameter for delta that is not defined '{item.ParameterForDelta}'.");
                }
            }

            foreach (var (parentName, item) in items.Where(
                           x => x.item.Equipment != null))
            {
                var equipmentSettings = gameSettings.Items.SingleOrDefault(z => z.Name == item.Equipment);
                if (equipmentSettings == null)
                {
                    errors.Add($"{parentKind} entry '{parentName}' refers to equipment '{item.Equipment}' that is not defined.");
                }
            }

            foreach (var (parentName, item) in items.Where(
                        x => x.item.Ability != null))
            {
                var abilitySettings = gameSettings.Abilities.SingleOrDefault(z => z.Name == item.Ability);
                if (abilitySettings == null)
                {
                    errors.Add($"{parentKind} entry '{parentName}' refers to ability '{item.Ability}' that is not defined.");
                }
                else
                {
                    var notAssignedParameters = abilitySettings.Parameters.Where(x => !item.Parameters.Any(y => y.Name == x));
                    var notDefinedParameters = item.Parameters.Where(x => !abilitySettings.Parameters.Any(y => y == x.Name));
                    foreach (var notAssignedParameter in notAssignedParameters)
                    {
                        errors.Add($"{parentKind} entry '{parentName}' refers to ability '{item.Ability}' that requires parameter '{notAssignedParameter}' and its value is not defined.");
                    }

                    foreach (var notDefinedParameter in notDefinedParameters)
                    {
                        errors.Add($"{parentKind} entry '{parentName}' refers to ability '{item.Ability}' and defines parameter '{notDefinedParameter.Name}' yet this parameter is not defined in ability.");
                    }
                }
            }

            var parametersValues = items.Where(x => x.item.Parameters.Any()).SelectMany(x => x.item.Parameters.Select(y => ($"{x.parentName} effect ablility:{x.item.Ability} parameter:{y.Name}", y.Value)))
                .Where(x => x.Value != null);

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, parametersValues, parentKind));

            var deltaValues = items.Where(x => x.item.Delta != null)
                .Select(x => ($"{x.parentName} effect delta", x.item.Delta));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, deltaValues, parentKind));

            var targetSelectors = items.Where(x => x.item.Target != null)
                .Select(x => (x.parentName, x.item.Target));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, targetSelectors, parentKind));

            var actions = items.Where(x => x.item.Action != null)
                           .Select(x => (Name: x.parentName, x.item.Action));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, actions, parentKind));

            foreach (var (parentName, item) in items)
            {
                if (item.Statistic != null
                      && (!(item.Delta == null ^ item.ParameterForDelta == null)
                         || item.Ability != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.Action != null
                         || item.EndTurn != null
                         || item.Equipment != null
                         || item.Invulnerability != null
                         || item.NoRestoration != null
                         || item.RuneCard != null
                         || item.Parameters.Any() && item.ParameterForDelta == null)
                 || item.Equipment != null
                      && (!(item.Delta == null ^ item.ParameterForDelta == null)
                         || item.Ability != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.Action != null
                         || item.EndTurn != null
                         || item.Invulnerability != null
                         || item.NoRestoration != null
                         || item.RuneCard != null
                         || item.Parameters.Any() && item.ParameterForDelta == null)
                 || item.RuneCard != null
                      && (!(item.Delta == null ^ item.ParameterForDelta == null)
                         || item.Ability != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.Action != null
                         || item.EndTurn != null
                         || item.Invulnerability != null
                         || item.NoRestoration != null
                         || item.IsNegative != null
                         || item.Parameters.Any() && item.ParameterForDelta == null)
                 || item.Ability != null
                      && (item.Delta != null
                         || item.ParameterForDelta != null
                         || item.Equipment != null
                         || item.Action != null
                         || item.EndTurn != null
                         || item.Invulnerability != null
                         || item.NoRestoration != null
                         || item.IsNegative != null)
                 || item.Action != null
                      && (item.Delta != null
                         || item.ParameterForDelta != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.EndTurn != null
                         || item.Invulnerability != null
                         || item.NoRestoration != null
                         || item.Parameters.Any()
                         || item.IsNegative != null)
                 || item.EndTurn != null
                      && (item.Delta != null
                         || item.ParameterForDelta != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.Invulnerability != null
                         || item.NoRestoration != null
                         || item.Parameters.Any()
                         || item.IsNegative != null)
                 || item.Invulnerability != null
                      && (item.Delta != null
                         || item.ParameterForDelta != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.NoRestoration != null
                         || item.Parameters.Any()
                         || item.IsNegative != null)
                 || item.NoRestoration != null
                      && (item.Delta != null
                         || item.ParameterForDelta != null
                         || item.Duration != null
                         || item.UseCount != null
                         || item.Parameters.Any()
                         || item.IsNegative != null)
                 || (item.Delta == null
                         && item.ParameterForDelta == null
                         && item.Ability == null
                         && item.Duration == null
                         && item.UseCount == null
                         && item.Action == null
                         && item.EndTurn == null
                         && item.Equipment == null
                         && item.Invulnerability == null
                         && item.NoRestoration == null
                         && item.RuneCard == null)
                 )
                {
                    errors.Add($@"{parentKind} entry '{parentName}' definies invalid effect. Only one of following sets of properties can be set:
({nameof(EffectSettings.Delta)} or {nameof(EffectSettings.ParameterForDelta)}) and {nameof(EffectSettings.Statistic)} and optional {nameof(EffectSettings.IsNegative)} or
({nameof(EffectSettings.Delta)} or {nameof(EffectSettings.ParameterForDelta)}) and {nameof(EffectSettings.Equipment)} and optional {nameof(EffectSettings.IsNegative)} or
({nameof(EffectSettings.Delta)} or {nameof(EffectSettings.ParameterForDelta)}) and {nameof(EffectSettings.RuneCard)} or
{nameof(EffectSettings.Ability)} and optional {nameof(EffectSettings.UseCount)}, {nameof(EffectSettings.Duration)} or
{nameof(EffectSettings.Action)} or
{nameof(EffectSettings.EndTurn)} or
{nameof(EffectSettings.Invulnerability)} or
{nameof(EffectSettings.NoRestoration)} or
");
                }
            }

            return errors;
        }
    }
}
