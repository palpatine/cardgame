﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{


    public class ConditionValueSettingsValidator : ValueSettingsValidatorBase, ISettingsValidator<ConditionValueSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ConditionValueSettingsValidator(ISettingsValidationManager settingsValidationManager)
            : base(settingsValidationManager) => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
           GameSettings gameSettings,
          IEnumerable<(string parentName, ConditionValueSettings item)> items, string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var conditions = items
                .Where(x => x.item.Group != null)
                .SelectMany(x => x.item.Group.Conditions.Select(z => (x.parentName, z)))
                .ToArray();
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, conditions, parentKind));

            errors.AddRange(base.Validate(
                gameSettings,
                items.Select(x => (x.parentName, (ValueSettings)x.item)),
                parentKind));

            return errors;
        }

        protected override void ValidatePropertyAssignmentSets(
            IEnumerable<(string parentName, ValueSettings item)> items,
            string parentKind,
            List<string> errors)
        {
            foreach (var (parentName, conditionValue) in items.Select(x => (x.parentName, (ConditionValueSettings)x.item)))
            {
                if (conditionValue.Value != null && (
                                conditionValue.Group != null
                                || conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Source != null
                                || conditionValue.Equipment != null
                                || conditionValue.Statistic != null
                                || conditionValue.Selected != null)
                    || (conditionValue.Source != null) && (
                                conditionValue.Group != null
                                || conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Selected != null)
                    || (conditionValue.Statistic != null) && (
                                conditionValue.Group != null
                                || conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || conditionValue.Source == null)
                    || conditionValue.Operation != null && (
                                conditionValue.Group != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || !conditionValue.Values.Any())
                    || conditionValue.Values.Any() && (
                                conditionValue.Group != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || conditionValue.Operation == null)
                    || conditionValue.Group != null && (
                                conditionValue.PreviousAction != null
                                || conditionValue.Values.Any()
                                || conditionValue.Operation != null
                                || conditionValue.Equipment != null
                                || conditionValue.StatisticsChange != null
                                || conditionValue.Selected != null)
                    || conditionValue.StatisticsChange != null && (
                                conditionValue.PreviousAction != null
                                || conditionValue.Equipment != null
                                || conditionValue.Selected != null
                                || conditionValue.Source == null)
                    || conditionValue.Equipment != null && (
                                conditionValue.PreviousAction != null
                                || conditionValue.Selected != null
                                || conditionValue.Source == null)
                    || conditionValue.Selected != null && (
                                conditionValue.PreviousAction != null)
                    || (conditionValue.Value == null
                                && conditionValue.Group == null
                                && !conditionValue.Values.Any()
                                && conditionValue.Operation == null
                                && conditionValue.StatisticsChange == null
                                && conditionValue.PreviousAction == null
                                && conditionValue.Source == null
                                && conditionValue.Equipment == null
                                && conditionValue.Statistic == null
                                && conditionValue.Selected == null))
                {
                    errors.Add($@"{parentKind} entry '{parentName}' definies invalid value. Only one of following sets of properties can be set:
{nameof(ConditionValueSettings.Value)} or
{nameof(ConditionValueSettings.Source)} or
{nameof(ConditionValueSettings.Source)} and {nameof(ConditionValueSettings.Equipment)} or
{nameof(ConditionValueSettings.Source)} and {nameof(ConditionValueSettings.Statistic)} or
{nameof(ConditionValueSettings.Operation)} and {nameof(ConditionValueSettings.Values)} or
{nameof(ConditionValueSettings.Group)} or
{nameof(ConditionValueSettings.StatisticsChange)} or
{nameof(ConditionValueSettings.PreviousAction)}");
                }
            }
        }
    }
}