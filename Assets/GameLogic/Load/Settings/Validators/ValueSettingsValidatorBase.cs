﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public abstract class ValueSettingsValidatorBase
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ValueSettingsValidatorBase(ISettingsValidationManager settingsValidationManager) => _settingsValidationManager = settingsValidationManager;


        public IEnumerable<string> Validate(
           GameSettings gameSettings,
           IEnumerable<(string parentName, ValueSettings item)> items, string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var sources = items
                .Where(x => x.item.Source != null)
                .Select(x => (x.parentName, x.item.Source));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, sources, parentKind));

            var statistics = items
                .Where(x => x.item.Statistic != null)
                .Select(x => (x.parentName, x.item.Statistic));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, statistics, parentKind));

            var statisticsChange = items
                .Where(x => x.item.StatisticsChange != null)
                .Select(x => (x.parentName, x.item.StatisticsChange));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, statisticsChange, parentKind));

            var previousAction = items
                .Where(x => x.item.PreviousAction != null)
                .Select(x => (x.parentName, x.item.PreviousAction));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, previousAction, parentKind));

            var equipment = items
                .Where(x => x.item.Equipment != null)
                .Select(x => (x.parentName, x.item.Equipment));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, equipment, parentKind));

            var innerValues = items
                .SelectMany(x => x.item.Values.Select(y => (x.parentName, y)))
                .ToArray();

            if (innerValues.Any())
            {
                errors.AddRange(_settingsValidationManager.Validate(gameSettings, innerValues, parentKind));
            }

            var operations = LoaderHelper.GetAllValues(typeof(Constants.ValuesOperations));
            foreach (var item in items.Where(x => x.item.Operation != null && !operations.Contains(x.item.Operation)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' in value settings refers to operation '{item.item.Operation}' that is not defined.");
            }

            ValidatePropertyAssignmentSets(items, parentKind, errors);

            foreach (var (parentName, item) in items.Where(x => x.item.Selected != null))
            {
                if (!_settingsValidationManager.VerifyParameter(item.Selected))
                {
                    errors.Add($"{parentKind} entry '{parentName}' in value settings refers to selector '{item.Selected}' that is not defined or not unique.");
                }
            }

            return errors;
        }

        protected abstract void ValidatePropertyAssignmentSets(IEnumerable<(string parentName, ValueSettings item)> items, string parentKind, List<string> errors);
    }
}