﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class PlatformCardSettingsValidator : IRootSettingsValidator<PlatformCardSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public PlatformCardSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();
            foreach (var item in gameSettings.PlatformDeck
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Platform card entry must have a name.");
            }

            foreach (var item in gameSettings.PlatformDeck
                        .Where(x => !string.IsNullOrEmpty(x.Name))
                        .GroupBy(x => x.Name)
                        .Where(x => x.Count() > 1))
            {
                errors.Add($"Platform card entry must have unique name '{item.Key}'.");
            }

            foreach (var item in gameSettings.PlatformDeck.Where(x => x.CountInDeck < 0))
            {
                errors.Add($"Platform card '{item.Name}' has negative count in deck.");
            }

            foreach (var item in gameSettings.PlatformDeck.Where(x => x.Feature != null && !gameSettings.Features.Any(y => y.Name != x.Feature)))
            {
                errors.Add($"Platform card {item.Name} refers to feature {item.Feature} that is not defined.");
            }

            var explorations = gameSettings.PlatformDeck
                .Where(x => x.Exploration != null)
                .Select(x => (x.Name, x.Exploration));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, explorations, "Platform card"));

            var resources = gameSettings.PlatformDeck
                .SelectMany(x => x.Resources.Select(y => (x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, resources, "Platform card"));

            var entranceEffects = gameSettings.PlatformDeck
                .SelectMany(x => x.EntranceEffects.Select(y => (x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, entranceEffects, "Platform card"));

            return errors;
        }
    }
}