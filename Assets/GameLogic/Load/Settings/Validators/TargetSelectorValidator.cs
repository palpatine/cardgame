﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class TargetSelectorValidator : ISettingsValidator<TargetSelector>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public TargetSelectorValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettins,
            IEnumerable<(string parentName, TargetSelector item)> items,
            string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var names = LoaderHelper.GetAllValues(typeof(Constants.ValueSources));
            foreach (var (parentName, source) in items)
            {
                if (!names.Contains(source.Name))
                {
                    errors.Add($"{parentKind} entry '{parentName}' in target selector refers to '{source.Name}' which is not defineid.");
                }
            }
            return errors;
        }

    }
}