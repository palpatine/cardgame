﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class RangeSettingsValidator : ISettingsValidator<RangeSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public RangeSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, RangeSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();

            var ranges = LoaderHelper.GetAllValues(typeof(Constants.Range));

            foreach (var (parentName, item) in items)
            {
                if (!ranges.Contains(item.Name))
                {
                    errors.Add($"{parentKind} entry '{parentName}' in range settings refers to range '{item.Name}' which is not defineid.");
                }

                if ((item.Name == Constants.Range.Short)
                    && (item.Value < 1 || item.Value > 5))
                {
                    errors.Add($"{parentKind} entry '{parentName}' in range settings refers to range '{item.Name}' with value out of allowed range <1,5> current {item.Value}.");
                }

                if (item.Name == Constants.Range.Medium
                    && (item.Value < 1 || item.Value > 6))
                {
                    errors.Add($"{parentKind} entry '{parentName}' in range settings refers to range '{item.Name}' with value out of allowed range <1,6> current {item.Value}.");
                }

                if (item.Name == Constants.Range.Far && item.Value < 3)
                {
                    errors.Add($"{parentKind} entry '{parentName}' in range settings refers to range '{item.Name}' with value out of allowed range <3,> current {item.Value}.");
                }
            }

            return errors;
        }
    }
}