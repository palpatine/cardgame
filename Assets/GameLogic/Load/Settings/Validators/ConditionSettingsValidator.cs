﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class ConditionSettingsValidator : ISettingsValidator<ConditionSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ConditionSettingsValidator(ISettingsValidationManager settingsValidationManager) => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
           GameSettings gameSettings,
           IEnumerable<(string parentName, ConditionSettings item)> conditions,
           string parentKind)
        {
            var errors = new List<string>();
            if (!conditions.Any())
            {
                return errors;
            }

            foreach (var item in conditions.Where(x => string.IsNullOrWhiteSpace(x.item.Compare) || x.item.LeftValue == null || x.item.RightValue == null))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' condition comparision does not provide some of required properties: {nameof(item.item.LeftValue)}, {nameof(item.item.Compare)} or {nameof(item.item.RightValue)}.");
            }

            var compareKinds = LoaderHelper.GetAllValues(typeof(Constants.Compare));

            foreach (var item in conditions.Where(x => !string.IsNullOrWhiteSpace(x.item.Compare) && !compareKinds.Contains(x.item.Compare)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' condition comparision kind '{item.item.Compare}' is not supported.");
            }

            var values = conditions.SelectMany(x => new[] { (x.parentName, x.item.LeftValue), (x.parentName, x.item.RightValue) })
                .Where(x => x.Item2 != null)
                .ToArray();
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, values, parentKind));

            return errors;
        }

    }
}