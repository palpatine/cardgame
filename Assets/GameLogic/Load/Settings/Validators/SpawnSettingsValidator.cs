﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class SpawnSettingsValidator : ISettingsValidator<SpawnSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public SpawnSettingsValidator(ISettingsValidationManager settingsValidationManager) => _settingsValidationManager = settingsValidationManager;
        public IEnumerable<string> Validate(
                      GameSettings gameSettings,
                      IEnumerable<(string parentName, SpawnSettings item)> items,
                      string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            var featureSelectors = items.Where(x => x.item.FeatureSelector != null)
                .Select(x => (x.parentName, x.item.FeatureSelector))
                .ToArray();

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, featureSelectors, parentKind));

            var platformSelectors = items.Where(x => x.item.PlatformSelector != null)
                .Select(x => (x.parentName, x.item.PlatformSelector));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, platformSelectors, parentKind));

            var statistics = items.Where(x => x.item.Statistic != null)
                .Select(x => (x.parentName, x.item.Statistic));

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, statistics, parentKind));

            foreach (var item in items.Where(x => !(
                   (x.item.Delta != null && x.item.Statistic != null || x.item.CountDelta != null)
                   && (x.item.PlatformSelector != null && x.item.FeatureSelector == null
                        || x.item.PlatformSelector == null && x.item.FeatureSelector != null)
                 )))
            {
                errors.Add($@"{parentKind} entry '{item.parentName}' in spawn defines invalid list of properties. Allowed sets:
{nameof(SpawnSettings.CountDelta)} and ({nameof(SpawnSettings.PlatformSelector)} or {nameof(SpawnSettings.FeatureSelector)}) or
{nameof(SpawnSettings.Delta)} and {nameof(SpawnSettings.Statistic)} and ({nameof(SpawnSettings.PlatformSelector)} or {nameof(SpawnSettings.FeatureSelector)})");
            }
            return errors;
        }
    }
}