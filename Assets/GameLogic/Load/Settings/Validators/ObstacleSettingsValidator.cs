﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public partial class ObstacleSettingsValidator : IRootSettingsValidator<ObstacleSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ObstacleSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();
            foreach (var item in gameSettings.Obstacles
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Obstacle entry must have a name.");
            }

            foreach (var item in gameSettings.Obstacles
                       .Where(x => !string.IsNullOrEmpty(x.Name))
                       .GroupBy(x => x.Name)
                       .Where(x => x.Count() > 1))
            {
                errors.Add($"Obstacle card entry must have unique name '{item.Key}'.");
            }

            foreach (var item in gameSettings.Obstacles
                        .Where(x => !string.IsNullOrWhiteSpace(x.Remedies?.Obstacle) && x.Remedies?.Obstacle == x.Name))
            {
                errors.Add($"Obstacle entry '{item.Name}' refers to itself in remedies settings.");
            }

            var remedies = gameSettings.Obstacles.Where(x => x.Remedies != null)
                .Select(x => (x.Name, x.Remedies))
                .ToArray();

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, remedies, "Obstacle"));

            var effects = gameSettings.Obstacles.Where(x => x.Effects != null)
               .SelectMany(x => x.Effects.Select(y => (x.Name, y)))
               .ToArray();

            errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, "Obstacle"));

            return errors;
        }
    }
}