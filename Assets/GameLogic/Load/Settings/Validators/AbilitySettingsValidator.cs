﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class AbilitySettingsValidator : IRootSettingsValidator<AbilitySettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public AbilitySettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();
            foreach (var item in gameSettings.Abilities
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Ability entry must have a name.");
            }

            foreach (var item in gameSettings.Abilities
                   .Where(x => !string.IsNullOrEmpty(x.Name))
                   .GroupBy(x => x.Name)
                   .Where(x => x.Count() > 1))
            {
                errors.Add($"Ability entry must have unique name '{item.Key}'.");
            }

            foreach (var ability in gameSettings.Abilities)
            {
                using (_settingsValidationManager.RegisterDeclaredParameters(ability, ability.Parameters))
                {
                    var effects = ability.Effects.Select(y => (Name: ability.Name, y));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, "Ability"));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, ability.Name, ability.Condition, "Ability"));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, ability.Name, ability.Trigger, "Ability"));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, ability.Name, ability.ActionOverride?.MinRange, "Ability action override min range"));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, ability.Name, ability.ActionOverride?.Range, "Ability action override max range"));

                    foreach (var item in ability.Parameters.Where(z => _settingsValidationManager.GetParameterUsage(ability, z) == 0))
                    {
                        errors.Add($"Ability entry '{ability.Name}' defines parameter '{item}' that is not used.");
                    }
                }
            }

            return errors;
        }
    }
}