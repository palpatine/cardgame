﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class FeatureSettingsValidator : IRootSettingsValidator<FeaturesSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public FeatureSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(GameSettings gameSettings)
        {
            var errors = new List<string>();
            foreach (var item in gameSettings.Features
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Feature entry must have a name.");
            }

            foreach (var item in gameSettings.Features
                        .Where(x => !string.IsNullOrEmpty(x.Name))
                        .GroupBy(x => x.Name)
                        .Where(x => x.Count() > 1))
            {
                errors.Add($"Feature entry must have unique name '{item.Key}'.");
            }

            foreach (var item in gameSettings.Features
                        .Where(x => gameSettings.PlatformDeck.All(z => !z.Kinds.Contains(x.TriggeringPlatformKind))))
            {
                errors.Add($"Feature entry '{item.Name}' refers platform kind '{item.TriggeringPlatformKind}' that is not defined.");
            }

            var resources = gameSettings.Features
                          .Where(x=>x.Resource != null)
                          .Select(x => (x.Name, x.Resource));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, resources, "Feature"));

            return errors;
        }
    }
}