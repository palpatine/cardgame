﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class PlatformSelectorValidator : ISettingsValidator<PlatformSelector>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public PlatformSelectorValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
        GameSettings data,
        IEnumerable<(string parentName, PlatformSelector item)> items,
        string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            foreach (var (parentName, selector) in items.Where(
                             x => x.item.Kind != null
                                 && data.PlatformDeck.All(z => !z.Kinds.Contains(x.item.Kind))))
            {
                errors.Add($"{parentKind} entry '{parentName}' refers to platform kind '{selector.Kind}' that is not defined.");
            }

            foreach (var (parentName, selector) in items.Where(
                             x => x.item.Name != null
                                 && data.PlatformDeck.All(z => z.Name != x.item.Name)))
            {
                errors.Add($"{parentKind} entry '{parentName}' refers to platform by name '{selector.Name}' that is not defined.");
            }

            foreach (var (parentName, selector) in items.Where(
                             x => x.item.Obstacle != null
                                 && data.Obstacles.All(z => z.Name != x.item.Obstacle)))
            {
                errors.Add($"{parentKind} entry '{parentName}' refers to obstacle by name '{selector.Obstacle}' that is not defined.");
            }

            return errors;
        }

    }
}