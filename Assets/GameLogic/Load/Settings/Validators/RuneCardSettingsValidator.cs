﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class RuneCardSettingsValidator : IRootSettingsValidator<RuneCardSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public RuneCardSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();

            foreach (var item in gameSettings.Runes
                .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Rune card entry must have a name.");
            }

            foreach (var item in gameSettings.Runes
                .Where(x => !string.IsNullOrEmpty(x.Name))
                .GroupBy(x => x.Name)
                .Where(x => x.Count() > 1))
            {
                errors.Add($"Rune card entry must have unique name '{item.Key}'.");
            }

            var effects = gameSettings.Runes
                           .SelectMany(x => x.Effects.Select(y => (Name: x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, "Rune card"));

            foreach (var item in gameSettings.Runes.Where(x => !x.Decks.Any()))
            {
                errors.Add($"Rune card entry must define at least one deck.");
            }

            foreach (var item in gameSettings.Runes.Where(x => x.Cost < 0))
            {
                errors.Add($"Rune card '{item.Name}' provides negative cost.");
            }

            foreach (var item in gameSettings.Runes.Where(x => x.CountInDeck < 0))
            {
                errors.Add($"Rune card '{item.Name}' provides negative cost.");
            }

            foreach (var item in gameSettings.Runes.Where(x => x.Value <= 0))
            {
                errors.Add($"Rune card '{item.Name}' provides nonpositive value.");
            }

            return errors;
        }
    }
}