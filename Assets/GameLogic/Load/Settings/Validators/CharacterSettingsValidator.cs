﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class CharacterSettingsValidator : IRootSettingsValidator<CharacterSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public CharacterSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();

            foreach (var (name, ability) in gameSettings.Character.Abilities.Select(
                         x => (x, gameSettings.Abilities.SingleOrDefault(z => z.Name == x))))
            {
                if (ability == null)
                {
                    errors.Add($"Character settings refers to ability '{name}' that is not defined.");
                }
                else if(ability.Parameters.Any())
                {
                    errors.Add($"Character settings refers to ability '{name}' that defines parameters.");
                }
            }

            foreach (var item in gameSettings.Character.Equipment.Where(
                            x => !gameSettings.Items.Any(z => z.Name == x)))
            {
                errors.Add($"Character settings refers to item '{item}' that is not defined.");
            }

            return errors;
        }
    }
}