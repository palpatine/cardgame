﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class ItemSettingsValidator : IRootSettingsValidator<ItemSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ItemSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings)
        {
            var errors = new List<string>();
            var allKinds = LoaderHelper.GetAllValues(typeof(Constants.ItemKinds));
            foreach (var item in gameSettings.EquipableItemKinds
                .Where(x => allKinds.All(z => z != x)))
            {
                errors.Add($"Equipable item kind {item} is not defined.");
            }

            foreach (var item in gameSettings.Items
                        .Where(x => string.IsNullOrEmpty(x.Name)))
            {
                errors.Add($"Item entry must have a name.");
            }

            foreach (var item in gameSettings.Items
                       .Where(x => !string.IsNullOrEmpty(x.Name))
                       .GroupBy(x => x.Name)
                       .Where(x => x.Count() > 1))
            {
                errors.Add($"Item entry must have unique name '{item.Key}'.");
            }

            foreach (var item in gameSettings.Items)
            {
                foreach (var kind in item.Kinds)
                {
                    if (!allKinds.Contains(kind))
                    {
                        errors.Add($"Item entry '{item.Name}' refers to undefinded kind of item '{kind}'.");
                    }
                }
            }

            foreach (var item in gameSettings.Items.Where(x => !x.Kinds.Any()))
            {
                errors.Add($"Item '{item.Name}' does not specify any kind.");
            }

            foreach (var item in gameSettings.Items.Where(x => x.Value <= 0))
            {
                errors.Add($"Item '{item.Name}' has nonpositive runic value.");
            }

            var effects = gameSettings.Items
                           .SelectMany(x => x.Effects.Select(y => (Name: x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, "Item"));

            var actions = gameSettings.Items
                           .SelectMany(x => x.Actions.Select(y => (Name: x.Name, y)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, actions, "Item"));

            return errors;
        }
    }
}