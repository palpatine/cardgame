﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{

    public class SelectorSettingsValidator : ISettingsValidator<SelectorSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public SelectorSettingsValidator(ISettingsValidationManager settingsValidationManager)
            => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, SelectorSettings item)> items,
            string parentKind)
        {
            var errors = new List<string>();

            foreach (var item in items
                       .Where(x => string.IsNullOrEmpty(x.item.Name)))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' all selectors must have a name.");
            }

            foreach (var item in items
                   .Where(x => !string.IsNullOrEmpty(x.item.Name))
                   .GroupBy(x => new { x.parentName, x.item.Name })
                   .Where(x => x.Count() > 1))
            {
                errors.Add($"{parentKind} entry '{item.Key.parentName}' in selector must have unique name '{item.Key.Name}'.");
            }

            foreach (var item in items.Where(x => x.item.Increment == null || x.item.Min == null || x.item.Max == null))
            {
                errors.Add($"{parentKind} entry '{item.parentName}' in selector  must have must define min max and increment values.");
            }

            var minValues = items.Where(x => x.item.Min != null).Select(x => (x.parentName, x.item.Min));
            _settingsValidationManager.Validate(gameSettings, minValues, parentKind + " min value");


            var maxValues = items.Where(x => x.item.Max != null).Select(x => (x.parentName, x.item.Max));
            _settingsValidationManager.Validate(gameSettings, maxValues, parentKind + " max value");

            var incrementValues = items.Where(x => x.item.Increment != null).Select(x => (x.parentName, x.item.Increment));
            _settingsValidationManager.Validate(gameSettings, incrementValues, parentKind + " increment value");

            return errors;
        }
    }
}