﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings.Validators
{
    public class ActionSettingsValidator : ISettingsValidator<ActionSettings>
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public ActionSettingsValidator(ISettingsValidationManager settingsValidationManager) => _settingsValidationManager = settingsValidationManager;

        public IEnumerable<string> Validate(
           GameSettings gameSettings,
           IEnumerable<(string parentName, ActionSettings item)> items,
           string parentKind)
        {
            var errors = new List<string>();
            if (!items.Any())
            {
                return errors;
            }

            foreach (var (parentName, _) in items.Where(x => !x.item.Effects.Any()))
            {
                errors.Add($"{parentKind} entry '{parentName}' defines action without any effect.");
            }

            foreach (var (parentName, action) in items.Where(x => x.item.Ammo != null))
            {
                var ammoItem = gameSettings.Items.SingleOrDefault(x => x.Name == action.Ammo);
                if (ammoItem == null || !ammoItem.Kinds.Contains(Constants.ItemKinds.Ammo))
                {
                    errors.Add($"{parentKind} entry '{parentName}' defines action witch ammunition required '{action.Ammo}' that is not definied or is not {Constants.ItemKinds.Ammo}.");
                }
            }

            foreach (var (parentName, _) in items.Where(x => x.item.Range == null))
            {
                errors.Add($"{parentKind} entry '{parentName}' defines action with undefined range.");
            }

            foreach (var (parentName, _) in items
                            .Where(x =>
                                   x.item.Range != null
                                && x.item.MinRange != null
                                && x.item.Range.CompareTo(x.item.MinRange) < 0))
            {
                errors.Add($"{parentKind} entry '{parentName}' defines action with minimal range greater than maximal range.");
            }

            foreach (var (parentName, _) in items.Where(x => !x.item.Kinds.Any()))
            {
                errors.Add($"{parentKind} entry '{parentName}' defines action without kind.");
            }

            var selectors = items.SelectMany(x => x.item.Selectors.Select(z => (x.parentName, z)));
            errors.AddRange(_settingsValidationManager.Validate(gameSettings, selectors, parentKind));

            foreach (var (parentName, action) in items)
            {
                using (_settingsValidationManager.RegisterDeclaredParameters(this, action.Selectors.Select(y => y.Name)))
                {
                    var effects = action.Effects.Select(y => (parentName, y));

                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, effects, parentKind));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, parentName, action.Condition, parentKind));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, parentName, action.Import, parentKind));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, parentName, action.MinRange, parentKind));
                    errors.AddRange(_settingsValidationManager.Validate(gameSettings, parentName, action.Range, parentKind));

                    foreach (var item in action.Selectors.Where(z => _settingsValidationManager.GetParameterUsage(this, z.Name) == 0))
                    {
                        errors.Add($"{parentKind} entry '{parentName}' defines action with selector '{item.Name}' that is not used.");
                    }
                }
            }

            return errors;
        }
    }
}