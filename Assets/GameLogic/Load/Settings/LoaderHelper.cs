﻿using System;
using System.Linq;
using System.Reflection;

namespace Assets.GameLogic.Load.Settings
{
    public static class LoaderHelper
    {
        public static string[] GetAllValues(Type type) =>
                    type
                        .GetFields(BindingFlags.Static | BindingFlags.Public)
                        .Select(x => (string)x.GetValue(null))
                        .ToArray();
    }
}