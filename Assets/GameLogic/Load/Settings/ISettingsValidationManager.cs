﻿using System;
using System.Collections.Generic;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings
{
    public interface ISettingsValidationManager
    {
        IEnumerable<string> Validate<T>(
            GameSettings gameSettings,
            IEnumerable<(string parentName, T item)> items,
            string parentKind);

        IEnumerable<string> Validate<T>(
            GameSettings gameSettings,
            string parentName,
            T item,
            string parentKind);

        IEnumerable<string> Validate<T>(
            GameSettings gameSettings);

        IDisposable RegisterDeclaredParameters(
            object parent, 
            IEnumerable<string> parameters);

        int GetParameterUsage(object parent, string name);

        bool VerifyParameter(string selected);
    }
}