﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools;

namespace Assets.GameLogic.Load.Settings
{
    public class SettingsValidationManager : ISettingsValidationManager
    {
        private Dictionary<Type, ISettingsValidator> _validators;
        private readonly Func<IEnumerable<ISettingsValidator>> _validatorsFactory;
        private readonly Stack<(object Parent, Dictionary<string, int> Usages)> _selectors 
            = new Stack<(object Parent, Dictionary<string, int> Usages)>();

        public SettingsValidationManager(Func<IEnumerable<ISettingsValidator>> validatorsFactory)
        {

            _validatorsFactory = validatorsFactory;
        }

        public Dictionary<Type, ISettingsValidator> Validators =>
            _validators
            ?? (
             _validators = _validatorsFactory().ToDictionary(
                x => x.GetType().GetInterfaces()
                    .Single(y => y.IsGenericType 
                        && (typeof(ISettingsValidator<>) == y.GetGenericTypeDefinition() 
                                || typeof(IRootSettingsValidator<>) == y.GetGenericTypeDefinition()))
                    .GetGenericArguments()[0],
                x => x));

        public IEnumerable<string> Validate<T>(
            GameSettings gameSettings,
            IEnumerable<(string parentName, T item)> items,
            string parentKind)
        {
            if (!Validators.ContainsKey(typeof(T)))
            {
                throw new InvalidOperationException($"Missing settings validator for {typeof(T).Name}");
            }

            var validator = (ISettingsValidator<T>)Validators[typeof(T)];
            return validator.Validate(gameSettings, items, parentKind);
        }

        public IEnumerable<string> Validate<T>(
            GameSettings gameSettings)
        {
            if (!Validators.ContainsKey(typeof(T)))
            {
                throw new InvalidOperationException($"Missing root settings validator for {typeof(T).Name}");
            }

            var validator = (IRootSettingsValidator<T>)Validators[typeof(T)];
            return validator.Validate(gameSettings);
        }

        public IEnumerable<string> Validate<T>(
            GameSettings gameSettings,
            string parentName,
            T item,
            string parentKind)
        {
            if (item == null)
            {
                return Enumerable.Empty<string>();
            }

            return Validate(gameSettings, new[] { (parentName, item) }, parentKind);
        }

        public int GetParameterUsage(object parent, string name)
        {
            if (!_selectors.Any() || _selectors.Peek().Parent != parent)
            {
                throw new InvalidOperationException("Selectors context broken");
            }

            return _selectors.Peek().Usages[name];
        }

        public IDisposable RegisterDeclaredParameters(object parent, IEnumerable<string> selectors)
        {
            _selectors.Push((parent, selectors.ToDictionary(x => x, x => 0)));
            return new Disposer(() =>
            {
                if (!_selectors.Any() || _selectors.Peek().Parent != parent)
                {
                    throw new InvalidOperationException("Selectors context broken");
                }

                _selectors.Pop();
            });
        }

        public bool VerifyParameter(string selected)
        {
            var count = 0;
            foreach (var (_, usage) in _selectors)
            {
                if (usage.ContainsKey(selected))
                {
                    count++;
                    usage[selected]++;
                }
            }

            return count == 1;
        }

    }
}