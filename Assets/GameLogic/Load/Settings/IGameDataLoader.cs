﻿using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings
{
    public interface IGameDataLoader
    {
        GameSettings LoadSettings();
    }
}