﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Model.Trace;
using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Assets.GameLogic.Load.Settings
{

    internal class GameDataLoader : IGameDataLoader
    {
        private readonly ISettingsValidationManager _settingsValidationManager;

        public GameDataLoader(ISettingsValidationManager settingsValidationManager) => _settingsValidationManager = settingsValidationManager;

        private const string Source = @"Assets\StreamingAssets\game.yml";


        public GameSettings LoadSettings()
        {
            var yaml = new YamlStream();
            using (var file = File.OpenText(Source))
            {
                var deserializer = new DeserializerBuilder()
                    .WithNamingConvention(new CamelCaseNamingConvention())
                    .Build();
                var data = deserializer.Deserialize<GameSettings>(file);
                Validate(data);

                return data;
            }
        }
        
       
        private IEnumerable<string> ValidateStartingPlatforms(GameSettings data)
        {
            var errors = new List<string>();
            foreach (var item in data.StartingPlatforms.Where(
                            x => data.PlatformDeck.All(z => z.Name != x)))
            {
                errors.Add($"Starting platform entry {item} refers card that is not defined.");
            }

            return errors;
        }

        private void Validate(GameSettings data)
        {
            var errors =
                  ValidateStartingPlatforms(data)
                  .Concat(_settingsValidationManager.Validate<CharacterSettings>(data))
                  .Concat(_settingsValidationManager.Validate<ItemSettings>(data))
                  .Concat(_settingsValidationManager.Validate<AbilitySettings>(data))
                  .Concat(_settingsValidationManager.Validate<PlatformCardSettings>(data))
                  .Concat(_settingsValidationManager.Validate<RuneCardSettings>(data))
                  .Concat(_settingsValidationManager.Validate<DangerSettings>(data))
                  .Concat(_settingsValidationManager.Validate<FoeSettings>(data))
                  .Concat(_settingsValidationManager.Validate<ObstacleSettings>(data))
                  .Concat(_settingsValidationManager.Validate<FeaturesSettings>(data));

            if (errors.Any())
            {
                throw new InvalidOperationException(string.Join(Environment.NewLine, errors));
            }
        }
    }
}