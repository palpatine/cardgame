﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Load.Settings
{
    public interface ISettingsValidator<T> : ISettingsValidator
    {
        IEnumerable<string> Validate(
            GameSettings gameSettings,
            IEnumerable<(string parentName, T item)> items,
            string parentKind);
    }

    public interface IRootSettingsValidator<T> : ISettingsValidator
    {
        IEnumerable<string> Validate(
            GameSettings gameSettings);
    }

}