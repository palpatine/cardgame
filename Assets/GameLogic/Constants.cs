﻿using System.Collections.Generic;

namespace Assets.GameLogic
{
    public static partial class Constants
    {
        public const string GameManager = "GameManager";

        public static class Actions
        {
            public const string EndTurn = "end turn";
            public const string Explore = "explore";
            public const string Harvest = "harvest";
            public const string MoveRight = "move right";
            public const string RevealLeftAction = "reveal left";
            public const string RevealRightAction = "reveal right";
            public const string Scout = "scout";
        }

        public static class Compare
        {
            public const string Contains = "co";
            public const string Equal = "eq";
            public const string GreaterThen = "gt";
            public const string GreaterThenOrEqual = "ge";
            public const string LowerThen = "lt";
            public const string LowerThenOrEqual = "le";
            public const string NotContains = "nc";
            public const string NotEqual = "ne";
        }

        public static class Decks
        {
            public const string Magic = "magic";
            public const string Might = "might";
            public const string Wild = "wild";
        }

        public static class Effects
        {
            public const string SpawnFoe = "spawn foe";
            public const string ReplacePlatform = "replace platform";
        }

        public static class EquipmentSelectorProperties
        {
            public const string Effects = "Effects";
            public const string FirstAction = "First action";
            public const string Item = "Item";
            public const string Kinds = "Kinds";
            public const string Name = "Name";
        }

        public static class Features
        {
            public const string Cave = "cave";
            public const string OreNugget = "ore nugget";
            public const string Tree = "tree";
        }

        public static class FoeAttitudes
        {
            public const string Aggressive = "aggressive";
            public const string Defensive = "defensive";
            public const string Tactical = "tactical";
        }

        public static class ItemKinds
        {
            public const string Ammo = "ammo";
            public const string BodyDefense = "body defense";
            public const string Collectable = "collectable";
            public const string Consumable = "consumable";
            public const string Ingridient = "ingridient";
            public const string Magic = "magic";
            public const string MagicItem = "magic item";
            public const string Melee = "melee";
            public const string Might = "might";
            public const string Range = "range";
            public const string Shield = "shield";
            public const string Thrown = "thrown";
            public const string Utility = "utility";
            public const string Weapon = "weapon";
            public const string Wild = "wild";
        }

        public static class Obstacles
        {
            public const string Boulder = "boulder";
        }

        public static class Range
        {
            public const string Self = "self";
            public const string Short = "short";
            public const string Medium = "medium";
            public const string Far = "far";
            public const string Foe = "foe";
        }

        public static class Statisitcs
        {
            public const string Attack = "attack";
            public const string Block = "block";
            public const string Damage = "damage";
            public const string Defense = "defense";
            public const string Energy = "energy";
            public const string Interact = "interact";
            public const string Life = "life";
            public const string MagicDamage = "magicDamage";
            public const string Mana = "mana";
            public const string Movement = "movement";
            public const string UseItem = "use item";
        }

        public static class StatisitcValues
        {
            public const string Current = "current";
            public const string Maximum = "maximum";
            public const string Regeneration = "regeneration";
        }


        public static IEnumerable<string> RegenerableStatistics => new[]
        {
            Constants.Statisitcs.Life,
            Constants.Statisitcs.Energy,
            Constants.Statisitcs.Mana
        };

        public static IEnumerable<string> ReplenishableStatistics => new[]
        {
            Constants.Statisitcs.Movement,
            Constants.Statisitcs.Block,
            Constants.Statisitcs.Mana,
            Constants.Statisitcs.Attack
        };


        public static class TraceActions
        {
            public const string Acted = "acted";
            public const string CardDiscarded = "card discarded";
            public const string CardDrawn = "card drawn";
            public const string DangerRevealed = "danger revealed";
            public const string ExplorationPerformed = "explored";
            public const string HarvestPerformed = "harvest performed";
            public const string Moved = "moved";
            public const string PlatformExplored = "platform explored";
            public const string PlatformFeatureRevealed = "platform feature reveled";
            public const string PlatformRevealedToLeft = "platform revealed to left";
            public const string PlatformRevealedToRight = "platform revealed to right";
            public const string Spawed = "spawed";
            public const string StatisitcsRecovered = "statistics recovered";
            public const string StatisticsSubtracted = "statisitcs subtracted";
            public const string UnitDied = "unit died";
            public const string UnitsOrderSelected = "units order selected";
        }

        public static class Triggers
        {
            public const string BeforeEffectApplied = "before effect applied";
            public const string BeforeStartTurn = "before start turn";
            public const string BlockAction = "block action";
            public const string ConsidersAction = "considers action";
            public const string ConsumableGained = "consumable gained";
            public const string ExecutesAction = "executes action";
            public const string RuneCardDrawn = "rune card drawn";
            public const string StartTurn = "start turn";
            public const string Targeted = "targeted";
        }

     
        public static class ValuesOperations
        {
            public const string Max = "max";
            public const string Min = "min";
            public const string Sum = "sum";
        }

        public static class ValueSources
        {
            public const string ActingUnit = "acting unit";
            public const string CurrentUnit = "current unit";
            public const string Target = "target";
            public const string Targets = "targets";
            public const string UnitByOffset = "unit by offset";
            public const string Characters = "characters";
        }
    }
}