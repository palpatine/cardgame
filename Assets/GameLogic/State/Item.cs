﻿using System;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class Item : Notifier<Item>, ITarget
    {
        public Item(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; }

        public ItemSettings Settings { get; set; }
    }
}