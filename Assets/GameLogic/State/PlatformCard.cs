﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class PlatformCard : Notifier<PlatformCard>, IHarvestable
    {
        private PlatformFeature _features;
        private bool _isExplored;
        private int _level;
        private Obstacle _obstacle;

        public PlatformCard(IUpdateNotifier updateNotifier, PlatformCardSettings settings) : base(updateNotifier)
        {
            Settings = settings;
        }

        public ObservableCollection<PlatformCard> AdjacentPlatformsOnDifferentLevel { get; } = new ObservableCollection<PlatformCard>();

        public IEnumerable<IUnit> AllUnits => UnitsToLeftOfObstacle.Concat(UnitsToRightOfObstacle);

        public bool CanHarvest => Resources.Any();

        public PlatformFeature Features { get => _features; set => ChangeValue(ref _features, value); }

        public bool IsExplored { get => _isExplored; set => ChangeValue(ref _isExplored, value); }

        public ObservableCollection<Item> Items { get; } = new ObservableCollection<Item>();

        public int Level { get => _level; set => ChangeValue(ref _level, value); }

        public PlatformCard Location => this;

        public Obstacle Obstacle { get => _obstacle; set => ChangeValue(ref _obstacle, value); }

        public ObservableCollection<Item> Resources { get; } = new ObservableCollection<Item>();

        public PlatformCardSettings Settings { get; }

        public ObservableCollection<IUnit> UnitsToLeftOfObstacle { get; } = new ObservableCollection<IUnit>();

        public ObservableCollection<IUnit> UnitsToRightOfObstacle { get; } = new ObservableCollection<IUnit>();
    }
}