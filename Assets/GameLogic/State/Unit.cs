﻿using System;
using System.Collections.ObjectModel;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public abstract class Unit<T> : Notifier<T>, IUnit where T : Notifier<T>
    {
        public Unit(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
            Location = new Location(updateNotifier);
            Energy = new RegenerableStatistic(updateNotifier);
            Life = new RegenerableStatistic(updateNotifier);
            Mana = new RegenerableStatistic(updateNotifier);
            Movement = new ReplenishableStatistic(updateNotifier);
            Attack = new ReplenishableStatistic(updateNotifier);
            Block = new ReplenishableStatistic(updateNotifier);
            Defense = new ValueStatistic(updateNotifier);
            Id = Guid.NewGuid();
        }

        public ObservableCollection<Ability> Abilities { get; } = new ObservableCollection<Ability>();

        public ObservableCollection<ActionSettings> Actions { get; } = new ObservableCollection<ActionSettings>();

        public ReplenishableStatistic Attack { get; }

        public ReplenishableStatistic Block { get; }

        public ValueStatistic Defense { get; }

        public RegenerableStatistic Energy { get; }

        public Guid Id { get; set; }

        public RegenerableStatistic Life { get; }

        public Location Location { get; }

        public RegenerableStatistic Mana { get; }

        public ReplenishableStatistic Movement { get; }

        public virtual IStatisticWithCurrentValue GetStatistic(string statistic)
        {
            switch (statistic)
            {
                case Constants.Statisitcs.Attack:
                    return Attack;

                case Constants.Statisitcs.Energy:
                    return Energy;

                case Constants.Statisitcs.Mana:
                    return Mana;

                case Constants.Statisitcs.Movement:
                    return Movement;

                case Constants.Statisitcs.Defense:
                    return Defense;

                case Constants.Statisitcs.Block:
                    return Block;

                case Constants.Statisitcs.Life:
                    return Life;

                default:
                    throw new InvalidOperationException($"Unknown statistic {statistic}");
            }
        }
    }
}