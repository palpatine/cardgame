﻿using System.Diagnostics;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    [DebuggerDisplay("{ToString()}")]
    public class Foe : Unit<Foe>, IFoe
    {
        public Foe(IUpdateNotifier updateNotifier, FoeSettings settings)
            : base(updateNotifier)
        {
            Settings = settings;
        }

        public FoeSettings Settings { get; }

        public ILocation StartingLocation { get; set; }

        public override string ToString() => "Foe:" + Settings.Name;
    }
}