﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GameLogic.State
{
    public class Deck<TCard>
    {
        public Deck(IEnumerable<TCard> cards)
        {
            Cards = new Stack<TCard>(Suffle(cards));
        }

        public Stack<TCard> Cards { get; }

        public List<TCard> Discarded { get; } = new List<TCard>();

        private IEnumerable<TCard> Suffle(IEnumerable<TCard> deck)
        {
            return deck.Select(x => (x, Random.value))
                 .OrderBy(x => x.value)
                 .Select(x => x.x)
                 .ToList();
        }
    }
}