﻿using Assets.GameLogic.Dtos;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class Location : Notifier<Location>, ILocation
    {
        private int _currentLevel;
        private bool _isFacingLeft;
        private bool _isToTheLeftOfObstacle;
        private PlatformCard _platformCard;
        private int _unitId;

        public Location(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public int CurrentLevelId
        {
            get => _currentLevel;
            set => ChangeValue(ref _currentLevel, value);
        }

        public bool IsFacingLeft { get => _isFacingLeft; set => ChangeValue(ref _isFacingLeft, value); }

        public bool IsToTheLeftOfObstacle { get => _isToTheLeftOfObstacle; set => ChangeValue(ref _isToTheLeftOfObstacle, value); }

        public PlatformCard PlatformCard
        {
            get => _platformCard;
            set => ChangeValue(ref _platformCard, value);
        }

        public int UnitId { get => _unitId; set => ChangeValue(ref _unitId, value); }

        public ILocation Clone()
        {
            return new LocationDescriptor
            {
                CurrentLevelId = CurrentLevelId,
                IsFacingLeft = IsFacingLeft,
                IsToTheLeftOfObstacle = IsToTheLeftOfObstacle,
                PlatformCard = PlatformCard,
                UnitId = UnitId,
            };
        }
    }
}