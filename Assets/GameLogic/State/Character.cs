﻿using System.Collections.ObjectModel;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;

namespace Assets.GameLogic.State
{
    public class Character : Unit<Character>
    {
        public Character(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
            Interact = new ReplenishableStatistic(updateNotifier);
            UseItem = new ReplenishableStatistic(updateNotifier);
            Equipment = new ObservableDictionary<Item>(updateNotifier);
            Damage = new ValueStatistic(updateNotifier);
            MagicDamage = new ValueStatistic(updateNotifier);
        }

        public ObservableCollection<ICharacterCard> Cards { get; } = new ObservableCollection<ICharacterCard>();

        public Color Color { get; set; }

        public ValueStatistic Damage { get; }

        public Deck<ICharacterCard> Deck { get; set; }

        public ObservableDictionary<Item> Equipment { get; }

        public ReplenishableStatistic Interact { get; }

        public ObservableCollection<Item> Inventory { get; } = new ObservableCollection<Item>();

        public ValueStatistic MagicDamage { get; }

        public ReplenishableStatistic UseItem { get; }

        public override IStatisticWithCurrentValue GetStatistic(string statistic)
        {
            switch (statistic)
            {
                case Constants.Statisitcs.UseItem:
                    return UseItem;

                case Constants.Statisitcs.Interact:
                    return Interact;

                case Constants.Statisitcs.Damage:
                    return Damage;

                case Constants.Statisitcs.MagicDamage:
                    return MagicDamage;
            }

            return base.GetStatistic(statistic);
        }
    }
}