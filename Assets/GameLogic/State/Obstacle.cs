﻿using Assets.GameLogic.Settings;

namespace Assets.GameLogic.State
{
    public class Obstacle : IHarvestable
    {
        public bool CanHarvest => Settings.Remedies.Action == Constants.Actions.Harvest;

        public PlatformCard Location { get; set; }

        public ObstacleSettings Settings { get; set; }
    }
}