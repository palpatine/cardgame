﻿using System.Collections.Generic;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Model.Trace
{
    public class ActionTrace
    {
        public string Action { get; set; }

        public IUnit CurrentUnit { get; set; }

        public ILocation From { get; set; }

        public string ItemName { get; set; }

        public string ItemType { get; set; }

        public StatisticChange StatisticChange { get; set; }

        public IUnit Target { get; set; }

        public IEnumerable<IUnit> Targets { get; set; }

        public ILocation To { get; set; }
    }
}