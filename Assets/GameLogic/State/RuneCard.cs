﻿using Assets.GameLogic.Settings;

namespace Assets.GameLogic.State
{
    public class RuneCard : ICharacterCard
    {
        public RuneCardSettings Settings { get; set; }

        INamedSetting ICharacterCard.Settings => Settings;
    }
}