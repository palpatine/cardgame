﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.State
{
    public class Ability
    {
        public Ability()
        {
            Parameters = new Dictionary<string, int>();
        }

        public Dictionary<string, int> Parameters { get; set; }

        public int? RemainingDuration { get; set; }

        public int? RemainingUseCount { get; set; }

        public AbilitySettings Settings { get; set; }
    }
}