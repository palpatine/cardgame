﻿using System;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class ReplenishableStatistic : Notifier<ReplenishableStatistic>, IStatisticWithMaximumValue
    {
        private int _current;
        private int _maximum;

        public ReplenishableStatistic(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public int Current { get => _current; set => ChangeValue(ref _current, value); }

        public int Maximum { get => _maximum; set => ChangeValue(ref _maximum, value); }

        public void Add(string value, int delta)
        {
            if (value == Constants.StatisitcValues.Maximum)
            {
                Maximum += delta;
            }
            else if (value == Constants.StatisitcValues.Current)
            {
                Current += delta;
            }
            else
            {
                throw new InvalidOperationException($"Not supperoted {value}");
            }
        }

        public int GetValue(string value)
        {
            if (value == Constants.StatisitcValues.Maximum)
            {
                return Maximum;
            }
            else if (value == Constants.StatisitcValues.Current)
            {
                return Current;
            }

            throw new InvalidOperationException($"Not supperoted {value}");
        }

        public void Replenish() => Current = Maximum;
    }
}