﻿using System;
using System.Collections.ObjectModel;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public interface ITarget
    {
        Guid Id { get; }
    }

    public interface IUnit : ITarget, INotifyOnChange
    {
        ObservableCollection<Ability> Abilities { get; }

        ObservableCollection<ActionSettings> Actions { get; }

        ReplenishableStatistic Attack { get; }

        ReplenishableStatistic Block { get; }

        ValueStatistic Defense { get; }

        RegenerableStatistic Energy { get; }

        RegenerableStatistic Life { get; }

        Location Location { get; }

        RegenerableStatistic Mana { get; }

        ReplenishableStatistic Movement { get; }

        IStatisticWithCurrentValue GetStatistic(string statistic);
    }
}