﻿using System;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public interface IStatisticWithCurrentValue
    {
        int Current { get; set; }

        void Add(string value, int delta);

        int GetValue(string value);
    }

    public interface IStatisticWithMaximumValue : IStatisticWithCurrentValue
    {
        int Maximum { get; set; }
    }

    public class RegenerableStatistic : Notifier<RegenerableStatistic>, IStatisticWithMaximumValue
    {
        private int _current;
        private int _maximum;
        private int _regenerationRate;

        public RegenerableStatistic(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public int Current { get => _current; set => ChangeValue(ref _current, value); }

        public int Maximum { get => _maximum; set => ChangeValue(ref _maximum, value); }

        public int RegenerationRate { get => _regenerationRate; set => ChangeValue(ref _regenerationRate, value); }

        public void Add(string value, int delta)
        {
            if (value == Constants.StatisitcValues.Maximum)
            {
                Maximum += delta;
            }
            else if (value == Constants.StatisitcValues.Current)
            {
                Current += delta;
            }
            else if (value == Constants.StatisitcValues.Regeneration)
            {
                RegenerationRate += delta;
            }
            else
            {
                throw new InvalidOperationException($"Not supperoted {value}");
            }
        }

        public int GetValue(string value)
        {
            if (value == Constants.StatisitcValues.Maximum)
            {
                return Maximum;
            }
            else if (value == Constants.StatisitcValues.Current)
            {
                return Current;
            }
            else if (value == Constants.StatisitcValues.Regeneration)
            {
                return RegenerationRate;
            }

            throw new InvalidOperationException($"Not supperoted {value}");
        }

        public void Regenerate() => Current = Math.Min(
                Current + RegenerationRate,
                Maximum);
    }
}