﻿using Assets.GameLogic.Settings;

namespace Assets.GameLogic.State
{
    public interface ICharacterCard
    {
        INamedSetting Settings { get; }
    }
}