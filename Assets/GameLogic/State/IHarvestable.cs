﻿namespace Assets.GameLogic.State
{
    public interface IHarvestable
    {
        bool CanHarvest { get; }

        PlatformCard Location { get; }
    }
}