﻿namespace Assets.GameLogic.State
{
    public interface ILocation
    {
        int CurrentLevelId { get; set; }

        bool IsFacingLeft { get; set; }

        bool IsToTheLeftOfObstacle { get; set; }

        PlatformCard PlatformCard { get; set; }

        int UnitId { get; set; }

        ILocation Clone();
    }
}