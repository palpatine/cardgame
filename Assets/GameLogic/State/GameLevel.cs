﻿using System.Collections.ObjectModel;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class GameLevel : Notifier<GameLevel>
    {
        private bool _isLocked;

        public GameLevel(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public int IndexOf { get; internal set; }

        public bool IsLocked { get => _isLocked; set => ChangeValue(ref _isLocked, value); }

        public ObservableCollection<PlatformCard> Platforms { get; } = new ObservableCollection<PlatformCard>();
    }
}