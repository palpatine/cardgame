﻿using System.Collections.ObjectModel;
using Assets.GameLogic.Model.Trace;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class GameState : Notifier<GameState>
    {
        private IUnit _currentUnit;

        public GameState(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public IUnit CurrentUnit
        {
            get => _currentUnit;
            set => ChangeValue(ref _currentUnit, value);
        }

        public Deck<DangerCard> DangersDeck { get; set; }

        public GameTrace GameTrace { get; } = new GameTrace();

        public ObservableCollection<GameLevel> Levels { get; } = new ObservableCollection<GameLevel>();

        public Deck<PlatformCard> PlatformCards { get; set; }

        public ObservableCollection<IUnit> Units { get; } = new ObservableCollection<IUnit>();

        public ObservableCollection<IUnit> UnitsOrder { get; } = new ObservableCollection<IUnit>();
    }
}