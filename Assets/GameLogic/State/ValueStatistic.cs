﻿using System;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.State
{
    public class ValueStatistic : Notifier<ValueStatistic>, IStatisticWithCurrentValue
    {
        private int _current;

        public ValueStatistic(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public int Current { get => _current; set => ChangeValue(ref _current, value); }

        public void Add(string value, int delta)
        {
            if (value == Constants.StatisitcValues.Current)
            {
                Current += delta;
            }
            else
            {
                throw new InvalidOperationException($"Not supperoted {value}");
            }
        }

        public int GetValue(string value)
        {
            if (value == Constants.StatisitcValues.Current)
            {
                return Current;
            }

            throw new InvalidOperationException($"Not supperoted {value}");
        }
    }
}