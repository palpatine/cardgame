﻿namespace Assets.GameLogic.State
{
    public class PlatformFeature : IHarvestable
    {
        public bool CanHarvest => Count > 0;

        public int Count { get; set; }

        public string Kind { get; set; }

        public PlatformCard Location { get; set; }
    }
}