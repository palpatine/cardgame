﻿using Assets.GameLogic.Settings;

namespace Assets.GameLogic.State
{
    public class ActionCard : ICharacterCard
    {
        public ActionCardSettings Settings { get; set; }

        INamedSetting ICharacterCard.Settings => Settings;
    }
}