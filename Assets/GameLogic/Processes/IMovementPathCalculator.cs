﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Processes
{
    public interface IMovementPathCalculator
    {
        IEnumerable<JourneyLegDescriptor> FindPathToTarget<TFoe>(IUnit unit, ILocation from, ILocation to, bool withoutMinimal)
            where TFoe : IUnit;

        DistanceDescriptor GetDistance<TFoe>(IUnit unit, ILocation from, ILocation to)
            where TFoe : IUnit;
    }
}
