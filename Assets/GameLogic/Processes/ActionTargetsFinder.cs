﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Processes
{
    public class ActionTargetsFinder : IActionTargetsFinder
    {
        private readonly IGameStateProvider _gameStateProvider;

        public ActionTargetsFinder(
            IGameStateProvider gameStateProvider) => _gameStateProvider = gameStateProvider;

        public IEnumerable<IEnumerable<IUnit>> GetTargets(
            IUnit actor,
            ActionSettings actionSettings)
        {
            if (actor is Foe)
            {
                return GetTargets<Character>(actor, actionSettings);
            }

            return GetTargets<Foe>(actor, actionSettings);
        }

        public IEnumerable<IEnumerable<IUnit>> GetTargets<TTarget>(
            IUnit actor,
            ActionSettings actionSettings)
            where TTarget : class, IUnit
        {
            actionSettings = ApplyAbilities(actor, actionSettings);
            if (actionSettings.AffectsArea)
            {
                return GetAreaTargets<TTarget>(actor, actionSettings);
            }
            else
            {
                var result = GetSingleUnitTargets<TTarget>(actor, actionSettings.Range, actionSettings.CanTargetSelf)
                    .Except(GetSingleUnitTargets<TTarget>(actor, actionSettings.MinRange, false))
                     ?.Select(x => new[] { x })
                     .ToArray();

                return result;
            }
        }

        private ActionSettings ApplyAbilities(IUnit actor, ActionSettings actionSettings)
        {
            var abilities = actor.GetAbilities(
                    Constants.Triggers.ConsidersAction,
                    Enumerable.Empty<EffectResult>(),
                    new NamedTargetsSets(),
                    _gameStateProvider.GameState,
                    actionSettings.Kinds);

            if (!abilities.Any())
            {
                return actionSettings;
            }

            var result = new ActionSettings
            {
                Range = abilities.Select(x => x.Settings.ActionOverride.Range)
                .Concat(new[] { actionSettings.Range })
                .Where(x => x != null)
                .Max(),

                MinRange = abilities.Select(x => x.Settings.ActionOverride.MinRange)
                .Concat(new[] { actionSettings.MinRange })
                .Where(x => x != null)
                .Min(),

                AffectsArea = actionSettings.AffectsArea,
                Ammo = actionSettings.Ammo,
                CanTargetSelf = true
            };

            return result;
        }

        private IEnumerable<IEnumerable<IUnit>> GetAreaTargets<TTarget>(
            IUnit actor,
            ActionSettings actionSettings)
            where TTarget : class, IUnit
        {
            var maximalRange = actionSettings.Range;
            var minimalRange = actionSettings.MinRange;
            var targets = new List<IEnumerable<IUnit>>();

            if (maximalRange.Name == Constants.Range.Short)
            {
                var set = actor.FacedVisibleUnits();
                if (set.Any(x => x is TTarget))
                {
                    targets.Add(set);
                }
            }
            else if (maximalRange.Name == Constants.Range.Medium)
            {
                targets = GetMediumRangeTargets<TTarget>(actor, minimalRange, maximalRange.Value.Value);

            }
            else if (maximalRange.Name == Constants.Range.Far)
            {
                targets = GetFarRangeAreaTargets<TTarget>(actor, minimalRange, maximalRange.Value.Value);
            }
            else if (maximalRange.Name == Constants.Range.Foe)
            {
                var facedVisiblieUnits = actor.AllFacedVisibleUnits(_gameStateProvider.GameState);
                var excluded = GetSingleUnitTargets<TTarget>(actor, minimalRange, false);
                var sets = facedVisiblieUnits.OfType<TTarget>().Take(maximalRange.Value.Value)
                    .Except(excluded)
                    .GroupBy(x => x.Location.PlatformCard)
                    .ToArray();

                foreach (var item in sets)
                {
                    if (item.Key.Obstacle?.Settings.IsBlockingVisibility == true)
                    {
                        if (actor.Location.PlatformCard == item.Key)
                        {
                            if (actor.Location.IsToTheLeftOfObstacle)
                            {
                                targets.Add(item.Key.UnitsToLeftOfObstacle.ToArray());
                            }
                            else
                            {
                                targets.Add(item.Key.UnitsToRightOfObstacle.ToArray());
                            }
                        }
                        else
                        {
                            if (actor.Location.IsFacingLeft)
                            {
                                targets.Add(item.Key.UnitsToRightOfObstacle.ToArray());
                            }
                            else
                            {
                                targets.Add(item.Key.UnitsToLeftOfObstacle.ToArray());
                            }
                        }
                    }
                    else
                    {
                        targets.Add(item.Key.AllUnits.ToArray());
                    }
                }
            }

            if (actionSettings.CanTargetSelf || maximalRange.Name == Constants.Range.Self)
            {
                if (actor.Location.IsToTheLeftOfObstacle)
                {
                    targets.Add(actor.Location.PlatformCard.UnitsToLeftOfObstacle.ToArray());
                }
                else
                {
                    targets.Add(actor.Location.PlatformCard.UnitsToRightOfObstacle.ToArray());
                }
            }

            return targets;
        }

        private List<IEnumerable<IUnit>> GetMediumRangeTargets<TTarget>(IUnit actor, RangeSettings minimalRange, int value) where TTarget : class, IUnit
        {
            var sets = new List<IEnumerable<IUnit>>();
            var faced = actor.FacedVisibleUnits();
            if (faced.Any() &&
                (minimalRange == null
                    || minimalRange.Name == Constants.Range.Short && minimalRange.Value.Value < faced.Count()))
            {
                sets.Add(faced);
            }

            if (actor.Location.PlatformCard.Obstacle != null && actor.Location.PlatformCard.Obstacle.Settings.IsBlockingVisibility)
            {
                return sets;
            }

            var platformCard = actor.Location.PlatformCard.PlatformTo(_gameStateProvider.GameState, actor.Location.IsFacingLeft);

            if (platformCard == null)
            {
                return sets;
            }

            var set = platformCard.UnitsVisibleTo(actor.Location.IsFacingLeft);
            if (set.Any() &&
                (minimalRange == null || minimalRange.Name != Constants.Range.Medium || set.Count() > minimalRange.Value.Value))
            {
                sets.Add(set);
            }

            return sets;
        }

        private List<IEnumerable<IUnit>> GetFarRangeAreaTargets<TTarget>(
            IUnit actor,
            RangeSettings minimalRange,
            int rangeValue)
            where TTarget : class, IUnit
        {
            var sets = new List<IEnumerable<IUnit>>();

            var faced = actor.FacedVisibleUnits();
            if (faced.Any() &&
                (minimalRange == null
                    || minimalRange.Name == Constants.Range.Short && minimalRange.Value.Value < faced.Count()))
            {
                sets.Add(faced);
            }

            if (actor.Location.PlatformCard.Obstacle != null && actor.Location.PlatformCard.Obstacle.Settings.IsBlockingVisibility)
            {
                return sets;
            }

            var platformCard = actor.Location.PlatformCard.PlatformTo(_gameStateProvider.GameState, actor.Location.IsFacingLeft);

            if (platformCard == null)
            {
                return sets;
            }

            var set = platformCard.UnitsVisibleTo(actor.Location.IsFacingLeft);
            if (set.Any() && (minimalRange == null
                || minimalRange.Name == Constants.Range.Short 
                || minimalRange.Name == Constants.Range.Medium && set.Count > minimalRange.Value.Value))
            {
                sets.Add(set);
            }

            if (platformCard.Obstacle != null && platformCard.Obstacle.Settings.IsBlockingVisibility)
            {
                return sets;
            }

            platformCard = platformCard.PlatformTo(_gameStateProvider.GameState, actor.Location.IsFacingLeft);
            var count = 2;
            if (minimalRange?.Name == Constants.Range.Far)
            {
                while (count < minimalRange.Value.Value - 1)
                {
                    count++;
                    platformCard = platformCard.PlatformTo(_gameStateProvider.GameState, actor.Location.IsFacingLeft);
                }

                var units = platformCard.UnitsVisibleTo(actor.Location.IsFacingLeft);
                if (units.Skip(1).Any())
                {
                    sets.Add(units);
                }

                count++;
                platformCard = platformCard.PlatformTo(_gameStateProvider.GameState, actor.Location.IsFacingLeft);
            }

            while (count < rangeValue)
            {
                if (platformCard == null)
                {
                    return sets;
                }

                var nextPlatformUnits = platformCard.UnitsVisibleTo(actor.Location.IsFacingLeft);

                if (nextPlatformUnits.Any())
                {
                    sets.Add(nextPlatformUnits);
                }

                if (platformCard.Obstacle != null && platformCard.Obstacle.Settings.IsBlockingVisibility)
                {
                    return sets;
                }

                platformCard = platformCard.PlatformTo(_gameStateProvider.GameState, actor.Location.IsFacingLeft);
                count++;
            }

            return sets.Where(x => x.OfType<TTarget>().Any()).ToList();
        }

        private IEnumerable<IUnit> GetSingleUnitTargets<TTarget>(
            IUnit actor,
            RangeSettings range,
            bool canTargetSelf)
            where TTarget : class, IUnit
        {
            var targets = new List<TTarget>();

            if (range == null)
            {
                return targets;
            }

            if (range.Name == Constants.Range.Self)
            {
                if (actor is TTarget at)
                {
                    targets.Add(at);
                }
            }
            else if (range.Name == Constants.Range.Short)
            {
                var units = actor.FacedVisibleUnits().Take(range.Value.Value);
                targets.AddRange(units.OfType<TTarget>());
            }
            else if (range.Name == Constants.Range.Medium)
            {
                targets.AddRange(actor.FacedVisibleUnits().OfType<TTarget>());
                var units = actor.AllFacedVisiblePlatfomrs(_gameStateProvider.GameState).FirstOrDefault()?.UnitsVisibleTo(actor.Location.IsFacingLeft);
                if (units != null)
                {
                    var set = units.Take(range.Value.Value).OfType<TTarget>().ToArray();
                    targets.AddRange(set);
                }
            }
            else if (range.Name == Constants.Range.Far)
            {
                targets.AddRange(actor.FacedVisibleUnits().OfType<TTarget>());
                var platforms = actor.AllFacedVisiblePlatfomrs(_gameStateProvider.GameState);
                var fullPlatforms = platforms.Take(range.Value.Value - 2);

                foreach (var item in fullPlatforms)
                {
                    targets.AddRange(item.UnitsVisibleTo(actor.Location.IsFacingLeft).OfType<TTarget>());
                }

                if (platforms.ElementAtOrDefault(range.Value.Value - 2)
                     ?.UnitsVisibleTo(actor.Location.IsFacingLeft)?.FirstOrDefault() is TTarget lastTarget)
                {
                    targets.Add(lastTarget);
                }
            }
            else if (range.Name == Constants.Range.Foe)
            {
                var facedVisiblieUnits = actor.AllFacedVisibleUnits(_gameStateProvider.GameState);
                targets.AddRange(facedVisiblieUnits.OfType<TTarget>().Take(range.Value.Value));
            }

            if (canTargetSelf && actor is TTarget actorTarget)
            {
                targets.Add(actorTarget);
            }

            return targets;
        }
    }
}