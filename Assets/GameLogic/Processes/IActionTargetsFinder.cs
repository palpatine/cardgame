﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Processes
{
    public interface IActionTargetsFinder
    {
        IEnumerable<IEnumerable<IUnit>> GetTargets(
            IUnit actor,
            ActionSettings actionSettings);

        IEnumerable<IEnumerable<IUnit>> GetTargets<TFoe>(
            IUnit actor,
            ActionSettings actionSettings)
            where TFoe : class, IUnit;
    }
}