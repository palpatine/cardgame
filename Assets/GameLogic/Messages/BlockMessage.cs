﻿using Assets.GameLogic.State;

namespace Assets.GameLogic.Messages
{
    public class BlockMessage
    {
        public IUnit Unit { get; set; }
    }
}