﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;

namespace Assets.GameLogic.Messages
{
    public class SelectedValue
    {
        public int Increment { get; set; }

        public int Max { get; set; }

        public int Min { get; set; }

        public SelectorSettings SelectorSetting { get; set; }

        public int Value { get; set; }
    }

    public class SelectValues
    {
        public bool CanCancel { get; set; }

        public bool Cancelled { get; set; }

        public IEnumerable<SelectedValue> Selectors { get; set; }
    }
}