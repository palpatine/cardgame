﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Messages
{
    public class DangerRevealed
    {
        public DangerCard DangerCard { get; set; }
    }
}
