﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Messages
{
    public class ActionMessage
    {
        public ActionSettings Action { get; set; }

        public IUnit Actor { get; set; }

        public IEnumerable<IUnit> Targets { get; set; }
    }
}