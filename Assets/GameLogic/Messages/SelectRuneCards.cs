﻿using System.Collections.Generic;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Messages
{
    public class SelectRuneCards
    {
        public IEnumerable<RuneCard> AllCards { get; set; }

        public RuneCard CardToActivate { get; set; }

        public int ExpectedValue { get; set; }

        public bool IsCancelled { get; set; }

        public IEnumerable<RuneCard> Set { get; set; }
    }
}