﻿using Assets.GameLogic.Processes;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Zenject;

namespace Assets.GameLogic
{
    public class CoreInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IUpdateNotifier>().To<UpdateNotifier>().AsCached();
            Container.Bind<IActionTargetsFinder>().To<ActionTargetsFinder>().AsTransient();
        }
    }
}
