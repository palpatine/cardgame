﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.GameLogic.Incidents.UnitIncidents;
using Zenject;

namespace Assets.GameLogic.Incidents
{
    public class IncidentsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            InstallIncidentHandlers();
            Container.Bind<IIncidentsManager>().To<IncidentsManager>().AsTransient();
        }

        private void InstallIncidentHandlers()
        {
            var validators = new Type[] {
                typeof(UnitDeathIncident)
            };

            Container.Bind(validators).AsTransient();

            Container.Bind<Func<IEnumerable<IIncident>>>().FromMethod(
                () => () => validators.Select(x => (IIncident)Container.Resolve(x)).ToArray()
            );
        }
    }
}
