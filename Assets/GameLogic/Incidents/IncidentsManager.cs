﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assets.GameLogic.Incidents
{
    public interface IIncident
    {
    }

    public interface IIncidnet<TIncidnetData> : IIncident
    {
        Task Hande(TIncidnetData data);
    }

    public interface IIncidentsManager
    {
        Task HandleIncitent<TIncidentData>(TIncidentData data);
    }

    public class IncidentsManager: IIncidentsManager
    {
        private readonly Func<IEnumerable<IIncident>> _incidentsFactory;
        private Dictionary<Type, IIncident> _incidents;

        private Dictionary<Type, IIncident> Incidents
        {
            get
            {
                return _incidents ?? (_incidents =
                                        _incidentsFactory().ToDictionary(
                                            x => x.GetType().GetInterfaces()
                                                  .Single(y => y.IsGenericType && y.GetGenericTypeDefinition() == typeof(IIncidnet<>))
                                                  .GetGenericArguments()[0],
                                            x => x));
            }
        }

        public IncidentsManager(Func<IEnumerable<IIncident>> incidentsFactory) => _incidentsFactory = incidentsFactory;

        public async Task HandleIncitent<TIncidentData>(TIncidentData data)
        {
            if (Incidents.ContainsKey(typeof(TIncidentData)))
            {
                var handler = (IIncidnet<TIncidentData>)_incidents[typeof(TIncidentData)];
                await handler.Hande(data);
            }
            else
            {
                throw new InvalidOperationException($"Incident Handler form {typeof(TIncidentData).Name} is missing.");
            }
        }
    }
}
