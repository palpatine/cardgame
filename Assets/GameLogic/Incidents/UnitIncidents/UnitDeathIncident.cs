﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Model.Trace;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Incidents.UnitIncidents
{
    public class UnitDeathIncidentData
    {
        public IUnit Unit { get; set; }
    }

    public class UnitDeathIncident : IIncidnet<UnitDeathIncidentData>
    {
        private readonly IGameStateProvider _gameStateProvider;

        private readonly IEndTurnActivity _endTurnAction;

        public UnitDeathIncident(IGameStateProvider gameStateProvider, IEndTurnActivity endTurnAction)
        {
            _gameStateProvider = gameStateProvider;
            _endTurnAction = endTurnAction;
        }

        public async Task Hande(UnitDeathIncidentData data)
        {
            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Target = data.Unit,
                Action = Constants.TraceActions.UnitDied,
            });

            _gameStateProvider.GameState.Units.Remove(data.Unit);
            var currentUnitIndexInOrder = _gameStateProvider.GameState.UnitsOrder.IndexOf(data.Unit);
            _gameStateProvider.GameState.UnitsOrder.Remove(data.Unit);

            IEnumerable<IUnit> unitsColection;
            if (data.Unit.Location.IsToTheLeftOfObstacle)
            {
                unitsColection = data.Unit.Location.PlatformCard.UnitsToLeftOfObstacle;
                data.Unit.Location.PlatformCard.UnitsToLeftOfObstacle.Remove(data.Unit);
            }
            else
            {
                unitsColection = data.Unit.Location.PlatformCard.UnitsToRightOfObstacle;
                data.Unit.Location.PlatformCard.UnitsToRightOfObstacle.Remove(data.Unit);
            }

            foreach (var item in unitsColection)
            {
                item.Location.UnitId--;
            }

            if (data.Unit is Character character)
            {
                if (_gameStateProvider.GameState.CurrentUnit == data.Unit)
                {
                    _gameStateProvider.GameState.CurrentUnit = null;
                    await _endTurnAction.Execute(currentUnitIndexInOrder - 1, true);
                }

                foreach (var item in character.Inventory)
                {
                    character.Location.PlatformCard.Items.Add(item);
                }

                foreach (var item in character.Equipment)
                {
                    character.Location.PlatformCard.Items.Add(item.Value);
                }
            }
        }
    }
}
