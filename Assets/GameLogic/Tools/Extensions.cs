﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Assets.GameLogic.Tools
{
    public static class Extensions
    {
        public static Nullable<T> FirstOrNull<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
            where T : struct
        {
            foreach (var item in collection)
            {
                if (predicate(item))
                {
                    return item;
                }
            }

            return null;
        }

        public static IEnumerable<TResult> FullOuterJoin<TA, TB, TKey, TResult>(
            this IEnumerable<TA> a,
            IEnumerable<TB> b,
            Func<TA, TKey> selectKeyA,
            Func<TB, TKey> selectKeyB,
            Func<TA, TB, TKey, TResult> projection,
            TA defaultA = default(TA),
            TB defaultB = default(TB),
            IEqualityComparer<TKey> cmp = null)
        {
            cmp = cmp ?? EqualityComparer<TKey>.Default;
            var alookup = a.ToLookup(selectKeyA, cmp);
            var blookup = b.ToLookup(selectKeyB, cmp);

            var keys = new HashSet<TKey>(alookup.Select(p => p.Key), cmp);
            keys.UnionWith(blookup.Select(p => p.Key));

            var join = from key in keys
                       from xa in alookup[key].DefaultIfEmpty(defaultA)
                       from xb in blookup[key].DefaultIfEmpty(defaultB)
                       select projection(xa, xb, key);

            return join;
        }

        public static int IndexOfFirst<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
            where T : struct => collection.Select((x, i) => (x, i)).FirstOrNull(x => predicate(x.x))?.i ?? -1;

        public static string JoinAsString<T>(
                                this IEnumerable<T> collection,
            Func<T, string> format = null,
            string separator = ", ")
        {
            if (collection == null || !collection.Any())
            {
                return "<none>";
            }

            return string.Join(separator,
                collection.Select(x => format?.Invoke(x) ?? x?.ToString() ?? "<null>"));
        }
    }
}