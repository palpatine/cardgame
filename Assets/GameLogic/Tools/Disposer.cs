﻿using System;

namespace Assets.GameLogic.Tools
{
    public class Disposer : IDisposable
    {
        public Disposer(Action dispose) => this.DisposeAction = dispose;

        public Action DisposeAction { get; }

        public void Dispose() => DisposeAction();
    }
}