﻿using System.Threading.Tasks;
using PubSub.Core;

namespace PubSub
{
    public class Publisher : IPublisher
    {
        private readonly Hub hub;

        public Publisher(Hub hub)
        {
            this.hub = hub;
        }

        public async Task Publish<T>(object sender, T data) => await hub.Publish(sender, data);
    }
}