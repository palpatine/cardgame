﻿using System;
using System.Threading.Tasks;

namespace PubSub
{
    public interface ISubscriber
    {
        bool Exists<T>(object subscriber);

        bool Exists<T>(object subscriber, Func<T, Task> handler);

        void Subscribe<T>(object subscriber, Func<T, Task> handler);

        void Unsubscribe(object subscriber);

        void Unsubscribe<T>(object subscriber);

        void Unsubscribe<T>(object subscriber, Func<T, Task> handler);
    }
}