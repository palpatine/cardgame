﻿using System.Threading.Tasks;

namespace PubSub
{
    public interface IPublisher
    {
        Task Publish<T>(object sender, T data);
    }
}