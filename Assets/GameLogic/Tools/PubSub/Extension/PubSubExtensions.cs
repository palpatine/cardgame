﻿using System;
using System.Threading.Tasks;
using PubSub.Core;

//namespace PubSub.Extension
//{
public static class PubSubExtensions
{
    private static readonly Hub hub = new Hub();

    public static bool Exists<T>(this object obj) => hub.Exists<T>(obj);

    public static async Task Publish<T>(this object obj) => await hub.Publish(obj, default(T));

    public static async Task Publish<T>(this object obj, T data) => await hub.Publish(obj, data);

    public static void Subscribe<T>(this object obj, Func<T, Task> handler) => hub.Subscribe(obj, handler);

    public static void Unsubscribe(this object obj) => hub.Unsubscribe(obj);

    public static void Unsubscribe<T>(this object obj) => hub.Unsubscribe(obj, (Func<T, Task>)null);

    public static void Unsubscribe<T>(this object obj, Func<T, Task> handler) => hub.Unsubscribe(obj, handler);
}

//}