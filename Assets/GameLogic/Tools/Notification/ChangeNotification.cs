﻿namespace Assets.GameLogic.Tools.Notification
{
    public class ChangeNotification
    {
        public object NewValue { get; set; }

        public object OldValue { get; set; }

        public string PropertyName { get; set; }
    }
}