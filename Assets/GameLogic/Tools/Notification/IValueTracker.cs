﻿using System;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Assets.GameLogic.Tools.Notification
{
    public interface IValueTracker<T>
        where T : INotifyOnChange
    {
        IChangedPublisher<G> TrackValue<G>(Expression<Func<T, G>> property)
            where G : INotifyOnChange;

        IChangedPublisher<G> TrackValue<G>(Expression<Func<T, G>> property,
            Action reaction)
            where G : INotifyOnChange;

        IChangedPublisher<G> TrackValue<G>(Expression<Func<T, G>> property,
            Action<ChangeNotification> reaction)
            where G : INotifyOnChange;

        IDisposable TrackValue(Expression<Func<T, INotifyCollectionChanged>> property,
            Action<NotifyCollectionChangedEventArgs> reaction);

        IChangedPublisher<G> TrackValueAsync<G>(Expression<Func<T, G>> property,
                            Func<Task> reaction)
            where G : INotifyOnChange;

        IChangedPublisher<G> TrackValueAsync<G>(Expression<Func<T, G>> property,
            Func<ChangeNotification, Task> reaction)
            where G : INotifyOnChange;
    }
}