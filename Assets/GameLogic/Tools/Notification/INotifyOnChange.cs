﻿using System;

namespace Assets.GameLogic.Tools.Notification
{
    public interface INotifyOnChange
    {
        event Action<ChangeNotification> PropertyChanged;

        IUpdateNotifier UpdateNotifier { get; }
    }
}