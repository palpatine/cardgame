﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Assets.GameLogic.Tools.Notification
{
    internal class TaskWrapper
    {
        private CancellationTokenSource _cancellationTokenSource;
        private Task _task;

        public Func<T, Task> ExecuteOneAtTime<T>(Func<T, CancellationToken, Task> action)
        {
            return async x =>
            {
                var result = new AsyncResult();
                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Cancel();
                    await _task;
                }
                _task = Task.Factory.FromAsync(result, _ => { });
                using (_cancellationTokenSource = new CancellationTokenSource())
                {
                    await action(x, _cancellationTokenSource.Token);
                }

                _cancellationTokenSource = null;
                result.IsCompleted = true;
            };
        }

        public Func<Task> ExecuteOneAtTime(Func<CancellationToken, Task> action)
        {
            return async () =>
            {
                var result = new AsyncResult();
                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Cancel();
                    await _task;
                }
                _task = Task.Factory.FromAsync(result, _ => { });
                using (_cancellationTokenSource = new CancellationTokenSource())
                {
                    await action(_cancellationTokenSource.Token);
                }

                _cancellationTokenSource = null;
                result.IsCompleted = true;
            };
        }
    }
}