﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Assets.GameLogic.Tools.Notification
{
    public class ChangeNotificationBuilder<T> :
        IChangeNotificationBuilder<T>, IChangedPublisher<T>
        where T : INotifyOnChange
    {
        private readonly IUpdateNotifier _updateNotifier;
        private bool _allChanges;
        private Action _detach;
        private List<IDisposable> _disposers = new List<IDisposable>();
        private Func<object> _getCurrentValue;
        private INotifyOnChange _notifier;
        private List<string> _properties = new List<string>();
        private string _propertyName;
        private Func<ChangeNotification, Task> _reaction;

        public ChangeNotificationBuilder(IUpdateNotifier updateNotifier, INotifyOnChange notifier)
        {
            _updateNotifier = updateNotifier;
            _notifier = notifier;
        }

        public IChangeNotificationBuilder<T> OnChange<G>(Expression<Func<T, G>> property) => Or(property);

        public IChangeNotificationBuilder<T> Or<G>(Expression<Func<T, G>> property)
        {
            var func = property.Compile();
            _propertyName = property.GetPropertyName();
            _getCurrentValue = () => _notifier == null ? default : func((T)_notifier);
            _properties.Add(_propertyName);
            return this;
        }

        public IChangeNotificationBuilder<T> Or(string property)
        {
            _properties.Add(property);
            _propertyName = property;
            _getCurrentValue = () => ((IObservableDictionary)_notifier)[property];
            return this;
        }

        public IDisposable React(Func<ChangeNotification, Task> reaction, bool allChanges)
        {
            _reaction = reaction ?? throw new ArgumentNullException(nameof(reaction));
            _allChanges = allChanges;

            if (_notifier != null)
            {
                Guid? currentOperationId = null;
                void OnPropertyChanged(ChangeNotification notification)
                {
                    if (!_properties.Contains(notification.PropertyName) || _updateNotifier.CurrentOperationId != null
                        && _updateNotifier.CurrentOperationId == currentOperationId)
                    {
                        return;
                    }

                    currentOperationId = _updateNotifier.CurrentOperationId;
                    _updateNotifier.AddToCurrentOperation(reaction(notification));
                };

                _notifier.PropertyChanged += OnPropertyChanged;

                _detach = () => _notifier.PropertyChanged -= OnPropertyChanged;
            }

            return new Disposer(() =>
            {
                _detach?.Invoke();
                foreach (var disposer in _disposers)
                {
                    disposer.Dispose();
                }
            });
        }

        public IDisposable React(Func<Task> reaction, bool allChanges) => React((x) => reaction(), allChanges);

        public IDisposable React(Action reaction, bool allChanges) => React(async () => { reaction(); await _updateNotifier.NextUpdate; }, allChanges);

        public IDisposable React(Action<ChangeNotification> reaction, bool allChanges) => React(async (x) => { reaction(x); await _updateNotifier.NextUpdate; }, allChanges);

        public IDisposable ReactOneAtTime(Func<ChangeNotification, CancellationToken, Task> reaction, bool allChanges)
                                            => React(reaction.OneAtTime(), allChanges);

        public IDisposable ReactOneAtTime(Func<CancellationToken, Task> reaction, bool allChanges)
            => React(reaction.OneAtTime(), allChanges);

        public IChangedPublisher<G> TrackValue<G>(Expression<Func<T, G>> property)
            where G : INotifyOnChange => TrackValueAsync(property, (Func<ChangeNotification, Task>)null);

        public IChangedPublisher<G> TrackValue<G>(Expression<Func<T, G>> property, Action reaction)
            where G : INotifyOnChange => TrackValueAsync(property, async () => { reaction(); await _updateNotifier.NextUpdate; });

        public IChangedPublisher<G> TrackValue<G>(Expression<Func<T, G>> property, Action<ChangeNotification> reaction)
            where G : INotifyOnChange => TrackValueAsync(property, async (x) => { reaction(x); await _updateNotifier.NextUpdate; });

        public IDisposable TrackValue(Expression<Func<T, INotifyCollectionChanged>> property,
            Action<NotifyCollectionChangedEventArgs> reaction)
        {
            void Reaction(object sender, NotifyCollectionChangedEventArgs args)
            {
                reaction(args);
            }

            var release = Or(property).React((x) =>
            {
                if (x.OldValue != null)
                {
                    var value = (INotifyCollectionChanged)x.OldValue;
                    value.CollectionChanged -= Reaction;
                }

                if (x.NewValue != null)
                {
                    var value = (INotifyCollectionChanged)x.NewValue;
                    value.CollectionChanged += Reaction;
                }

                reaction(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            },
             allChanges: true);

            var current = (INotifyCollectionChanged)_getCurrentValue();
            if (current != null)
            {
                current.CollectionChanged += Reaction;
            }

            return release;
        }

        public IChangedPublisher<G> TrackValueAsync<G>(
                    Expression<Func<T, G>> property,
            Func<Task> reaction)
        where G : INotifyOnChange => TrackValueAsync(property, (x) => reaction());

        public IChangedPublisher<G> TrackValueAsync<G>(
            Expression<Func<T, G>> property,
            Func<ChangeNotification, Task> reaction)
        where G : INotifyOnChange
        {
            var builder = new ChangeNotificationBuilder<G>(_updateNotifier, null);
            var release = Or(property).React((x) =>
             {
                 if (x.NewValue != null)
                 {
                     var value = (G)x.NewValue;
                     builder.Bind(value);
                 }
                 else
                 {
                     builder.Bind(default);
                 }

                 if (reaction != null)
                     _updateNotifier.AddToCurrentOperation(reaction(x));
             },
             allChanges: true);

            _reaction(new ChangeNotification
            {
                PropertyName = _propertyName,
                NewValue = _getCurrentValue()
            });
            builder.AttachDisposer(release);
            return builder;
        }

        private void AttachDisposer(IDisposable disposer) => _disposers.Add(disposer);

        private void Bind(T value)
        {
            _detach?.Invoke();
            _detach = null;
            _notifier = value;
            if (_notifier != null && _reaction != null)
            {
                React(_reaction, _allChanges);
            }

            _reaction?.Invoke(new ChangeNotification
            {
                PropertyName = _propertyName,
                NewValue = _getCurrentValue()
            });
        }
    }
}