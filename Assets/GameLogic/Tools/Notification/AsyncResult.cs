﻿using System;
using System.Threading;

namespace Assets.GameLogic.Tools.Notification
{
    internal class AsyncResult : IAsyncResult
    {
        private bool _isCompleted;
        private ManualResetEvent _manualResetEvent = new ManualResetEvent(false);

        public object AsyncState => null;

        public WaitHandle AsyncWaitHandle => _manualResetEvent;

        public bool CompletedSynchronously => false;

        public bool IsCompleted
        {
            get => _isCompleted; set
            {
                _isCompleted = value;
                _manualResetEvent.Set();
            }
        }
    }
}