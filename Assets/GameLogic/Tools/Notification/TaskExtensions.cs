﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Assets.GameLogic.Tools.Notification
{
    internal static class TaskExtensions
    {
        public static Func<T, Task> OneAtTime<T>(this Func<T, CancellationToken, Task> action)
        {
            var wrapper = new TaskWrapper();
            return wrapper.ExecuteOneAtTime(action);
        }

        public static Func<Task> OneAtTime(this Func<CancellationToken, Task> action)
        {
            var wrapper = new TaskWrapper();
            return wrapper.ExecuteOneAtTime(action);
        }
    }
}