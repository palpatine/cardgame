﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Assets.GameLogic.Tools.Notification
{
    public abstract class Notifier<TSelf> : INotifyOnChange
        where TSelf : Notifier<TSelf>
    {
        private List<ChangeNotification> _pending = new List<ChangeNotification>();

        protected Notifier(IUpdateNotifier updateNotifier)
        {
            updateNotifier.OnUpdate += OnUpdate;
            UpdateNotifier = updateNotifier;
        }

        public event Action<ChangeNotification> PropertyChanged;

        public IEnumerable<ChangeNotification> PendingNotifications { get => _pending; }

        public IUpdateNotifier UpdateNotifier { get; }

        protected void ChangeValue<T>(ref T variable, T value, [CallerMemberName]string name = null)
        {
            if (!Equals(variable, value))
            {
                var change = _pending.SingleOrDefault(x => x.PropertyName == name);
                if (change == null)
                {
                    change = new ChangeNotification
                    {
                        PropertyName = name,
                        OldValue = variable
                    };
                    _pending.Add(change);
                }

                change.NewValue = value;
                variable = value;
            }
        }

        private void OnUpdate()
        {
            var pending = _pending.ToList();
            _pending.Clear();
            foreach (var item in pending)
            {
                PropertyChanged?.Invoke(item);
            }
        }
    }
}