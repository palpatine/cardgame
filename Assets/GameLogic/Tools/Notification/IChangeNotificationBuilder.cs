﻿using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Assets.GameLogic.Tools.Notification
{
    public interface IChangeNotificationBuilder<T> : IValueTracker<T>
        where T : INotifyOnChange
    {
        IChangeNotificationBuilder<T> Or<G>(Expression<Func<T, G>> property);

        IDisposable React(Func<Task> reaction, bool allChanges = false);

        IDisposable React(Func<ChangeNotification, Task> reaction, bool allChanges = false);

        IDisposable React(Action<ChangeNotification> reaction, bool allChanges = false);

        IDisposable React(Action reaction, bool allChanges = false);

        IDisposable ReactOneAtTime(Func<CancellationToken, Task> reaction, bool allChanges = false);

        IDisposable ReactOneAtTime(Func<ChangeNotification, CancellationToken, Task> reaction, bool allChanges = false);
    }
}