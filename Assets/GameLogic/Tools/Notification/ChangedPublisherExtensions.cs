﻿namespace Assets.GameLogic.Tools.Notification
{
    public static class ChangedPublisherExtensions
    {
        public static IChangeNotificationBuilder<ObservableDictionary<T>> OnChange<T>(
             this IChangedPublisher<ObservableDictionary<T>> self,
             string key) => ((ChangeNotificationBuilder<ObservableDictionary<T>>)self).Or(key);
    }
}