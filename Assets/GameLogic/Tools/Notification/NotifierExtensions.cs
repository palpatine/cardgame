﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
namespace Assets.GameLogic.Tools.Notification
{
    public static class NotifierExtensions
    {
        public static Task AwaitChangeOnce<T, G>(this T notifier, Expression<Func<T, G>> propertyExpression)
            where T : Notifier<T>
        {
            var property = propertyExpression.GetPropertyName();
            var result = new AsyncResult();
            var waitOne = notifier.PendingNotifications.Any(x => x.PropertyName == property);
            void OnChange(ChangeNotification e)
            {
                if (waitOne)
                {
                    waitOne = false;
                }
                else if (e.PropertyName == property)
                {
                    result.IsCompleted = true;
                    notifier.PropertyChanged -= OnChange;
                }
            }

            notifier.PropertyChanged += OnChange;

            var task = Task.Factory.FromAsync(result, x => { });
            return task;
        }

        public static IChangeNotificationBuilder<TSelf> OnChange<TSelf, G>(this TSelf self, Expression<Func<TSelf, G>> property)
                where TSelf : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).Or(property);
        }

        public static IChangeNotificationBuilder<ObservableDictionary<T>> OnChange<T>(
            this ObservableDictionary<T> self,
            string key)
        {
            return new ChangeNotificationBuilder<ObservableDictionary<T>>(self.UpdateNotifier, self).Or(key);
        }

        public static IChangedPublisher<G> TrackValue<TSelf, G>(this TSelf self, Expression<Func<TSelf, G>> property)
            where TSelf : INotifyOnChange
            where G : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).TrackValue(property);
        }

        public static IChangedPublisher<G> TrackValue<TSelf, G>(this TSelf self, Expression<Func<TSelf, G>> property, Func<Task> reaction)
            where TSelf : INotifyOnChange
            where G : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).TrackValueAsync(property, reaction);
        }

        public static IChangedPublisher<G> TrackValue<TSelf, G>(this TSelf self, Expression<Func<TSelf, G>> property, Func<ChangeNotification, Task> reaction)
            where TSelf : INotifyOnChange
           where G : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).TrackValueAsync(property, reaction);
        }

        public static IChangedPublisher<G> TrackValue<TSelf, G>(this TSelf self, Expression<Func<TSelf, G>> property, Action reaction)
            where TSelf : INotifyOnChange
            where G : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).TrackValue(property, reaction);
        }

        public static IChangedPublisher<G> TrackValue<TSelf, G>(this TSelf self, Expression<Func<TSelf, G>> property, Action<ChangeNotification> reaction)
            where TSelf : INotifyOnChange
            where G : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).TrackValue(property, reaction);
        }

        public static IDisposable TrackValue<TSelf>(this TSelf self, Expression<Func<TSelf, INotifyCollectionChanged>> property,
                Action<NotifyCollectionChangedEventArgs> reaction)
            where TSelf : INotifyOnChange
        {
            return new ChangeNotificationBuilder<TSelf>(self.UpdateNotifier, self).TrackValue(property, reaction);
        }
    }
}