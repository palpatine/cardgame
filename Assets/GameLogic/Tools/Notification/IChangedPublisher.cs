﻿using System;
using System.Linq.Expressions;

namespace Assets.GameLogic.Tools.Notification
{
    public interface IChangedPublisher<T> : IValueTracker<T>
        where T : INotifyOnChange
    {
        IChangeNotificationBuilder<T> OnChange<G>(Expression<Func<T, G>> property);
    }
}