﻿using System;
using System.Threading.Tasks;

namespace Assets.GameLogic.Tools.Notification
{
    public interface IUpdateNotifier
    {
        event Action OnUpdate;

        Guid? CurrentOperationId { get; }

        Task NextLateUpdate { get; }

        Task NextUpdate { get; }

        void AddToCurrentOperation(Task task);

        void Await(Task task);

        void Await(Func<Task> task);

        Task StartOperation(Action modifyState);

        Task StartOperation(Func<Task> modifyState);
    }
}