﻿using System.IO;
namespace Assets.GameLogic.Tools
{
    /// <summary>
    /// Extensions on string.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Converts string to camel case.
        /// </summary>
        /// <remarks>Code as implemented in Newtonsoft.Json/Utilities/StringUtils.cs.</remarks>
        /// <param name="text">Text to convert.</param>
        /// <returns>Text in camel case.</returns>
        public static string ToCamelCase(string text)
        {
            if (string.IsNullOrEmpty(text) || !char.IsUpper(text[0]))
            {
                return text;
            }

            char[] chars = text.ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                if (i == 1 && !char.IsUpper(chars[i]))
                {
                    break;
                }

                bool hasNext = i + 1 < chars.Length;
                if (i > 0 && hasNext && !char.IsUpper(chars[i + 1]))
                {
                    // if the next character is a space, which is not considered uppercase
                    // (otherwise we wouldn't be here...)
                    // we want to ensure that the following:
                    // 'FOO bar' is rewritten as 'foo bar', and not as 'foO bar'
                    // The code was written in such a way that the first word in uppercase
                    // ends when if finds an uppercase letter followed by a lowercase letter.
                    // now a ' ' (space, (char)32) is considered not upper
                    // but in that case we still want our current character to become lowercase
                    if (char.IsSeparator(chars[i + 1]))
                    {
                        chars[i] = char.ToLowerInvariant(chars[i]);
                    }

                    break;
                }

                chars[i] = char.ToLowerInvariant(chars[i]);
            }

            return new string(chars);
        }

        /// <summary>
        /// Converts string to unread stream - position is set to 0.
        /// </summary>
        /// <param name="text">Text to insert into stream.</param>
        /// <returns>Stream containing text.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Method is creating stream")]
        public static Stream ToStream(this string text)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}