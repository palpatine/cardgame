﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.Tools
{
    public interface IObservableDictionary
    {
        object this[string key] { get; }
    }

    public class ObservableDictionary<TValue> :
        IDictionary<string, TValue>,
        IObservableDictionary,
        INotifyCollectionChanged,
        INotifyOnChange
    {
        private const string CountString = "Count";
        private const string IndexerName = "Item[]";
        private const string KeysName = "Keys";
        private const string ValuesName = "Values";

        private IDictionary<string, TValue> _Dictionary;

        protected IDictionary<string, TValue> Dictionary
        {
            get { return _Dictionary; }
        }

        #region Constructors

        public ObservableDictionary(IUpdateNotifier updateNotifier)
        {
            UpdateNotifier = updateNotifier;
            _Dictionary = new Dictionary<string, TValue>();
        }

        #endregion Constructors

        #region IDictionary<string,TValue> Members

        public ICollection<string> Keys
        {
            get { return Dictionary.Keys; }
        }

        public ICollection<TValue> Values
        {
            get { return Dictionary.Values; }
        }

        public TValue this[string key]
        {
            get
            {
                if (Dictionary.ContainsKey(key))
                    return Dictionary[key];
                return default;
            }
            set
            {
                Insert(key, value, false);
            }
        }

        object IObservableDictionary.this[string key] => Dictionary.ContainsKey(key) ? Dictionary[key] : default;

        public void Add(string key, TValue value)
        {
            Insert(key, value, true);
        }

        public bool ContainsKey(string key)
        {
            return Dictionary.ContainsKey(key);
        }

        public bool Remove(string key)
        {
            if (key == null) throw new ArgumentNullException("key");

            TValue value;
            Dictionary.TryGetValue(key, out value);
            var removed = Dictionary.Remove(key);
            if (removed)
                OnItemRemoved(new KeyValuePair<string, TValue>(key, value));

            return removed;
        }

        public bool TryGetValue(string key, out TValue value)
        {
            return Dictionary.TryGetValue(key, out value);
        }

        #endregion IDictionary<string,TValue> Members

        #region ICollection<KeyValuePair<string,TValue>> Members

        public int Count
        {
            get { return Dictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return Dictionary.IsReadOnly; }
        }

        public void Add(KeyValuePair<string, TValue> item)
        {
            Insert(item.Key, item.Value, true);
        }

        public void Clear()
        {
            if (Dictionary.Count > 0)
            {
                var itmes = Dictionary.ToArray();
                Dictionary.Clear();
                OnReset(itmes);
            }
        }

        public bool Contains(KeyValuePair<string, TValue> item)
        {
            return Dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, TValue>[] array, int arrayIndex)
        {
            Dictionary.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<string, TValue> item)
        {
            return Remove(item.Key);
        }

        #endregion ICollection<KeyValuePair<string,TValue>> Members

        #region IEnumerable<KeyValuePair<string,TValue>> Members

        public IEnumerator<KeyValuePair<string, TValue>> GetEnumerator()
        {
            return Dictionary.GetEnumerator();
        }

        #endregion IEnumerable<KeyValuePair<string,TValue>> Members

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Dictionary).GetEnumerator();
        }

        #endregion IEnumerable Members

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion INotifyCollectionChanged Members

        #region INotifyOnChange Members

        public event Action<ChangeNotification> PropertyChanged;

        public IUpdateNotifier UpdateNotifier { get; }

        #endregion INotifyOnChange Members

        public void AddRange(IDictionary<string, TValue> items)
        {
            if (items == null) throw new ArgumentNullException("items");

            if (items.Count > 0)
            {
                if (Dictionary.Count > 0)
                {
                    if (items.Keys.Any((k) => Dictionary.ContainsKey(k)))
                        throw new ArgumentException("An item with the same key has already been added.");
                    else
                        foreach (var item in items) Dictionary.Add(item);
                }
                else
                    _Dictionary = new Dictionary<string, TValue>(items);

                OnItemsAdded(items.ToArray());
            }
        }

        protected virtual void OnPropertyChanged(string propertyName, TValue oldValue)
        {
            PropertyChanged?.Invoke(new ChangeNotification
            {
                PropertyName = propertyName,
                NewValue = this.ContainsKey(propertyName) ? this[propertyName] : default(TValue),
                OldValue = oldValue
            });
        }

        private void Insert(string key, TValue value, bool add)
        {
            if (key == null) throw new ArgumentNullException("key");

            TValue item;
            if (Dictionary.TryGetValue(key, out item))
            {
                if (add) throw new ArgumentException("An item with the same key has already been added.");
                if (Equals(item, value)) return;
                Dictionary[key] = value;

                OnItemsReplaced(new KeyValuePair<string, TValue>(key, value), new KeyValuePair<string, TValue>(key, item));
            }
            else
            {
                Dictionary[key] = value;

                OnItemAdded(new KeyValuePair<string, TValue>(key, value));
            }
        }

        private void OnItemAdded(KeyValuePair<string, TValue> newItem)
        {
            OnPropertyChanged(newItem.Key, default(TValue));
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, newItem));
        }

        private void OnItemRemoved(KeyValuePair<string, TValue> old)
        {
            OnPropertyChanged(old.Key, old.Value);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, old));
        }

        private void OnItemsAdded(
            IList<KeyValuePair<string, TValue>> newItems)
        {
            foreach (var item in newItems)
            {
                OnPropertyChanged(item.Key, default(TValue));
            }

            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, newItems));
        }

        private void OnItemsReplaced(KeyValuePair<string, TValue> newItem, KeyValuePair<string, TValue> oldItem)
        {
            OnPropertyChanged(oldItem.Key, oldItem.Value);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, newItem, oldItem));
        }

        private void OnReset(IList<KeyValuePair<string, TValue>> itmes)
        {
            foreach (var item in itmes)
            {
                OnPropertyChanged(item.Key, item.Value);
            }
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}