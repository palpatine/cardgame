﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.Tools
{
    public class UpdateNotifier : IUpdateNotifier
    {
        private AsyncResult _nextLateUpdateAsyncResult;
        private AsyncResult _nextUpdateAsyncResult;
        private List<Task> _tasks;
        private List<Task> _tasksToAwait = new List<Task>();

        public UpdateNotifier()
        {
            CreateNextUpdateTask();
            CreateNextLateUpdateTask();
        }

        public event Action OnUpdate;

        public Guid? CurrentOperationId { get; private set; }

        public Task NextLateUpdate { get; private set; }

        public Task NextUpdate { get; private set; }

        public async void AddToCurrentOperation(Task task)
        {
            if (CurrentOperationId != null)
            {
                _tasks.Add(task);
            }
            else
            {
                await task;
            }
        }

        public void Await(Task task) => _tasksToAwait.Add(task);

        public void Await(Func<Task> task) => _tasksToAwait.Add(task());

        public async Task StartOperation(Action modifyState) => await StartOperation(async () => { modifyState(); await NextUpdate; });

        public async Task StartOperation(Func<Task> modifyState)
        {
            if (CurrentOperationId != null)
            {
                _tasks.Add(modifyState());
                return;
            }

            _tasks = new List<Task>();
            CurrentOperationId = Guid.NewGuid();
            await modifyState();
            CurrentOperationId = null;
            do
            {
                await NextUpdate;
            } while (_tasks.Any(x => x.Status != TaskStatus.RanToCompletion));

            await Task.WhenAll(_tasks);
        }

        private void CreateNextLateUpdateTask()
        {
            _nextLateUpdateAsyncResult = new AsyncResult();
            NextLateUpdate = Task.Factory.FromAsync(_nextUpdateAsyncResult, x => { });
        }

        private void CreateNextUpdateTask()
        {
            _nextUpdateAsyncResult = new AsyncResult();
            NextUpdate = Task.Factory.FromAsync(_nextUpdateAsyncResult, x => { });
        }

        public void LateUpdate()
        {
            var result = _nextLateUpdateAsyncResult;
            CreateNextLateUpdateTask();
            result.IsCompleted = true;
        }

        public void Update()
        {
            var result = _nextUpdateAsyncResult;
            CreateNextUpdateTask();
            result.IsCompleted = true;
            OnUpdate?.Invoke();
            foreach (var item in _tasksToAwait.ToArray())
            {
                if (item.IsFaulted)
                {
                    throw new AggregateException(item.Exception);
                }
                if (item.IsCompleted || item.IsCanceled)
                {
                    _tasksToAwait.Remove(item);
                }
            }
        }
    }
}