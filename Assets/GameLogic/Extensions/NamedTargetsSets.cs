﻿using System.Collections.Generic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;

public class NamedTargetsSets : Dictionary<string, IEnumerable<ITarget>>
{
    public void Add(string name, ITarget target)
    {
        Add(name, new[] { target });
    }

    public override string ToString()
    {
        return this.JoinAsString(x => $"{x.Key}:{x.Value.JoinAsString(z => z.Id.ToString(), " ")}");
    }
}