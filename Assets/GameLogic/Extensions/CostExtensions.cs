﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;

public static class CostExtensions
{
    public static StatisticCost GetCost(this IEnumerable<EffectResult> results, IUnit unit)
    {
        return new StatisticCost(results
                .FirstOrDefault(x => x.Target == unit)?.StatisticsChange
                .Where(x => x.Key.valueName == Constants.StatisitcValues.Current)
                .ToDictionary(x => x.Key.statistic, x => -x.Value));
    }

    public static bool HasStatisitcsToCoverCost(this IUnit unit, IEnumerable<EffectResult> results)
    {
        return HasStatisitcsToCoverCost(
            unit,
            GetCost(results, unit));
    }

    public static bool HasStatisitcsToCoverCost(this IUnit unit, StatisticCost statisticsCost)
    {
        return statisticsCost.All(x => unit.GetStatistic(x.Key).Current >= x.Value);
    }

    public static void IncrementCost(this Dictionary<string, int> statisticsCost, string statistic)
    {
        if (!statisticsCost.ContainsKey(statistic))
        {
            statisticsCost.Add(statistic, 1);
        }
        else
        {
            statisticsCost[statistic]++;
        }
    }

    //todo: there can be cost in items
    public static StatisticCost MergeCost(this Dictionary<string, int> statisticsCosts, params Dictionary<string, int>[] other)
    {
        return other.Append(statisticsCosts).MergeCost();
    }

    public static StatisticCost MergeCost(this IEnumerable<Dictionary<string, int>> statisticsCosts)
    {
        return new StatisticCost(statisticsCosts
            .Where(x => x != null)
            .SelectMany(x => x)
            .GroupBy(x => x.Key)
            .ToDictionary(x => x.Key, x => x.Sum(z => z.Value)));
    }
}

public class StatisticCost : Dictionary<string, int>
{
    public StatisticCost(IDictionary<string, int> dictionary) : base(dictionary ?? new Dictionary<string, int>())
    {
    }

    public StatisticCost()
    {
    }

    public void Apply(string name, int value)
    {
        if (!ContainsKey(name))
        {
            Add(name, value);
        }
        else
        {
            this[name] += value;
        }
    }
}