﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;

public static class ActionTraceFilterExtensions
{
    public static object GetValue(
        this ActionTraceFilter filter,
        GameState gameState,
        Dictionary<string, IEnumerable<ITarget>> targets)
    {
        var target = filter.Target?.ResolveTargets(gameState, targets);

        for (int i = gameState.GameTrace.Actions.Count - 1; i > 0; i--)
        {
            var action = gameState.GameTrace.Actions[i];
            if ((target == null || target.Contains(action.Target))
                && (filter.Action == null || filter.Action == action.Action)
                && (!filter.HasStatisticChange.HasValue || filter.HasStatisticChange == (action.StatisticChange != null)))
            {
                return typeof(ActionTrace).GetProperty(filter.Get).GetValue(action);
            }
        }

        return null;
    }
}