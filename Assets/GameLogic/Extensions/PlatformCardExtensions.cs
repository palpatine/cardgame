﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;

public static class PlatformCardExtensions
{
    public static IReadOnlyCollection<PlatformCard> AllVisiblePlatfomrsToLeft(this PlatformCard card, GameState gameState)
    {
        var list = new List<PlatformCard>();
        var level = gameState.Levels[card.Level];
        var cardIndexInLevel = level.Platforms.IndexOf(card);

        var id = cardIndexInLevel - 1;
        while (id >= 0)
        {
            var platformCard = level.Platforms[id];
            if (platformCard.Obstacle != null && platformCard.Obstacle.Settings.IsBlockingVisibility)
            {
                list.Add(platformCard);
                break;
            }

            list.Add(platformCard);

            id--;
        }

        if (level.IsLocked)
        {
            id = level.Platforms.Count - 1;
            do
            {
                var platformCard = level.Platforms[id];
                if (platformCard.Obstacle != null && platformCard.Obstacle.Settings.IsBlockingVisibility)
                {
                    list.Add(platformCard);
                    break;
                }

                list.Add(platformCard);

                id--;
            } while (id > cardIndexInLevel);
        }

        return list;
    }

    public static IReadOnlyCollection<PlatformCard> AllVisiblePlatfomrsToRight(this PlatformCard card, GameState gameState)
    {
        var list = new List<PlatformCard>();
        var level = gameState.Levels[card.Level];
        var cardIndexInLevel = level.Platforms.IndexOf(card);

        var id = cardIndexInLevel + 1;
        while (id < level.Platforms.Count)
        {
            var platformCard = level.Platforms[id];
            if (platformCard.Obstacle != null && platformCard.Obstacle.Settings.IsBlockingVisibility)
            {
                list.Add(platformCard);
                break;
            }

            list.Add(platformCard);

            id++;
        }

        if (level.IsLocked)
        {
            id = 0;
            do
            {
                var platformCard = level.Platforms[id];
                if (platformCard.Obstacle != null && platformCard.Obstacle.Settings.IsBlockingVisibility)
                {
                    list.Add(platformCard);
                    break;
                }

                list.Add(platformCard);

                id++;
            } while (id < cardIndexInLevel);
        }

        return list;
    }

    public static ILocation LocationAfterLastUnit(this PlatformCard card, bool isFacingLeft)
    {
        var toTheLeftOfObstacle = card.Obstacle == null;
        return new LocationDescriptor
        {
            PlatformCard = card,
            CurrentLevelId = card.Level,
            UnitId =
                    toTheLeftOfObstacle ? card.UnitsToLeftOfObstacle.Count()
                    : card.UnitsToRightOfObstacle.Count(),
            IsToTheLeftOfObstacle = toTheLeftOfObstacle,
            IsFacingLeft = isFacingLeft
        };
    }

    public static ILocation LocationAfterLastUnitToTheLeftOfObstacle(this PlatformCard card, bool isFacingLeft)
    {
        return new LocationDescriptor
        {
            PlatformCard = card,
            CurrentLevelId = card.Level,
            UnitId =
                    card.UnitsToLeftOfObstacle.Count(),
            IsToTheLeftOfObstacle = true,
            IsFacingLeft = isFacingLeft
        };
    }

    public static ILocation LocationAsFirstUnit(this PlatformCard card, bool isFacingLeft)
    {
        return new LocationDescriptor
        {
            PlatformCard = card,
            CurrentLevelId = card.Level,
            UnitId = 0,
            IsToTheLeftOfObstacle = true,
            IsFacingLeft = isFacingLeft
        };
    }

    public static PlatformCard PlatformTo(this PlatformCard platform, GameState gameState, bool left)
    {
        if (left)
        {
            return platform.PlatformToLeft(gameState);
        }

        return platform.PlatformToRight(gameState);
    }

    public static PlatformCard PlatformToLeft(this PlatformCard card, GameState gameState)
    {
        var level = gameState.Levels[card.Level];
        var cardIndexInLevel = level.Platforms.IndexOf(card);
        if (cardIndexInLevel > 0 || level.IsLocked)
        {
            var id = (cardIndexInLevel - 1 + level.Platforms.Count()) % level.Platforms.Count();
            return level.Platforms[id];
        }

        return null;
    }

    public static PlatformCard PlatformToRight(this PlatformCard card, GameState gameState)
    {
        var level = gameState.Levels[card.Level];
        var cardIndexInLevel = level.Platforms.IndexOf(card);
        if (cardIndexInLevel < level.Platforms.Count() - 1 || level.IsLocked)
        {
            var id = (cardIndexInLevel + 1) % level.Platforms.Count();
            return level.Platforms[id];
        }

        return null;
    }

    /// <summary>
    ///   CCFFF -> fromRight true = FFFCC
    ///   CCFFF -> fromRight false = CCFFF
    ///   CCFOFF -> fromRight false = CCF
    ///   CCFOFF -> fromRight true = FF
    /// </summary>
    /// <param name="card"></param>
    /// <param name="fromRight"></param>
    /// <returns></returns>
    public static IReadOnlyCollection<IUnit> UnitsVisibleTo(this PlatformCard card, bool toLeft)
    {
        if (toLeft)
        {
            if (card.Obstacle == null || !card.Obstacle.Settings.IsBlockingVisibility)
            {
                return card.AllUnits.Reverse().ToArray();
            }
            else
            {
                return card.UnitsToRightOfObstacle.Reverse().ToArray();
            }
        }
        else
        {
            if (card.Obstacle == null || !card.Obstacle.Settings.IsBlockingVisibility)
            {
                return card.AllUnits.ToArray();
            }
            else
            {
                return card.UnitsToLeftOfObstacle.ToArray();
            }
        }
    }
}