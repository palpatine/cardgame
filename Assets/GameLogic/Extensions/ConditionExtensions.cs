﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class ConditionExtensions
{
    public static object GetValue(
        this ConditionValueSettings value,
        GameState gameState,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        if (value.Group != null)
        {
            return value.Group.IsMeet(gameState, targets, currentEffects, parameters);
        }

        return ValueSettingsExtensions.GetValue(value, gameState, targets, currentEffects, parameters);
    }

    public static bool IsMeet(
        this ConditionGroupSettings condition,
        GameState gameState,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        bool result;
        if (condition.IsOr)
        {
            result = condition.Conditions.Any(x => x.IsMeet(gameState, targets, currentEffects, parameters));
        }

        result = condition.Conditions.All(x => x.IsMeet(gameState, targets, currentEffects, parameters));

        return result;
    }

    public static bool IsMeet(
        this ConditionSettings condition,
        GameState gameState,
        IEnumerable<ITarget> targets)
    {
        return condition.IsMeet(
            gameState,
            targets,
            Array.Empty<EffectResult>(),
            new Dictionary<string, int>());
    }

    public static bool IsMeet(
        this ConditionSettings condition,
        GameState gameState,
        IEnumerable<ITarget> targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        var conditionTargets = new NamedTargetsSets { { Constants.ValueSources.Targets, targets } };
        return condition.IsMeet(gameState, conditionTargets, currentEffects, parameters);
    }

    public static bool IsMeet(
        this ConditionSettings condition,
        GameState gameState,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        if (condition == null)
        {
            return true;
        }

        var leftValue = condition.LeftValue.GetValue(gameState, targets, currentEffects, parameters);
        var rightValue = condition.RightValue.GetValue(gameState, targets, currentEffects, parameters);

        switch (condition.Compare)
        {
            case Constants.Compare.LowerThen:
                return (decimal)Convert.ChangeType(leftValue, typeof(decimal)) < (decimal)Convert.ChangeType(rightValue, typeof(decimal));

            case Constants.Compare.LowerThenOrEqual:
                return (decimal)Convert.ChangeType(leftValue, typeof(decimal)) <= (decimal)Convert.ChangeType(rightValue, typeof(decimal));

            case Constants.Compare.GreaterThen:
                return (decimal)Convert.ChangeType(leftValue, typeof(decimal)) > (decimal)Convert.ChangeType(rightValue, typeof(decimal));

            case Constants.Compare.GreaterThenOrEqual:
                return (decimal)Convert.ChangeType(leftValue, typeof(decimal)) >= (decimal)Convert.ChangeType(rightValue, typeof(decimal));

            case Constants.Compare.Equal:
                return Equals(leftValue, rightValue);

            case Constants.Compare.NotEqual:
                return !Equals(leftValue, rightValue);

            case Constants.Compare.Contains:
                return ((IEnumerable<object>)leftValue).Any(x => Equals(x, rightValue));

            case Constants.Compare.NotContains:
                return ((IEnumerable<object>)leftValue).All(x => !Equals(x, rightValue));
        }

        throw new InvalidOperationException($"invalid operator {condition.Compare}");
    }
}

public static class ValueSettingsExtensions
{
    public static object GetValue(
        this ValueSettings value,
        GameState gameState,
        IEnumerable<IUnit> targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        return GetValue(
            value,
            gameState,
            new NamedTargetsSets { { Constants.ValueSources.Targets, targets } },
            currentEffects,
            parameters);
    }

    public static object GetValue(
        this ValueSettings value,
        GameState gameState,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        if (value.PreviousAction != null)
        {
            var result = value.PreviousAction.GetValue(gameState, targets);
            return NegateIfRequired(value, result);
        }

        if (value.Value != null)
        {
            return ProcessRawValue(value);
        }

        if (!string.IsNullOrEmpty(value.Operation))
        {
            return ProcessOperation(value, gameState, targets, currentEffects, parameters);
        }

        if (value.Selected != null)
        {
            return ProcessSelectedParameter(value, parameters);
        }

        var source = value.Source.ResolveTargets(gameState, targets);

        if (source == null || !source.Any())
        {
            return null;
        }

        if (value.Statistic != null)
        {
            return GetStatisticValue(value, source);
        }

        if (value.StatisticsChange != null)
        {
            return GetStatisiticChangeValue(value, currentEffects, source);
        }

        if (value.Equipment != null)
        {
            return GetEquipmentValue(value, source);
        }

        return source.Single();
    }

    private static object GetEquipmentValue(ValueSettings value, IEnumerable<ITarget> source)
    {
        var character = (Character)source.Single();
        return value.Equipment.GetEquipmentProperty(character);
    }

    private static object GetStatisiticChangeValue(ValueSettings value, IEnumerable<EffectResult> currentEffects, IEnumerable<ITarget> source)
    {
        var unit = ((IUnit)source.Single());
        var result = currentEffects.SingleOrDefault(x => x.Target == unit)?.StatisticsChange[value.StatisticsChange] ?? 0;
        if (value.IsNegative)
            return -result;
        else
            return result;
    }

    private static object GetStatisticValue(ValueSettings value, IEnumerable<ITarget> source)
    {
        var result = ((IUnit)source.Single()).GetStatistic(value.Statistic.Name).GetValue(value.Statistic.Value);
        if (value.IsNegative)
            return -result;
        else
            return result;
    }

    private static object NegateIfRequired(ValueSettings value, object result)
    {
        if (value.IsNegative && result is int i) return -i;
        if (value.IsNegative && result is decimal d) return -d;

        if (value.IsNegative)
            throw new InvalidOperationException();

        return result;
    }

    private static object ProcessOperation(ValueSettings value, GameState gameState, NamedTargetsSets targets, IEnumerable<EffectResult> currentEffects, Dictionary<string, int> parameters)
    {
        if (value.Operation == Constants.ValuesOperations.Max)
        {
            var result = value.Values.Select(x => x.GetValue(gameState, targets, currentEffects, parameters)).Max();

            return NegateIfRequired(value, result);
        }

        if (value.Operation == Constants.ValuesOperations.Min)
        {
            var result = value.Values.Select(x => x.GetValue(gameState, targets, currentEffects, parameters)).Min();
            return NegateIfRequired(value, result);
        }

        if (value.Operation == Constants.ValuesOperations.Sum)
        {
            return value.Values.Select(x => x.GetValue(gameState, targets, currentEffects, parameters)).Aggregate((decimal)0, (x, y) =>
            {
                if (y is int i) return x + i;
                if (y is decimal d) return x + d;
                throw new InvalidOperationException($"To use sum all values must be a numbers {y}");
            });
        }

        throw new NotSupportedException($"Operation kind not supported {value.Operation}.");
    }

    private static object ProcessRawValue(ValueSettings value)
    {
        if (value.Value == "none")
            return null;

        if (decimal.TryParse(value.Value, out var d))
            return d * (value.IsNegative ? -1 : 1);

        if (bool.TryParse(value.Value, out var b))
        {
            if (value.IsNegative)
                return !b;
            return b;
        }

        if (value.IsNegative)
            throw new InvalidOperationException();

        return value.Value;
    }

    private static object ProcessSelectedParameter(ValueSettings value, Dictionary<string, int> parameters)
    {
        var result = parameters[value.Selected];
        if (value.IsNegative)
            return -result;
        else
            return result;
    }
}