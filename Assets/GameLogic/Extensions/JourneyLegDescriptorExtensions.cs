﻿using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;

public static class JourneyLegDescriptorExtensions
{
    public static bool IsMovingLeft(this JourneyLegDescriptor leg, GameState gameState)
    {
        if (leg.From.PlatformCard == leg.To.PlatformCard)
        {
            return leg.From.IsToTheLeftOfObstacle == leg.To.IsToTheLeftOfObstacle && leg.From.UnitId > leg.To.UnitId
                || leg.From.IsToTheLeftOfObstacle != leg.To.IsToTheLeftOfObstacle && leg.To.IsToTheLeftOfObstacle;
        }

        var currentPlatformCard = leg.From.PlatformCard;
        var currentLevel = gameState.Levels[leg.From.CurrentLevelId];
        var currentPlatformIndex = currentLevel.Platforms.IndexOf(currentPlatformCard);

        return currentPlatformIndex > 0
            && leg.To.PlatformCard == currentLevel.Platforms[currentPlatformIndex - 1];
    }

    public static bool IsMovingRight(this JourneyLegDescriptor leg, GameState gameState)
    {
        if (leg.From.PlatformCard == leg.To.PlatformCard)
        {
            return leg.From.IsToTheLeftOfObstacle == leg.To.IsToTheLeftOfObstacle && leg.From.UnitId < leg.To.UnitId
                || leg.From.IsToTheLeftOfObstacle != leg.To.IsToTheLeftOfObstacle && !leg.To.IsToTheLeftOfObstacle;
        }

        var currentPlatformCard = leg.From.PlatformCard;
        var currentLevel = gameState.Levels[leg.From.CurrentLevelId];
        var currentPlatformIndex = currentLevel.Platforms.IndexOf(currentPlatformCard);

        return currentPlatformIndex < currentLevel.Platforms.Count - 1
            && leg.To.PlatformCard == currentLevel.Platforms[currentPlatformIndex + 1];
    }
}