﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class FoeStateExtensions
{
    public static IReadOnlyCollection<ActionSettings> AllActions(this Foe foe)
    {
        return foe.Settings.Actions
            .Concat(foe.Actions)
            .ToArray();
    }
}