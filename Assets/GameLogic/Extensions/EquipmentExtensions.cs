﻿using System;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class EquipmentExtensions
{
    public static object GetEquipmentProperty(this EquipmentSelector value, Character character)
    {
        if (!character.Equipment.ContainsKey(value.Kind))
            return null;

        var item = character.Equipment[value.Kind];

        if (value.Property == Constants.EquipmentSelectorProperties.Item)
            return item;

        if (value.Property == Constants.EquipmentSelectorProperties.Kinds)
            return item?.Settings?.Kinds;

        if (value.Property == Constants.EquipmentSelectorProperties.Name)
            return item?.Settings?.Name;

        if (value.Property == Constants.EquipmentSelectorProperties.Effects)
            return item?.Settings?.Effects;

        if (value.Property == Constants.EquipmentSelectorProperties.FirstAction)
            return item?.Settings?.Actions?.FirstOrDefault();

        throw new NotSupportedException($"Equipment Value Property was {value.Property}");
    }
}