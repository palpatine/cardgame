﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class TargetSelectorExtensions
{
    public static IEnumerable<ITarget> ResolveTargets(
        this TargetSelector targetSelector,
        GameState gameState,
        Dictionary<string, IEnumerable<ITarget>> targets)
    {
        IUnit source = null;

        if (targetSelector == null
            || targetSelector.Name == Constants.ValueSources.Targets
            || targetSelector.Name == Constants.ValueSources.Target)
        {
            return targets[Constants.ValueSources.Targets];
        }
        else if (targetSelector.Name == Constants.ValueSources.Characters)
        {
            return gameState.Units.OfType<Character>().ToArray();
        }
        else if (targetSelector.Name == Constants.ValueSources.CurrentUnit)
        {
            source = gameState.CurrentUnit;
        }
        else if (targetSelector.Name == Constants.ValueSources.ActingUnit)
        {
            if (targets.ContainsKey(Constants.ValueSources.ActingUnit))
                return targets[Constants.ValueSources.ActingUnit];

            source = gameState.CurrentUnit;
        }
        else if (targetSelector.Name == Constants.ValueSources.UnitByOffset)
        {
            if (targetSelector.IdOffset == 0)
            {
                source = gameState.CurrentUnit;
            }
            else if (targetSelector.IdOffset < 0 || gameState.CurrentUnit.Location.IsFacingLeft)
            {
                source = gameState.CurrentUnit.VisibleUnitsToLeft()
                   .ElementAtOrDefault(Math.Abs(targetSelector.IdOffset) - 1);
            }
            else if (targetSelector.IdOffset > 0 || !gameState.CurrentUnit.Location.IsFacingLeft)
            {
                source = gameState.CurrentUnit.VisibleUnitsToRight()
                   .ElementAtOrDefault(Math.Abs(targetSelector.IdOffset) - 1);
            }
        }
        else
        {
            throw new NotImplementedException();
        }

        if (source != null)
            return new[] { source };

        return null;
    }
}