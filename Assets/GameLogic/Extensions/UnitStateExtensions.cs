﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class UnitStateExtensions
{
    public static IUnit AdiecentFacedVisibleUnit(this IUnit unit)
    {
        if (unit.Location.IsFacingLeft)
            return unit.AdiecentVisibleUnitToLeft();
        else
            return unit.AdiecentVisibleUnitToRight();
    }

    public static IUnit AdiecentVisibleUnitToLeft(this IUnit unit)
    {
        return unit.Location.AdiecentVisibleUnitToLeft();
    }

    public static IUnit AdiecentVisibleUnitToRight(this IUnit unit)
    {
        return unit.Location.AdiecentVisibleUnitToRight();
    }

    public static IReadOnlyCollection<ActionSettings> AllActions(this IUnit unit)
    {
        if (unit is Character character)
            return character.AllActions();
        else if (unit is Foe foe)
            return foe.AllActions();

        throw new NotSupportedException();
    }

    public static IReadOnlyCollection<PlatformCard> AllFacedVisiblePlatfomrs(this IUnit unit, GameState gameState)
    {
        return unit.Location.AllFacedVisiblePlatfomrs(gameState);
    }

    public static IReadOnlyCollection<IUnit> AllFacedVisibleUnits(this IUnit unit, GameState gameState)
    {
        if (unit.Location.IsFacingLeft)
            return unit.AllVisibleUnitsToLeft(gameState);
        else
            return unit.AllVisibleUnitsToRight(gameState);
    }

    public static IReadOnlyCollection<IUnit> AllVisibleUnitsToLeft(this IUnit unit, GameState gameState)
    {
        return unit.Location.AllVisibleUnitsToLeft(gameState);
    }

    public static IReadOnlyCollection<IUnit> AllVisibleUnitsToRight(this IUnit unit, GameState gameState)
    {
        return unit.Location.AllVisibleUnitsToRight(gameState);
    }

    public static bool CanBlock(this IUnit unit)
    {
        return unit.Energy.Current != 0 && unit.Defense.Current != 0 && unit.Block.Current != 0;
    }

    public static bool CanMove(this IUnit unit)
    {
        return unit.Energy.Current != 0 && unit.Movement.Current != 0;
    }

    public static IReadOnlyCollection<IUnit> FacedVisibleUnits(this IUnit unit)
    {
        if (unit.Location.IsFacingLeft)
            return unit.VisibleUnitsToLeft();
        else
            return unit.VisibleUnitsToRight();
    }

    public static List<Ability> GetAbilities(
        this IUnit unit,
        string triggerName,
        IEnumerable<EffectResult> effects,
        NamedTargetsSets targetSets,
        GameState gameState,
        IEnumerable<string> kinds = null)
    {
        return unit.Abilities.Where(x => x.Settings.Trigger.Name == triggerName
                                && (kinds == null || kinds.Contains(x.Settings.Trigger.Kind))
                                && x.Settings.Condition.IsMeet(gameState, targetSets, effects, x.Parameters))
                                .ToList();
    }

    public static bool IsAtLeftEdgeOfPlatform(this IUnit unit)
    {
        return unit.Location.IsAtLeftEdgeOfPlatform();
    }

    public static bool IsAtRightEdgeOfPlatform(this IUnit unit)
    {
        return unit.Location.IsAtRightEdgeOfPlatform();
    }

    public static PlatformCard NextFacedPlatform(this IUnit unit, GameState gameState)
    {
        return unit.Location.NextFacedPlatform(gameState);
    }

    public static PlatformCard PlatformToLeft(this IUnit unit, GameState gameState)
    {
        return unit.Location.PlatformToLeft(gameState);
    }

    public static PlatformCard PlatformToRight(this IUnit unit, GameState gameState)
    {
        return unit.Location.PlatformToRight(gameState);
    }

    public static IReadOnlyCollection<IUnit> VisibleUnitsToLeft(this IUnit unit)
    {
        return unit.Location.VisibleUnitsToLeft();
    }

    public static IReadOnlyCollection<IUnit> VisibleUnitsToRight(this IUnit unit)
    {
        return unit.Location.VisibleUnitsToRight();
    }
}