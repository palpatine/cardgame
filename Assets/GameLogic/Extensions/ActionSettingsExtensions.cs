﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Messages;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class ActionSettingsExtensions
{
    public static async Task<(IEnumerable<EffectResult> effect, StatisticCost cost)> CalculateEffectsAsync(
        this ActionSettings action,
        IUnit actor,
        IEnumerable<IUnit> targets,
         bool staticAnalisys,
         bool canCancel,
         IGameStateProvider gameStateProvider)
    {
        var actorEffect = new EffectResult(actor);
        IEnumerable<EffectResult> effects;
        var selectorsSettings = new List<SelectorSettings>(action.Selectors);

        var actionsToCheck = new Stack<ActionSettings>();
        actionsToCheck.Push(action);
        var allEffects = new List<EffectSettings>();
        while (actionsToCheck.Any())
        {
            var checkedAction = actionsToCheck.Pop();
            if (checkedAction.Import != null)
            {
                var importedAction = (ActionSettings)checkedAction.Import
                    .GetEquipmentProperty((Character)actor);
                selectorsSettings.AddRange(importedAction.Selectors); // todo: this can cause problems if imported actions contain parameters with the same names
                actionsToCheck.Push(importedAction);
            }

            allEffects.AddRange(checkedAction.Effects);
        }

        var (parameters, shouldCancel) = await GetParameters(
            targets,
            staticAnalisys,
            canCancel,
            gameStateProvider,
            actorEffect,
            selectorsSettings);

        if (shouldCancel)
        {
            return (null, null);
        }

        effects = allEffects
               .DetermineResult(parameters, gameStateProvider, targets, new[] { actorEffect });

        var targetSets = new NamedTargetsSets
            {
                { Constants.ValueSources.Targets, targets },
            };

        effects = actor.CalculateTriggeredEffects(
            Constants.Triggers.ExecutesAction,
            effects,
            targetSets,
            gameStateProvider,
            action.Kinds);

        var cost = GetActorCost(actor, effects);

        foreach (var target in targets)
        {
            var sets = new NamedTargetsSets
                {
                    { Constants.ValueSources.Targets, targets },
                };
            effects = CalculateTriggeredEffects(
                target,
                Constants.Triggers.Targeted,
                effects,
                sets,
                gameStateProvider);
        }

        return (effects, cost);
    }

    public static IEnumerable<EffectResult> CalculateTriggeredEffects(
        this IUnit actor,
        string triggerName,
        IEnumerable<EffectResult> effects,
        NamedTargetsSets targetSets,
        IGameStateProvider gameStateProvider,
        IEnumerable<string> kinds = null)
    {
        targetSets.Add(Constants.ValueSources.ActingUnit, actor);
        var abilitiesUtilized = new List<Ability>();

        var abilities = actor.GetAbilities(
            triggerName, effects, targetSets, gameStateProvider.GameState, kinds);
        abilitiesUtilized.AddRange(abilities);
        effects = abilities.DetermineResult(gameStateProvider, targetSets, effects);

        do
        {
            abilities = actor.Abilities
                  .Where(x => abilities.Any(z => z.Settings.Name == x.Settings.Trigger.Name)
                        && x.Settings.Condition.IsMeet(gameStateProvider.GameState, targetSets, effects, x.Parameters))
                  .ToList();
            abilitiesUtilized.AddRange(abilities);
            effects = abilities.DetermineResult(gameStateProvider, targetSets, effects);
        } while (abilities.Any());

        var actorEffect = effects.SingleOrDefault(x => x.Target == actor);
        if (actorEffect == null)
        {
            actorEffect = new EffectResult(actor);
            effects = effects.Concat(new[] { actorEffect }).ToArray();
        }

        actorEffect.UtilizedAbiliites.AddRange(abilitiesUtilized);
        return effects;
    }

    private static StatisticCost GetActorCost(IUnit actor, IEnumerable<EffectResult> result)
    {
        var cost = result.Single(x => x.Target == actor).StatisticsChange
            .Where(x => x.Key.valueName == Constants.StatisitcValues.Current)
            .ToDictionary(x => x.Key.statistic, x => -x.Value);

        return new StatisticCost(cost);
    }

    private static async Task<(Dictionary<string, int>, bool)> GetParameters(
                IEnumerable<IUnit> targets,
        bool staticAnalisys,
        bool canCancel,
        IGameStateProvider gameStateProvider,
        EffectResult actorEffect,
        IEnumerable<SelectorSettings> selectorsSettings)
    {
        var selectors = selectorsSettings.Select(x =>
        {
            var min = x.Min.GetValue(gameStateProvider.GameState, targets, new[] { actorEffect }, null);
            var max = x.Max.GetValue(gameStateProvider.GameState, targets, new[] { actorEffect }, null);
            var increment = x.Increment.GetValue(gameStateProvider.GameState, targets, new[] { actorEffect }, null);
            return new SelectedValue
            {
                SelectorSetting = x,
                Min = (int)Convert.ChangeType(min, typeof(int)),
                Max = (int)Convert.ChangeType(max, typeof(int)),
                Increment = (int)Convert.ChangeType(increment, typeof(int)),
            };
        }).ToArray();

        Dictionary<string, int> parameters;
        if (selectors?.Any() == true)
        {
            if (staticAnalisys)
            {
                foreach (var item in selectors)
                {
                    item.Value = item.Min;
                }
            }
            else
            {
                var data = new SelectValues { Selectors = selectors, CanCancel = canCancel };
                await gameStateProvider.Publish(data);
                if (data.Cancelled && canCancel)
                {
                    return (null, true);
                }
            }

            parameters = selectors.ToDictionary(x => x.SelectorSetting.Name, x => x.Value);
        }
        else
        {
            parameters = new Dictionary<string, int>();
        }

        return (parameters, false);
    }
}
