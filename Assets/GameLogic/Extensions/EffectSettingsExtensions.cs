﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;

public static class EffectSettingsExtensions
{
    private static Dictionary<string, int> EmptyParameters = new Dictionary<string, int>();

    public static IEnumerable<EffectResult> DetermineResult(
            this IEnumerable<EffectSettings> effects,
            Dictionary<string, int> parameters,
            IGameStateProvider gameStateProvider,
            IEnumerable<ITarget> targets,
            IEnumerable<EffectResult> currentEffects = null)
    {
        return DetermineResult(
         effects.Select(x => (x, parameters)).ToList(),
         gameStateProvider,
         new NamedTargetsSets { { Constants.ValueSources.Targets, targets } },
         currentEffects,
         false);
    }

    public static IEnumerable<EffectResult> DetermineResult(
        this IEnumerable<EffectSettings> effects,
        IGameStateProvider gameStateProvider,
        IEnumerable<ITarget> targets,
        IEnumerable<EffectResult> currentEffects = null)
    {
        return DetermineResult(
             effects.Select(x => (x, EmptyParameters)).ToList(),
             gameStateProvider,
             new NamedTargetsSets { { Constants.ValueSources.Targets, targets } },
             currentEffects,
             false);
    }

    public static IEnumerable<EffectResult> DetermineResult(
        this IEnumerable<Ability> abilities,
        IGameStateProvider gameStateProvider,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects = null,
        bool reverse = false)
    {
        return DetermineResult(
             abilities.SelectMany(x => x.Settings.Effects.Select(y => (y, x.Parameters))).ToList(),
             gameStateProvider,
             targets,
             currentEffects,
             reverse);
    }

    public static IEnumerable<EffectResult> DetermineResult(
        IEnumerable<(EffectSettings effect, Dictionary<string, int> parameters)> effects,
        IGameStateProvider gameStateProvider,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects = null,
        bool reverse = false)
    {
        var current = currentEffects?.ToList() ?? new List<EffectResult>();
        foreach (var (effect, parameters) in effects)
        {
            current = DetermineResult(
                gameStateProvider,
                targets,
                currentEffects,
                reverse,
                current,
                effect,
                parameters);
        }

        return current;
    }

    public static IEnumerable<EffectResult> DetermineResultReverse(
            this IEnumerable<EffectSettings> effects,
            IGameStateProvider gameStateProvider,
            IEnumerable<ITarget> targets,
            IEnumerable<EffectResult> currentEffects = null)
    {
        return DetermineResult(
             effects.Select(x => (x, EmptyParameters)).ToList(),
             gameStateProvider,
             new NamedTargetsSets { { Constants.ValueSources.Targets, targets } },
             currentEffects,
             true);
    }

    public static bool MeetsCondition(
                            this EffectSettings effect,
        GameState gameState,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        return effect.Condition.IsMeet(gameState, targets, currentEffects, parameters);
    }

    public static IEnumerable<EffectResult> MergeResult(
        this IEnumerable<IEnumerable<EffectResult>> effects)
    {
        var result = effects.First().ToList();

        foreach (var other in effects.Skip(1))
        {
            result = result
                .FullOuterJoin(
                other,
                x => x.Target,
                x => x.Target,
                (x, y, key) => x == null ? y : y == null ? x : EffectResult.Merge(x, y))
              .ToList();
        }

        return result;
    }

    private static void CalculateParameters(
        Ability ability,
        EffectSettings effect,
        IGameStateProvider gameStateProvider,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        Dictionary<string, int> parameters)
    {
        foreach (var parameter in effect.Parameters)
        {
            var rawValue = parameter.Value.GetValue(gameStateProvider.GameState, targets, currentEffects, parameters);
            var value = (int)(parameter.Scale * int.Parse(rawValue.ToString()));
            ability.Parameters.Add(parameter.Name, value);
        }
    }

    private static List<EffectResult> DetermineResult(
            IGameStateProvider gameStateProvider,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        bool reverse,
        List<EffectResult> current,
        EffectSettings effect,
        Dictionary<string, int> parameters)
    {
        var effectTargets = effect.Target.ResolveTargets(gameStateProvider.GameState, targets);
        if (effectTargets != null && effect.MeetsCondition(gameStateProvider.GameState, targets, currentEffects, parameters))
        {
            if (effect.Invulnerability == true)
            {
                current = current.Where(x => !effectTargets.Contains(x.Target)).ToList();
                targets[Constants.ValueSources.Targets]
                    = targets[Constants.ValueSources.Targets].Except(effectTargets).ToArray();
            }
            else
            {
                foreach (var target in effectTargets)
                {
                    DetermineResult(
                        gameStateProvider,
                        targets,
                        currentEffects,
                        reverse,
                        current,
                        effect,
                        parameters,
                        target);
                }
            }
        }

        return current;
    }

    private static void DetermineResult(
        IGameStateProvider gameStateProvider,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        bool reverse,
        List<EffectResult> current,
        EffectSettings effect,
        Dictionary<string, int> parameters,
        ITarget target)
    {
        var targetResult = current.SingleOrDefault(x => x.Target == target);
        if (targetResult == null)
        {
            targetResult = new EffectResult(target);
            current.Add(targetResult);
        }

        var unit = target as IUnit;
        if (effect.Statistic != null)
        {
            var value = GetDeltaValue(gameStateProvider, targets, currentEffects, effect, parameters, reverse);

            if (unit != null)
            {
                targetResult.StatisticsChange.Add(
                    effect.Statistic.Name,
                    effect.Statistic.Value,
                    value);
            }
            else
            {
                throw new NotImplementedException(); // todo: magic rod and fish will be a target
            }
        }
        else if (effect.Ability != null)
        {
            if (unit == null)
            {
                throw new InvalidOperationException();
            }

            var ability = gameStateProvider.GameSettings.Abilities.Single(x => x.Name == effect.Ability);
            if (reverse)
            {
                targetResult.RemoveAbilities.Add(ability);
            }
            else
            {
                var item = new Ability
                {
                    Settings = ability,
                    RemainingDuration = effect.Duration,
                    RemainingUseCount = effect.UseCount
                };
                CalculateParameters(item, effect, gameStateProvider, targets, currentEffects, parameters);
                targetResult.AddAbilities.Add(item);
            }
        }
        else if (effect.Action != null)
        {
            if (unit == null)
            {
                throw new InvalidOperationException();
            }
            if (reverse)
            {
                targetResult.RemoveActions.Add(effect.Action);
            }
            else
            {
                targetResult.AddActions.Add(effect.Action);
            }
        }
        else if (effect.Equipment != null)
        {
            var value = GetDeltaValue(gameStateProvider, targets, currentEffects, effect, parameters, reverse);

            var itemSettings = gameStateProvider.GameSettings.Items.Single(x => x.Name == effect.Equipment);
            if (value > 0)
            {
                targetResult.AddItems.AddRange(Enumerable.Repeat(itemSettings, value));
            }
            else
            {
                targetResult.RemoveItems.AddRange(Enumerable.Repeat(itemSettings, value));
            }
        }
        else if (effect.RuneCard == true)
        {
            var value = GetDeltaValue(gameStateProvider, targets, currentEffects, effect, parameters, reverse);
            targetResult.DrawRuneCards = value;
        }
        else if (effect.EndTurn == true)
        {
            targetResult.EndTurn = true;
        }
        else if (effect.NoRestoration == true)
        {
            targetResult.NoRestoration = true;
        }
    }

    private static int GetDeltaValue(
        IGameStateProvider gameStateProvider,
        NamedTargetsSets targets,
        IEnumerable<EffectResult> currentEffects,
        EffectSettings effect,
        Dictionary<string, int> parameters,
        bool reverse)
    {
        int value;

        if (effect.ParameterForDelta != null)
        {
            value = parameters[effect.ParameterForDelta];
        }
        else
        {
            var rawValue = effect.Delta.GetValue(gameStateProvider.GameState, targets, currentEffects, parameters);
            value = int.Parse(rawValue.ToString());
        }

        if (effect.IsNegative == true)
        {
            value *= -1;
        }

        if (reverse)
        {
            value *= -1;
        }

        return value;
    }
}