﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.State;

public static class LocationStateExtensions
{
    public static IUnit AdiecentVisibleUnitToLeft(this ILocation location) => location.VisibleUnitsToLeft().FirstOrDefault();

    public static IUnit AdiecentVisibleUnitToRight(this ILocation location) => location.VisibleUnitsToRight().FirstOrDefault();

    public static IReadOnlyCollection<PlatformCard> AllFacedVisiblePlatfomrs(this ILocation location, GameState gameState)
    {
        if (location.IsFacingLeft)
        {
            return location.AllVisiblePlatfomrsToLeft(gameState);
        }

        return location.AllVisiblePlatfomrsToRight(gameState);
    }

    public static IReadOnlyCollection<PlatformCard> AllVisiblePlatfomrsToLeft(this ILocation location, GameState gameState)
    {
        if (location.PlatformCard.Obstacle != null
            && location.PlatformCard.Obstacle.Settings.IsBlockingVisibility
            && !location.IsToTheLeftOfObstacle)
        {
            return Array.Empty<PlatformCard>();
        }

        return location.PlatformCard.AllVisiblePlatfomrsToLeft(gameState);
    }

    public static IReadOnlyCollection<PlatformCard> AllVisiblePlatfomrsToRight(this ILocation location, GameState gameState)
    {
        if (location.PlatformCard.Obstacle != null
            && location.PlatformCard.Obstacle.Settings.IsBlockingVisibility
            && location.IsToTheLeftOfObstacle)
        {
            return Array.Empty<PlatformCard>();
        }

        return location.PlatformCard.AllVisiblePlatfomrsToRight(gameState);
    }

    public static IReadOnlyCollection<IUnit> AllVisibleUnitsToLeft(this ILocation location, GameState gameState)
    {
        return location.VisibleUnitsToLeft().Concat(
            location.AllVisiblePlatfomrsToLeft(gameState).SelectMany(x => x.UnitsVisibleTo(true)))
            .ToArray();
    }

    public static IReadOnlyCollection<IUnit> AllVisibleUnitsToRight(this ILocation location, GameState gameState)
    {
        return location.VisibleUnitsToRight().Concat(
             location.AllVisiblePlatfomrsToRight(gameState).SelectMany(x => x.UnitsVisibleTo(false)))
             .ToArray();
    }

    public static IUnit FacedVisibleUnit(this ILocation location, int idOffset)
    {
        IUnit target = null;
        var platform = location.PlatformCard;
        if (location.IsFacingLeft)
        {
            if (location.IsToTheLeftOfObstacle
                && 0 < location.UnitId - idOffset)
            {
                target = platform.UnitsToLeftOfObstacle[location.UnitId - idOffset];
            }
            else if (!location.IsToTheLeftOfObstacle
                && 0 < location.UnitId - idOffset)
            {
                target = platform.UnitsToRightOfObstacle[location.UnitId - idOffset];
            }
        }
        else
        {
            if (location.IsToTheLeftOfObstacle
                && platform.UnitsToLeftOfObstacle.Count > location.UnitId + idOffset)
            {
                target = platform.UnitsToLeftOfObstacle[location.UnitId + idOffset];
            }
            else if (!location.IsToTheLeftOfObstacle
                && platform.UnitsToRightOfObstacle.Count > location.UnitId + idOffset)
            {
                target = platform.UnitsToRightOfObstacle[location.UnitId + idOffset];
            }
        }

        return target;
    }

    public static bool IsAtLeftEdgeOfPlatform(this ILocation location) => location.UnitId == 0 && location.IsToTheLeftOfObstacle;

    public static bool IsAtRightEdgeOfPlatform(this ILocation location)
    {
        return location.PlatformCard.Obstacle == null
                    && location.UnitId == location.PlatformCard.UnitsToLeftOfObstacle.Count - 1
            || !location.IsToTheLeftOfObstacle
                    && location.UnitId == location.PlatformCard.UnitsToRightOfObstacle.Count - 1;
    }

    public static bool IsInFrontOf(this ILocation location, IUnit unit) => location.IsInFrontOf(unit.Location);

    public static bool IsInFrontOf(this ILocation first, ILocation second)
    {
        return first.PlatformCard == second.PlatformCard
               && second.UnitId + (second.IsFacingLeft ? 0 : 1) == first.UnitId;
    }

    /// <summary>
    /// case: xxxOxExX -> xxxOxXExx - move past one enemy
    /// case: xExOxXx -> xEXxOxxx - move past an obstacle and all frendly units
    /// case: xExXOxxx -> xXExxOxxx - move past one enemy
    /// case: xxxOxEx xxXOxxx -> xxxOxEXx xxxOxxx - move to next platform close to enemy or obstacle
    /// </summary>
    /// <typeparam name="TUnit"></typeparam>
    /// <param name="location"></param>
    /// <param name="gameStateProvider"></param>
    /// <returns></returns>
    public static ILocation MoveLeftANotch<TUnit>(this ILocation location, GameState gameState)
        where TUnit : IUnit
    {
        if (!location.IsToTheLeftOfObstacle)
        {
            if (location.UnitId > 0)
            {
                var notEnemiesCount = location.PlatformCard.UnitsToRightOfObstacle
                    .Take(location.UnitId)
                    .Reverse()
                    .TakeWhile(x => x is TUnit)
                    .Count();

                if (location.UnitId - notEnemiesCount - 1 > 0)
                {
                    var newLocation = location.Clone();
                    newLocation.UnitId = location.UnitId - notEnemiesCount - 1;
                    return newLocation;
                }
            }

            if (location.PlatformCard.Obstacle != null)
            {
                var newLocation = location.Clone();
                newLocation.IsToTheLeftOfObstacle = true;
                newLocation.UnitId = location.PlatformCard
                    .UnitsToLeftOfObstacle
                    .Reverse()
                    .TakeWhile(x => x is TUnit)
                    .Last()
                    .Location
                    .UnitId;
                return newLocation;
            }
        }
        else
        {
            if (location.UnitId > 0)
            {
                var notEnemiesCount = location.PlatformCard.UnitsToLeftOfObstacle
                   .Take(location.UnitId)
                   .Reverse()
                   .TakeWhile(x => x is TUnit)
                   .Count();

                if (location.UnitId - notEnemiesCount - 1 > 0)
                {
                    var newLocation = location.Clone();
                    newLocation.UnitId = location.UnitId - notEnemiesCount - 1;
                    return newLocation;
                }
            }
        }

        var nextPlatformLocation = location.PlatformCard.PlatformToLeft(gameState)?.LocationAfterLastUnit(location.IsFacingLeft);

        if (nextPlatformLocation == null)
        {
            return null;
        }

        if (nextPlatformLocation.PlatformCard.Obstacle != null)
        {
            nextPlatformLocation.UnitId =
                nextPlatformLocation.PlatformCard.UnitsToRightOfObstacle.Count -
                nextPlatformLocation.PlatformCard.UnitsToRightOfObstacle.TakeWhile(x => x is IUnit).Count();
        }
        else
        {
            nextPlatformLocation.UnitId =
                nextPlatformLocation.PlatformCard.UnitsToLeftOfObstacle.Count -
                nextPlatformLocation.PlatformCard.UnitsToLeftOfObstacle.TakeWhile(x => x is IUnit).Count();
        }

        return nextPlatformLocation;
    }

    /// <summary>
    /// case: xXxExEOxxx -> xxxEXxEOxxx - move past one enemy
    /// case: xXxOxEExx -> xxxOxXEExx - move past an obstacle and all frendly units
    /// case: xxxOXxEx -> xxxOxxEXx - move past one enemy
    /// case: xxxOXxxx xxExOxxx -> xxxOxxxx xxXExOxxx - move to next platform close to enemy or obstacle
    /// </summary>
    /// <typeparam name="TUnit"></typeparam>
    /// <param name="location"></param>
    /// <param name="gameStateProvider"></param>
    /// <returns></returns>
    public static ILocation MoveRightANotch<TUnit>(this ILocation location, GameState gameState)
        where TUnit : IUnit
    {
        if (location.IsToTheLeftOfObstacle)
        {
            if (location.UnitId < location.PlatformCard.UnitsToLeftOfObstacle.Count)
            {
                var notEnemiesCount = location.PlatformCard.UnitsToLeftOfObstacle
                    .Skip(location.UnitId + 1)
                    .TakeWhile(x => x is TUnit)
                    .Count();

                if (location.UnitId + notEnemiesCount + 1 < location.PlatformCard.UnitsToLeftOfObstacle.Count)
                {
                    var newLocation = location.Clone();
                    newLocation.UnitId = location.UnitId + notEnemiesCount + 1;
                    return newLocation;
                }
            }

            if (location.PlatformCard.Obstacle != null)
            {
                var newLocation = location.Clone();
                newLocation.IsToTheLeftOfObstacle = false;
                newLocation.UnitId = location.PlatformCard.UnitsToRightOfObstacle.TakeWhile(x => x is TUnit).Count();
                return newLocation;
            }
        }
        else
        {
            if (location.UnitId < location.PlatformCard.UnitsToRightOfObstacle.Count)
            {
                var notEnemiesCount = location.PlatformCard.UnitsToRightOfObstacle
                    .Skip(location.UnitId + 1)
                    .TakeWhile(x => x is TUnit)
                    .Count();

                if (location.UnitId + notEnemiesCount + 1 < location.PlatformCard.UnitsToRightOfObstacle.Count)
                {
                    var newLocation = location.Clone();
                    newLocation.UnitId = location.UnitId + notEnemiesCount + 1;
                    return newLocation;
                }
            }
        }

        var nextPlatformLocation = location.PlatformCard.PlatformToRight(gameState)?.LocationAsFirstUnit(location.IsFacingLeft);

        if (nextPlatformLocation == null)
        {
            return null;
        }

        nextPlatformLocation.UnitId = nextPlatformLocation.PlatformCard.UnitsToLeftOfObstacle.TakeWhile(x => x is IUnit).Count();

        return nextPlatformLocation;
    }

    public static PlatformCard NextFacedPlatform(this ILocation location, GameState gameState)
    {
        if (location.IsFacingLeft)
        {
            return location.PlatformToLeft(gameState);
        }

        return location.PlatformToRight(gameState);
    }

    public static PlatformCard PlatformToLeft(this ILocation location, GameState gameState) => location.PlatformCard.PlatformToLeft(gameState);

    public static PlatformCard PlatformToRight(this ILocation location, GameState gameState) => location.PlatformCard.PlatformToRight(gameState);

    public static IUnit UnitAtThisLocation(this ILocation location)
    {
        if (location.IsToTheLeftOfObstacle)
        {
            if (location.PlatformCard.UnitsToLeftOfObstacle.Count <= location.UnitId || location.UnitId < 0)
            {
                return null;
            }

            return location.PlatformCard.UnitsToLeftOfObstacle[location.UnitId];
        }
        else
        {
            if (location.PlatformCard.UnitsToRightOfObstacle.Count <= location.UnitId || location.UnitId < 0)
            {
                return null;
            }

            return location.PlatformCard.UnitsToRightOfObstacle[location.UnitId];
        }
    }

    public static IReadOnlyCollection<IUnit> UnitsToLeft(this ILocation location)
    {
        if (location.IsToTheLeftOfObstacle)
        {
            return location.PlatformCard.UnitsToLeftOfObstacle.Take(location.UnitId).Reverse().ToArray();
        }
        else
        {
            return location.PlatformCard.UnitsToRightOfObstacle.Take(location.UnitId).Reverse().Concat(location.PlatformCard.UnitsToLeftOfObstacle.Reverse()).ToArray();
        }
    }

    public static IReadOnlyCollection<IUnit> UnitsToRight(this ILocation location)
    {
        if (location.IsToTheLeftOfObstacle)
        {
            return location.PlatformCard.UnitsToLeftOfObstacle.Skip(location.UnitId + 1)
                .Concat(location.PlatformCard.UnitsToRightOfObstacle).ToArray();
        }
        else
        {
            return location.PlatformCard.UnitsToRightOfObstacle.Skip(location.UnitId + 1).ToArray();
        }
    }

    /// <summary>
    /// If location is to the left of obstacle
    /// [uu]UuuOuuu
    /// else
    /// uuO[uu]U
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    public static IReadOnlyCollection<IUnit> VisibleUnitsToLeft(this ILocation location)
    {
        if (location.PlatformCard.Obstacle?.Settings.IsBlockingVisibility == true)
        {
            if (location.IsToTheLeftOfObstacle)
            {
                return location.PlatformCard.UnitsToLeftOfObstacle.Take(location.UnitId).Reverse().ToArray();
            }
            else
            {
                return location.PlatformCard.UnitsToRightOfObstacle.Take(location.UnitId).Reverse().ToArray();
            }
        }
        else
        {
            if (location.IsToTheLeftOfObstacle)
            {
                return location.PlatformCard.UnitsToLeftOfObstacle.Take(location.UnitId).Reverse().ToArray();
            }
            else
            {
                return
                    location.PlatformCard.UnitsToLeftOfObstacle.Concat(
                        location.PlatformCard.UnitsToRightOfObstacle.Take(location.UnitId))
                    .Reverse()
                    .ToArray();
            }
        }
    }

    /// <summary>
    /// If location is to the left of obstacle
    /// uuU[uu]Ouuu
    /// else
    /// uuOuuU[uu]
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    public static IReadOnlyCollection<IUnit> VisibleUnitsToRight(this ILocation location)
    {
        if (location.PlatformCard.Obstacle?.Settings.IsBlockingVisibility == true)
        {
            if (location.IsToTheLeftOfObstacle)
            {
                return location.PlatformCard.UnitsToLeftOfObstacle.Skip(location.UnitId + 1).ToArray();
            }
            else
            {
                return location.PlatformCard.UnitsToRightOfObstacle.Skip(location.UnitId + 1).ToArray();
            }
        }
        else
        {
            if (location.IsToTheLeftOfObstacle)
            {
                return location.PlatformCard.UnitsToLeftOfObstacle.Skip(location.UnitId + 1)
                    .Concat(location.PlatformCard.UnitsToRightOfObstacle)
                    .ToArray();
            }
            else
            {
                return location.PlatformCard.UnitsToRightOfObstacle.Skip(location.UnitId + 1).ToArray();
            }
        }
    }
}