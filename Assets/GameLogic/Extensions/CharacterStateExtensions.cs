﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

public static class CharacterStateExtensions
{
    public static IReadOnlyCollection<ActionSettings> AllActions(this Character character)
    {
        return character.Equipment.Values
            .SelectMany(x => x.Settings.Actions)
            .Concat(character.Actions)
            .ToArray();
    }

    public static bool CanInteract(this Character unit)
    {
        return unit.Interact.Current != 0;
    }

    public static bool CanUseItem(this Character unit)
    {
        return unit.UseItem.Current != 0;
    }
}