﻿using Assets.GameLogic.State;

namespace Assets.GameLogic.Dtos
{
    public class LocationDescriptor : ILocation
    {
        public int CurrentLevelId { get; set; }

        public bool IsFacingLeft { get; set; }

        public bool IsToTheLeftOfObstacle { get; set; }

        public PlatformCard PlatformCard { get; set; }

        public int UnitId { get; set; }

        public ILocation Clone()
        {
            return new LocationDescriptor
            {
                CurrentLevelId = CurrentLevelId,
                IsFacingLeft = IsFacingLeft,
                IsToTheLeftOfObstacle = IsToTheLeftOfObstacle,
                PlatformCard = PlatformCard,
                UnitId = UnitId,
            };
        }
    }
}