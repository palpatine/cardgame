﻿namespace Assets.GameLogic.Dtos
{
    public class DistanceDescriptor
    {
        public int AbsoluteUnitCount { get; set; }

        public int PlatformsCount { get; set; }
    }
}