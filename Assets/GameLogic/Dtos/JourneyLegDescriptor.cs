﻿using System.Collections.Generic;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Dtos
{

    public class JourneyLegDescriptor
    {
        public IEnumerable<EffectResult> EffectResults { get; set; }

        public ILocation From { get; set; }

        public ILocation To { get; set; }
    }
}