﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;

namespace Assets.GameLogic.Dtos
{
    public class EffectResult
    {
        public EffectResult(ITarget target) => Target = target;

        public List<Ability> AddAbilities { get; } = new List<Ability>();

        public List<ActionSettings> AddActions { get; } = new List<ActionSettings>();

        public List<ItemSettings> AddItems { get; } = new List<ItemSettings>();

        public bool EndTurn { get; set; }

        public bool NoRestoration { get; set; }

        public List<AbilitySettings> RemoveAbilities { get; } = new List<AbilitySettings>();

        public List<ActionSettings> RemoveActions { get; } = new List<ActionSettings>();

        public List<ItemSettings> RemoveItems { get; } = new List<ItemSettings>();

        public StatisticChange StatisticsChange { get; } = new StatisticChange();

        public ITarget Target { get; }

        public List<Ability> UtilizedAbiliites { get; } = new List<Ability>();
        public int DrawRuneCards { get; set; }

        public override string ToString()
        {
            return $"Target:{Target.Id} StatisticsChange:{StatisticsChange} " +
                $"Abilities:{AddAbilities.JoinAsString(x => $"+{x.Settings.Name} {x.Parameters.JoinAsString(z => $"{z.Key}:{z.Value}")}")}" +
                $":{RemoveAbilities.JoinAsString(x => "-" + x.Name)}" +
                $"Actions:{AddActions.JoinAsString(x => "+" + x.DisplayName)}" +
                $":{RemoveActions.JoinAsString(x => "-" + x.DisplayName)}";
        }

        public static EffectResult Merge(EffectResult x, EffectResult y)
        {
            var result = new EffectResult(x.Target);

            result.StatisticsChange.Add(x.StatisticsChange);
            result.StatisticsChange.Add(y.StatisticsChange);

            result.AddAbilities.AddRange(x.AddAbilities);
            result.AddAbilities.AddRange(y.AddAbilities);

            result.RemoveAbilities.AddRange(x.RemoveAbilities);
            result.RemoveAbilities.AddRange(y.RemoveAbilities);

            result.AddActions.AddRange(x.AddActions);
            result.AddActions.AddRange(y.AddActions);

            result.RemoveActions.AddRange(x.RemoveActions);
            result.RemoveActions.AddRange(y.RemoveActions);

            result.AddItems.AddRange(x.AddItems);
            result.AddItems.AddRange(y.AddItems);

            result.RemoveItems.AddRange(x.RemoveItems);
            result.RemoveItems.AddRange(y.RemoveItems);

            return result;
        }
    }
}