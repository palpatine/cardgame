﻿using System.Collections.Generic;
using Assets.GameLogic.Settings;
using Assets.GameLogic.Tools;

namespace Assets.GameLogic.Dtos
{
    public class StatisticChange : Dictionary<(string statistic, string valueName), int>
    {
        public int this[string statistic]
        {
            get => this[(statistic, Constants.StatisitcValues.Current)];
            set => this[(statistic, Constants.StatisitcValues.Current)] = value;
        }

        public int this[StatisticReference reference]
        {
            get
            {
                var key = (reference.Name, reference.Value);
                if (ContainsKey(key))
                {
                    return this[key];
                }

                return 0;
            }
        }

        public void Add(string statistic, string valueName, int value)
        {
            if (!ContainsKey((statistic, valueName)))
            {
                Add((statistic, valueName), value);
            }
            else
            {
                this[(statistic, valueName)] += value;
            }
        }

        public void Add(StatisticChange statisticChange)
        {
            foreach (var item in statisticChange)
            {
                Add(item.Key.statistic, item.Key.valueName, item.Value);
            }
        }

        public void Add(string statistic, int value) => Add(statistic, Constants.StatisitcValues.Current, value);

        public override string ToString() => this.JoinAsString(x => $"{x.Key}:{x.Value}");
    }
}