﻿namespace Assets.GameLogic.Dtos
{
    public class MapPathPointDescriptor
    {
        public double F { get; set; }

        public double G { get; set; }

        public JourneyLegDescriptor Journey { get; set; }

        public MapPathPointDescriptor Parent { get; set; }
    }
}