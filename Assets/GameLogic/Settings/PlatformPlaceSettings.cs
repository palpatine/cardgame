﻿namespace Assets.GameLogic.Settings
{
    public class PlatformPlaceSettings : INamedSetting
    {
        public string DisplayName { get; set; }

        public string Name { get; set; }
    }
}