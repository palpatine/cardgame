﻿namespace Assets.GameLogic.Settings
{
    public class ResourceSettings : INamedSetting
    {
        public int Count { get; set; }

        public bool FeatureMultiplier { get; set; }

        // public string Kind { get; set; }

        public string Name { get; set; }
    }
}