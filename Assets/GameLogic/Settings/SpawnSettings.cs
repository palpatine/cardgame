﻿namespace Assets.GameLogic.Settings
{
    public class SpawnSettings
    {
        public int? CountDelta { get; set; }

        public int? Delta { get; set; }

        public FeatureSelector FeatureSelector { get; set; }

        public PlatformSelector PlatformSelector { get; set; }

        public StatisticReference Statistic { get; set; }
    }
}