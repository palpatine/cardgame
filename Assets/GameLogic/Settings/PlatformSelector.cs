﻿namespace Assets.GameLogic.Settings
{
    public class PlatformSelector
    {
        public bool? HasFeature { get; set; }

        public int? IdOffset { get; set; }

        public string Kind { get; set; }

        public string Name { get; set; }

        public string Obstacle { get; set; }
    }
}