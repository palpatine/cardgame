﻿namespace Assets.GameLogic.Settings
{
    public interface INamedSetting
    {
        string Name { get; }
    }
}