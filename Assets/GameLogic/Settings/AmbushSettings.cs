﻿namespace Assets.GameLogic.Settings
{
    public class AmbushSettings
    {
        public int Count { get; set; }

        public string Foe { get; set; }
    }
}