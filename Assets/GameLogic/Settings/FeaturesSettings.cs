﻿namespace Assets.GameLogic.Settings
{
    public class FeaturesSettings : INamedSetting
    {
        public string DisplayName { get; set; }

        public string Name { get; set; }

        public ResourceSettings Resource { get; set; }

        public string TriggeringPlatformKind { get; set; }
    }
}