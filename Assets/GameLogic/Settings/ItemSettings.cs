﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class ItemSettings : INamedSetting
    {
        public ItemSettings()
        {
            Kinds = Enumerable.Empty<string>();
            Effects = Enumerable.Empty<EffectSettings>();
            Actions = Enumerable.Empty<ActionSettings>();
        }

        public IEnumerable<ActionSettings> Actions { get; set; }

        public IEnumerable<EffectSettings> Effects { get; set; }

        public IEnumerable<string> Kinds { get; set; }

        public string Name { get; set; }

        public bool Starting { get; set; }

        /// <summary>
        /// Equivalent to rune value, items with this property can be used to buy runes.
        /// </summary>
        public int? Value { get; set; }
    }
}