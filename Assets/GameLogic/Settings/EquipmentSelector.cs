﻿namespace Assets.GameLogic.Settings
{
    public class EquipmentSelector
    {
        public string Kind { get; set; }

        public string Property { get; set; }
    }
}