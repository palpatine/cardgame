﻿namespace Assets.GameLogic.Settings
{
    public class ConditionValueSettings : ValueSettings
    {
        public ConditionGroupSettings Group { get; set; }

        public static implicit operator ConditionValueSettings(string value)
            => new ConditionValueSettings { Value = value };
    }
}