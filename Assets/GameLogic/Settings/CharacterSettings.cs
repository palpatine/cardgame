﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class CharacterSettings
    {
        public CharacterSettings()
        {
            Equipment = Enumerable.Empty<string>();
            Abilities = Enumerable.Empty<string>();
        }

        public IEnumerable<string> Abilities { get; set; }

        public BaseStatistics BaseStatistics { get; set; }

        public IEnumerable<string> Equipment { get; set; }
    }
}