﻿using System;

namespace Assets.GameLogic.Settings
{
    public class RangeSettings : IComparable
    {
        public string Name { get; set; }

        public int? Value { get; set; }

        public static implicit operator RangeSettings(string value)
            => new RangeSettings
            {
                Name = value,
                Value = value == Constants.Range.Far ? 3 : 1
            };

        public int CompareTo(object obj)
        {
            var other = obj as RangeSettings;
            if (other == null)
            {
                throw new InvalidOperationException();
            }

            if (Name == other.Name)
            {
                return Value.HasValue ? Value.Value.CompareTo(other.Value.Value) : 0;
            }

            if (Name == Constants.Range.Self)
            {
                return -1;
            }

            if (other.Name == Constants.Range.Self)
            {
                return 1;
            }

            if (Name == Constants.Range.Short)
            {
                return -1;
            }

            if (other.Name == Constants.Range.Short)
            {
                return 1;
            }

            if (Name == Constants.Range.Medium)
            {
                return -1;
            }

            if (other.Name == Constants.Range.Medium)
            {
                return 1;
            }

            return 1;
        }

        internal RangeSettings Copy() => (RangeSettings)MemberwiseClone();
    }
}