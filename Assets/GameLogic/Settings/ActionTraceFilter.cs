﻿namespace Assets.GameLogic.Settings
{
    public class ActionTraceFilter
    {
        public string Action { get; set; }

        public string Get { get; set; }

        public bool? HasStatisticChange { get; set; }

        public TargetSelector Target { get; set; }
    }
}