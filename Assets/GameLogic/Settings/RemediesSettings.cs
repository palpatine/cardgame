﻿namespace Assets.GameLogic.Settings
{
    public class RemediesSettings
    {
        public string Action { get; set; }

        public EffectSettings Effect { get; set; }

        public string Obstacle { get; set; }
    }
}