﻿namespace Assets.GameLogic.Settings
{
    public class ParameterSettings
    {
        public string Name { get; set; }

        public decimal Scale { get; set; } = 1;

        public ValueSettings Value { get; set; }
    }
}