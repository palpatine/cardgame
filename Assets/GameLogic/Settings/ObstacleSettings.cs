﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class ObstacleSettings : INamedSetting
    {
        public ObstacleSettings()
        {
            Effects = Enumerable.Empty<EffectSettings>();
        }

        public string DisplayName { get; set; }

        public IEnumerable<EffectSettings> Effects { get; set; }

        public bool IsBlockingVisibility { get; set; }

        public string Name { get; set; }

        public RemediesSettings Remedies { get; set; }
    }
}