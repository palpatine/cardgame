﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class DictionaryEntry
    {
        public DictionaryEntry()
        {
            Tags = Enumerable.Empty<string>();
        }

        public string Description { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public string Term { get; set; }
    }
}