﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class RuneCardSettings : INamedSetting
    {
        public RuneCardSettings()
        {
            Effects = Enumerable.Empty<EffectSettings>();
            Decks = Enumerable.Empty<string>();
        }

        public int Cost { get; set; }

        public int CountInDeck { get; set; }

        public IEnumerable<string> Decks { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public IEnumerable<EffectSettings> Effects { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }
    }
}