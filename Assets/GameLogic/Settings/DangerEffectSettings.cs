﻿namespace Assets.GameLogic.Settings
{
    public class DangerEffectSettings
    {
        public EffectSettings Effect { get; set; }

        public string Foe { get; set; }

        public string Kind { get; set; }

        public PlatformSelector PlatformSelector { get; set; }

        public string Replacement { get; set; }
    }
}