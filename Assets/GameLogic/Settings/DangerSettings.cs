﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class DangerSettings : INamedSetting
    {
        public DangerSettings()
        {
            Effects = Enumerable.Empty<DangerEffectSettings>();
            Reward = Enumerable.Empty<EffectSettings>();
        }

        public int CountInDeck { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public IEnumerable<DangerEffectSettings> Effects { get; set; }

        public string Name { get; set; }

        public IEnumerable<EffectSettings> Reward { get; set; }
    }
}