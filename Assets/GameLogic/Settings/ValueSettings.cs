﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class ValueSettings
    {
        public ValueSettings() => Values = Enumerable.Empty<ValueSettings>();

        public EquipmentSelector Equipment { get; set; }
               
        public bool IsNegative { get; set; }

        public ActionTraceFilter PreviousAction { get; set; }

        /// <summary>
        /// Name of selector to use as value.
        /// </summary>
        public string Selected { get; set; }

        public TargetSelector Source { get; set; }

        public StatisticReference Statistic { get; set; }

        public StatisticReference StatisticsChange { get; set; }

        public string Value { get; set; }

        #region Arythmetic operations on set of values
        public string Operation { get; set; }

        public IEnumerable<ValueSettings> Values { get; set; }
        #endregion

        public static implicit operator ValueSettings(string value)
            => new ValueSettings { Value = value };
    }
}