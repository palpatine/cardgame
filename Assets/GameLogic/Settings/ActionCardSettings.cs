﻿namespace Assets.GameLogic.Settings
{
    public class ActionCardSettings : INamedSetting
    {
        public int CountInDeck { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public int EnergyCost { get; set; }

        public string Name { get; set; }
    }
}