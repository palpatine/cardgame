﻿namespace Assets.GameLogic.Settings
{
    public class BaseStatistics
    {
        public int Attack { get; set; }

        public int Block { get; set; }

        public int Damage { get; set; }

        public int Defense { get; set; }

        public int Energy { get; set; }

        public int EnergyRecovery { get; set; }

        public int Interact { get; set; }

        public int Life { get; set; }

        public int LifeRecovery { get; set; }

        public int MagicDamage { get; set; }

        public int Mana { get; set; }

        public int ManaRecovery { get; set; }

        public int Movement { get; set; }

        public int UseItem { get; set; }
    }
}