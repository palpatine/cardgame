﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class EffectSettings
    {
        public EffectSettings() => Parameters = Enumerable.Empty<ParameterSettings>();

        #region value
        public ValueSettings Delta { get; set; }

        public string ParameterForDelta { get; set; }
        #endregion

        public IEnumerable<ParameterSettings> Parameters { get; set; }

        #region ability
        public string Ability { get; set; }

        public int? Duration { get; set; }

        public int? UseCount { get; set; }
        #endregion

        public ActionSettings Action { get; set; }

        public ConditionSettings Condition { get; set; }

        public bool? EndTurn { get; set; }

        public string Equipment { get; set; }

        public bool? Invulnerability { get; set; }

        public bool? IsNegative { get; set; }

        public bool? NoRestoration { get; set; }

        public bool? RuneCard { get; set; }

        public StatisticReference Statistic { get; set; }

        public TargetSelector Target { get; set; }
    }
}