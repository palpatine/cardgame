﻿namespace Assets.GameLogic.Settings
{
    public class SelectorSettings
    {
        public ValueSettings Increment { get; set; }

        public ValueSettings Max { get; set; }

        public ValueSettings Min { get; set; }

        public string Name { get; set; }
    }
}