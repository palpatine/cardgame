﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class FoeSettings : INamedSetting
    {
        public FoeSettings()
        {
            Abilities = Enumerable.Empty<string>();
            Actions = Enumerable.Empty<ActionSettings>();
        }

        public IEnumerable<string> Abilities { get; set; }

        public IEnumerable<ActionSettings> Actions { get; set; }

        public string Attitude { get; set; }

        public BaseStatistics BaseStatistics { get; set; }

        public int Count { get; set; }

        public string Name { get; set; }

        public SpawnSettings Spawn { get; set; }
    }
}