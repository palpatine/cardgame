﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class GameSettings
    {
        public GameSettings()
        {
            ActionCards = Enumerable.Empty<ActionCardSettings>();
            Runes = Enumerable.Empty<RuneCardSettings>();
            PlatformDeck = Enumerable.Empty<PlatformCardSettings>();
            Dangers = Enumerable.Empty<DangerSettings>();
            Dictionary = Enumerable.Empty<DictionaryEntry>();
            Foes = Enumerable.Empty<FoeSettings>();
            StartingPlatforms = Enumerable.Empty<string>();
            PlatformPlaces = Enumerable.Empty<PlatformPlaceSettings>();
            Obstacles = Enumerable.Empty<ObstacleSettings>();
            Features = Enumerable.Empty<FeaturesSettings>();
            Abilities = Enumerable.Empty<AbilitySettings>();
            Items = Enumerable.Empty<ItemSettings>();
            EquipableItemKinds = Enumerable.Empty<string>();
        }

        public IEnumerable<AbilitySettings> Abilities { get; set; }

        public IEnumerable<ActionCardSettings> ActionCards { get; set; }

        public CharacterSettings Character { get; set; }

        public IEnumerable<DangerSettings> Dangers { get; set; }

        public IEnumerable<DictionaryEntry> Dictionary { get; set; }

        public IEnumerable<string> EquipableItemKinds { get; set; }

        public IEnumerable<FeaturesSettings> Features { get; set; }

        public IEnumerable<FoeSettings> Foes { get; set; }

        public IEnumerable<ItemSettings> Items { get; set; }

        public int MaximalActiveDangersCount { get; set; }

        public IEnumerable<ObstacleSettings> Obstacles { get; set; }

        public IEnumerable<PlatformCardSettings> PlatformDeck { get; set; }

        public IEnumerable<PlatformPlaceSettings> PlatformPlaces { get; set; }

        public IEnumerable<RuneCardSettings> Runes { get; set; }

        public IEnumerable<string> StartingPlatforms { get; set; }

        public int StartingRuneCardsCount { get; set; }
    }
}