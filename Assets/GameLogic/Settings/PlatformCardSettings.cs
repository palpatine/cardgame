﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class PlatformCardSettings : INamedSetting
    {
        public PlatformCardSettings()
        {
            Resources = Enumerable.Empty<ResourceSettings>();
            EntranceEffects = Enumerable.Empty<EffectSettings>();
            Kinds = Enumerable.Empty<string>();
        }

        public int CountInDeck { get; set; }

        public string DisplayName { get; set; }

        public IEnumerable<EffectSettings> EntranceEffects { get; set; }

        public ExplorationSettings Exploration { get; set; }

        public string Feature { get; set; }

        public IEnumerable<string> Kinds { get; set; }

        public string Name { get; set; }

        public IEnumerable<ResourceSettings> Resources { get; set; }
    }
}