﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class AbilitySettings : INamedSetting
    {
        public AbilitySettings()
        {
            Effects = Enumerable.Empty<EffectSettings>();
            Parameters = Enumerable.Empty<string>();
        }

        public ActionSettings ActionOverride { get; set; }

        public ConditionSettings Condition { get; set; }

        public IEnumerable<EffectSettings> Effects { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> Parameters { get; set; }

        public TriggerSettings Trigger { get; set; }
    }
}