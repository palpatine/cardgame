﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class ConditionGroupSettings
    {
        public ConditionGroupSettings()
        {
            Conditions = Enumerable.Empty<ConditionSettings>();
        }

        public IEnumerable<ConditionSettings> Conditions { get; set; }

        public bool IsOr { get; set; }
    }
}