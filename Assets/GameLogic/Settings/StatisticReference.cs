﻿namespace Assets.GameLogic.Settings
{
    public class StatisticReference
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public static implicit operator StatisticReference(string value)
            => new StatisticReference { Name = value, Value = "current" };
    }
}