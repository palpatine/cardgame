﻿namespace Assets.GameLogic.Settings
{
    public class TargetSelector
    {
        public int IdOffset { get; set; }

        public string Name { get; set; }

        public static implicit operator TargetSelector(string value)
            => new TargetSelector { Name = value };
    }
}