﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class ActionSettings
    {
        public ActionSettings()
        {
            Effects = Enumerable.Empty<EffectSettings>();
            Kinds = Enumerable.Empty<string>();
            Selectors = Enumerable.Empty<SelectorSettings>();
        }

        public bool AffectsArea { get; set; }

        public string Ammo { get; set; }

        public bool CanTargetSelf { get; set; }

        public ConditionSettings Condition { get; set; }

        public string DisplayName { get; set; }

        public IEnumerable<EffectSettings> Effects { get; set; }

        /// <summary>
        /// not used
        /// </summary>
        public EquipmentSelector Import { get; set; }

        public IEnumerable<string> Kinds { get; set; }

        public RangeSettings MinRange { get; set; }

        public RangeSettings Range { get; set; }

        public IEnumerable<SelectorSettings> Selectors { get; set; }
    }
}