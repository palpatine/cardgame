﻿namespace Assets.GameLogic.Settings
{
    public class TreasureSettings
    {
        public int Count { get; set; }

        public string Kind { get; set; }

        public string Name { get; set; }
    }
}