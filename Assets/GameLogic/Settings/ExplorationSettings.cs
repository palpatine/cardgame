﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Settings
{
    public class ExplorationSettings
    {
        public ExplorationSettings()
        {
            Ambushes = Enumerable.Empty<AmbushSettings>();
            Treasures = Enumerable.Empty<TreasureSettings>();
        }

        public IEnumerable<AmbushSettings> Ambushes { get; set; }

        public string Place { get; set; }

        public IEnumerable<TreasureSettings> Treasures { get; set; }
    }
}