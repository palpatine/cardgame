﻿namespace Assets.GameLogic.Settings
{
    public class TriggerSettings
    {
        public string Kind { get; set; }

        public string Name { get; set; }

        public static implicit operator TriggerSettings(string value)
            => new TriggerSettings { Name = value };
    }
}