﻿namespace Assets.GameLogic.Settings
{
    public class ConditionSettings
    {
        public string Compare { get; set; }

        public ConditionValueSettings LeftValue { get; set; }

        public ConditionValueSettings RightValue { get; set; }
    }
}