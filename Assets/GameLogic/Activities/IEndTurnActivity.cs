﻿using System.Threading.Tasks;

namespace Assets.GameLogic.Activities
{
    public interface IEndTurnActivity
    {
        string DisplayName { get; }

        string Name { get; }

        bool CanExecute();

        Task Execute();

        Task Execute(int currentUnitIndex, bool suppressTurnRollover = false);
    }
}