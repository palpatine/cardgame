﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;

namespace Assets.GameLogic
{
    public interface IGameStateProvider
    {
        GameState GameState { get; }
        
        GameSettings GameSettings { get; }
    }
}
