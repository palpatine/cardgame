﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Attitudes;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;
using Assets.GameLogic.Tools;
using Zenject;

namespace Assets.GameLogic.Incidents
{
    public interface IFoesIncidents : IUnitIncidents
    {
        Task ExecuteAction(Foe foe);

        void InitializeStatisitcs(
            Foe unit,
            BaseStatistics statisticsSettints,
            StatisticReference statisticWithMode = null,
            int modeValue = 0);

        void RecoverUnitStatisitcs(Foe unit);

        void Spawn(PlatformCard card, string foeName, bool toTheLeftOfObstacle);
    }

    public class FoesIncidents : UnitIncidents_, IFoesIncidents
    {
#pragma warning disable CS0649

        [Inject]
        private IEnumerable<IFoeAttitude> _behaviours;

        [Inject]
        private IPlatformFinder _platformFinder;

#pragma warning restore CS0649

        public async Task ExecuteAction(Foe foe)
        {
            var behaviour = _behaviours.Single(x => x.AttitudeName == foe.Settings.Attitude);
            await behaviour.ExecuteActionAsync(foe);
        }

        public void InitializeStatisitcs(
                    Foe unit,
            BaseStatistics statisticsSettints,
            StatisticReference statisticWithMode = null,
            int modeValue = 0) => base.InitializeStatisitcs(unit, statisticsSettints, statisticWithMode, modeValue);

        public void RecoverUnitStatisitcs(Foe unit)
        {
            unit.Energy.Regenerate();
            unit.Life.Regenerate();
            unit.Mana.Regenerate();
            unit.Movement.Replenish();
            unit.Attack.Replenish();
            unit.Block.Replenish();

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Target = unit,
                Action = Constants.TraceActions.StatisitcsRecovered
            });
        }

        public void Spawn(PlatformCard card, string foeName, bool toTheLeftOfObstacle)
        {
            var foeSettings = _gameStateProvider.GameSettings.Foes.Single(x => x.Name == foeName);

            var foes = new List<Foe>();
            var foesCount = foeSettings.Count;
            if (foeSettings.Spawn != null && foeSettings.Spawn.CountDelta != null)
            {
                if (foeSettings.Spawn.PlatformSelector != null)
                {
                    foesCount += _platformFinder.Find(
                                card,
                                foeSettings.Spawn.PlatformSelector)
                            .Count() * foeSettings.Spawn.CountDelta.Value;
                }
                else if (foeSettings.Spawn.FeatureSelector != null)
                {
                    foesCount += _platformFinder.CountFeaures(
                                card,
                                foeSettings.Spawn.FeatureSelector)
                                * foeSettings.Spawn.CountDelta.Value;
                }
            }

            for (var i = 0; i < foesCount; i++)
            {
                var foe = new Foe(_updateNotifier, foeSettings);
                foe.Location.CurrentLevelId = card.Level;
                foe.Location.PlatformCard = card;
                foe.Location.IsFacingLeft = _gameStateProvider.GameState.Levels[card.Level].Platforms.First() != card;

                StatisticReference statisticWithMode = null;
                var modeValue = 0;

                if (foeSettings.Spawn != null && foeSettings.Spawn.Delta != null)
                {
                    if (foe.Settings.Spawn.PlatformSelector != null)
                    {
                        modeValue = _platformFinder.Find(card, foeSettings.Spawn.PlatformSelector).Count()
                            * foeSettings.Spawn.Delta.Value;
                    }
                    else if (foe.Settings.Spawn.FeatureSelector != null)
                    {
                        modeValue = _platformFinder.CountFeaures(
                                card,
                                foeSettings.Spawn.FeatureSelector)
                            * foeSettings.Spawn.Delta.Value;
                    }

                    statisticWithMode = foeSettings.Spawn.Statistic;
                }

                InitializeStatisitcs(foe, foeSettings.BaseStatistics, statisticWithMode, modeValue);

                _gameStateProvider.GameState.Units.Add(foe);
                if (toTheLeftOfObstacle || card.Obstacle == null)
                {
                    foe.Location.IsToTheLeftOfObstacle = true;
                    foe.Location.UnitId = card.UnitsToLeftOfObstacle.Count();
                    card.UnitsToLeftOfObstacle.Add(foe);
                }
                else
                {
                    foe.Location.UnitId = card.UnitsToRightOfObstacle.Count();
                    card.UnitsToRightOfObstacle.Add(foe);
                }

                foe.StartingLocation = foe.Location.Clone();
                foes.Add(foe);

                _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
                {
                    CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                    Target = foe,
                    Action = Constants.TraceActions.Spawed
                });

                var abilities = _gameStateProvider.GameSettings.Abilities
                    .Where(x => foeSettings.Abilities.Contains(x.Name));
                foreach (var item in abilities)
                {
                    foe.Abilities.Add(new Ability
                    {
                        Settings = item
                    });
                }
            }
        }
    }
}