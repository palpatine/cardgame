﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents.UnitIncidents;
using Assets.GameLogic.Messages;
using Assets.GameLogic.Model.Trace;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using Zenject;

namespace Assets.GameLogic.Incidents
{
    public interface IUnitIncidents
    {
        Task ChangeLocation(
            IUnit unit,
            ILocation location);

        Task ChangeStatisitcs(IUnit unit, StatisticChange statisticsCost);

        Task StartSelectUnitsOrderAsync();

        void SetUnitsOrder(IEnumerable<IUnit> order);

        Task ExecuteAction(
           IUnit actor,
           ActionSettings action,
           IEnumerable<IUnit> targets);

        Task Apply(IEnumerable<EffectResult> effects, bool handleEndTurn = true);
    }

    public class UnitIncidents_ : IUnitIncidents
    {
#pragma warning disable CS0649

        [Inject]
        protected IUIGameStateProvider _gameStateProvider;

        [Inject]
        protected IUpdateNotifier _updateNotifier;

        [Inject]
        private IEndTurnActivity _endTurnAction;

        [Inject]
        private ICharacterIncidents _characterIncidents;

        [Inject]
        private IIncidentsManager _incidentsManager;

#pragma warning restore CS0649

        protected void InitializeStatisitcs<TUnit>(
              TUnit unit,
              BaseStatistics statisitcsSettints,
              StatisticReference statisticWithMode = null,
            int modeValue = 0)
            where TUnit : IUnit
        {
            unit.Energy.Maximum = statisitcsSettints.Energy;
            unit.Life.Maximum = statisitcsSettints.Life;
            unit.Mana.Maximum = statisitcsSettints.Mana;
            unit.Movement.Maximum = statisitcsSettints.Movement;
            unit.Attack.Maximum = statisitcsSettints.Attack;
            unit.Block.Maximum = statisitcsSettints.Block;

            unit.Energy.RegenerationRate = statisitcsSettints.EnergyRecovery;
            unit.Life.RegenerationRate = statisitcsSettints.LifeRecovery;
            unit.Mana.RegenerationRate = statisitcsSettints.ManaRecovery;

            if (statisticWithMode != null && statisticWithMode.Value != Constants.StatisitcValues.Current)
            {
                var moded = unit.GetStatistic(statisticWithMode.Name);
                if (statisticWithMode.Value == Constants.StatisitcValues.Maximum)
                {
                    ((IStatisticWithMaximumValue)moded).Maximum += modeValue;
                }

                if (statisticWithMode.Value == Constants.StatisitcValues.Regeneration)
                {
                    ((RegenerableStatistic)moded).RegenerationRate += modeValue;
                }
            }

            unit.Energy.Current = unit.Energy.Maximum;
            unit.Life.Current = unit.Life.Maximum;
            unit.Mana.Current = unit.Mana.Maximum;
            unit.Movement.Current = unit.Movement.Maximum;
            unit.Attack.Current = unit.Attack.Maximum;
            unit.Block.Current = unit.Block.Maximum;
            unit.Defense.Current = statisitcsSettints.Defense;

            if (statisticWithMode != null && statisticWithMode.Value == Constants.StatisitcValues.Current)
            {
                var moded = unit.GetStatistic(statisticWithMode.Name);
                moded.Current += modeValue;
            }
        }

        public async Task ChangeLocation(
          IUnit unit,
          ILocation location)
        {
            var from = unit.Location.Clone();
            await _updateNotifier.StartOperation(() =>
            {
                if (!unit.Location.PlatformCard.UnitsToLeftOfObstacle.Remove(unit))
                {
                    unit.Location.PlatformCard.UnitsToRightOfObstacle.Remove(unit);
                    UpdateUnitsUnitId(unit.Location.PlatformCard.UnitsToRightOfObstacle);
                }
                else
                {
                    UpdateUnitsUnitId(unit.Location.PlatformCard.UnitsToLeftOfObstacle);
                }

                if (location.PlatformCard == unit.Location.PlatformCard
                    && location.IsToTheLeftOfObstacle == unit.Location.IsToTheLeftOfObstacle
                    && location.UnitId > unit.Location.UnitId)
                {
                    location.UnitId--;
                }

                if (location.IsToTheLeftOfObstacle)
                {
                    location.PlatformCard.UnitsToLeftOfObstacle.Insert(location.UnitId, unit);
                    UpdateUnitsUnitId(location.PlatformCard.UnitsToLeftOfObstacle);
                }
                else
                {
                    location.PlatformCard.UnitsToRightOfObstacle.Insert(location.UnitId, unit);
                    UpdateUnitsUnitId(location.PlatformCard.UnitsToRightOfObstacle);
                }

                unit.Location.IsToTheLeftOfObstacle = location.IsToTheLeftOfObstacle;
                unit.Location.UnitId = location.UnitId;
                unit.Location.PlatformCard = location.PlatformCard;
                unit.Location.IsFacingLeft = location.IsFacingLeft;
                unit.Location.CurrentLevelId = location.CurrentLevelId;

                _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
                {
                    CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                    Target = unit,
                    Action = Constants.TraceActions.Moved,
                    From = from,
                    To = location
                });
            });
        }

        private static void UpdateUnitsUnitId(IEnumerable<IUnit> set)
        {
            foreach (var item in set.Select((x, i) => (x, i)))
            {
                item.x.Location.UnitId = item.i;
            }
        }

        public async Task ChangeStatisitcs(IUnit unit, StatisticChange statisticsChange)
        {
            if (statisticsChange == null)
            {
                return;
            }

            var currentUnitIndexInOrder = _gameStateProvider.GameState.UnitsOrder.IndexOf(unit);
            var hasDied = false;
            await _updateNotifier.StartOperation(() =>
             {
                 foreach (var item in statisticsChange)
                 {
                     unit.GetStatistic(item.Key.statistic).Add(item.Key.valueName, item.Value);
                 }

                 _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
                 {
                     CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                     Target = unit,
                     Action = Constants.TraceActions.StatisticsSubtracted,
                     StatisticChange = statisticsChange
                 });
             });


            if (unit.Life.Current <= 0)
            {
                hasDied = true;
               await _incidentsManager.HandleIncitent(new UnitDeathIncidentData { Unit = unit });
            }
        }

        public async Task StartSelectUnitsOrderAsync()
        {
            var characters = _gameStateProvider.GameState.UnitsOrder.OfType<Character>().ToArray();
            _gameStateProvider.GameState.UnitsOrder.Clear();
            // todo: foes should be ordered based on distance to characters and grouped by Danger
            var foes = _gameStateProvider.GameState.Units.OfType<Foe>().ToArray();
            var count = Math.Max(foes.Length, characters.Length);
            for (var i = 0; i < count; i++)
            {
                if (i < characters.Length)
                {
                    _gameStateProvider.GameState.UnitsOrder.Add(characters[i]);
                }
                else
                {
                    for (var j = i; j < foes.Length; j++)
                    {
                        _gameStateProvider.GameState.UnitsOrder.Add(foes[j]);
                    }

                    break;
                }

                if (i < foes.Length)
                {
                    _gameStateProvider.GameState.UnitsOrder.Add(foes[i]);
                }
                else
                {
                    for (var j = i + 1; j < characters.Length; j++)
                    {
                        _gameStateProvider.GameState.UnitsOrder.Add(characters[j]);
                    }

                    break;
                }
            }

            await this.Publish<SelectUnitsOrder>();
        }

        public void SetUnitsOrder(IEnumerable<IUnit> order)
        {
            _gameStateProvider.GameState.UnitsOrder.Clear();

            foreach (var item in order)
            {
                _gameStateProvider.GameState.UnitsOrder.Add(item);
            }

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Targets = order.ToArray(),
                Action = Constants.TraceActions.UnitsOrderSelected,
            });

            _gameStateProvider.UIState.UnitsOrderSelection.IsOpen = false;
        }

        public async Task ExecuteAction(
           IUnit actor,
           ActionSettings action,
           IEnumerable<IUnit> targets)
        {
            var result = await action.CalculateEffectsAsync(actor, targets, staticAnalisys: false, canCancel: true, _gameStateProvider);

            if (result.effect == null || !actor.HasStatisitcsToCoverCost(result.cost))
            {
                return;
            }

            var actionTask = this.Publish(new ActionMessage { Action = action, Actor = actor, Targets = targets });

            var statisticsUpdate = _updateNotifier.StartOperation(async () =>
            {
                await Apply(result.effect);

                _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
                {
                    CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                    Target = actor,
                    Targets = targets.ToArray(),
                    Action = Constants.TraceActions.Acted,
                });
            });

            await Task.WhenAll(new Task[] { actionTask, statisticsUpdate });
        }

        public async Task Apply(
            IEnumerable<EffectResult> effects,
            bool handleEndTurn = true)
        {
            foreach (var unit in effects.Select(x => x.Target).OfType<IUnit>())
            {
                effects = unit.CalculateTriggeredEffects(
                             Constants.Triggers.BeforeEffectApplied,
                             effects,
                             new NamedTargetsSets(),
                             _gameStateProvider);
            }

            foreach (var effect in effects)
            {
                await Apply(effect);
            }

            if (!handleEndTurn)
            {
                return;
            }

            var currentUnitEffect = effects.SingleOrDefault(x => x.Target == _gameStateProvider.GameState.CurrentUnit);
            if (currentUnitEffect?.EndTurn == true)
            {
                await _endTurnAction.Execute();
            }
        }

        private async Task Apply(EffectResult effect)
        {
            if (effect.Target is IUnit unit)
            {
                foreach (var item in effect.UtilizedAbiliites.Where(x => x.RemainingUseCount.HasValue))
                {
                    if (item.RemainingUseCount == 1)
                    {
                        unit.Abilities.Remove(item);
                    }
                    else
                    {
                        item.RemainingUseCount--;
                    }
                }

                await ChangeStatisitcs(unit, effect.StatisticsChange);

                foreach (var item in effect.AddAbilities)
                {
                    unit.Abilities.Add(item);
                }

                foreach (var item in effect.RemoveAbilities)
                {
                    var ability = unit.Abilities.First(x => x.Settings == item);
                    unit.Abilities.Remove(ability);
                }

                foreach (var item in effect.AddActions)
                {
                    unit.Actions.Add(item);
                }

                foreach (var item in effect.RemoveActions)
                {
                    unit.Actions.Remove(item);
                }

                if (effect.Target is Character character && character.Life.Current > 0)
                {
                    foreach (var itemSettings in effect.AddItems)
                    {
                        var item = new Item(_updateNotifier)
                        {
                            Settings = itemSettings
                        };
                        character.Inventory.Add(item);
                    }

                    foreach (var itemSettings in effect.RemoveItems)
                    {
                        var item = character.Inventory.First(x => x.Settings == itemSettings);
                        character.Inventory.Remove(item);
                    }

                    for (var i = 0; i < effect.DrawRuneCards; i++)
                    {
                        _characterIncidents.DrawCard((Character)effect.Target);
                    }
                }
            }
        }
    }
}