﻿using System.Linq;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Zenject;
using Random = UnityEngine.Random;

namespace Assets.GameLogic.Incidents
{
    public interface IItemIncidents
    {
        Item SpawnItemByName(string name);

        Item SpawnRandomStartingWeapon();
    }

    public class ItemIncidents : IItemIncidents
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

        public Item SpawnItemByName(string name)
            => new Item(_updateNotifier) { Settings = _gameStateProvider.GameSettings.Items.Single(x => x.Name == name) };

        public Item SpawnRandomStartingWeapon()
        {
            var items = _gameStateProvider.GameSettings.Items
                .Where(x => x.Starting && x.Kinds.Contains(Constants.ItemKinds.Weapon))
                .ToArray();
            var itemId = (int)(Random.value * items.Length);
            var item = new Item(_updateNotifier) { Settings = items[itemId] };
            return item;
        }
    }
}