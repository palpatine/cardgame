﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Features;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Zenject;

namespace Assets.GameLogic.Incidents
{
    
    public interface IPlatformIncidents
    {
        void AddItem(PlatformCard platformCard, Item item);

        void Explore(PlatformCard platform);

        Task RemoveHarvestable(IHarvestable harvestable);

        void RemoveItem(PlatformCard platformCard, Item item);

        void RemoveResource(PlatformCard card, Item item);

        Task<PlatformCard> RevealPlatformToLeft(int level);

        Task<PlatformCard> RevealPlatformToRight(int level);

        Task RevealStartingPlatforms(List<PlatformCard> startingPlatforms);

        void ReplacePlatforms(PlatformSelector platformSelector, string replacement);
    }

    public class PlatformIncidents : IPlatformIncidents
    {
#pragma warning disable CS0649

        [Inject]
        private readonly IEnumerable<IFeatureGenerator> _featureGenerators;

        [Inject]
        private readonly IFoesIncidents _foesIncidents;

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUnitIncidents _unitIncidents;

        [Inject]
        private IUpdateNotifier _updateNotifier;

        [Inject]
        private IPlatformFinder _platformFinder;

        [Inject]
        private IDangerIncidents _dangerIncidents;


#pragma warning restore CS0649

        public void AddItem(PlatformCard platformCard, Item item) => platformCard.Items.Add(item);

        public void CreateFeatureOnNewPlatformIfAppropriate(PlatformCard card)
        {
            if (card.Settings.Feature == null)
            {
                return;
            }

            foreach (var generator in _featureGenerators)
            {
                if (generator.CanHandle(card))
                {
                    generator.CreateFeatureOnNewPlatform(card);
                    break;
                }
            }

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Action = Constants.TraceActions.PlatformFeatureRevealed,
                ItemName = card.Features?.Kind,
                ItemType = card.Features?.Kind
            });
        }

        public void CreateResourcesOnNewPlatformIfAppropriate(PlatformCard card)
        {
            foreach (var resource in card.Settings.Resources)
            {
                var settings = _gameStateProvider.GameSettings.Items.Single(x => x.Name == resource.Name);
                var count = resource.Count;
                if (resource.FeatureMultiplier)
                {
                    count = count * (card.Features?.Count ?? 0);
                }

                for (var i = 0; i < count; i++)
                {
                    var item = new Item(_updateNotifier)
                    {
                        Settings = settings
                    };

                    card.Resources.Add(item);
                }
            }
        }

        public void Explore(PlatformCard platform)
        {
            platform.IsExplored = true;
            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Action = Constants.TraceActions.PlatformExplored,
                ItemName = platform.Settings.Name,
                ItemType = platform.Settings.GetType().Name
            });
        }

        public async Task RemoveHarvestable(IHarvestable harvestable)
        {
            if (harvestable is PlatformCard card)
            {
                card.Resources.Clear();
            }
            else if (harvestable is PlatformFeature platformFeaure)
            {
                platformFeaure.Location.Features = null;
            }
            else if (harvestable is Obstacle obstacle)
            {
                var units = obstacle.Location.UnitsToRightOfObstacle.ToArray();
                var tasks = units.Select(item => _unitIncidents.ChangeLocation(item, obstacle.Location.LocationAfterLastUnitToTheLeftOfObstacle(item.Location.IsFacingLeft)));
                await Task.WhenAll(tasks);
                obstacle.Location.Obstacle = null;
            }
        }

        public void RemoveItem(PlatformCard platformCard, Item item) => platformCard.Items.Remove(item);

        public void RemoveResource(PlatformCard card, Item item) => card.Resources.Remove(item);


        public async Task<PlatformCard> RevealPlatformToLeft(int level)
        {
            var card = _gameStateProvider.GameState.PlatformCards.Cards.Pop();
            card.Level = level;
            _gameStateProvider.GameState.Levels[level].Platforms.Insert(0, card);

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Action = Constants.TraceActions.PlatformRevealedToLeft,
                ItemName = card.Settings.Name,
                ItemType = card.Settings.GetType().Name
            });

            CreateFeatureOnNewPlatformIfAppropriate(card);
            CreateResourcesOnNewPlatformIfAppropriate(card);
            await _dangerIncidents.RevealNextDanger(card);

            return card;
        }

        public async Task<PlatformCard> RevealPlatformToRight(int level)
        {
            var card = _gameStateProvider.GameState.PlatformCards.Cards.Pop();
            card.Level = level;
            _gameStateProvider.GameState.Levels[level].Platforms.Add(card);

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Action = Constants.TraceActions.PlatformRevealedToRight,
                ItemName = card.Settings.Name,
                ItemType = card.Settings.GetType().Name
            });
            CreateFeatureOnNewPlatformIfAppropriate(card);
            CreateResourcesOnNewPlatformIfAppropriate(card);
            await _dangerIncidents.RevealNextDanger(card);

            return card;
        }

        public async Task RevealStartingPlatforms(List<PlatformCard> startingPlatforms)
        {
            foreach (var card in startingPlatforms)
            {
                await _updateNotifier.StartOperation(() =>
                {
                    _gameStateProvider.GameState.Levels[0].Platforms.Add(card);
                    _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
                    {
                        CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                        Action = Constants.TraceActions.PlatformRevealedToLeft,
                        ItemName = card.Settings.Name,
                        ItemType = card.Settings.GetType().Name
                    });
                    CreateFeatureOnNewPlatformIfAppropriate(card);
                    CreateResourcesOnNewPlatformIfAppropriate(card);
                });
            }
        }

        public void ReplacePlatforms(PlatformSelector platformSelector, string replacement)
        {
            var platforms = _platformFinder.Find(
             _gameStateProvider.GameState.CurrentUnit.Location.PlatformCard,
                 platformSelector);
            var replacementPlatformSettings = _gameStateProvider.GameSettings.PlatformDeck
                .Single(x => x.Name == replacement);

            foreach (var platform in platforms)
            {
                var set = _gameStateProvider.GameState.Levels[platform.Level].Platforms;
                var index = set.IndexOf(platform);
                set.Remove(platform);
                var newPlatform = new PlatformCard(_updateNotifier, replacementPlatformSettings)
                {
                    Level = platform.Level
                };
                CreateFeatureOnNewPlatformIfAppropriate(newPlatform);
                CreateResourcesOnNewPlatformIfAppropriate(newPlatform);
                foreach (var item in platform.UnitsToLeftOfObstacle)
                {
                    newPlatform.UnitsToLeftOfObstacle.Add(item);
                    item.Location.PlatformCard = newPlatform;
                }

                foreach (var item in platform.UnitsToRightOfObstacle)
                {
                    if (newPlatform.Obstacle != null)
                    {
                        newPlatform.UnitsToRightOfObstacle.Add(item);
                    }
                    else
                    {
                        newPlatform.UnitsToLeftOfObstacle.Add(item);
                        item.Location.IsToTheLeftOfObstacle = true;
                    }

                    item.Location.PlatformCard = newPlatform;
                }

                foreach (var item in platform.Items)
                {
                    newPlatform.Items.Add(item);
                }

                set.Insert(index, newPlatform);
            }
        }
    }
}