﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Messages;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;
using Zenject;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameLogic.Incidents
{
    public interface IDangerIncidents
    {
        Task RevealNextDanger(PlatformCard card);
    }

    public class DangerIncicents : IDangerIncidents
    {
#pragma warning disable CS0649
        [Inject]
        private IFoesIncidents _foesIncidents;

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private readonly IUnitIncidents _unitIncidents;

        [Inject]
        private IPlatformIncidents _platformIncidents;

#pragma warning restore CS0649

        public async Task RevealNextDanger(PlatformCard card)
        {
            var danger = _gameStateProvider.GameState.DangersDeck.Cards.Pop();
            await this.Publish(new DangerRevealed
            {
                DangerCard = danger
            });
            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Action = Constants.TraceActions.DangerRevealed,
                ItemName = danger.Settings.Name,
                ItemType = danger.Settings.GetType().Name
            });

            var foesSpawned = new List<Foe>();
            foreach (var effect in danger.Settings.Effects)
            {
                switch (effect.Kind)
                {
                    case Constants.Effects.SpawnFoe:
                        _foesIncidents.Spawn(card, effect.Foe, _gameStateProvider.GameState.Levels[card.Level].Platforms.IndexOf(card) == 0);
                        break;
                    case Constants.Effects.ReplacePlatform:
                        _platformIncidents.ReplacePlatforms(effect.PlatformSelector, effect.Replacement);
                        break;
                    default:
                        break;
                }
            }

            if (!foesSpawned.Any())
            {
                DangerResolved(danger);
            }
            else
            {
                IDisposable removeTracker = null;
                removeTracker = _gameStateProvider.GameState.TrackValue(x => x.Units, x =>
                {
                    if (x.Action == NotifyCollectionChangedAction.Remove)
                    {
                        foreach (var item in x.OldItems)
                        {
                            if (item is Foe foe)
                            {
                                foesSpawned.Remove(foe);
                            }
                        }

                        if (!foesSpawned.Any())
                        {
                            removeTracker.Dispose();
                            DangerResolved(danger);
                        }
                    }
                });
            }
        }

        public void DangerResolved(DangerCard danger)
        {
            var result = danger.Settings.Reward.DetermineResult(
                _gameStateProvider,
                Enumerable.Empty<ITarget>());

            _unitIncidents.Apply(result);
        }
    }
}