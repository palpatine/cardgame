﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;
using Zenject;

namespace Assets.GameLogic.Incidents
{
    public interface ICharacterIncidents : IUnitIncidents
    {
        ICharacterCard DrawCard(Character character);

        void DiscardCard(Character character, ICharacterCard card, bool permanently = false);

        void RecoverUnitStatisitcs(Character character);

        Task EquipItemAsync(Character character, Item item);

        void AddToInventory(Character character, Item item);

        void InitializeStatisitcs(
            Character unit,
            BaseStatistics statisticsSettints,
            StatisticReference statisticWithMode = null,
            int modeValue = 0);

        Task<bool> TryEquipItemAsync(
            Character character,
            Item item,
            string equipementKind);

        Task UnequipItemAsync(Character character, Item item);

        Task DropItemAsync(Character character, Item item);
    }

    internal class CharacterIncidents : UnitIncidents_, ICharacterIncidents
    {
#pragma warning disable CS0649

        [Inject]
        private IPlatformIncidents _platformIncidents;

#pragma warning restore CS0649

        public void AddToInventory(Character character, Item item)
        {
            character.Inventory.Add(item);
        }

        public void DiscardCard(Character character, ICharacterCard card, bool permanently = false)
        {
            character.Cards.Remove(card);
            if (!permanently)
            {
                character.Deck.Discarded.Add(card);
            }

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Target = character,
                Action = Constants.TraceActions.CardDiscarded,
                ItemName = card.Settings.Name,
                ItemType = card.Settings.GetType().Name
            });
        }

        public ICharacterCard DrawCard(Character character)
        {
            var card = character.Deck.Cards.Pop();
            character.Cards.Add(card);
            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                Target = character,
                Action = Constants.TraceActions.CardDrawn,
                ItemName = card.Settings.Name,
                ItemType = card.Settings.GetType().Name
            });
            return card;
        }

        public void InitializeStatisitcs(
            Character unit,
            BaseStatistics statisticsSettints,
            StatisticReference statisticWithMode = null,
            int modeValue = 0)
        {
            base.InitializeStatisitcs(unit, statisticsSettints, statisticWithMode, modeValue);
            unit.Damage.Current = statisticsSettints.Damage;
            unit.MagicDamage.Current = statisticsSettints.MagicDamage;
            unit.UseItem.Current = statisticsSettints.UseItem;
            unit.Interact.Current = statisticsSettints.Interact;
        }

        public async Task EquipItemAsync(Character character, Item item)
        {
            foreach (var equipementKind in _gameStateProvider.GameSettings.EquipableItemKinds)
            {
                if (await TryEquipItemAsync(character, item, equipementKind))
                    break;
            }
        }

        public async Task<bool> TryEquipItemAsync(
            Character character,
            Item item,
            string equipementKind)
        {
            if (item.Settings.Kinds.Contains(equipementKind))
            {
                await UnequipItemAsync(character, equipementKind);
                character.Equipment.Add(equipementKind, item);
                await ApplyEffectsAsync(character, item.Settings.Effects);
                character.Inventory.Remove(item);
                return true;
            }

            return false;
        }

        public async Task DropItemAsync(Character character, Item item)
        {
            await UnequipItemAsync(character, item);
            character.Inventory.Remove(item);
            _platformIncidents.AddItem(
                character.Location.PlatformCard,
                item);
        }

        public async Task UnequipItemAsync(Character character, Item item)
        {
            var kind = character.Equipment.FirstOrDefault(x => x.Value == item).Key;
            if (kind != null)
            {
                character.Inventory.Add(item);
                await ApplyReverseEffectsAsync(character, item.Settings.Effects);
                character.Equipment.Remove(kind);
            }
        }

        private async Task UnequipItemAsync(Character character, string equipementKind)
        {
            if (character.Equipment.ContainsKey(equipementKind))
            {
                var oldItem = character.Equipment[equipementKind];
                character.Inventory.Add(oldItem);
                await ApplyReverseEffectsAsync(character, oldItem.Settings.Effects);
                character.Equipment.Remove(equipementKind);
            }
        }

        private async Task ApplyEffectsAsync(Character character, IEnumerable<EffectSettings> effects)
        {
            var result = effects.DetermineResult(_gameStateProvider, new[] { character });
            await Apply(result);
        }

        private async Task ApplyReverseEffectsAsync(Character character, IEnumerable<EffectSettings> effects)
        {
            var result = effects.DetermineResultReverse(_gameStateProvider, new[] { character });
            await Apply(result);
        }

        public void RecoverUnitStatisitcs(Character character)
        {
            character.Energy.Regenerate();
            character.Life.Regenerate();
            character.Mana.Regenerate();
            character.Movement.Replenish();
            character.Attack.Replenish();
            character.Block.Replenish();
            character.Interact.Replenish();
            character.UseItem.Replenish();

            _gameStateProvider.GameState.GameTrace.Actions.Add(
                new ActionTrace
                {
                    CurrentUnit = _gameStateProvider.GameState.CurrentUnit,
                    Target = character,
                    Action = Constants.TraceActions.StatisitcsRecovered
                });
        }
    }
}