﻿using System.Linq;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Zenject;

namespace Assets.GameLogic.Features
{
    public class OreNuggetFeatureGenerator : IFeatureGenerator
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

        public bool CanHandle(PlatformCard card)
        {
            return card.Settings.Feature == Constants.Features.OreNugget;
        }

        public void CreateFeatureOnNewPlatform(PlatformCard card)
        {
            var featuresSettings = _gameStateProvider.GameSettings.Features
                .Single(x => x.Name == card.Settings.Feature);
            var existingFeaturesCount =
                    _gameStateProvider.GameState.Levels.SelectMany(
                        x => x.Platforms.Where(
                            z => (z.Features == null || z.Features.Kind == featuresSettings.Name)
                            && z.Settings.Kinds.Contains(featuresSettings.TriggeringPlatformKind)
                            && (z.Obstacle == null || z.Obstacle.Settings.Name == Constants.Obstacles.Boulder)))
                    .Select(x => x.Features?.Count ?? 0 + (x.Obstacle == null ? 0 : GetOreGain(x.Obstacle)))
                    .Where(x => x > 0)
                    .OrderBy(x => x);

            var countToAdd = 1;
            foreach (var featureCount in existingFeaturesCount)
            {
                if (featureCount != countToAdd)
                {
                    break;
                }

                countToAdd++;
            }

            var addObstacle = false;
            if (countToAdd >= 3)
            {
                addObstacle = true;
                countToAdd -= 3;
            }

            var features = new PlatformFeature
            {
                Kind = featuresSettings.Name,
                Count = countToAdd,
                Location = card
            };
            card.Features = features;

            if (addObstacle)
            {
                card.Obstacle = new Obstacle
                {
                    Settings = _gameStateProvider.GameSettings.Obstacles
                                .Single(x => x.Name == Constants.Obstacles.Boulder),
                    Location = card
                };
            }
        }

        private int GetOreGain(Obstacle boulder)
        {
            var result = new[] { boulder.Settings.Remedies.Effect }.DetermineResult(
                 _gameStateProvider, new[] { new Character(_updateNotifier) });

            return result.Single().AddItems.Count();
        }
    }
}