﻿using System.Linq;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Features
{
    public class TreeFeatureGenerator : IFeatureGenerator
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

        public bool CanHandle(PlatformCard card)
        {
            return card.Settings.Feature == Constants.Features.Tree;
        }

        public void CreateFeatureOnNewPlatform(PlatformCard card)
        {
            var featuresSettings = _gameStateProvider.GameSettings.Features.Single(x => x.Name == card.Settings.Feature);
            var platforms =
                    _gameStateProvider.GameState.Levels.SelectMany(
                        x => x.Platforms.Where(
                            z => z.Features?.Kind == featuresSettings.Name
                            && z.Settings.Kinds.Contains(featuresSettings.TriggeringPlatformKind)))
                    .OrderBy(x => x.Features.Count);

            var countToAdd = 1;
            foreach (var platform in platforms)
            {
                if (platform.Features.Count != countToAdd)
                {
                    break;
                }

                countToAdd++;
            }

            var features = new PlatformFeature
            {
                Kind = featuresSettings.Name,
                Count = countToAdd,
                Location = card
            };

            card.Features = features;
        }
    }
}