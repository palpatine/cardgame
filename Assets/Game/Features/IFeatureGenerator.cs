﻿using Assets.GameLogic.State;

namespace Assets.GameLogic.Features
{
    public interface IFeatureGenerator
    {
        bool CanHandle(PlatformCard card);

        void CreateFeatureOnNewPlatform(PlatformCard card);
    }
}