﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Tools
{
    public interface IMovementResultCalculator
    {
        IEnumerable<EffectResult> CalculateMovementResultForAdiecentPlatforms<TFoe>(
            IUnit unit,
            ILocation from,
            ILocation to,
            bool withoutMinimal = false)
            where TFoe : IUnit;
    }

    public class MovementResultCalculator : IMovementResultCalculator
    {
        private const int EnergyActionCost = 1;
        private const int MovementActionCost = 1;

#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

        public IEnumerable<EffectResult> CalculateMovementResultForAdiecentPlatforms<TFoe>(
            IUnit unit,
            ILocation from,
            ILocation to,
            bool withoutMinimal = false)
            where TFoe : IUnit
        {
            var currentPlatformCard = from.PlatformCard;
            var currentLevel = _gameStateProvider.GameState.Levels[from.CurrentLevelId];
            var currentPlatformIndex = currentLevel.Platforms.IndexOf(currentPlatformCard);

            var movingLeft = currentPlatformIndex > 0 && to.PlatformCard == currentLevel.Platforms[currentPlatformIndex - 1];
            var movingRight = currentPlatformIndex < currentLevel.Platforms.Count - 1 && to.PlatformCard == currentLevel.Platforms[currentPlatformIndex + 1];
            var movingAcrosLevels = currentPlatformCard.AdjacentPlatformsOnDifferentLevel.Contains(to.PlatformCard);
            var movingWithinCurrentPlatform = to.PlatformCard == currentPlatformCard
                && (from.IsToTheLeftOfObstacle != to.IsToTheLeftOfObstacle
                    || from.UnitId != to.UnitId && from.UnitId + 1 != to.UnitId
                    || from.IsFacingLeft != to.IsFacingLeft);

            if (!(movingLeft
                || movingRight
                || movingAcrosLevels
                || movingWithinCurrentPlatform))
            {
                return null;
            }

            var foesAlongTheWayCount = 0;

            IEnumerable<Obstacle> obstacles;
            obstacles = CalculateFoesAndObstacles<TFoe>(
               from,
               to,
               movingLeft,
               movingRight,
               movingWithinCurrentPlatform,
               out foesAlongTheWayCount);

            var platformsDistance = movingWithinCurrentPlatform ? 0 : 1;
            var movementCost = (platformsDistance + foesAlongTheWayCount) * MovementActionCost;
            var energyCost = (platformsDistance + foesAlongTheWayCount) * EnergyActionCost;

            var currentUnitResult = new EffectResult(unit) { };
            currentUnitResult.StatisticsChange.Add(Constants.Statisitcs.Energy, -energyCost);
            currentUnitResult.StatisticsChange.Add(Constants.Statisitcs.Movement, -movementCost);

            var effectsResult = obstacles.SelectMany(x => x.Settings.Effects)
                .DetermineResult(
                    _gameStateProvider,
                    new ITarget[] { unit },
                    new[] { currentUnitResult });

            if (withoutMinimal)
            {
                return effectsResult;
            }

            // movement if in any way significant must cost at least 1
            if (movingWithinCurrentPlatform)
            {
                currentUnitResult.StatisticsChange[Constants.Statisitcs.Movement]
                       = Math.Min(-1, currentUnitResult.StatisticsChange[Constants.Statisitcs.Movement]);
                currentUnitResult.StatisticsChange[Constants.Statisitcs.Energy]
                    = Math.Min(-1, currentUnitResult.StatisticsChange[Constants.Statisitcs.Energy]);
            }

            return effectsResult;
        }

        private IEnumerable<Obstacle> CalculateFoesAndObstacles<TFoe>(
            ILocation from,
            ILocation to,
            bool movingLeft,
            bool movingRight,
            bool movingWithinCurrentPlatform,
            out int foesAlongTheWayCount)
        {
            foesAlongTheWayCount = 0;
            IEnumerable<Obstacle> obstacles;
            if (movingRight)
            {
                foesAlongTheWayCount += CountFoesAlongTheWayMovingRight<TFoe>(
                    from,
                    to,
                    out obstacles);
            }
            else if (movingLeft)
            {
                foesAlongTheWayCount += CountFoesAlongTheWayMovingLeft<TFoe>(
                    from,
                    to,
                    out obstacles);
            }
            else if (movingWithinCurrentPlatform)
            {
                foesAlongTheWayCount += CountFoesAlongTheWayWithinCurrentPlatform<TFoe>(
                    from.PlatformCard,
                    to.IsToTheLeftOfObstacle,
                    to.UnitId,
                    from.IsToTheLeftOfObstacle,
                    from.UnitId,
                    out obstacles);
            }
            else
            {
                obstacles = Enumerable.Empty<Obstacle>();
            }

            return obstacles;
        }

        private static int CountFoesAlongTheWayMovingRight<TFoe>(
            ILocation from,
            ILocation to,
            out IEnumerable<Obstacle> obstaclesResult)
        {
            int foesAlongTheWayCount = 0;
            var obstacles = new List<Obstacle>();
            obstaclesResult = obstacles;
            // count all foes on currnet platform that are more to the right than character
            if (from.IsToTheLeftOfObstacle)
            {
                foesAlongTheWayCount += from.PlatformCard.AllUnits
                                     .Skip(from.UnitId + 1).Count(x => x is TFoe);

                if (from.PlatformCard.Obstacle != null)
                {
                    obstacles.Add(from.PlatformCard.Obstacle);
                }
            }
            else
            {
                foesAlongTheWayCount += from.PlatformCard.UnitsToRightOfObstacle
                                     .Skip(from.UnitId + 1).Count(x => x is TFoe);
            }

            // count all foes on target platform that are more to the left than character target location
            if (to.IsToTheLeftOfObstacle)
            {
                foesAlongTheWayCount += to.PlatformCard.UnitsToLeftOfObstacle.Take(to.UnitId)
                    .Count(x => x is TFoe);
            }
            else
            {
                foesAlongTheWayCount += to.PlatformCard.AllUnits
                                   .Take(to.UnitId).Count(x => x is TFoe);

                if (to.PlatformCard.Obstacle != null)
                {
                    obstacles.Add(to.PlatformCard.Obstacle);
                }
            }

            return foesAlongTheWayCount;
        }

        private static int CountFoesAlongTheWayMovingLeft<TFoe>(
            ILocation from,
            ILocation to,
            out IEnumerable<Obstacle> obstaclesResult)
        {
            int foesAlongTheWayCount = 0;
            var obstacles = new List<Obstacle>();
            obstaclesResult = obstacles;

            // count all foes on currnet platform that are more to the left than character
            if (!from.IsToTheLeftOfObstacle)
            {
                foesAlongTheWayCount += from.PlatformCard.AllUnits
                                     .Take(from.UnitId).Count(x => x is TFoe);

                if (from.PlatformCard.Obstacle != null)
                {
                    obstacles.Add(from.PlatformCard.Obstacle);
                }
            }
            else
            {
                foesAlongTheWayCount += from.PlatformCard.UnitsToLeftOfObstacle
                                     .Take(from.UnitId).Count(x => x is TFoe);
            }

            // count all foes on target platform that are more to the right than character target location
            if (!to.IsToTheLeftOfObstacle)
            {
                foesAlongTheWayCount += to.PlatformCard.UnitsToRightOfObstacle.Skip(to.UnitId).Count(x => x is TFoe);
            }
            else
            {
                foesAlongTheWayCount += to.PlatformCard.AllUnits
                                   .Skip(to.UnitId).Count(x => x is TFoe);

                if (to.PlatformCard.Obstacle != null)
                {
                    obstacles.Add(to.PlatformCard.Obstacle);
                }
            }

            return foesAlongTheWayCount;
        }

        private static int CountFoesAlongTheWayWithinCurrentPlatform<TFoe>(
            PlatformCard currentPlatformCard,
            bool isTargetToTheLeftOfObstacle,
            int targetUnitId,
            bool isCharacterToTheLeftOfObstacle,
            int currentUnitId,
            out IEnumerable<Obstacle> obstaclesResult)
        {
            var obstacles = new List<Obstacle>();
            obstaclesResult = obstacles;
            int foesAlongTheWayCount = 0;

            if (isTargetToTheLeftOfObstacle == isCharacterToTheLeftOfObstacle)
            {
                IEnumerable<IUnit> units;
                if (isTargetToTheLeftOfObstacle)
                {
                    units = currentPlatformCard.UnitsToLeftOfObstacle;
                }
                else
                {
                    units = currentPlatformCard.UnitsToRightOfObstacle;
                }

                foesAlongTheWayCount = units.Skip(Math.Min(targetUnitId, currentUnitId) + 1)
                    .Take(Math.Max(targetUnitId, currentUnitId) - Math.Min(targetUnitId, currentUnitId) - 1)
                    .Count(x => x is TFoe);
            }
            else if (isCharacterToTheLeftOfObstacle)
            {
                if (currentPlatformCard.Obstacle != null)
                {
                    obstacles.Add(currentPlatformCard.Obstacle);
                }

                foesAlongTheWayCount = currentPlatformCard.UnitsToLeftOfObstacle
                    .Skip(currentUnitId + 1)
                    .Concat(currentPlatformCard.UnitsToRightOfObstacle.Take(targetUnitId))
                    .Count(x => x is TFoe);
            }
            else if (isTargetToTheLeftOfObstacle)
            {
                if (currentPlatformCard.Obstacle != null)
                {
                    obstacles.Add(currentPlatformCard.Obstacle);
                }

                foesAlongTheWayCount = currentPlatformCard.UnitsToLeftOfObstacle
                    .Skip(targetUnitId)
                    .Concat(currentPlatformCard.UnitsToRightOfObstacle.Take(currentUnitId))
                    .Count(x => x is TFoe);
            }

            return foesAlongTheWayCount;
        }
    }
}