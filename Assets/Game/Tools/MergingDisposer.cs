﻿using System;

namespace Assets.GameLogic.Tools
{
    public class MergingDisposer : IDisposable
    {
        private readonly IDisposable[] items;

        public MergingDisposer(params IDisposable[] items)
        {
            this.items = items;
        }

        public void Dispose()
        {
            foreach (var item in items)
            {
                item.Dispose();
            }
        }
    }
}