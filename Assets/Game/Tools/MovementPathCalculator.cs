﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Processes;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Tools
{
    public class MovementPathCalculator : IMovementPathCalculator
    {
#pragma warning disable CS0649

        [Inject]
        private IMovementResultCalculator _movementResultCalculator;

        [Inject]
        private IGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

        public DistanceDescriptor GetDistance<TFoe>(IUnit unit, ILocation from, ILocation to)
            where TFoe : IUnit
        {
            var path = FindPathToTarget<TFoe>(unit, from, to, false);
            if (path == null) return null;

            var distance = new DistanceDescriptor();
            distance.PlatformsCount = path.Select(x => x.From.PlatformCard)
                .Concat(path.Select(x => x.To.PlatformCard)).Distinct().Count();

            foreach (var item in path)
            {
                if (item.From.PlatformCard == item.To.PlatformCard)
                {
                    if (item.IsMovingLeft(_gameStateProvider.GameState))
                    {
                        if (item.From.IsToTheLeftOfObstacle == item.To.IsToTheLeftOfObstacle)
                        {
                            var count = item.From.UnitId - item.To.UnitId;
                            distance.AbsoluteUnitCount += count;
                        }
                        else
                        {
                            var count = from.UnitsToLeft().Count() - item.To.UnitId;
                            distance.AbsoluteUnitCount += count;
                        }
                    }
                    else
                    {
                        if (item.From.IsToTheLeftOfObstacle == item.To.IsToTheLeftOfObstacle)
                        {
                            var count = item.To.UnitId - item.From.UnitId;
                            distance.AbsoluteUnitCount += count;
                        }
                        else
                        {
                            var count = from.VisibleUnitsToRight().Count() + item.To.UnitId;
                            distance.AbsoluteUnitCount += count;
                        }
                    }
                }
                else
                {
                    if (item.IsMovingLeft(_gameStateProvider.GameState))
                    {
                        var count = from.UnitsToLeft().Count()
                            + (item.To.IsToTheLeftOfObstacle
                                ? item.To.PlatformCard.UnitsToLeftOfObstacle.Count - item.To.UnitId
                                : item.To.PlatformCard.UnitsToRightOfObstacle.Count - item.To.UnitId);
                        distance.AbsoluteUnitCount += count;
                    }
                    else
                    {
                        var count = from.UnitsToRight().Count() + item.To.UnitId
                            + (item.To.IsToTheLeftOfObstacle
                                ? 0
                                : item.To.PlatformCard.UnitsToLeftOfObstacle.Count);

                        distance.AbsoluteUnitCount += count;
                    }
                }
            }

            return distance;
        }

        public IEnumerable<JourneyLegDescriptor> FindPathToTarget<TFoe>(IUnit unit, ILocation from, ILocation to, bool withoutMinimal)
            where TFoe : IUnit
        {
            var targetCardIndex = _gameStateProvider.GameState.Levels[to.CurrentLevelId]
                .Platforms.IndexOf(to.PlatformCard);

            if (ReferenceEquals(from.PlatformCard, to.PlatformCard))
            {
                var result = CalculateMovmementResult<TFoe>(unit, from, to, withoutMinimal);

                if (result == null)
                {
                    return null;
                }

                return new[] { new JourneyLegDescriptor{
                    From = from,
                    To = to,
                    EffectResults = result
                } };
            }

            var openList = new List<MapPathPointDescriptor>
            {
                new MapPathPointDescriptor
                {
                    Journey = new JourneyLegDescriptor
                    {
                      From = from,
                      To = from,
                      EffectResults = Enumerable.Empty<EffectResult>()
                    }
                }
            };

            var closeList = new List<MapPathPointDescriptor>();

            while (openList.Any())
            {
                var min = openList.Min(x => x.F);
                var current = openList.First(x => x.F == min);
                openList.Remove(current);

                var candidates = new List<MapPathPointDescriptor>();
                candidates.AddRange(current.Journey.To.PlatformCard.AdjacentPlatformsOnDifferentLevel
                    .Select(x =>
                        new MapPathPointDescriptor
                        {
                            Journey = new JourneyLegDescriptor
                            {
                                From = current.Journey.To,
                                To = x.LocationAsFirstUnit(isFacingLeft: true)
                            },
                            Parent = current
                        }));

                var cardToTheLeft = current.Journey.To.PlatformToLeft(_gameStateProvider.GameState);
                if (cardToTheLeft != null)
                {
                    candidates.Add(new MapPathPointDescriptor
                    {
                        Journey = new JourneyLegDescriptor
                        {
                            From = current.Journey.To,
                            To = cardToTheLeft.LocationAfterLastUnit(isFacingLeft: true),
                        },
                        Parent = current
                    });
                }

                var cardToTheRight = current.Journey.To.PlatformToRight(_gameStateProvider.GameState);
                if (cardToTheRight != null)
                {
                    candidates.Add(new MapPathPointDescriptor
                    {
                        Journey = new JourneyLegDescriptor
                        {
                            From = current.Journey.To,
                            To = cardToTheRight.LocationAsFirstUnit(isFacingLeft: false)
                        },
                        Parent = current
                    });
                }

                foreach (var item in candidates)
                {
                    if (ReferenceEquals(item.Journey.To.PlatformCard, to.PlatformCard))
                    {
                        item.Journey.To = to;
                        item.Journey.EffectResults = CalculateMovmementResult<TFoe>(
                            unit,
                            item.Journey.From,
                            item.Journey.To);

                        var journey = new List<JourneyLegDescriptor>();
                        var element = item;
                        while (element != null)
                        {
                            journey.Add(element.Journey);
                            element = element.Parent;
                        }

                        journey.Reverse();
                        journey.RemoveAt(0);
                        return journey;
                    }

                    item.Journey.EffectResults =
                                            CalculateMovmementResult<TFoe>(
                                                unit,
                                                item.Journey.From,
                                                item.Journey.To);

                    item.G = current.G + GetWeightedResult(unit, item.Journey.EffectResults);

                    var itemTargetCardIndex = _gameStateProvider.GameState.Levels[item.Journey.To.CurrentLevelId]
                                    .Platforms.IndexOf(item.Journey.To.PlatformCard);

                    item.F = item.G
                                            + Math.Abs(to.CurrentLevelId - item.Journey.To.CurrentLevelId)
                                            + Math.Abs(targetCardIndex - itemTargetCardIndex);

                    if (openList.Any(z => ReferenceEquals(z.Journey.To, item.Journey.To) && z.F < item.F)
                        || closeList.Any(z => ReferenceEquals(z.Journey.To, item.Journey.To) && z.F < item.F))
                    {
                        continue;
                    }

                    openList.Add(item);
                }

                closeList.Add(current);
            }

            return null;
        }

        private IEnumerable<EffectResult> CalculateMovmementResult<TFoe>(IUnit unit, ILocation from, ILocation to, bool withoutMinimal = false)
            where TFoe : IUnit
        {
            var result = _movementResultCalculator.CalculateMovementResultForAdiecentPlatforms<TFoe>(unit, from, to, withoutMinimal);
            if (from.PlatformCard != to.PlatformCard)
            {
                var resultWithPlatform = to.PlatformCard.Settings.EntranceEffects.DetermineResult(
                    _gameStateProvider,
                    new ITarget[] { unit }, result);
                return resultWithPlatform;
            }

            return result;
        }

        private double GetWeightedResult(IUnit unit, IEnumerable<EffectResult> result)
        {
            var changeForCurrentUnit = result.Single(x => ReferenceEquals(x.Target, unit));
            return changeForCurrentUnit.StatisticsChange
                .Where(x => x.Key.valueName == Constants.StatisitcValues.Current)
                .Sum(x => -x.Value * (_costWeights.ContainsKey(x.Key.statistic) ? _costWeights[x.Key.statistic] : 1));
        }

        private Dictionary<string, double> _costWeights = new Dictionary<string, double>
        {
            {Constants.Statisitcs.Movement, 1 },
            {Constants.Statisitcs.Energy, 2 },
            {Constants.Statisitcs.Life, 5 },
        };
    }
}