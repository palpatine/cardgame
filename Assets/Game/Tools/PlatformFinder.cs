﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Tools
{
    public interface IPlatformFinder
    {
        IEnumerable<PlatformCard> Find(PlatformCard current, PlatformSelector selector);

        int CountFeaures(PlatformCard current, FeatureSelector selector);
    }

    public class PlatformFinder : IPlatformFinder
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

        public IEnumerable<PlatformCard> Find(PlatformCard current, PlatformSelector selector)
        {
            if (selector.IdOffset != null)
            {
                if (selector.IdOffset == 0)
                {
                    return new[] { current };
                }
                else
                {
                    var platforms = _gameStateProvider.GameState.Levels[current.Level].Platforms;
                    var index = platforms.IndexOf(current)
                        + selector.IdOffset.Value;

                    return new[] { platforms[index] };
                }
            }

            return _gameStateProvider.GameState.Levels.SelectMany(x => x.Platforms)
                     .Where(x => IsMatch(x, selector))
                     .ToArray();
        }

        public int CountFeaures(PlatformCard current, FeatureSelector selector)
        {
            return _gameStateProvider.GameState.Levels.SelectMany(x => x.Platforms)
                     .Where(x => x.Features != null && x.Features.Kind == selector.Name)
                     .Sum(x => x.Features.Count);
        }

        private bool IsMatch(PlatformCard platform, PlatformSelector selector)
        {
            return (selector.Kind == null 
                || selector.Kind != null && platform.Settings.Kinds.Contains(selector.Kind))

                && (selector.Obstacle == null 
                || selector.Obstacle != null && platform.Obstacle?.Settings.Name == selector.Obstacle)

                && (selector.Name == null 
                || selector.Name != null && platform.Settings.Name == selector.Name)

                && (!selector.HasFeature.HasValue 
                || selector.HasFeature.HasValue
                    && selector.HasFeature == (platform.Features != null && platform.Features.Count > 0));
        }
    }
}