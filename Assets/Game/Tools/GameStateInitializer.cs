﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;
using Zenject;

namespace Assets.GameLogic.Tools
{
    public interface IGameStateInitializer
    {
        GameState CreateInitialGameState(GameSettings settings, out Func<Task> start);
    }

    public class GameStateInitializer : IGameStateInitializer
    {
#pragma warning disable CS0649

        [Inject]
        private IPlatformIncidents _platformIncidents;

        [Inject]
        private ICharacterIncidents _characterIncidents;

        [Inject]
        private IItemIncidents _itemIncidents;

        [Inject]
        private IUnitIncidents _unitIncidents;

        [Inject]
        private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

        public GameState CreateInitialGameState(GameSettings settings, out Func<Task> start)
        {
            var platformCards = CreatePlatformCards(settings);
            var startingPlatforms = DrawStartingPlatforms(platformCards, settings);
            var platformDeck = new Deck<PlatformCard>(platformCards);
            var mightDeck = new Deck<ICharacterCard>(CreateMightDeck(settings));
            var magicDeck = new Deck<ICharacterCard>(CreateMagicDeck(settings));
            var wildDeck = new Deck<ICharacterCard>(CreateWildDeck(settings));
            var state = CreateBasicState(settings, platformDeck, mightDeck, magicDeck, wildDeck);
            start = async () =>
            {
                await OnStart(startingPlatforms, settings, state);
            };

            return state;
        }

        private async Task OnStart(
            List<PlatformCard> startingPlatforms,
            GameSettings settings,
            GameState state)
        {
            await _platformIncidents.RevealStartingPlatforms(startingPlatforms);
            await PlaceCharacters(startingPlatforms, state);
            AddPlayerCards(state, settings);

            foreach (var character in state.Units.OfType<Character>())
            {
                var startingItem = _itemIncidents.SpawnRandomStartingWeapon();
                await _characterIncidents.EquipItemAsync(character, startingItem);

                foreach (var itemName in settings.Character.Equipment)
                {
                    var item = _itemIncidents.SpawnItemByName(itemName);
                    _characterIncidents.AddToInventory(character, item);
                    _characterIncidents.RecoverUnitStatisitcs(character);
                }
            }

            await _unitIncidents.StartSelectUnitsOrderAsync();
            state.CurrentUnit = state.UnitsOrder[0];
        }

        private async Task PlaceCharacters(List<PlatformCard> startingPlatforms, GameState state)
        {
            var startingPlatform = startingPlatforms[startingPlatforms.Count / 2];
            foreach (var character in state.Units)
            {
                await _updateNotifier.StartOperation(() =>
                {
                    character.Location.PlatformCard = startingPlatform;
                    character.Location.IsToTheLeftOfObstacle = true;
                    character.Location.UnitId = startingPlatform.UnitsToLeftOfObstacle.Count();
                    startingPlatform.UnitsToLeftOfObstacle.Add(character);
                });
            }
        }

        private void AddPlayerCards(GameState state, GameSettings settings)
        {
            foreach (Character character in state.Units)
            {
                for (int i = 0; i < settings.StartingRuneCardsCount; i++)
                {
                    _characterIncidents.DrawCard(character);
                }
            };
        }

        private GameState CreateBasicState(
            GameSettings settings,
            Deck<PlatformCard> platformDeck,
            Deck<ICharacterCard> mightDeck,
            Deck<ICharacterCard> magicDeck,
            Deck<ICharacterCard> wildDeck)
        {
            var state = new GameState(_updateNotifier)
            {
                PlatformCards = platformDeck,
                DangersDeck = new Deck<DangerCard>(CreateDangerCards(settings))
            };

            state.Units.Add(CreateCharacter(settings, mightDeck, Color.red));
            state.Units.Add(CreateCharacter(settings, magicDeck, Color.blue));
            state.Units.Add(CreateCharacter(settings, wildDeck, new Color(0, 166 / 255f, 0)));

            foreach (var item in state.Units)
            {
                state.UnitsOrder.Add(item);
            }

            var gameLevel = new GameLevel(_updateNotifier);
            state.Levels.Add(gameLevel);

            return state;
        }

        private Character CreateCharacter(
            GameSettings settings,
            Deck<ICharacterCard> deck,
            Color color)
        {
            var character = new Character(_updateNotifier)
            {
                Deck = deck,
                Color = color,
            };

            _characterIncidents.InitializeStatisitcs(character, settings.Character.BaseStatistics);

            foreach (var item in settings.Character.Abilities)
            {
                character.Abilities.Add(
                    new Ability { Settings = settings.Abilities.Single(x => x.Name == item) });
            }

            return character;
        }

        private List<PlatformCard> DrawStartingPlatforms(List<PlatformCard> platformCards, GameSettings settings)
        {
            var startingCards = new List<PlatformCard>();
            foreach (var startingPlatform in settings.StartingPlatforms)
            {
                var platform = platformCards.First(x => x.Settings.Name == startingPlatform);
                startingCards.Add(platform);
                platformCards.Remove(platform);
            }

            return startingCards;
        }

        private IEnumerable<ICharacterCard> CreateMightDeck(GameSettings settings) =>
            settings.Runes.Where(x => x.Decks.Contains(Constants.Decks.Might))
                                        .SelectMany(might =>
                                        Enumerable.Range(0, might.CountInDeck)
                                            .Select(
                                                id => new RuneCard
                                                {
                                                    Settings = might
                                                }))
                                           .Cast<ICharacterCard>()
                                           .Concat(
                                               settings.ActionCards
                                        .SelectMany(action =>
                                        Enumerable.Range(0, action.CountInDeck)
                                            .Select(
                                                id => new ActionCard
                                                {
                                                    Settings = action
                                                })));

        private IEnumerable<ICharacterCard> CreateMagicDeck(GameSettings settings) =>
            settings.Runes.Where(x => x.Decks.Contains(Constants.Decks.Magic))
                                        .SelectMany(magic =>
                                        Enumerable.Range(0, magic.CountInDeck)
                                            .Select(
                                                id => new RuneCard
                                                {
                                                    Settings = magic
                                                }))
                                           .Cast<ICharacterCard>()
                                           .Concat(
                                               settings.ActionCards
                                        .SelectMany(action =>
                                        Enumerable.Range(0, action.CountInDeck)
                                            .Select(
                                                id => new ActionCard
                                                {
                                                    Settings = action
                                                })));

        private IEnumerable<ICharacterCard> CreateWildDeck(GameSettings settings) =>
            settings.Runes.Where(x => x.Decks.Contains(Constants.Decks.Wild))
                                       .SelectMany(wild =>
                                       Enumerable.Range(0, wild.CountInDeck)
                                           .Select(
                                               id => new RuneCard
                                               {
                                                   Settings = wild
                                               }))
                                          .Cast<ICharacterCard>()
                                          .Concat(
                                              settings.ActionCards
                                       .SelectMany(action =>
                                       Enumerable.Range(0, action.CountInDeck)
                                           .Select(
                                               id => new ActionCard
                                               {
                                                   Settings = action
                                               })));

        private List<PlatformCard> CreatePlatformCards(GameSettings settings) => settings.PlatformDeck
                                        .SelectMany(platform =>
                                        Enumerable.Range(0, platform.CountInDeck)
                                            .Select(
                                                id => new PlatformCard(_updateNotifier, platform)))
                                                .ToList();

        private List<DangerCard> CreateDangerCards(GameSettings settings) => settings.Dangers
                                        .SelectMany(danger =>
                                        Enumerable.Range(0, danger.CountInDeck)
                                            .Select(
                                                id => new DangerCard
                                                {
                                                    Settings = danger
                                                })).ToList();
    }
}