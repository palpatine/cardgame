﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Attitudes;
using Assets.GameLogic.Features;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Load.Settings;
using Assets.GameLogic.Processes;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

namespace Assets.GameLogic
{
    internal class ApplicationInstaller : MonoInstaller<ApplicationInstaller>
    {
        public Transform _root;

        public override void InstallBindings()
        {
            RegisterGameLogicElements();
            RegisterUIElements();
        }

        private void RegisterActionHandlers()
        {
            Container.Bind<IFoeActionManager>().To<FoeActionManager>().AsTransient();
        }

        private void RegisterActions()
        {
            var actions = new Type[] {
                typeof(EndTurnAction),
            };

            Container.Bind<IMoveActivity>().To<MoveAction>().AsCached();
            Container.Bind<IExecuteActionActivity>().To<ExecuteActionAction>().AsCached();
            Container.Bind<IRevealLeftActivity>().To<RevealLeftAction>().AsCached();
            Container.Bind<IRevealRightActivity>().To<RevealRightAction>().AsCached();
            Container.Bind<IExploreActivity>().To<ExploreAction>().AsCached();
            Container.Bind<IActivateRuneCardActivity>().To<ActivateRuneCardAction>().AsCached();
            Container.Bind<IEndTurnActivity>().To<EndTurnAction>().AsCached();
            Container.Bind<IPickUpItemActivity>().To<PickUpItemAction>().AsCached();
            Container.Bind<IHarvestActivity>().To<HarvestAction>().AsCached();

            RegisterAsCollection<IAction>(actions);
        }

        private void RegisterActionUIManger()
        {
            var actions = new Type[] {
                typeof(ExploreActivityUIManger),
                typeof(MoveActivityUIManger),
                typeof(ExecuteActionActivityUIManger),
                typeof(RevealPlatformActivityUIManager),
                typeof(UseActionCardActivityUIManager),
                typeof(UseRuneCardActivityUIManager),
                typeof(PickUpItemActivityUIManager),
                typeof(HarvestActivityUIManger),
            };

            Container.Bind(actions).AsTransient();

            Container.Bind<IEnumerable<IActivityUIManager>>().FromMethod(() =>
                actions.Select(x => (IActivityUIManager)Container.Resolve(x)).ToArray()
            );

            Container.Bind<IActivitySummaryProvider>().To<ActivitySummaryProvider>().AsTransient();
        }

        private void RegisterAsCollection<T>(Type[] generators)
        {
            Container.Bind(generators).AsTransient();
            Container.Bind<IEnumerable<T>>().FromMethod(() =>
                generators.Select(x => (T)Container.Resolve(x)).ToArray()
            );
        }

        private void RegisterCards()
        {
            Container.BindMemoryPool<PlatformCardBehaviour, PlatformCardBehaviour.Pool>()
                     .FromComponentInNewPrefab(
                        Resources.Load<GameObject>(UIConstants.Cards.PlatformCardPrefab.Location));
            Container.Bind<IPlatformFeatureBehaviourPool>().To<PlatformFeatureBehaviourPool>().AsTransient();
            Container.Bind<IPlatformObstacleBehaviourPool>().To<PlatformObstacleBehaviourPool>().AsCached();
            Container.BindMemoryPool<ActionCardBehaviour, ActionCardBehaviour.Pool>()
                     .FromComponentInNewPrefab(
                        Resources.Load<GameObject>(UIConstants.Cards.ActionCardPrefab.Location));
            Container.BindMemoryPool<RuneCardBehaviour, RuneCardBehaviour.Pool>()
          .FromComponentInNewPrefab(
             Resources.Load<GameObject>(UIConstants.Cards.RuneCardPrefab.Location));
            Container.BindMemoryPool<DangerCardBehaviour, DangerCardBehaviour.Pool>()
                         .FromComponentInNewPrefab(
                            Resources.Load<GameObject>(UIConstants.Cards.DangerCardPrefab.Location));
        }

        private void RegisterCharacterDetails()
        {
            Container.BindFactory<Character, CharacterToggleBehaviour, CharacterToggleBehaviour.Factory>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.TogglePrefab.Location));

            Container.BindMemoryPool<RegenerableStatisticBehaviour, RegenerableStatisticBehaviour.Pool>()
                                .FromComponentInNewPrefab(
                                   Resources.Load<GameObject>(UIConstants.UI.RegenerableStatisticPrefab.Location));

            Container.BindMemoryPool<ReplenishableStatisticBehaviour, ReplenishableStatisticBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.ReplenishableStatisticPrefab.Location));

            Container.BindMemoryPool<ValueStatisticBehaviour, ValueStatisticBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.ValueStatisticPrefab.Location));

            Container.BindMemoryPool<EquipmentSlotBehaviour, EquipmentSlotBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.EquipmentSlotPrefab.Location));

            Container.BindMemoryPool<InventoryItemBehaviour, InventoryItemBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.InventoryItemPrefab.Location));

            Container.Bind<Transform>()
                .WithId(UIConstants.UI.CardsInventoryTag)
                                        .FromMethod(() => GameObject.FindWithTag(UIConstants.UI.CardsInventoryTag).transform)
                                        .AsTransient();

            Container.Bind<Transform>()
                .WithId(UIConstants.UI.CharacterDetailsContainerTag)
                                        .FromMethod(() => GameObject.FindWithTag(UIConstants.UI.CharacterDetailsContainerTag).transform)
                                        .AsTransient();
        }

        private void RegisterFeatureGenerators()
        {
            var generators = new Type[] {
                typeof(TreeFeatureGenerator),
                typeof(OreNuggetFeatureGenerator),
            };

            RegisterAsCollection<IFeatureGenerator>(generators);
        }

        private void RegisterFeaturesAndObjstacles()
        {
            Container.BindMemoryPool<TreeBehaviour, TreeBehaviour.Pool>()
                .FromComponentInNewPrefab(Resources.Load<GameObject>(UIConstants.Cards.TreePrefab.Location));

            Container.BindMemoryPool<OreNuggetBehaviour, OreNuggetBehaviour.Pool>()
                .FromComponentInNewPrefab(Resources.Load<GameObject>(UIConstants.Cards.OreNuggetPrefab.Location));

            Container.BindMemoryPool<BoulderBehaviour, BoulderBehaviour.Pool>()
                .FromComponentInNewPrefab(Resources.Load<GameObject>(UIConstants.Cards.BoulderPrefab.Location));
        }

        private void RegisterFoeAttitudes()
        {
            var behaviours = new Type[] {
                typeof(AggressiveAttitude),
                typeof(TacticalAttitude),
                typeof(DefensiveAttitude),
            };

            RegisterAsCollection<IFoeAttitude>(behaviours);
        }

        private void RegisterGameLogicElements()
        {
            Container.Bind<IGameStateProvider>().FromMethod(() => _root.GetComponent<GameManager>()).AsCached();
            Container.Bind<IUIGameStateProvider>().FromMethod(() => _root.GetComponent<GameManager>()).AsCached();

            RegisterTools();
            RegisterActionHandlers();
            RegisterMovementHandlers();
            RegisterIncidents();
            RegisterActions();
            RegisterFeatureGenerators();
            RegisterFoeAttitudes();
        }

        private void RegisterIncidents()
        {
            Container.Bind<IGameStateInitializer>().To<GameStateInitializer>().AsCached();
            Container.Bind<IPlatformIncidents>().To<PlatformIncidents>().AsCached();
            Container.Bind<IDangerIncidents>().To<DangerIncicents>().AsCached();
            Container.Bind<ICharacterIncidents>().To<CharacterIncidents>().AsCached();
            Container.Bind<IFoesIncidents>().To<FoesIncidents>().AsCached();
            Container.Bind<IUnitIncidents>().To<UnitIncidents_>().AsCached();
            Container.Bind<IItemIncidents>().To<ItemIncidents>().AsCached();
        }

        private void RegisterMainUIElements()
        {
            Container.Bind<PlatformCardsLayoutBehaviour>()
                                        .FromMethod(() => GameObject.FindWithTag(UIConstants.UI.MapContainerTag).GetComponent<PlatformCardsLayoutBehaviour>())
                                        .AsSingle();

            Container.Bind<Transform>()
                .WithId(UIConstants.UI.AnimationAreaTag)
                                        .FromMethod(() => GameObject.FindWithTag(UIConstants.UI.AnimationAreaTag).transform)
                                        .AsTransient();

            Container.BindFactory<LevelContainerBehaviour, LevelContainerBehaviour.Factory>()
                .FromComponentInNewPrefab(Resources.Load<GameObject>(
                    UIConstants.UI.LevelContainerPrefab.Location));

            Container.BindMemoryPool<UnitBehaviour, UnitBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UnitPrefab.Location));

            Container.BindMemoryPool<UnitStatisticChangeIndicator, UnitStatisticChangeIndicator.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UnitStatisticChangeIndicatorPrefab.Location));

            Container.BindMemoryPool<UnitAvatarBehaviour, UnitAvatarBehaviour.Pool>()
                   .FromComponentInNewPrefab(
                      Resources.Load<GameObject>(UIConstants.UnitAvatarPrefab.Location));

            Container.BindMemoryPool<ActivityOptionsSetBehaviour, ActivityOptionsSetBehaviour.Pool>()
                   .FromComponentInNewPrefab(
                      Resources.Load<GameObject>(UIConstants.ActivityOptionsSetPrefab.Location));

            Container.BindMemoryPool<ActivityOptionBehaviour, ActivityOptionBehaviour.Pool>()
                   .FromComponentInNewPrefab(
                      Resources.Load<GameObject>(UIConstants.ActivityOptionPrefab.Location));

            Container.BindMemoryPool<ActionBehaviour, ActionBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.ActionPrefab.Location));

            Container.BindMemoryPool<LevelEdgeBehaviour, LevelEdgeBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.LevelEdgePrefab.Location));

            Container.BindMemoryPool<ValueSelectorBehaviour, ValueSelectorBehaviour.Pool>()
                    .FromComponentInNewPrefab(
                       Resources.Load<GameObject>(UIConstants.UI.ValueSelector.Location));
        }

        private void RegisterMovementHandlers()
        {
            Container.Bind<IMovementResultCalculator>().To<MovementResultCalculator>().AsTransient();
            Container.Bind<IMovementPathCalculator>().To<MovementPathCalculator>().AsTransient();
            Container.Bind<IFoeMovementManager>().To<FoeMovementManager>().AsTransient();
        }

        private void RegisterTools()
        {
            Container.Bind<IPlatformFinder>().To<PlatformFinder>().AsTransient();
        }

        private void RegisterUIElements()
        {
            RegisterMainUIElements();
            RegisterCards();
            RegisterCharacterDetails();
            RegisterFeaturesAndObjstacles();
            RegisterActionUIManger();
            RegisterUITools();
        }

        private void RegisterUITools()
        {
            Container.Bind<IRayCastManger>().To<RayCastManger>().AsTransient();
            Container.Bind<ITooltipManager>()
                                        .FromMethod(() => GameObject.FindWithTag(UIConstants.UI.TooltipDisplay.TooltipTag)
                                        .GetComponent<TooltipManager>())
                                        .AsSingle();
        }
    }
}