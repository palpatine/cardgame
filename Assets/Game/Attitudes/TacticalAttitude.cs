﻿using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Attitudes
{
    internal class TacticalAttitude : IFoeAttitude
    {
#pragma warning disable CS0649

        [Inject]
        private IFoeMovementManager _foeMovementManager;

        [Inject]
        private IFoeActionManager _foeActionManager;

        [Inject]
        private IGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

        public string AttitudeName => Constants.FoeAttitudes.Tactical;

        public async Task ExecuteActionAsync(Foe foe)
        {
            do
            {
                await TacticalMove(foe);

                bool hasActed;
                bool canAct;

                do
                {
                    (hasActed, canAct) = await _foeActionManager.TryAct(foe);
                } while (hasActed);

                if (canAct || !await Move(foe))
                {
                    return;
                }
            } while (foe.Energy.Current > 0);
        }

        private async Task TacticalMove(Foe foe)
        {
            var charactersOnPlatform = foe.VisibleUnitsToLeft()
                .Concat(foe.VisibleUnitsToRight())
                .OfType<Character>()
                .ToArray();

            if (!charactersOnPlatform.Any() || foe.Movement.Current == 0 || foe.Energy.Current <= 1)
            {
                return;
            }

            var closest = _foeMovementManager.FindClosestPathToAnyCharacter(foe, charactersOnPlatform, this);

            if (closest != null)
            {
                await _foeMovementManager.MoveAsFarAsPossible(foe, closest);
            }
        }

        public async Task<bool> Move(Foe foe)
        {
            var closest = _foeMovementManager.FindClosestPathToAnyCharacter(foe, this);
            if (closest != null)
            {
                var hasMoved = await _foeMovementManager.MoveAsFarAsPossible(foe, closest);

                if (foe.Life.Current == 0)
                    return false;

                return hasMoved;
            }

            return false;
        }

        public int Evaluate(Foe foe, ILocation targetLocation)
        {
            var enemyLocation = targetLocation.Clone();
            if (targetLocation.IsFacingLeft)
            {
                enemyLocation.UnitId--;
            }

            var value = 0;
            var locaitonToLeft = enemyLocation.Clone();
            locaitonToLeft.UnitId--;

            if (locaitonToLeft.UnitAtThisLocation() is Foe leftAlly)
            {
                if (leftAlly == foe && (foe.Location.UnitId == targetLocation.UnitId || foe.Location.UnitId == targetLocation.UnitId - 1))
                {
                    value += 100;
                }
                else if (!leftAlly.Location.IsFacingLeft && targetLocation.IsFacingLeft)
                {
                    value += 20;
                }
                else if (!leftAlly.Location.IsFacingLeft)
                {
                    value -= 40;
                }
            }

            var locaitonToRight = enemyLocation.Clone();
            locaitonToRight.UnitId++;
            if (locaitonToRight.UnitAtThisLocation() is Foe rightAlly)
            {
                if (rightAlly == foe && (foe.Location.UnitId == targetLocation.UnitId || foe.Location.UnitId == targetLocation.UnitId - 1))
                {
                    value += 100;
                }
                else if (rightAlly.Location.IsFacingLeft && !targetLocation.IsFacingLeft)
                {
                    value += 20;
                }
                else if (rightAlly.Location.IsFacingLeft)
                {
                    value -= 40;
                }
            }

            return value;
        }

        public int Evaluate(Foe foe, StatisticCost totalCost)
        {
            var value = 0;
            if (totalCost.ContainsKey(Constants.Statisitcs.Life))
            {
                value -= 10 * totalCost[Constants.Statisitcs.Life];
            }

            foreach (var item in totalCost)
            {
                value -= item.Value;
            }

            return value;
        }
    }
}