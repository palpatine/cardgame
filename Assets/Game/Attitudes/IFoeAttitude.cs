﻿using System.Threading.Tasks;
using Assets.GameLogic.State;

namespace Assets.GameLogic.Attitudes
{
    public interface IFoeAttitude : IJourneyEvaluator
    {
        string AttitudeName { get; }

        Task ExecuteActionAsync(Foe foe);
    }
}