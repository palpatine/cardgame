﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Processes;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Zenject;

namespace Assets.GameLogic.Attitudes
{
    public interface IFoeMovementManager
    {
        Task<bool> MoveAsFarAsPossible(Foe foe, IEnumerable<JourneyLegDescriptor> journey);

        IEnumerable<JourneyLegDescriptor> FindClosestPathToAnyCharacter(Foe foe,
            IJourneyEvaluator journeyEvaluator);

        IEnumerable<JourneyLegDescriptor> FindClosestPathToAnyCharacter(Foe foe, IEnumerable<Character> characters,
            IJourneyEvaluator journeyEvaluator);

        IEnumerable<JourneyLegDescriptor> FindClosestPathTo(Foe foe, ILocation location);

        IEnumerable<ILocation> GetLocationsAdjecentToCharacters(
            IEnumerable<Character> characters);
    }

    public class FoeMovementManager : IFoeMovementManager
    {
#pragma warning disable CS0649

        [Inject]
        private IMovementPathCalculator _movementPathCalculator;

        [Inject]
        private IMovementResultCalculator _movementCostCalculator;

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUnitIncidents _unitIncidents;

#pragma warning restore CS0649

        public async Task<bool> MoveAsFarAsPossible(Foe foe, IEnumerable<JourneyLegDescriptor> journey)
        {
            JourneyLegDescriptor nextLeg = null;
            var hasMoved = false;

            foreach (var item in journey)
            {
                if (foe.HasStatisitcsToCoverCost(item.EffectResults))
                {
                    await _unitIncidents.Apply(item.EffectResults);
                    await _unitIncidents.ChangeLocation(foe, item.To);
                    hasMoved = true;

                    if (foe.Life.Current == 0)
                        return false;
                }
                else
                {
                    nextLeg = item;
                    break;
                }
            }

            if (foe.Life.Current == 0)
                return false;

            if (nextLeg == null || foe.Energy.Current == 0 || foe.Movement.Current == 0)
                return hasMoved;

            if (nextLeg.IsMovingLeft(_gameStateProvider.GameState))
            {
                var current = nextLeg.To;
                do
                {
                    var target = current.MoveRightANotch<Foe>(_gameStateProvider.GameState);
                    target.IsFacingLeft = target.AdiecentVisibleUnitToLeft() is Character;
                    if (target == null)
                    {
                        break;
                    }

                    var cost = _movementCostCalculator.CalculateMovementResultForAdiecentPlatforms<Character>(
                        foe,
                        nextLeg.From,
                        target);

                    if (foe.HasStatisitcsToCoverCost(cost))
                    {
                        await _unitIncidents.Apply(cost);
                        await _unitIncidents.ChangeLocation(foe, target);
                        hasMoved = true;
                        break;
                    }

                    current = target;
                }
                while (current != null);
            }
            else if (nextLeg.IsMovingRight(_gameStateProvider.GameState))
            {
                var current = nextLeg.To;
                do
                {
                    var target = current.MoveLeftANotch<Foe>(_gameStateProvider.GameState);
                    target.IsFacingLeft = target.AdiecentVisibleUnitToLeft() is Character;
                    if (target == null)
                    {
                        break;
                    }

                    var cost = _movementCostCalculator.CalculateMovementResultForAdiecentPlatforms<Character>(
                        foe,
                        nextLeg.From,
                        target);

                    if (foe.HasStatisitcsToCoverCost(cost))
                    {
                        await _unitIncidents.Apply(cost);
                        await _unitIncidents.ChangeLocation(foe, target);
                        hasMoved = true;
                        break;
                    }

                    current = target;
                }
                while (current != null);
            }

            return hasMoved;
        }

        public IEnumerable<JourneyLegDescriptor> FindClosestPathTo(Foe foe, ILocation to)
        {
            return _movementPathCalculator.FindPathToTarget<Character>(foe, foe.Location, to, false);
        }

        public IEnumerable<JourneyLegDescriptor> FindClosestPathToAnyCharacter(
            Foe foe,
            IJourneyEvaluator journeyEvaluator)
        {
            return FindClosestPathToAnyCharacter(foe, _gameStateProvider.GameState.Units.OfType<Character>(), journeyEvaluator);
        }

        public IEnumerable<JourneyLegDescriptor> FindClosestPathToAnyCharacter(
            Foe foe,
            IEnumerable<Character> characters,
            IJourneyEvaluator journeyEvaluator)
        {
            var journeys = GetLocationsAdjecentToCharacters(characters)
                .Select(c => new
                {
                    TargetLocationValue = journeyEvaluator.Evaluate(foe, c),
                    Journey = _movementPathCalculator
                    .FindPathToTarget<Character>(foe, foe.Location, c, false)
                })
                .Select(c => new
                {
                    c.Journey,
                    c.TargetLocationValue,
                    TotalResult = c.Journey?.Select(x => x.EffectResults).MergeResult()
                })
                .Select(c => new
                {
                    c.Journey,
                    TotalJourneyValue = c.TargetLocationValue
                    + (c.TotalResult != null ? journeyEvaluator.Evaluate(foe, c.TotalResult.GetCost(foe)) : 0),
                })
                .OrderByDescending(x => x.TotalJourneyValue)
                .ToList();

            var preffered = journeys.FirstOrDefault();
            return journeys
                .TakeWhile(x => x.TotalJourneyValue == preffered.TotalJourneyValue)
                .Where(x => x.Journey != null)
                .FirstOrDefault()?.Journey;
        }

        public IEnumerable<ILocation> GetLocationsAdjecentToCharacters(
            IEnumerable<Character> characters)
        {
            return characters
                .SelectMany(c => new ILocation[]
                {
                    new LocationDescriptor
                    {
                        CurrentLevelId = c.Location.CurrentLevelId,
                        IsFacingLeft = false,
                        IsToTheLeftOfObstacle = c.Location.IsToTheLeftOfObstacle,
                        PlatformCard = c.Location.PlatformCard,
                        UnitId = c.Location.UnitId
                    },
                    new LocationDescriptor
                    {
                        CurrentLevelId = c.Location.CurrentLevelId,
                        IsFacingLeft = true,
                        IsToTheLeftOfObstacle = c.Location.IsToTheLeftOfObstacle,
                        PlatformCard = c.Location.PlatformCard,
                        UnitId = c.Location.UnitId + 1
                    }
                });
        }
    }
}