﻿using System.Threading.Tasks;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Attitudes
{
    internal class AggressiveAttitude : IFoeAttitude
    {
#pragma warning disable CS0649

        [Inject]
        private IFoeMovementManager _foeMovementManager;

        [Inject]
        private IFoeActionManager _foeActionManager;

#pragma warning restore CS0649

        public string AttitudeName => Constants.FoeAttitudes.Aggressive;

        public async Task ExecuteActionAsync(Foe foe)
        {
            do
            {
                bool hasActed;
                bool canAct;

                do
                {
                    (hasActed, canAct) = await _foeActionManager.TryAct(foe);
                } while (hasActed);

                if (canAct || !await Move(foe))
                {
                    return;
                }
            } while (foe.Energy.Current > 0);
        }

        public async Task<bool> Move(Foe foe)
        {
            var closest = _foeMovementManager.FindClosestPathToAnyCharacter(foe, this);
            if (closest != null)
            {
                var hasMoved = await _foeMovementManager.MoveAsFarAsPossible(foe, closest);

                if (foe.Life.Current == 0)
                    return false;

                return hasMoved;
            }

            return false;
        }

        public int Evaluate(Foe foe, ILocation targetLocation)
        {
            return 0;
        }

        public int Evaluate(Foe foe, StatisticCost totalCost)
        {
            var value = 0;
            if (totalCost.ContainsKey(Constants.Statisitcs.Life))
            {
                value -= 10 * totalCost[Constants.Statisitcs.Life];
            }

            foreach (var item in totalCost)
            {
                value -= item.Value;
            }

            return value;
        }
    }
}