﻿using Assets.GameLogic.State;

namespace Assets.GameLogic.Attitudes
{
    public interface IJourneyEvaluator
    {
        int Evaluate(Foe foe, ILocation location);

        int Evaluate(Foe foe, StatisticCost totalCost);
    }
}