﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Processes;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Zenject;

namespace Assets.GameLogic.Attitudes
{
    public interface IFoeActionManager
    {
        Task<(bool hasActed, bool canAct)> TryAct(Foe foe);
    }

    public class FoeActionManager : IFoeActionManager
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUpdateNotifier _updateNotifier;

        [Inject]
        private IUnitIncidents _unitIncidents;

        [Inject]
        private IActionTargetsFinder _actionTargetsFinder;

#pragma warning restore CS0649

        private Dictionary<string, double> _costWeights = new Dictionary<string, double>
        {
            {Constants.Statisitcs.Movement, 1 },
            {Constants.Statisitcs.Energy, 2 },
            {Constants.Statisitcs.Block, 3 },
            {Constants.Statisitcs.Life, 7 },
        };

        public async Task<(bool hasActed, bool canAct)> TryAct(Foe foe)
        {
            var items = foe.AllActions()
                .SelectMany(x => _actionTargetsFinder.GetTargets(foe, x).Select(z => (actionSettings: x, targets: z)))
                .Where(x => x.targets?.Any() == true);

            var set = new List<(ActionSettings, IEnumerable<IUnit>, (IEnumerable<EffectResult> effect, StatisticCost cost) result)>();
            foreach (var item in items)
            {
                var s = (item.actionSettings,
                        item.targets,
                        await item.actionSettings.CalculateEffectsAsync(foe, item.targets, staticAnalisys: true, canCancel: false, _gameStateProvider));

                set.Add(s);
            }

            var (actionSettings, targets, (_, cost)) = set
                .OrderByDescending(x => GetWeightedEffectValue(foe, x.result.effect))
                .FirstOrDefault();

            if (actionSettings != null && foe.HasStatisitcsToCoverCost(cost))
            {
                await _unitIncidents.ExecuteAction(foe, actionSettings, targets);
                return (true, targets.Any(x => x.Life.Current > 0));
            }

            return (false, targets?.Any() == true);
        }

        private double GetWeightedEffectValue(Foe foe, IEnumerable<EffectResult> effect)
        {
            var value = 0.0;

            foreach (var item in effect.Select(x => x.Target).OfType<IUnit>())
            {
                value += effect.GetCost(item)
                     .Sum(x => x.Value * (_costWeights.ContainsKey(x.Key) ? _costWeights[x.Key] : 1))
                     * (item is Foe ? -1 : 1);
            }

            return value;
        }
    }
}