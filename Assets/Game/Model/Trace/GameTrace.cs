﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.GameLogic.Model.Trace
{
    public class GameTrace
    {
        public List<ActionTrace> Actions { get; } = new List<ActionTrace>();

        public ActionTrace LastAction => Actions.Last();
    }
}