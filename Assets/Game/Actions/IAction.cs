﻿using System.Threading.Tasks;

namespace Assets.GameLogic.Activities
{
    public interface IAction
    {
        string DisplayName { get; }

        string Name { get; }

        bool CanExecute();

        Task Execute();
    }
}