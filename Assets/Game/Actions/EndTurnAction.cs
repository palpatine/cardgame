﻿using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Activities
{
    public class EndTurnAction : IAction, IEndTurnActivity
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private ICharacterIncidents _characterIncidents;

        [Inject]
        private IFoesIncidents _foeIncidents;

        [Inject]
        private IUnitIncidents _unitIncidents;

#pragma warning restore CS0649

        public bool CanExecute() => true;

        public string DisplayName => "End turn";

        public string Name => Constants.Actions.EndTurn;

        public async Task Execute()
        {
            var current = _gameStateProvider.GameState.CurrentUnit;
            var currentUnitIndex = _gameStateProvider.GameState.UnitsOrder.IndexOf(current);
            await Execute(currentUnitIndex, false);
        }

        public async Task Execute(int currentUnitIndex, bool shouldSuppressTurnRollover)
        {
            if (_gameStateProvider.GameState.CurrentUnit != null)
            {
                ProcessTemporaryAbiliites(_gameStateProvider.GameState.CurrentUnit);
            }

            currentUnitIndex = await ProcessFoesAsync(currentUnitIndex);

            var nextCharacterIndex = currentUnitIndex + 1 >= _gameStateProvider.GameState.UnitsOrder.Count
                ? 0
                : currentUnitIndex + 1;

            if (nextCharacterIndex == 0 && !shouldSuppressTurnRollover)
            {
                _gameStateProvider.GameState.CurrentUnit = null;
                await _unitIncidents.StartSelectUnitsOrderAsync();
            }

            var nextCharacter = (Character)_gameStateProvider.GameState.UnitsOrder[nextCharacterIndex];
            _gameStateProvider.GameState.CurrentUnit = nextCharacter;
            var (endTurn, noRestoration, hasDied) = await ProcessEffectsAsync(Constants.Triggers.BeforeStartTurn);

            if (hasDied)
            {
                return;
            }

            if (endTurn)
            {
                await Execute(nextCharacterIndex, false);
                return;
            }
            if (!noRestoration)
            {
                _characterIncidents.RecoverUnitStatisitcs(nextCharacter);
            }

            (endTurn, _, hasDied) = await ProcessEffectsAsync(Constants.Triggers.StartTurn);

            if (hasDied)
            {
                return;
            }

            if (endTurn)
            {
                await Execute(nextCharacterIndex, false);
                return;
            }
        }

        private void ProcessTemporaryAbiliites(IUnit currentUnit)
        {
            foreach (var item in currentUnit.Abilities.Where(x => x.RemainingDuration.HasValue).ToArray())
            {
                if (item.RemainingDuration == 1)
                {
                    currentUnit.Abilities.Remove(item);
                }
                else
                {
                    item.RemainingDuration--;
                }
            }
        }

        private async Task<(bool endTurn, bool noRestoration, bool hasDied)> ProcessEffectsAsync(
            string trigger)
        {
            var effects = _gameStateProvider.GameState.CurrentUnit.CalculateTriggeredEffects(
             trigger,
             null,
             new NamedTargetsSets(),
             _gameStateProvider);

            var currentUnit = _gameStateProvider.GameState.CurrentUnit;
            await _unitIncidents.Apply(effects, handleEndTurn: false);

            if (currentUnit != _gameStateProvider.GameState.CurrentUnit)
            {
                return (false, false, true);
            }

            var currentUnitEffect = effects.SingleOrDefault(
                x => x.Target == _gameStateProvider.GameState.CurrentUnit);
            return (currentUnitEffect?.EndTurn == true, currentUnitEffect?.NoRestoration == true, false);
        }

        private async Task<int> ProcessFoesAsync(int currentUnitIndex)
        {
            var loop = true;
            do
            {
                if (_gameStateProvider.GameState.UnitsOrder.Count > currentUnitIndex + 1
                    && _gameStateProvider.GameState.UnitsOrder[currentUnitIndex + 1] is Foe foe)
                {
                    _gameStateProvider.GameState.CurrentUnit = foe;
                    currentUnitIndex = _gameStateProvider.GameState.UnitsOrder.IndexOf(foe);
                    var (endTurn, noRestoration, _) = await ProcessEffectsAsync(Constants.Triggers.BeforeStartTurn);

                    if (endTurn)
                    {
                        ProcessTemporaryAbiliites(foe);
                        continue;
                    }

                    if (!noRestoration)
                    {
                        _foeIncidents.RecoverUnitStatisitcs(foe);
                    }

                    if ((await ProcessEffectsAsync(Constants.Triggers.StartTurn)).endTurn)
                    {
                        ProcessTemporaryAbiliites(foe);
                        continue;
                    }

                    if (foe.Life.Current > 0)
                    {
                        await _foeIncidents.ExecuteAction(foe);
                        ProcessTemporaryAbiliites(foe);
                        currentUnitIndex = _gameStateProvider.GameState.UnitsOrder.IndexOf(foe);
                    }
                }
                else
                {
                    loop = false;
                }
            } while (loop);

            return currentUnitIndex;
        }
    }
}