﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents;
using Zenject;

namespace Assets.GameLogic.Activities
{
    public interface IRevealRightActivity
    {
        string Name { get; }

        bool CanExecute(int level, out IEnumerable<EffectResult> effectResults);

        Task Execute(int level);

        bool IsEligible(int level, out IEnumerable<EffectResult> effectResults);
    }

    public class RevealRightAction : IRevealRightActivity
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IPlatformIncidents _platformIncidents;

        [Inject]
        private ICharacterIncidents _characterIndicents;

        [Inject]
        private IMoveActivity _moveAction;

#pragma warning restore CS0649

        public string Name => Constants.Actions.RevealLeftAction;

        public bool CanExecute(
            int level,
            out IEnumerable<EffectResult> effectResults)
        {
            var character = _gameStateProvider.GameState.CurrentUnit;
            return IsEligible(level, out effectResults)
                && character.HasStatisitcsToCoverCost(effectResults);
        }

        public bool IsEligible(int level, out IEnumerable<EffectResult> effectResults)
        {
            var character = _gameStateProvider.GameState.CurrentUnit;
            var cardsLevel = _gameStateProvider.GameState.Levels[character.Location.CurrentLevelId];
            var platformId = cardsLevel.Platforms.IndexOf(character.Location.PlatformCard);

            if (character.Location.PlatformCard.AllUnits.Last() != character)
            {
                var toTheLeftOfObstacle = character.Location.PlatformCard.Obstacle == null;
                var to = character.Location.PlatformCard.LocationAfterLastUnit(isFacingLeft: false);

                if (!_moveAction.IsEligible(
                    character,
                    to,
                    out effectResults,
                    withoutMinimal: true))
                {
                    return false;
                }

                var currentCharacterEffect = effectResults.Single(x => x.Target == character);
                currentCharacterEffect.StatisticsChange[Constants.Statisitcs.Movement] -= 1;
                currentCharacterEffect.StatisticsChange[Constants.Statisitcs.Energy] -= 1;
            }
            else
            {
                var result = new EffectResult(character);
                result.StatisticsChange.Add(Constants.Statisitcs.Energy, -1);
                result.StatisticsChange.Add(Constants.Statisitcs.Movement, -1);
                effectResults = new[] { result };
            }

            return level == character.Location.CurrentLevelId
                && !cardsLevel.IsLocked
                && platformId == cardsLevel.Platforms.Count - 1
                && (character.Location.PlatformCard.Obstacle == null || !character.Location.IsToTheLeftOfObstacle);
        }

        public async Task Execute(int level)
        {
            if (!CanExecute(level, out var _))
            {
                return;
            }

            var character = _gameStateProvider.GameState.CurrentUnit;
            var card = await _platformIncidents.RevealPlatformToRight(character.Location.CurrentLevelId);

            var to = card.LocationAsFirstUnit(isFacingLeft: false);

            await _moveAction.Execute(
                 _gameStateProvider.GameState.CurrentUnit,
                 to,
                 verifyCost: false);
        }
    }
}