﻿using System.Threading.Tasks;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Activities
{
    public interface IPickUpItemActivity
    {
        bool CanExecute(Item item);

        Task Execute(Item item);
    }

    public class PickUpItemAction : IPickUpItemActivity
    {
#pragma warning disable CS0649

        [Inject]
        private ICharacterIncidents _characterIndicents;

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IPlatformIncidents _platformIncidents;

#pragma warning restore CS0649

        public bool CanExecute(Item item)
        {
            return _gameStateProvider.GameState.CurrentUnit.Location.PlatformCard
                .Items.Contains(item);
        }

        public Task Execute(Item item)
        {
            if (CanExecute(item))
            {
                _platformIncidents.RemoveItem(
                    _gameStateProvider.GameState.CurrentUnit.Location.PlatformCard,
                    item);
                _characterIndicents.AddToInventory(
                    (Character)_gameStateProvider.GameState.CurrentUnit,
                    item);
            }

            return Task.CompletedTask;
        }
    }
}