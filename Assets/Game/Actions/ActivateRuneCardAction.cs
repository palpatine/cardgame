﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Messages;
using Assets.GameLogic.State;
using Zenject;

namespace Assets.GameLogic.Activities
{
    public interface IActivateRuneCardActivity
    {
        Task Execute(RuneCard runeCard);

        bool IsEligible(RuneCard runeCard);
    }

    public class ActivateRuneCardAction : IActivateRuneCardActivity
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private ICharacterIncidents _characterIncidents;

#pragma warning restore CS0649

        public bool IsEligible(RuneCard runeCard)
        {
            if (runeCard.Settings.Cost == 0)
            {
                return false;
            }

            var character = (Character)_gameStateProvider.GameState.CurrentUnit;
            var otherCards = character.Cards.OfType<RuneCard>().Except(new[] { runeCard }).ToArray();
            return otherCards.Any() && otherCards.Sum(z => z.Settings.Value) >= runeCard.Settings.Cost;
        }

        public async Task Execute(RuneCard runeCard)
        {
            if (!IsEligible(runeCard))
            {
                return;
            }

            var character = (Character)_gameStateProvider.GameState.CurrentUnit;
            var allCards = character.Cards.OfType<RuneCard>().Except(new[] { runeCard }).ToArray();
            var cards = allCards.OrderBy(x => x.Settings.Cost).ThenByDescending(x => x.Settings.Value);
            var value = 0;
            var set = new List<RuneCard>();
            foreach (var card in cards)
            {
                value += card.Settings.Value;
                set.Add(card);
                if (value >= runeCard.Settings.Cost)
                {
                    break;
                }
            }

            var data = new SelectRuneCards
            {
                CardToActivate = runeCard,
                ExpectedValue = runeCard.Settings.Cost,
                AllCards = allCards,
                Set = set,
            };

            await (this).Publish(data);

            if (data.IsCancelled)
            {
                return;
            }

            foreach (var item in data.Set)
            {
                _characterIncidents.DiscardCard(character, item);
            }

            _characterIncidents.DiscardCard(character, runeCard, permanently: true);

            var result = runeCard.Settings.Effects.DetermineResult(_gameStateProvider, new[] { character }, Enumerable.Empty<EffectResult>());

            await _characterIncidents.Apply(result);
        }
    }
}