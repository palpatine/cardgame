﻿namespace Assets.GameLogic.Activities
{
    public interface IScoutAction
    {
    }

    public class ScoutAction : IScoutAction
    {
        public string DisplayName => "Scout";

        public string Name => Constants.Actions.Scout;

        public bool CanExecute() => false;

        public void Execute() => throw new System.NotImplementedException();
    }
}