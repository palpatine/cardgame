﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Processes;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Zenject;

namespace Assets.GameLogic.Activities
{
    public interface IMoveActivity
    {
        string Name { get; }

        bool CanExecute(IUnit unit, ILocation to, out IEnumerable<EffectResult> statisticsCost);

        Task Execute(IUnit unit, ILocation to, bool verifyCost = true);

        bool IsEligible(IUnit unit, ILocation to, out IEnumerable<EffectResult> statisticsCost, bool withoutMinimal = false);
    }

    public class MoveAction : IMoveActivity
    {
        private const int EnergyActionCost = 1;
        private const int MovementActionCost = 1;
#pragma warning disable CS0649

        [Inject]
        private IMovementPathCalculator _movementPathCalculator;

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUnitIncidents _unitIndicents;

#pragma warning restore CS0649

        public string Name => Constants.Actions.RevealLeftAction;

        public bool IsEligible(
            IUnit unit,
            ILocation to,
            out IEnumerable<EffectResult> effectResult,
            bool withoutMinimal = false)
        {
            IEnumerable<JourneyLegDescriptor> path;

            if (unit is Character)
            {
                path = _movementPathCalculator.FindPathToTarget<Foe>(
                      unit,
                      unit.Location,
                      to,
                      withoutMinimal);
            }
            else
            {
                path = _movementPathCalculator.FindPathToTarget<Character>(
                    unit,
                    unit.Location,
                    to,
                    withoutMinimal);
            }

            if (path == null)
            {
                effectResult = null;
                return false;
            }

            effectResult = path.Select(x => x.EffectResults).MergeResult();

            return effectResult != null;
        }

        public bool CanExecute(
            IUnit unit,
            ILocation to,
            out IEnumerable<EffectResult> effectResult)
        {
            if (!IsEligible(unit, to, out effectResult))
            {
                return false;
            }

            return unit.HasStatisitcsToCoverCost(effectResult);
        }

        public async Task Execute(
            IUnit unit,
            ILocation to,
            bool verifyCost = true)
        {
            if (!CanExecute(unit, to, out var statisticsCost) && verifyCost)
            {
                return;
            }

            var character = _gameStateProvider.GameState.CurrentUnit;
            await _unitIndicents.Apply(statisticsCost);
            await _unitIndicents.ChangeLocation(unit, to);
        }
    }
}