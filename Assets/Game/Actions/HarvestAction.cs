﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;
using Assets.GameLogic.Tools.Notification;
using Zenject;
using Assets.GameLogic.Dtos;

namespace Assets.GameLogic.Activities
{
    public interface IHarvestActivity
    {
        ActionCardSettings Settings { get; }

        bool CanExecute(IHarvestable harvestable);

        Task Execute(IHarvestable harvestable);

        bool HasActionCard(Character character);

        bool HasEnergy(Character character);

        IEnumerable<EffectResult> GetEffectResult(IHarvestable harvestable);
    }

    public class HarvestAction : IHarvestActivity
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IPlatformIncidents _platformIncidents;

        [Inject]
        private ICharacterIncidents _characterIncidents;

        [Inject]
        private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

        private ActionCardSettings _settings;

        public ActionCardSettings Settings
        {
            get => _settings ?? (_settings =
                    _gameStateProvider.GameSettings.ActionCards
                    .Single(x => x.Name == Constants.Actions.Harvest));
        }

        public bool HasActionCard(Character character)
        {
            var card = character
                .Cards.OfType<ActionCard>()
                .FirstOrDefault(x => x.Settings.Name == Constants.Actions.Harvest);

            return card != null;
        }

        public bool HasEnergy(Character character) => character.Energy.Current >= Settings.EnergyCost;

        public bool CanExecute(IHarvestable harvestable)
        {
            var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
            return IsEligible(harvestable)
                && IsEligible(currentCharacter)
                && currentCharacter.Location.PlatformCard == harvestable.Location;
        }

        public bool IsEligible(IHarvestable harvestable)
        {
            return harvestable.CanHarvest;
        }

        public bool IsEligible(Character character)
        {
            return HasActionCard(character)
                && HasEnergy(character);
        }

        public IEnumerable<EffectResult> GetEffectResult(IHarvestable harvestable)
        {
            var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
            var result = new EffectResult(currentCharacter);
            result.StatisticsChange.Add(Constants.Statisitcs.Energy, -Settings.EnergyCost);
            if (harvestable is PlatformCard platformCard)
            {
                result.AddItems.AddRange(platformCard.Resources.Select(x => x.Settings));
                return new[] { result };
            }

            if (harvestable is PlatformFeature feature)
            {
                var featureSettings = _gameStateProvider.GameSettings.Features.Single(x => x.Name == feature.Kind);

                var itemSettings = _gameStateProvider.GameSettings.Items.Single(x => x.Name == featureSettings.Resource.Name);

                result.AddItems.AddRange(Enumerable.Repeat(itemSettings, feature.Count * featureSettings.Resource.Count));

                return new[] { result };
            }

            if (harvestable is Obstacle obstacle)
            {
                var obstacleRemedieEffect = new[] { obstacle.Settings.Remedies.Effect }
                          .DetermineResult(_gameStateProvider, new[] { currentCharacter });
                return (new[]
                    {
                        new[] { result },
                        obstacleRemedieEffect
                    })
                      .MergeResult();
            }

            return Enumerable.Empty<EffectResult>();
        }

        public async Task Execute(IHarvestable harvestable)
        {
            if (!CanExecute(harvestable))
            {
                return;
            }

            var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
            if (harvestable is PlatformCard platformCard)
            {
                foreach (var item in platformCard.Resources.ToArray())
                {
                    _characterIncidents.AddToInventory(
                        currentCharacter,
                        item);
                    _platformIncidents.RemoveResource(platformCard, item);
                }
            }

            var card = currentCharacter.Cards.OfType<ActionCard>()
                .First(x => x.Settings.Name == Constants.Actions.Harvest);
            _characterIncidents.DiscardCard(currentCharacter, card);

            var effects = GetEffectResult(harvestable);
            await _characterIncidents.Apply(effects);
            await _platformIncidents.RemoveHarvestable(harvestable);

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = currentCharacter,
                Action = Constants.TraceActions.HarvestPerformed,
                StatisticChange = effects.Single().StatisticsChange,
            });
        }
    }
}