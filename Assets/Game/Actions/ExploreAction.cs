﻿using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Model.Trace;
using Zenject;
using Assets.GameLogic.Dtos;

namespace Assets.GameLogic.Activities
{
    public interface IExploreActivity
    {
        ActionCardSettings Settings { get; }

        bool CanExecute();

        Task Execute();

        bool HasActionCard(Character character);

        bool HasEnergy(Character character);

        bool IsEligible(Character character);

        bool IsEligible(PlatformCard card);
    }

    public class ExploreAction : IExploreActivity
    {
#pragma warning disable CS0649

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IPlatformIncidents _platformIncidents;

        [Inject]
        private ICharacterIncidents _characterIncidents;

#pragma warning restore CS0649

        private ActionCardSettings _settings;

        public ActionCardSettings Settings
        {
            get => _settings ?? (_settings =
                    _gameStateProvider.GameSettings.ActionCards
                    .Single(x => x.Name == Constants.Actions.Explore));
        }

        public bool IsEligible(PlatformCard card)
        {
            return card.Settings.Exploration != null
                && !card.IsExplored;
        }

        public bool IsEligible(Character character)
        {
            return HasActionCard(character)
                && HasEnergy(character);
        }

        public bool HasActionCard(Character character)
        {
            var card = character
                .Cards.OfType<ActionCard>()
                .FirstOrDefault(x => x.Settings.Name == Constants.Actions.Explore);

            return card != null;
        }

        public bool HasEnergy(Character character) => character.Energy.Current >= Settings.EnergyCost;

        public bool CanExecute()
        {
            var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
            return IsEligible(currentCharacter.Location.PlatformCard)
                && IsEligible(currentCharacter);
        }

        public async Task Execute()
        {
            if (!CanExecute())
            {
                return;
            }

            var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
            _platformIncidents.Explore(currentCharacter.Location.PlatformCard);
            var card = currentCharacter.Cards.OfType<ActionCard>()
                .First(x => x.Settings.Name == Constants.Actions.Explore);
            _characterIncidents.DiscardCard(currentCharacter, card);
            var cost = new StatisticChange { { Constants.Statisitcs.Energy, -card.Settings.EnergyCost } };
            await _characterIncidents.ChangeStatisitcs(
                 currentCharacter,
                 cost);

            _gameStateProvider.GameState.GameTrace.Actions.Add(new ActionTrace
            {
                CurrentUnit = currentCharacter,
                Action = Constants.TraceActions.ExplorationPerformed,
                StatisticChange = cost
            });
        }
    }
}