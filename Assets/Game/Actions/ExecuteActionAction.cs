﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Processes;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Zenject;

namespace Assets.GameLogic.Activities
{
    public interface IExecuteActionActivity
    {
        Task Execute(ActivityOption action);

        Task<IEnumerable<ActivityOption>> GetActionOptionsAsync(IUnit target);
    }

    public class ExecuteActionAction : IExecuteActionActivity
    {
#pragma warning disable CS0649

        [Inject]
        private IMovementResultCalculator _movementCostCalculator;

        [Inject]
        private IGameStateProvider _gameStateProvider;

        [Inject]
        private IUnitIncidents _unitIndicents;

        [Inject]
        private IActionTargetsFinder _actionTargetsFinder;

#pragma warning restore CS0649

        public async Task<IEnumerable<ActivityOption>> GetActionOptionsAsync(IUnit target)
        {
            var actor = _gameStateProvider.GameState.CurrentUnit;
            var actions = new List<ActivityOption>();
            foreach (var action in actor.AllActions())
            {
                var targets = target is Foe
                    ? _actionTargetsFinder.GetTargets<Foe>(actor, action)
                    : _actionTargetsFinder.GetTargets<Character>(actor, action);

                if (targets == null)
                {
                    continue;
                }

                targets = targets
                            .Where(x => action.Condition == null ||
                                        action.Condition.IsMeet(_gameStateProvider.GameState, x))
                            .ToArray();

                if (!targets.Any())
                {
                    continue;
                }

                var validSetsOfTargets = targets.Where(x => x.Contains(target)).ToArray();

                foreach (var item in validSetsOfTargets)
                {
                    var result = await action.CalculateEffectsAsync(actor, item, staticAnalisys: true, canCancel: false, _gameStateProvider);
                    actions.Add(new ActivityOption
                    {
                        Action = action,
                        Result = result.effect,
                        Cost = result.cost,
                        Targets = item,
                    });
                }
            }

            return actions;
        }

        public async Task Execute(ActivityOption action)
        {
            var actor = _gameStateProvider.GameState.CurrentUnit;
            await _unitIndicents.ExecuteAction(actor, action.Action, action.Targets);
        }
    }

    public class ActivityOption
    {
        public ActionSettings Action { get; set; }

        public StatisticCost Cost { get; set; }

        public IEnumerable<IUnit> Targets { get; set; }

        public IEnumerable<EffectResult> Result { get; internal set; }
    }
}