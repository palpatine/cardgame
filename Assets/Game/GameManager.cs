﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Load.Settings;
using Assets.GameLogic.Settings;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using Assets.GameUI.State;
using UnityEngine;
using Zenject;

namespace Assets.GameLogic
{
    internal class GameManager : MonoBehaviour, IUIGameStateProvider
    {
#pragma warning disable CS0649

        [Inject]
        private IGameDataLoader _gameDataLoader;

        [Inject]
        private IGameStateInitializer _gameStateInitializer;

        [Inject]
        private readonly IUnitIncidents _unitsIncidents;

        [Inject]
        private UnitBehaviour.Pool _unitsPool;

        [Inject]
        private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

        private Dictionary<IUnit, UnitBehaviour> _units;

        public GameState GameState { get; private set; }

        public GameSettings GameSettings { get; private set; }

        public UIState UIState { get; private set; }

        private void Awake()
        {
            UIState = new UIState(_updateNotifier);
            GameSettings = _gameDataLoader.LoadSettings();
            GameState = _gameStateInitializer.CreateInitialGameState(GameSettings, out var start);
            _updateNotifier.Await(start());
        }

        private void Start()
        {
            _units = GameState.
                Units
                .ToDictionary(item => item, item => _unitsPool.Spawn(item));

            GameState.Units.CollectionChanged += UnitsChanged;
        }

        private void UnitsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    foreach (IUnit item in e.NewItems)
                    {
                        _units.Add(item, _unitsPool.Spawn(item));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (IUnit item in e.OldItems)
                    {
                        _unitsPool.Despawn(_units[item]);
                    }

                    if (!GameState.Units.Any(x => x is Character))
                    {
                        Awake();
                        Start();
                    }
                    break;
            }
        }
    }
}