﻿using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;
using Zenject;

internal class RevealButtonBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IPlatformIncidents _platformIncidents;

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

    public void RevealPlatformToLeft()
    {
        if (_gameStateProvider.GameState.CurrentUnit != null)
            _updateNotifier.Await(_platformIncidents.RevealPlatformToLeft(
     _gameStateProvider.GameState.CurrentUnit.Location.CurrentLevelId));
    }

    public void RevealPlatformToRight()
    {
        if (_gameStateProvider.GameState.CurrentUnit != null)
            _updateNotifier.Await(_platformIncidents.RevealPlatformToRight(
       _gameStateProvider.GameState.CurrentUnit.Location.CurrentLevelId));
    }
}