﻿using Assets.GameLogic;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;

public interface ITooltipManager
{
    bool Active { get; set; }

    float Height { get; }

    Vector3 Position { get; set; }

    string Text { get; set; }

    float Width { get; }
}

public class TooltipManager : MonoBehaviour, ITooltipManager
{
    private Transform _container;
    private RectTransform _popup;
    private Text _popupText;

    public bool Active
    {
        get => _container.gameObject.activeInHierarchy;
        set => _container.gameObject.SetActive(value);
    }

    public float Height => _popup.rect.height;

    public Vector3 Position
    {
        get => _popup.position;
        set => _popup.position = value;
    }

    public string Text
    {
        get => _popupText.text;
        set => _popupText.text = value;
    }

    public float Width => _popup.rect.width;

    public void Start()
    {
        _container = transform.Find(UIConstants.UI.TooltipDisplay.Container);
        _popup = (RectTransform)_container.Find(UIConstants.UI.TooltipDisplay.Tooltip);
        _popupText = _popup.GetComponentInChildren<Text>();
    }
}