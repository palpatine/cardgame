﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public interface IRayCastManger
{
    List<RaycastResult> CastRay();
}

public class RayCastManger : IRayCastManger
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    public List<RaycastResult> CastRay()
    {
        var pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };
        pointerData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        results = results.TakeWhile(x => x.gameObject.GetComponent<Canvas>() == null).ToList();
        _gameStateProvider.UIState.CurrentActonRaycast = results;
        return results;
    }
}