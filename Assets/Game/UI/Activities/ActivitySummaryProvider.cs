﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.State;
using Zenject;

public interface IActivitySummaryProvider
{
    string CreateCostSummary(StatisticCost cost, string displayName);

    string GetExploreActionSummary();

    string GetActivateRuneCardActivitySummary(RuneCard runeCard);

    string GetCharacterMustActInItsTurn();

    string GetHarvestActionSummary(IEnumerable<EffectResult> effectResult);
}

public class ActivitySummaryProvider : IActivitySummaryProvider
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private IExploreActivity _exploreAction;

    [Inject]
    private IHarvestActivity _harvestAction;

#pragma warning restore CS0649

    public string CreateCostSummary(StatisticCost statisticsCost, string displayName)
    {
        var currentCharacter = _gameStateProvider.GameState.CurrentUnit;
        var builder = new StringBuilder($@"{displayName} requirements:");
        builder.AppendLine();
        foreach (var item in statisticsCost.OrderBy(x => x.Key))
        {
            var statistic = _gameStateProvider.GameState.CurrentUnit.GetStatistic(item.Key);
            builder.AppendLine($"<color={(statistic.Current >= item.Value ? "green" : "red")}>{item.Value} {item.Key}</color>");
        }

        builder.Length -= Environment.NewLine.Length;

        return builder.ToString();
    }

    public string GetCharacterMustActInItsTurn()
    {
        return "This activity is possible ony on characters turn.";
    }

    public string GetActivateRuneCardActivitySummary(RuneCard runeCard)
    {
        if (runeCard.Settings.Cost == 0)
        {
            return "This card cannot be activated";
        }

        return $"To activate '{runeCard.Settings.Name}' discard other rune cards of value not less than {runeCard.Settings.Cost}.";
    }

    public string GetExploreActionSummary()
    {
        var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
        return $@"Explore requirements:
<color={(_exploreAction.HasActionCard(currentCharacter) ? "green" : "red")}>1 {_exploreAction.Settings.DisplayName} Action Card</color>
<color={(_exploreAction.HasEnergy(currentCharacter) ? "green" : "red")}>{_exploreAction.Settings.EnergyCost} Energy </color>";
    }

    public string GetHarvestActionSummary(IEnumerable<EffectResult> effectResult)
    {
        var itemsGain = effectResult.Single().AddItems.GroupBy(x => x.Name).ToDictionary(x => x.Key, x => x.Count());
        var currentCharacter = (Character)_gameStateProvider.GameState.CurrentUnit;
        return $@"Harvest requirements:
<color={(_harvestAction.HasActionCard(currentCharacter) ? "green" : "red")}>1 {_harvestAction.Settings.DisplayName} Action Card</color>
<color={(_harvestAction.HasEnergy(currentCharacter) ? "green" : "red")}>{_harvestAction.Settings.EnergyCost} Energy </color>
Harvesting will produce:
{string.Join(", ", itemsGain.Select(x => $"{x.Value} {x.Key}"))}";
    }
}