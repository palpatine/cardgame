﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Tools;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class HarvestActivityUIManger : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IHarvestActivity _harvestActivity;

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IActivitySummaryProvider _activitySummaryProvider;

#pragma warning restore CS0649

    public void DestroyUIEffect()
    {
    }

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var platform = results.Select(x => x.gameObject)
           .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.PlatformCardPrefab.PlatformResourcesTag));

        if (platform != null)
        {
            return HandlePlatformResourcesHarvest(results, platform);
        }

        var platformFeature = results.Select(x => x.gameObject)
           .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.PlatformCardPrefab.PlatformFeatureTag));

        if (platformFeature == null)
        {
            return null;
        }

        return HandlePlatformFeatureHarvest(results, platformFeature);
    }

    private ActivityUIProcessingResult HandlePlatformResourcesHarvest(List<RaycastResult> results, GameObject platform)
    {
        var platformBehaviour = platform.GetComponentInParent<PlatformCardBehaviour>();
        string summary = _activitySummaryProvider.GetHarvestActionSummary(_harvestActivity.GetEffectResult(platformBehaviour.Card));
        return new ActivityUIProcessingResult(async x =>
        {
            if (Input.GetMouseButtonDown(0))
            {
                x.HideSummary();
                await _harvestActivity.Execute(platformBehaviour.Card);
            }
        })
        {
            Description = summary,
            ZIndex = results.IndexOfFirst(x => x.gameObject == platform)
        };
    }

    private ActivityUIProcessingResult HandlePlatformFeatureHarvest(List<RaycastResult> results, GameObject platformFeature)
    {
        var harvestableProvider = platformFeature.GetComponent<IHarvestableProvider>();
        if (harvestableProvider.Harvestable == null)
            return null;

        string summary = _activitySummaryProvider.GetHarvestActionSummary(_harvestActivity.GetEffectResult(harvestableProvider.Harvestable));

        return new ActivityUIProcessingResult(async x =>
        {
            if (Input.GetMouseButtonDown(0))
            {
                x.HideSummary();
                await _harvestActivity.Execute(harvestableProvider.Harvestable);
            }
        })
        {
            Description = summary,
            ZIndex = results.IndexOfFirst(x => x.gameObject == platformFeature)
        };
    }
}