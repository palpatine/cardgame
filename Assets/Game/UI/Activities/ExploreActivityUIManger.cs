﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class ExploreActivityUIManger : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IExploreActivity _exploreActivity;

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IActivitySummaryProvider _activitySummaryProvider;

#pragma warning restore CS0649
    private string DisplayName => "Explore";

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var platform = results.Select(x => x.gameObject)
            .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.PlatformCardPrefab.PlatformTag));

        if (platform == null)
        {
            return null;
        }

        var platformBehaviour = platform.GetComponent<PlatformCardBehaviour>();
        var actionArea = results.FirstOrDefault(x => x.gameObject.CompareTag(UIConstants.Cards.PlatformCardPrefab.ExplorationActionAreaTag));

        if (_exploreActivity.IsEligible(platformBehaviour.Card)
                && platformBehaviour.Card.AllUnits.Contains(_gameStateProvider.GameState.CurrentUnit)
                && actionArea.gameObject != null)
        {
            string summary = _activitySummaryProvider.GetExploreActionSummary();
            return new ActivityUIProcessingResult(async x =>
            {
                if (Input.GetMouseButtonDown(0))
                {
                    x.HideSummary();
                    await _exploreActivity.Execute();
                }
            })
            {
                Description = summary,
                ZIndex = results.IndexOf(actionArea)
            };
        }

        return null;
    }

    public void DestroyUIEffect()
    {
    }
}