﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Tools;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class PickUpItemActivityUIManager : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IPickUpItemActivity _pickUpItemActivity;

    private InventoryItemBehaviour _inventoryItemBehaviour;
#pragma warning restore CS0649

    public void DestroyUIEffect()
    {
        if (_inventoryItemBehaviour != null)
            _inventoryItemBehaviour.Deemphasize();
    }

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var itemGameObject = results
            .Select(x => x.gameObject)
            .FirstOrDefault(x => x.CompareTag(UIConstants.UI.InventoryItemTag));

        if (itemGameObject == null)
        {
            return null;
        }

        _inventoryItemBehaviour = itemGameObject.GetComponent<InventoryItemBehaviour>();
        var item = _inventoryItemBehaviour.Item;

        if (!_pickUpItemActivity.CanExecute(item))
        {
            return null;
        }

        _inventoryItemBehaviour.Emphasize();

        return new ActivityUIProcessingResult(async (x) =>
        {
            if (Input.GetMouseButtonDown(0))
            {
                DestroyUIEffect();
                x.HideSummary();
                await _pickUpItemActivity.Execute(
                       item);
            }
        })
        {
            Description = "Pick up",
            ZIndex = results.IndexOfFirst(x => x.gameObject == itemGameObject)
        };
    }
}