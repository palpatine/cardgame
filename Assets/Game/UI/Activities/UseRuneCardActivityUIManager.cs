﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Tools;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class UseRuneCardActivityUIManager : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IActivateRuneCardActivity _activateRuneCardActivity;

    [Inject]
    private IActivitySummaryProvider _activitySummaryProvider;

    [Inject(Id = UIConstants.UI.CardsInventoryTag)]
    private Transform _cardsInventory;

    [Inject]
    private RuneCardBehaviour.Pool _runeCardPool;

#pragma warning restore CS0649

    private RuneCardBehaviour _highlightedRuneCard;

    private string DisplayName => "Use card";

    public void DestroyUIEffect()
    {
        if (_highlightedRuneCard != null)
        {
            _runeCardPool.Despawn(_highlightedRuneCard);
            _highlightedRuneCard = null;
        }
    }

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (!_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var runeCard = results
            .Select(x => x.gameObject)
            .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.RuneCardPrefab.RuneCardTag));

        if (runeCard == null)
        {
            return null;
        }

        HighlightRuneCard(runeCard);

        ActivityUIProcessingResult result = null;

        if (_gameStateProvider.GameState.CurrentUnit == _gameStateProvider.UIState.CharacterDetails.Character)
        {
            if (runeCard != null)
            {
                result = ProcessRuneCard(runeCard, results.IndexOfFirst(x => x.gameObject == runeCard));
            }
            else
            {
                return null;
            }
        }

        var actionUIProcessingResult = new ActivityUIProcessingResult(x =>
           {
               return Task.CompletedTask;
           })
        {
            ZIndex = results.IndexOfFirst(x => x.gameObject == runeCard),
            Description = _activitySummaryProvider.GetCharacterMustActInItsTurn()
        };
        return result ?? actionUIProcessingResult;
    }

    private void HighlightRuneCard(GameObject runeCard)
    {
        var behaviour = runeCard.GetComponent<RuneCardBehaviour>();
        if ((_highlightedRuneCard == null || _highlightedRuneCard.RuneCard != behaviour.RuneCard))
        {
            DestroyUIEffect();

            _highlightedRuneCard = _runeCardPool.Spawn(behaviour.RuneCard);
            _highlightedRuneCard.transform.position = runeCard.transform.position;
            _highlightedRuneCard.transform.SetParent(_cardsInventory, true);
            _highlightedRuneCard.transform.localScale = new Vector3(2f, 2f, 2f);
        }
    }

    private ActivityUIProcessingResult ProcessRuneCard(GameObject runeCard, int zIndex)
    {
        var behaviour = runeCard.GetComponent<RuneCardBehaviour>();

        return new ActivityUIProcessingResult(async x =>
        {
            if (_activateRuneCardActivity.IsEligible(behaviour.RuneCard) && Input.GetMouseButtonDown(0))
            {
                await _activateRuneCardActivity.Execute(behaviour.RuneCard);
            }
        })
        {
            ZIndex = zIndex,
            Description = _activitySummaryProvider.GetActivateRuneCardActivitySummary(behaviour.RuneCard)
        };
    }
}