﻿using System;
using System.Threading.Tasks;

public class ActivityUIProcessingResult
{
    public readonly static ActivityUIProcessingResult Reserved = new ActivityUIProcessingResult(null)
    {
        ZIndex = int.MinValue
    };

    private readonly Func<IActivitiesUIManager, Task> _handleInput;

    public ActivityUIProcessingResult(Func<IActivitiesUIManager, Task> handleInput)
    {
        _handleInput = handleInput;
    }

    public string Description { get; set; }

    public int ZIndex { get; set; }

    public Task HandleInput(IActivitiesUIManager manager) => _handleInput?.Invoke(manager);
}