﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Tools;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class RevealPlatformActivityUIManager : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IRevealLeftActivity _revealLeftActivity;

    [Inject]
    private IRevealRightActivity _revealRightActivity;

    [Inject]
    private IActivitySummaryProvider _activitySummaryProvider;

#pragma warning restore CS0649

    private string DisplayName => "Reveal";

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var levelEdge = results.FirstOrDefault(x => x.gameObject.CompareTag(UIConstants.UI.LevelEdgePrefab.LevelEdgeTag));

        if (levelEdge.gameObject != null)
        {
            var component = levelEdge.gameObject.GetComponent<LevelEdgeBehaviour>();

            IEnumerable<EffectResult> effectResult;
            if (component.IsLeft && _revealLeftActivity.IsEligible(component.Level, out effectResult)
                || _revealRightActivity.IsEligible(component.Level, out effectResult))
            {
                string summary = _activitySummaryProvider.CreateCostSummary(effectResult.GetCost(_gameStateProvider.GameState.CurrentUnit), DisplayName);
                return new ActivityUIProcessingResult(async x =>
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (component.IsLeft)
                        {
                            await _revealLeftActivity.Execute(component.Level);
                        }
                        else
                        {
                            await _revealRightActivity.Execute(component.Level);
                        }
                    }
                })
                {
                    Description = summary,
                    ZIndex = results.IndexOfFirst(x => x.gameObject == levelEdge.gameObject)
                };
            }
        }

        return null;
    }

    public void DestroyUIEffect()
    {
    }
}