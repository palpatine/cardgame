﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.State;
using UnityEngine;
using Zenject;

public interface IActivitiesUIManager
{
    void HideSummary();

    void ShowSummary();
}

public class ActivitiesUIManager : MonoBehaviour, IActivitiesUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private readonly IEnumerable<IActivityUIManager> _managers;

    [Inject]
    private IRayCastManger _rayCastManger;

    [Inject]
    private ITooltipManager _tooltipManager;

#pragma warning restore CS0649

    private Task _processing;

    public void Update()
    {
        if (_processing != null && _processing.IsFaulted)
        {
            throw new AggregateException(_processing.Exception);
        }

        if (_processing != null && (_processing.IsCompleted || _processing.IsCanceled))
        {
            _processing = null;
        }

        if (_processing != null
            || !(_gameStateProvider.GameState.CurrentUnit is Character))
        {
            HideSummary();
            foreach (var manager in _managers)
            {
                manager.DestroyUIEffect();
            }
            return;
        }

        _processing = ProcessInput();
    }

    private async Task ProcessInput()
    {
        var result = await GetActionFromMousePositionAsync();
        await Process(result);
    }

    private async Task<ActivityUIProcessingResult> GetActionFromMousePositionAsync()
    {
        var results = _rayCastManger.CastRay();
        (IActivityUIManager Manager, ActivityUIProcessingResult Result) processingResult = (null, null);

        foreach (var item in _managers)
        {
            var result = await item.ProcessAsync(results);
            if (result != null && (processingResult.Result == null || processingResult.Result.ZIndex > result.ZIndex))
            {
                processingResult = (item, result);
            }
        }

        foreach (var manager in _managers)
        {
            if (manager != processingResult.Manager)
            {
                manager.DestroyUIEffect();
            }
        }

        if (ReferenceEquals(processingResult.Result, ActivityUIProcessingResult.Reserved))
        {
            return null;
        }

        return processingResult.Result;
    }

    private async Task Process(ActivityUIProcessingResult processingResult)
    {
        if (processingResult != null)
        {
            _tooltipManager.Active = processingResult.Description != null;
            await processingResult.HandleInput(this);

            if (_tooltipManager.Active)
            {
                var description = processingResult.Description;
                ConfigureTooltip(description);
            }
        }
        else
        {
            HideSummary();
        }
    }

    private void ConfigureTooltip(string description)
    {
        _tooltipManager.Text = description;
        var halfPopupWidth = _tooltipManager.Width / 2f;
        var halfPopupHeight = _tooltipManager.Height / 2f;
        _tooltipManager.Position = new Vector3(
           Math.Min(Screen.width - halfPopupWidth,
                    Math.Max(halfPopupWidth, Input.mousePosition.x)),
           Math.Min(Screen.height - halfPopupHeight,
                    Math.Max(halfPopupHeight, Input.mousePosition.y - 2f / 3f * _tooltipManager.Height)));
    }

    public void HideSummary() => _tooltipManager.Active = false;

    public void ShowSummary() => _tooltipManager.Active = true;
}