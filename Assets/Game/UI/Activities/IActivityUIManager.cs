﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.EventSystems;

public interface IActivityUIManager
{
    Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results);

    void DestroyUIEffect();
}
