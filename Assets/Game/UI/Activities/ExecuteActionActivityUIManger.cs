﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class ExecuteActionActivityUIManger : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IExecuteActionActivity _executeActionActifity;

    [Inject]
    private readonly IActivitySummaryProvider _actionSummaryProvider;

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private readonly IUpdateNotifier _updateNotifier;

    [Inject(Id = UIConstants.UI.AnimationAreaTag)]
    private readonly Transform _animationArea;

    [Inject]
    private ActivityOptionsSetBehaviour.Pool _actionOptionsSetBehaviourPool;

    private ActivityOptionsSetBehaviour _optionUi;
    private IUnit _target;
#pragma warning restore CS0649

    private string DisplayName => "Act";

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return null;
        }

        var unit = results
            .Select(x => x.gameObject)
            .FirstOrDefault(x => x.CompareTag(UIConstants.UnitPrefab.UnitTag));

        if (unit != null)
        {
            var newTarget = unit.transform.GetComponent<UnitBehaviour>().Unit;
            if (_optionUi != null && newTarget == _target)
            {
                return ActivityUIProcessingResult.Reserved;
            }

            _target = newTarget;
            var options = await _executeActionActifity.GetActionOptionsAsync(_target);

            if (options.Any())
            {
                return new ActivityUIProcessingResult((x) =>
                {
                    DestroyUIEffect();

                    _optionUi = _actionOptionsSetBehaviourPool.Spawn(_actionOptionsSetBehaviourPool, options, _animationArea);

                    return Task.CompletedTask;
                })
                {
                    Description = null,
                    ZIndex = results.IndexOfFirst(x => x.gameObject == unit)
                };
            }
        }
        else if (_optionUi != null && _optionUi.isActiveAndEnabled)
        {
            return ActivityUIProcessingResult.Reserved;
        }

        return null;
    }

    public void DestroyUIEffect()
    {
        _optionUi?.Dispose();
        _optionUi = null;
    }
}