﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Tools;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class UseActionCardActivityUIManager : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IExploreActivity _exploreActivity;

    [Inject]
    private IActivitySummaryProvider _activitySummaryProvider;

    [Inject(Id = UIConstants.UI.CardsInventoryTag)]
    private Transform _cardsInventory;

    [Inject]
    private ActionCardBehaviour.Pool _actionCardPool;

#pragma warning restore CS0649

    private ActionCardBehaviour _highlightedActionCard;

    private string DisplayName => "Use card";

    public void DestroyUIEffect()
    {
        if (_highlightedActionCard != null)
        {
            _actionCardPool.Despawn(_highlightedActionCard);
            _highlightedActionCard = null;
        }
    }

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (!_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var actionCard = results
            .Select(x => x.gameObject)
            .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.ActionCardPrefab.ActionCardTag));

        if (actionCard == null)
        {
            return null;
        }

        HighlightActionCard(actionCard);

        ActivityUIProcessingResult result = null;

        if (_gameStateProvider.GameState.CurrentUnit == _gameStateProvider.UIState.CharacterDetails.Character)
        {
            if (actionCard != null)
            {
                result = ProcessActionCard(actionCard, results.IndexOfFirst(x => x.gameObject == actionCard));
            }
            else
            {
                return null;
            }
        }

        return result ?? new ActivityUIProcessingResult(x =>
        {
            x.HideSummary();
            return Task.CompletedTask;
        })
        {
            ZIndex = results.IndexOfFirst(x => x.gameObject == actionCard),
            Description = _activitySummaryProvider.GetCharacterMustActInItsTurn()
        };
    }

    private void HighlightActionCard(GameObject actionCard)
    {
        var behaviour = actionCard.GetComponent<ActionCardBehaviour>();
        if (actionCard != null && (_highlightedActionCard == null || _highlightedActionCard.ActionCard != behaviour.ActionCard))
        {
            DestroyUIEffect();

            _highlightedActionCard = _actionCardPool.Spawn(behaviour.ActionCard);
            _highlightedActionCard.transform.position = actionCard.transform.position;
            _highlightedActionCard.transform.SetParent(_cardsInventory, true);
            _highlightedActionCard.transform.localScale = new Vector3(2f, 2f, 2f);
        }
    }

    private ActivityUIProcessingResult ProcessActionCard(GameObject actionCard, int zIndex)
    {
        var behaviour = actionCard.GetComponent<ActionCardBehaviour>();
        var currentPlatform = _gameStateProvider.GameState.CurrentUnit.Location.PlatformCard;
        if (behaviour.ActionCard.Settings.Name == Constants.Actions.Explore && _exploreActivity.IsEligible(currentPlatform))
        {
            return new ActivityUIProcessingResult(async x =>
            {
                if (Input.GetMouseButtonDown(0))
                {
                    await _exploreActivity.Execute();
                }
            })
            {
                ZIndex = zIndex,
                Description = _activitySummaryProvider.GetExploreActionSummary()
            };
        }

        // todo: other cards

        return null;
    }
}