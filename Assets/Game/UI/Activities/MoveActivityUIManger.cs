﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Dtos;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using Assets.GameUI.Tools;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class MoveActivityUIManger : IActivityUIManager
{
#pragma warning disable CS0649

    [Inject]
    private IMoveActivity _moveActivity;

    [Inject]
    private IActivitySummaryProvider _activitySummaryProvider;

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private UnitAvatarBehaviour.Pool _avatarPool;

    [Inject]
    private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649

    private UnitAvatarBehaviour _avatar;

    private string DisplayName => "Move";

    public async Task<ActivityUIProcessingResult> ProcessAsync(List<RaycastResult> results)
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen)
        {
            return await Task.FromResult<ActivityUIProcessingResult>(null);
        }

        var platform = results.Select(x => x.gameObject)
    .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.PlatformCardPrefab.PlatformTag));

        if (platform == null)
        {
            return null;
        }
        var unit = results.Select(x => x.gameObject)
            .FirstOrDefault(x => x.CompareTag(UIConstants.UnitPrefab.UnitTag));
        if (unit != null)
        {
            var rectTransform = (RectTransform)unit.transform;
            var unitXStart = rectTransform.position.x - rectTransform.rect.width / 2;
            var start = unitXStart + rectTransform.rect.width / 3;
            var end = unitXStart + 2 * rectTransform.rect.width / 3;
            var current = _gameStateProvider.GameState.CurrentUnit;
            if (current.Location.IsFacingLeft &&
                Input.mousePosition.x >= start && Input.mousePosition.x <= unitXStart + rectTransform.rect.width
                || !current.Location.IsFacingLeft &&
                Input.mousePosition.x >= unitXStart && Input.mousePosition.x <= end)
            {
                return null;
            }
        }

        var platformBehaviour = platform.GetComponent<PlatformCardBehaviour>();
        var actionAreaLeft = results.FirstOrDefault(x => x.gameObject.CompareTag(UIConstants.Cards.PlatformCardPrefab.UnitsAreaLeftTag));
        var actionAreaRight = results.FirstOrDefault(x => x.gameObject.CompareTag(UIConstants.Cards.PlatformCardPrefab.UnitsAreaRightTag));

        var actionArea = actionAreaLeft.gameObject?.transform ?? actionAreaRight.gameObject?.transform;
        if (actionArea != null)
        {
            var units = actionArea.FindAllByTag(UIConstants.UnitPrefab.UnitTag);
            var unitsToLeft = units.Where(x => x.transform.position.x < Input.mousePosition.x).ToArray();
            var isFacingLeft = IsFacingLeft(platform, units, unitsToLeft);
            var unitId = unitsToLeft.Count();

            var toTheLeftOfObstacle = actionAreaLeft.gameObject != null;
            var to = new LocationDescriptor
            {
                PlatformCard = platformBehaviour.Card,
                CurrentLevelId = platformBehaviour.Card.Level,
                IsFacingLeft = isFacingLeft,
                UnitId = unitId,
                IsToTheLeftOfObstacle = toTheLeftOfObstacle
            };

            if (to.IsInFrontOf(_gameStateProvider.GameState.CurrentUnit))
            {
                return null;
            }

            if (_moveActivity.IsEligible(
                    _gameStateProvider.GameState.CurrentUnit,
                    to,
                    out var statisticsCost))
            {
                if (_avatar == null)
                {
                    _avatar = _avatarPool.Spawn(_gameStateProvider.GameState.CurrentUnit);
                    _avatar.MakeEphemeral();
                }

                string summary = _activitySummaryProvider.CreateCostSummary(statisticsCost.GetCost(_gameStateProvider.GameState.CurrentUnit), DisplayName);
                return new ActivityUIProcessingResult(async (x) =>
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        DestroyUIEffect();
                        x.HideSummary();
                        await _updateNotifier.NextLateUpdate;
                        await _moveActivity.Execute(
                               _gameStateProvider.GameState.CurrentUnit,
                               to);
                    }
                    else
                    {
                        _avatar.transform.SetParent(actionArea.transform, false);
                        _avatar.transform.SetSiblingIndex(unitId);
                        _avatar.SetIsFacingLeft(isFacingLeft);
                    }
                })
                {
                    Description = summary,
                    ZIndex = results.IndexOfFirst(x => x.gameObject == actionArea)
                };
            }
        }

        DestroyUIEffect();

        return null;
    }

    public void DestroyUIEffect()
    {
        if (_avatar != null)
        {
            _avatarPool.Despawn(_avatar);
            _avatar = null;
        }
    }

    private bool IsFacingLeft(GameObject platform, IEnumerable<Transform> units, Transform[] unitsToLeft)
    {
        var isFacingLeft = false;

        if (_avatar != null && _avatar.transform.parent != null)
        {
            isFacingLeft = _avatar.transform.position.x > Input.mousePosition.x;
        }
        else
        {
            var lastUnit = unitsToLeft.OrderBy(x => x.transform.position.x).LastOrDefault();
            if (lastUnit != null && unitsToLeft.Length == units.Count())
            {
                isFacingLeft = lastUnit.transform.position.x + ((RectTransform)lastUnit.transform).rect.width > Input.mousePosition.x;
            }
            else if (lastUnit != null)
            {
                isFacingLeft = lastUnit.transform.position.x + ((RectTransform)lastUnit.transform).rect.width / 2 > Input.mousePosition.x;
            }
            else
            {
                var firstUnitToRight = units.OrderBy(x => x.transform.position.x).FirstOrDefault();
                if (firstUnitToRight != null)
                {
                    isFacingLeft = firstUnitToRight.transform.position.x - ((RectTransform)firstUnitToRight.transform).rect.width * 2 > Input.mousePosition.x;
                }
                else
                {
                    isFacingLeft = platform.transform.position.x > Input.mousePosition.x;
                }
            }
        }

        return isFacingLeft;
    }
}