﻿using UnityEngine;

public class AutoSizeToTextBehaviour : MonoBehaviour
{
    private RectTransform _rectTransform;
    private RectTransform _text;

    private void Start()
    {
        _rectTransform = (RectTransform)transform;
        _text = (RectTransform)transform.Find("ActionDescription");
    }

    private void Update()
    {
        var height = _text.rect.height * _text.localScale.y + 10;
        _rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        _text.localPosition = new Vector3(_text.localPosition.x, 5, _text.localPosition.z);
    }
}