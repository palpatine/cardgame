﻿using Assets.GameLogic.Activities;
using UnityEngine;
using Zenject;

public class ActionsContainerBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private EndTurnAction _action;

    [Inject]
    private ActionBehaviour.Pool _actionBehaviourPool;

#pragma warning restore CS0649

    private void Start()
    {
        var actionPrefab = _actionBehaviourPool.Spawn(_action);
        actionPrefab.transform.SetParent(transform, false);
    }
}