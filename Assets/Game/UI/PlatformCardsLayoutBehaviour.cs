﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class PlatformCardsLayoutBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    private Dictionary<int, List<PlatformCardBehaviour>> _currnetDisplay = new Dictionary<int, List<PlatformCardBehaviour>>();

    [Inject]
    private PlatformCardBehaviour.Pool _platformBehaviourPool;

    [Inject]
    private LevelContainerBehaviour.Factory _levelContainerFactory;

    [Inject]
    private LevelEdgeBehaviour.Pool _levelEdgeFactory;

#pragma warning restore CS0649

    private Dictionary<int, RectTransform> _levels = new Dictionary<int, RectTransform>();
    private int _isDirty;

    public RectTransform GetLevelContainer(int level)
    {
        return _levels[level];
    }

    private void Start()
    {
        var id = 0;
        foreach (var level in _gameStateProvider.GameState.Levels)
        {
            CreateNewLevel(id);
            id++;
        }

        _gameStateProvider.GameState.Levels.CollectionChanged += (x, data) =>
        {
            if (data.Action == NotifyCollectionChangedAction.Add)
            {
                var index = data.NewStartingIndex;
                foreach (var item in data.NewItems)
                {
                    CreateNewLevel(index);
                    index++;
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        };
    }

    private void CreateNewLevel(int index)
    {
        _gameStateProvider.GameState.Levels[index].Platforms.CollectionChanged += OnLevelUpdate;
        var container = (RectTransform)_levelContainerFactory.Create().transform;
        container.SetParent(transform, false);
        _levels.Add(index, container);
        var leftEdge = _levelEdgeFactory.Spawn(true, index);
        leftEdge.transform.SetParent(container, false);
        _currnetDisplay.Add(index, new List<PlatformCardBehaviour>());
        var rightEdge = _levelEdgeFactory.Spawn(false, index);
        rightEdge.transform.SetParent(container, false);
        DrawLevel(index);
    }

    private void DrawLevel(int level)
    {
        foreach (var item in _currnetDisplay[level])
        {
            _platformBehaviourPool.Despawn(item);
        }

        _currnetDisplay[level].Clear();

        InsertPlatforms(_gameStateProvider.GameState.Levels[level].Platforms, 0, level);
    }

    private void OnLevelUpdate(object sender, NotifyCollectionChangedEventArgs e)
    {
        var collection = (ObservableCollection<PlatformCard>)sender;
        var level = _gameStateProvider.GameState.Levels.Single(x => ReferenceEquals(x.Platforms, collection));
        var levelId = _gameStateProvider.GameState.Levels.IndexOf(level);

        switch (e.Action)
        {
            case NotifyCollectionChangedAction.Add:
                InsertPlatforms(e, levelId);
                break;

            case NotifyCollectionChangedAction.Move:
                break;

            case NotifyCollectionChangedAction.Remove:
                RemovePlatforms(e, levelId);
                break;

            case NotifyCollectionChangedAction.Replace:
                RemovePlatforms(e, levelId);
                InsertPlatforms(e, levelId);
                break;

            case NotifyCollectionChangedAction.Reset:
                DrawLevel(levelId);
                break;

            default:
                break;
        }
    }

    private void RemovePlatforms(NotifyCollectionChangedEventArgs e, int level)
    {
        foreach (PlatformCard platform in e.OldItems)
        {
            var item = _currnetDisplay[level].Single(x => x.Card == platform);
            _platformBehaviourPool.Despawn(item);
            _currnetDisplay[level].Remove(item);
        }
        _isDirty = 2;
    }

    private void InsertPlatforms(NotifyCollectionChangedEventArgs e, int level)
    {
        InsertPlatforms(e.NewItems.Cast<PlatformCard>(), e.NewStartingIndex, level);
    }

    private void InsertPlatforms(IEnumerable<PlatformCard> cards, int startingIndex, int level)
    {
        var i = 1;
        foreach (var platform in cards)
        {
            var platformBehaviour = _platformBehaviourPool.Spawn(platform);
            _currnetDisplay[level].Add(platformBehaviour);
            platformBehaviour.transform.SetParent(_levels[level], false);
            platformBehaviour.transform.SetSiblingIndex(startingIndex + i++);
        }
        _isDirty = 2;
    }

    private void Update()
    {
        // hack: (2018.3.6f1) I beleve there is a bug in content size fitter that cause error in size calculation in this setup
        // modifying child size or layout group child alignment fixes the issue
        // yet this change is not wanted so I revert it but change and revert cannot occure on the same frame
        if (_isDirty == 2)
        {
            _isDirty = 1;
            GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.LowerLeft;
        }
        else if (_isDirty == 1)
        {
            _isDirty = 0;
            GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.MiddleCenter;
            var rect = (RectTransform)transform;
            var mapAreaLayoutElement = transform.parent.GetComponent<LayoutElement>();
            mapAreaLayoutElement.minWidth = rect.rect.width;
            mapAreaLayoutElement.minHeight = rect.rect.height;

            var animationAreaLayoutElement = transform.parent.parent.GetComponent<LayoutElement>();
            animationAreaLayoutElement.minWidth = rect.rect.width;
            animationAreaLayoutElement.minHeight = rect.rect.height;
        }
    }
}