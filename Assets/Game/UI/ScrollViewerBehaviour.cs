﻿using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI.Tools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

internal class ScrollViewerBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private IUpdateNotifier _updateNotifier;

    [Inject]
    private PlatformCardsLayoutBehaviour _mapContainer;

#pragma warning restore CS0649
    private RectTransform _animationArea;
    private int _isDirty;

    private void Start()
    {
        _animationArea = (RectTransform)_mapContainer.transform.parent.parent;
        var previousCharacter = _gameStateProvider.GameState.CurrentUnit;

        _gameStateProvider.GameState
            .TrackValue(x => x.CurrentUnit, async (x) =>
            {
                if (x.NewValue != null)
                    await BringCurrentPlayersPlatformToView();
            })
            .TrackValue(x => x.Location)
            .OnChange(x => x.PlatformCard)
                .React(() => { _isDirty = 4; });
    }

    public void Update()
    {
        if (_isDirty > 1)
        {
            _isDirty--;
        }
        else if (_isDirty == 1)
        {
            _updateNotifier.Await(BringCurrentPlayersPlatformToView());
            _isDirty--;
        }
    }

    private async Task BringCurrentPlayersPlatformToView()
    {
        if (_gameStateProvider.GameState.CurrentUnit == null)
        {
            return;
        }

        var currentLevelId = _gameStateProvider.GameState.CurrentUnit.Location.CurrentLevelId;
        var platforms = _gameStateProvider.GameState.Levels[currentLevelId].Platforms;
        var platformId =
            platforms.Count / 2f -
            platforms.IndexOf(
            _gameStateProvider.GameState.CurrentUnit.Location.PlatformCard);

        var _currentLevelContainer = _mapContainer.GetLevelContainer(currentLevelId);
        var edgeSample = _currentLevelContainer.Cast<RectTransform>().First();
        var platformSample = _currentLevelContainer.Cast<RectTransform>().Skip(1).First();
        var platformWidth = platformSample.rect.width
            + _currentLevelContainer.GetComponent<HorizontalLayoutGroup>().spacing;

        var left = (platformId - 0.5f) * platformWidth + edgeSample.rect.width;
        var expectedMapContainerPositon = new Vector2(left, _animationArea.anchoredPosition.y);
        var currentPosition = _animationArea.anchoredPosition;

        do
        {
            var speed = Vector2.zero;
            currentPosition = _animationArea.anchoredPosition;
            _animationArea.anchoredPosition = Vector2.SmoothDamp(
                _animationArea.anchoredPosition,
                expectedMapContainerPositon,
                ref speed,
                .1f);
            await _updateNotifier.NextUpdate;
        }
        while (!currentPosition.NearlyEqual(_animationArea.anchoredPosition));
    }
}