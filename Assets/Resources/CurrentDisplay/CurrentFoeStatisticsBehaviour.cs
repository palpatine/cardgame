﻿using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine.UI;

public class CurrentFoeStatisticsBehaviour : UnitStatisticsBehaviour
{
    public void Start()
    {
        var title = transform.parent.Find("Title").GetComponent<Text>();
        var baseText = title.text;
        _gameStateProvider.UIState
            .OnChange(x => x.CurrentActonRaycast)
            .React(() =>
            {
                var unit = _gameStateProvider.UIState.CurrentActonRaycast
                  .Select(x => x.gameObject)
                  .FirstOrDefault(x => x.CompareTag(UIConstants.UnitPrefab.UnitTag))
                  ?.GetComponent<UnitBehaviour>()
                  ?.Unit;

                UpdateDisplay(unit);

                if (unit is Foe foe)
                    title.text = $"{baseText} {foe.Settings.Name}";
                else
                    title.text = baseText;
            });
    }
}