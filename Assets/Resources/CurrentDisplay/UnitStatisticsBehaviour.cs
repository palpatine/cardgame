﻿using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public abstract class UnitStatisticsBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    protected IUIGameStateProvider _gameStateProvider;

    [Inject]
    private RegenerableStatisticBehaviour.Pool _regenerableStatisticPool;

    [Inject]
    private ReplenishableStatisticBehaviour.Pool _replenishableStatisticPool;

    [Inject]
    private ValueStatisticBehaviour.Pool _valueStatisticPool;

#pragma warning restore CS0649

    private RegenerableStatisticBehaviour _life;
    private RegenerableStatisticBehaviour _energy;
    private RegenerableStatisticBehaviour _mana;
    private ReplenishableStatisticBehaviour _movement;
    private ReplenishableStatisticBehaviour _attack;
    private ReplenishableStatisticBehaviour _block;
    private ReplenishableStatisticBehaviour _useItem;
    private ReplenishableStatisticBehaviour _interact;
    private IUnit _unit;

    public void UpdateDisplay(IUnit unit)
    {
        if (_unit == unit) return;
        Desroy();
        Create(unit);
        _unit = unit;
    }

    protected void Desroy()
    {
        _unit = null;
        if (_life != null)
        {
            _regenerableStatisticPool.Despawn(_life);
            _regenerableStatisticPool.Despawn(_mana);
            _regenerableStatisticPool.Despawn(_energy);

            _replenishableStatisticPool.Despawn(_movement);
            _replenishableStatisticPool.Despawn(_attack);
            _replenishableStatisticPool.Despawn(_block);
            if (_interact != null)
            {
                _replenishableStatisticPool.Despawn(_interact);
                _interact = null;
            }
            if (_useItem != null)
            {
                _replenishableStatisticPool.Despawn(_useItem);
                _useItem = null;
            }

            _life = null;
        }
    }

    protected void Create(IUnit unit)
    {
        if (unit == null)
        {
            return;
        }

        _life = _regenerableStatisticPool.Spawn(
            "Life",
            unit.Life);
        _life.transform.SetParent(transform, false);
        _energy = _regenerableStatisticPool.Spawn(
            "Energy",
           unit.Energy);
        _energy.transform.SetParent(transform, false);
        _mana = _regenerableStatisticPool.Spawn(
            "Mana",
            unit.Mana);
        _mana.transform.SetParent(transform, false);

        _attack = _replenishableStatisticPool.Spawn(
            "Attack",
            unit.Attack);
        _attack.transform.SetParent(transform, false);

        _block = _replenishableStatisticPool.Spawn(
            "Block",
            unit.Block);
        _block.transform.SetParent(transform, false);
        _movement = _replenishableStatisticPool.Spawn(
            "Movement",
            unit.Movement);
        _movement.transform.SetParent(transform, false);

        if (unit is Character character)
        {
            _useItem = _replenishableStatisticPool.Spawn(
                "Use item",
                character.UseItem);
            _useItem.transform.SetParent(transform, false);
            _interact = _replenishableStatisticPool.Spawn(
                "Interact",
                character.Interact);
            _interact.transform.SetParent(transform, false);
        }
    }
}