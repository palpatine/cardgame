﻿using Assets.GameLogic.Tools.Notification;

public class CurrentCharacterStatisticsBehaviour : UnitStatisticsBehaviour
{
    public void Start()
    {
        _gameStateProvider.GameState
            .OnChange(x => x.CurrentUnit)
            .React(() =>
            {
                UpdateDisplay(_gameStateProvider.GameState.CurrentUnit);
            });
    }
}