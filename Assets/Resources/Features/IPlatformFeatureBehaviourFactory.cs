﻿using System;
using Assets.GameLogic;
using Assets.GameLogic.State;
using UnityEngine;
using Zenject;

public interface IPlatformFeatureBehaviourPool
{
    Transform Spawn(PlatformCard platform, string featureKind);

    void Despawn(Transform feature, string featureKind);
}

public class PlatformFeatureBehaviourPool : IPlatformFeatureBehaviourPool
{
#pragma warning disable CS0649

    [Inject]
    private OreNuggetBehaviour.Pool _oreNuggetPool;

    [Inject]
    private TreeBehaviour.Pool _treesPool;

#pragma warning restore CS0649

    public Transform Spawn(PlatformCard platform, string featureKind)
    {
        switch (featureKind)
        {
            case Constants.Features.Tree:
                return _treesPool.Spawn(platform).transform;

            case Constants.Features.Cave:
            case Constants.Features.OreNugget:
                return _oreNuggetPool.Spawn(platform).transform;

            default:
                throw new NotSupportedException(featureKind);
        }
    }

    public void Despawn(Transform feature, string featureKind)
    {
        switch (featureKind)
        {
            case Constants.Features.Tree:
                _treesPool.Despawn(feature.GetComponent<TreeBehaviour>());
                break;

            case Constants.Features.OreNugget:
                _oreNuggetPool.Despawn(feature.GetComponent<OreNuggetBehaviour>());
                break;

            default:
                throw new NotSupportedException(featureKind);
        }
    }
}