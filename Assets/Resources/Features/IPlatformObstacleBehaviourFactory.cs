﻿using System;
using Assets.GameLogic;
using Assets.GameLogic.State;
using UnityEngine;
using Zenject;

public interface IPlatformObstacleBehaviourPool
{
    RectTransform Spawn(PlatformCard platform, string featureKind);

    void Despawn(RectTransform feature, string featureKind);
}

public class PlatformObstacleBehaviourPool : IPlatformObstacleBehaviourPool
{
#pragma warning disable CS0649

    [Inject]
    private BoulderBehaviour.Pool _bouldersPool;

#pragma warning restore CS0649

    public RectTransform Spawn(PlatformCard platform, string featureKind)
    {
        switch (featureKind)
        {
            case Constants.Obstacles.Boulder:
                return (RectTransform)_bouldersPool.Spawn(platform).transform;

            default:
                throw new NotSupportedException(featureKind);
        }
    }

    public void Despawn(RectTransform feature, string featureKind)
    {
        switch (featureKind)
        {
            case Constants.Obstacles.Boulder:
                _bouldersPool.Despawn(feature.GetComponent<BoulderBehaviour>());
                break;

            default:
                throw new NotSupportedException(featureKind);
        }
    }
}