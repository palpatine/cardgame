﻿using Assets.GameLogic.State;
using UnityEngine;
using Zenject;

public interface IHarvestableProvider
{
    IHarvestable Harvestable { get; }
}

public class OreNuggetBehaviour : MonoBehaviour, IHarvestableProvider, IPoolable<PlatformCard>
{
    public IHarvestable Harvestable => PlatformCard.Features;

    public PlatformCard PlatformCard { get; private set; }

    public void OnDespawned()
    {
        PlatformCard = null;
    }

    public void OnSpawned(PlatformCard platformCard)
    {
        PlatformCard = platformCard;
    }

    public class Pool : MonoPoolableMemoryPool<PlatformCard, OreNuggetBehaviour>
    {
    }
}