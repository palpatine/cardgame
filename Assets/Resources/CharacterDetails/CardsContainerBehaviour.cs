﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class CardsContainerBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private ActionCardBehaviour.Pool _actionCardPool;

    [Inject]
    private RuneCardBehaviour.Pool _runeCardPool;

#pragma warning restore CS0649

    private List<MonoBehaviour> _cards = new List<MonoBehaviour>();

    private void Start()
    {
        _gameStateProvider.UIState.CharacterDetails
            .OnChange(x => x.Character)
            .React(x =>
       {
           var previous = (Character)x.OldValue;
           if (previous != null)
               previous.Cards.CollectionChanged -= OnChange;
           if (_gameStateProvider.UIState.CharacterDetails.Character != null)
           {
               _gameStateProvider.UIState.CharacterDetails.Character.Cards.CollectionChanged += OnChange;
               Recreate(_gameStateProvider.UIState.CharacterDetails.Character);
           }
       });
    }

    private void Recreate(Character character)
    {
        Remove(_cards);

        _cards.Clear();

        AddCards(character.Cards, 0);
    }

    private void Remove(IEnumerable<MonoBehaviour> cards)
    {
        foreach (var item in cards.ToArray())
        {
            if (item is RuneCardBehaviour rune)
                _runeCardPool.Despawn(rune);
            else if (item is ActionCardBehaviour action)
                _actionCardPool.Despawn(action);

            _cards.Remove(item);
        }
    }

    private void AddCards(IEnumerable<ICharacterCard> cards, int index)
    {
        foreach (var card in cards)
        {
            Transform cardTransform = null;
            if (card is ActionCard actionCard)
            {
                var actionCardPrefab = _actionCardPool.Spawn(actionCard);
                _cards.Add(actionCardPrefab);
                cardTransform = actionCardPrefab.transform;
            }
            else if (card is RuneCard runeCard)
            {
                var runeCardPrefab = _runeCardPool.Spawn(runeCard);
                _cards.Add(runeCardPrefab);
                cardTransform = runeCardPrefab.transform;
            }

            cardTransform.SetParent(transform, false);
            cardTransform.SetSiblingIndex(index++);
        }
    }

    private void OnChange(object sender, NotifyCollectionChangedEventArgs e)
    {
        switch (e.Action)
        {
            case NotifyCollectionChangedAction.Add:
                AddCards(e.NewItems.Cast<ICharacterCard>(), e.NewStartingIndex);
                break;

            case NotifyCollectionChangedAction.Remove:
                var index = e.OldStartingIndex;
                var count = e.OldItems.Count;
                Remove(_cards.Skip(index).Take(count));
                break;

            case NotifyCollectionChangedAction.Reset:
                Recreate(_gameStateProvider.UIState.CharacterDetails.Character);
                break;

            case NotifyCollectionChangedAction.Move:
            case NotifyCollectionChangedAction.Replace:
            default:
                throw new NotImplementedException();
        }
    }
}