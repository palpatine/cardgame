﻿using System;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ReplenishableStatisticBehaviour : MonoBehaviour, IPoolable<string, ReplenishableStatistic>
{
    private Text _current;
    private Text _max;
    private Text _name;
    private IDisposable _reactionsDisposer;
    private ReplenishableStatistic _replenishableStatistic;

    public void Awake()
    {
        _name = transform.Find(UIConstants.UI.ReplenishableStatisticPrefab.Name).GetComponent<Text>();
        _current = transform.Find(UIConstants.UI.ReplenishableStatisticPrefab.Current).GetComponent<Text>();
        _max = transform.Find(UIConstants.UI.ReplenishableStatisticPrefab.Max).GetComponent<Text>();
    }

    public void OnDespawned() => _reactionsDisposer.Dispose();

    public void OnSpawned(string name, ReplenishableStatistic replenishableStatistic)
    {
        _replenishableStatistic = replenishableStatistic;
        _name.text = name + ":";
        _current.text = replenishableStatistic.Current.ToString();
        _max.text = replenishableStatistic.Maximum.ToString();
        _reactionsDisposer = new MergingDisposer(
     _replenishableStatistic.OnChange(x => x.Current).React(() =>
       _current.text = _replenishableStatistic.Current.ToString()),
     _replenishableStatistic.OnChange(x => x.Maximum).React(() =>
       _max.text = _replenishableStatistic.Maximum.ToString()));
    }

    public class Pool : MonoPoolableMemoryPool<string, ReplenishableStatistic, ReplenishableStatisticBehaviour>
    {
    }
}