﻿using Assets.GameLogic;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class CloseCharacterDetailsBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    public void OnClick()
    {
        _gameStateProvider.UIState.CharacterDetails.IsOpen = false;
    }
}