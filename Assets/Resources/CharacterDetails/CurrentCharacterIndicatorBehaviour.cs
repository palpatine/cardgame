﻿using Assets.GameLogic;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class CurrentCharacterIndicatorBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    private void Start()
    {
        transform.gameObject.SetActive(_gameStateProvider.UIState.CharacterDetails.IsOpen);

        _gameStateProvider.UIState.CharacterDetails
            .OnChange(x => x.IsOpen)
            .React(() =>
                transform.gameObject.SetActive(_gameStateProvider.UIState.CharacterDetails.IsOpen));
        _gameStateProvider.UIState.CharacterDetails
            .OnChange(x => x.Character)
            .React(() =>
            transform.GetComponent<Image>().color = _gameStateProvider.UIState.CharacterDetails.Character.Color);
    }
}