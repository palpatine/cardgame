﻿using System;
using System.ComponentModel;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ValueStatisticBehaviour : MonoBehaviour, IPoolable<string, ValueStatistic>
{
    private Text _current;
    private Text _name;
    private IDisposable _reactionsDisposer;
    private ValueStatistic _valueStatisitc;

    public void Awake()
    {
        _name = transform.Find(UIConstants.UI.ValueStatisticPrefab.Name).GetComponent<Text>();
        _current = transform.Find(UIConstants.UI.ValueStatisticPrefab.Current).GetComponent<Text>();
    }

    public void OnDespawned() => _reactionsDisposer.Dispose();

    public void OnSpawned(string name, ValueStatistic valueStatistic)
    {
        _valueStatisitc = valueStatistic;
        _name.text = name + ":";
        _current.text = valueStatistic.Current.ToString();
        _reactionsDisposer = valueStatistic.OnChange(x => x.Current).React(
            () => _current.text = _valueStatisitc.Current.ToString());
    }

    private void OnChange(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(ValueStatistic.Current))
        {
        }
    }

    public class Pool : MonoPoolableMemoryPool<string, ValueStatistic, ValueStatisticBehaviour>
    {
    }
}