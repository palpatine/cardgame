﻿using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class InventoryItemBehaviour : MonoBehaviour, IPoolable<Item>, IInventoryItemProvider
{
    private Text _itemName;

    public Item Item { get; private set; }

    public void Awake()
    {
        _itemName = transform.Find(UIConstants.UI.InventoryItemPrefab.ItemName).GetComponent<Text>();
    }

    public void Deemphasize()
    {
        transform.localScale = Vector3.one;
    }

    public void Emphasize()
    {
        transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
    }

    public void OnDespawned()
    {
    }

    public void OnSpawned(Item item)
    {
        Deemphasize();
        transform.GetComponent<Image>().raycastTarget = true;
        Item = item;
        _itemName.text = item.Settings.Name;
    }

    public void SetInnert()
    {
        transform.GetComponent<Image>().raycastTarget = false;
    }

    public class Pool : MonoPoolableMemoryPool<Item, InventoryItemBehaviour>
    {
    }
}