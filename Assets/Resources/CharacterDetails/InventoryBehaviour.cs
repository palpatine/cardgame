﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class InventoryBehaviour : MonoBehaviour, IInventoryItemTarget
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private InventoryItemBehaviour.Pool _inventoryItemBehaviourPool;

#pragma warning restore CS0649

    public string ItemTargetName => "Inventory";

    private Dictionary<Item, InventoryItemBehaviour> _items = new Dictionary<Item, InventoryItemBehaviour>();

    private void Start()
    {
        _gameStateProvider.UIState
          .CharacterDetails
          .TrackValue(x => x.Character)
          .TrackValue(x => x.Inventory, OnChange);
    }

    private void OnChange(NotifyCollectionChangedEventArgs e)
    {
        if (e.Action == NotifyCollectionChangedAction.Reset)
        {
            Remove(_items.Keys.Cast<Item>().ToArray());
            if (_gameStateProvider.UIState.CharacterDetails.Character.Inventory != null)
                Add(_gameStateProvider.UIState.CharacterDetails.Character.Inventory);
        }
        else if (e.Action == NotifyCollectionChangedAction.Add)
        {
            Add(e.NewItems.Cast<Item>());
        }
        else if (e.Action == NotifyCollectionChangedAction.Remove)
        {
            Remove(e.OldItems.Cast<Item>());
        }
        else if (e.Action == NotifyCollectionChangedAction.Replace)
        {
            Add(e.NewItems.Cast<Item>());
            Remove(e.OldItems.Cast<Item>());
        }
    }

    private void Add(IEnumerable<Item> items)
    {
        foreach (var item in items)
        {
            var behaviour = _inventoryItemBehaviourPool.Spawn(item);
            _items.Add(item, behaviour);
            behaviour.transform.SetParent(transform, false);
        }
    }

    private void Remove(IEnumerable<Item> values)
    {
        foreach (var item in values)
        {
            _inventoryItemBehaviourPool.Despawn(_items[item]);
            _items.Remove(item);
        }
    }
}