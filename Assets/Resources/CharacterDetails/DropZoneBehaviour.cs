﻿using UnityEngine;

public class DropZoneBehaviour : MonoBehaviour, IInventoryItemTarget
{
    public string ItemTargetName => "Drop";
}