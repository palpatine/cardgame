﻿using Assets.GameLogic.State;

public interface IInventoryItemProvider
{
    Item Item { get; }
}

public interface IInventoryItemTarget
{
    string ItemTargetName { get; }
}