﻿using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class EquipmentManagementBehaviour : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject(Id = UIConstants.UI.CharacterDetailsContainerTag)]
    private Transform _characterDetailsContainer;

    [Inject]
    private ICharacterIncidents _characterIncidents;

    [Inject]
    private IUpdateNotifier _updateNotifier;

    [Inject]
    private InventoryItemBehaviour.Pool _inventoryItemBehaviourPool;

#pragma warning restore CS0649

    private Item _item;
    private InventoryItemBehaviour _itemIndicator;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (_gameStateProvider.UIState.CharacterDetails.Character
            != _gameStateProvider.GameState.CurrentUnit)
        {
            return;
        }

        var hoveredItem = eventData.hovered.SingleOrDefault(x => x.CompareTag(UIConstants.UI.InventoryItemTag));

        if (hoveredItem != null)
        {
            var provider = hoveredItem.GetComponent<IInventoryItemProvider>()
               ?? hoveredItem.GetComponentInParent<IInventoryItemProvider>();
            _item = provider.Item;
            _itemIndicator = _inventoryItemBehaviourPool.Spawn(_item);
            _itemIndicator.SetInnert();
            _itemIndicator.transform.SetParent(_characterDetailsContainer);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_item == null) return;

        _itemIndicator.transform.position = new Vector2(Input.mousePosition.x + 10,
            Input.mousePosition.y);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_item == null) return;

        var hoveredItem = eventData.hovered.SingleOrDefault(x => x.CompareTag(UIConstants.UI.InventoryItemTargetTag));

        if (hoveredItem != null)
        {
            var target = hoveredItem.GetComponent<IInventoryItemTarget>();

            if (target.ItemTargetName == "Inventory")
            {
                _updateNotifier.Await(_characterIncidents.UnequipItemAsync(
                    _gameStateProvider.UIState.CharacterDetails.Character,
                    _item));
            }
            else if (target.ItemTargetName == "Drop")
            {
                _updateNotifier.Await(_characterIncidents.DropItemAsync(
                    _gameStateProvider.UIState.CharacterDetails.Character,
                    _item));
            }
            else
            {
                _updateNotifier.Await(_characterIncidents.TryEquipItemAsync(
                    _gameStateProvider.UIState.CharacterDetails.Character,
                    _item,
                    target.ItemTargetName));
            }
        }

        _inventoryItemBehaviourPool.Despawn(_itemIndicator);
        _itemIndicator = null;
        _item = null;
    }
}