﻿using System;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class EquipmentSlotBehaviour : MonoBehaviour, IPoolable<string>, IInventoryItemProvider,
    IInventoryItemTarget
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    private IDisposable _reactionsDisposer;
    private Text _slotName;
    private Text _itemName;

    public Item Item { get; private set; }

    public string SlotName { get; private set; }

    public string ItemTargetName => SlotName;

    public class Pool : MonoPoolableMemoryPool<string, EquipmentSlotBehaviour>
    {
    }

    public void OnDespawned()
    {
        _reactionsDisposer.Dispose();
    }

    public void Awake()
    {
        _slotName = transform.Find(UIConstants.UI.EquipmentSlotPrefab.SlotName).GetComponent<Text>();
        _itemName = transform.Find(UIConstants.UI.EquipmentSlotPrefab.ItemName).GetComponent<Text>();
    }

    public void OnSpawned(string slotName)
    {
        SlotName = slotName;
        _slotName.text = slotName;

        _reactionsDisposer = _gameStateProvider.UIState
            .CharacterDetails
            .TrackValue(x => x.Character)
            .TrackValue(x => x.Equipment)
            .OnChange(slotName)
            .React(x =>
                SetItemName(x));
    }

    private void SetItemName(ChangeNotification change)
    {
        Item = (Item)change.NewValue;
        _itemName.text = Item?.Settings.Name;
    }
}