﻿using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class CharacterToggleBehaviour : MonoBehaviour
{
    public class Factory : PlaceholderFactory<Character, CharacterToggleBehaviour>
    {
    }

#pragma warning disable CS0649

    [Inject]
    private Character _character;

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    public void OnClick()
    {
        if (_gameStateProvider.UIState.CharacterDetails.IsOpen
            && _gameStateProvider.UIState.CharacterDetails.Character == _character)
        {
            _gameStateProvider.UIState.CharacterDetails.IsOpen = false;
        }
        else
        {
            _gameStateProvider.UIState.CharacterDetails.Character = _character;
            _gameStateProvider.UIState.CharacterDetails.IsOpen = true;
        }
    }
}