﻿using System.Linq;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Messages;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class RuneCardSelectorBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private IUpdateNotifier _updateNotifier;

    [Inject(Id = UIConstants.UI.CardsInventoryTag)]
    private Transform _cardsInventory;

    [Inject]
    private IRayCastManger _rayCastManger;

    [Inject]
    private RuneCardBehaviour.Pool _runeCardPool;

#pragma warning restore CS0649

    private bool _run;
    private Transform _accept;
    private Transform _cancel;
    private RuneCardBehaviour _highlightedRuneCard;
    private ActionCardBehaviour[] _actionCardBehaviours;
    private RuneCardBehaviour[] _runeCardBehaviours;

    internal SelectRuneCards Data { get; private set; }

    private void Start()
    {
        this.Subscribe<SelectRuneCards>(SelectCardsAsync);
        _accept = _cardsInventory.Find(UIConstants.UI.RuneCardSelector.Accept);
        _cancel = _cardsInventory.Find(UIConstants.UI.RuneCardSelector.Cancel);
    }

    private async Task SelectCardsAsync(SelectRuneCards arg)
    {
        Data = arg;
        _accept.gameObject.SetActive(true);
        _cancel.gameObject.SetActive(true);
        HideActionCards();
        await _updateNotifier.NextUpdate;

        _runeCardBehaviours = transform.GetComponentsInChildren<RuneCardBehaviour>();
        PlaceCardToActivate(arg);

        var set = arg.Set.ToArray();
        arg.Set = Enumerable.Empty<RuneCard>();
        foreach (var card in set)
        {
            var behaviour = _runeCardBehaviours.Single(x => x.RuneCard == card);
            AddCardToSet(behaviour);
        }

        _run = true;
        while (_run)
        {
            await ProcessMousePosition();
        }

        _accept.gameObject.SetActive(false);
        _cancel.gameObject.SetActive(false);

        ShowActionCards();
        foreach (var item in _runeCardBehaviours)
        {
            item.transform.SetParent(transform, false);
            item.transform.localScale = Vector3.one;
        }
    }

    private async Task ProcessMousePosition()
    {
        var results = _rayCastManger.CastRay();
        var runeCard = results
        .Select(x => x.gameObject)
        .FirstOrDefault(x => x.CompareTag(UIConstants.Cards.RuneCardPrefab.RuneCardTag));

        if (runeCard != null)
        {
            var runeCardBehaviour = runeCard.GetComponent<RuneCardBehaviour>();
            var card = runeCardBehaviour.RuneCard;
            runeCardBehaviour = _runeCardBehaviours.Single(x => x.RuneCard == card);
            if (card != Data.CardToActivate)
            {
                HighlightRuneCard(runeCard);

                if (Input.GetMouseButtonDown(0))
                {
                    DestroyUIEffect();
                    if (Data.Set.Contains(card))
                    {
                        RemoveCardFromSet(runeCardBehaviour);
                    }
                    else
                    {
                        AddCardToSet(runeCardBehaviour);
                    }
                }
            }
            else
            {
                DestroyUIEffect();
            }
        }
        else
        {
            DestroyUIEffect();
        }

        await _updateNotifier.NextUpdate;
    }

    private void ShowActionCards()
    {
        foreach (var actionCard in _actionCardBehaviours)
        {
            actionCard.gameObject.SetActive(true);
        }
    }

    private void HideActionCards()
    {
        _actionCardBehaviours = transform.GetComponentsInChildren<ActionCardBehaviour>();
        foreach (var actionCard in _actionCardBehaviours)
        {
            actionCard.gameObject.SetActive(false);
        }
    }

    private void PlaceCardToActivate(SelectRuneCards arg)
    {
        var cardToActivate = _runeCardBehaviours.Single(x => x.RuneCard == arg.CardToActivate);
        cardToActivate.transform.localScale = 2 * Vector3.one;
        cardToActivate.transform.SetParent(_cardsInventory, false);
        var cardToActivateRectTransform = (RectTransform)cardToActivate.transform;
        cardToActivate.transform.position = _cardsInventory.position
            + Vector3.up * cardToActivateRectTransform.rect.height;
    }

    private void AddCardToSet(RuneCardBehaviour runeCardBehaviour)
    {
        runeCardBehaviour.transform.SetParent(_cardsInventory, true);
        var rectTransform = (RectTransform)runeCardBehaviour.transform;
        var x = 100 + Data.Set.Count() * rectTransform.rect.width;
        runeCardBehaviour.transform.position = new Vector3(x, _accept.position.y, 0);
        Data.Set = Data.Set.Concat(new[] { runeCardBehaviour.RuneCard }).ToArray();
    }

    private void RemoveCardFromSet(RuneCardBehaviour runeCardBehaviour)
    {
        runeCardBehaviour.transform.SetParent(transform, false);
        Data.Set = Data.Set.Where(x => x != runeCardBehaviour.RuneCard).ToArray();

        var behavioursInSet = _runeCardBehaviours
            .Where(x => x.transform.position.y == _accept.position.y)
            .OrderBy(x => x.transform.position.x);

        var rectTransform = (RectTransform)runeCardBehaviour.transform;
        var id = 0;
        foreach (var behaviour in behavioursInSet)
        {
            var x = 100 + id * rectTransform.rect.width;
            behaviour.transform.position = new Vector3(x, _accept.position.y, 0);
        }
    }

    private void HighlightRuneCard(GameObject runeCard)
    {
        var behaviour = runeCard.GetComponent<RuneCardBehaviour>();
        if ((_highlightedRuneCard == null || _highlightedRuneCard.RuneCard != behaviour.RuneCard))
        {
            DestroyUIEffect();

            _highlightedRuneCard = _runeCardPool.Spawn(behaviour.RuneCard);
            _highlightedRuneCard.transform.position = runeCard.transform.position;
            _highlightedRuneCard.transform.SetParent(_cardsInventory, true);
            _highlightedRuneCard.transform.localScale = new Vector3(2f, 2f, 2f);
        }
    }

    private void DestroyUIEffect()
    {
        if (_highlightedRuneCard != null)
        {
            _runeCardPool.Despawn(_highlightedRuneCard);
            _highlightedRuneCard = null;
        }
    }

    public void Cancel()
    {
        Data.IsCancelled = true;
        _run = false;
    }

    public void Accept()
    {
        if (Data.Set.Any() && Data.Set.Sum(x => x.Settings.Value) >= Data.ExpectedValue)
        {
            _run = false;
        }
    }
}