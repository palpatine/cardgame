﻿using Assets.GameLogic;
using UnityEngine;
using Zenject;

public class EquipmentBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private EquipmentSlotBehaviour.Pool _equipmentSlotBehaviourPool;

#pragma warning restore CS0649

    private void Start()
    {
        foreach (var item in _gameStateProvider.GameSettings.EquipableItemKinds)
        {
            _equipmentSlotBehaviourPool.Spawn(item).transform.SetParent(transform, false);
        }
    }
}