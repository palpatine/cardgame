﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class CharacterToggleControllerBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private CharacterToggleBehaviour.Factory _toggleFactory;

#pragma warning restore CS0649

    private List<CharacterToggleBehaviour> _toggles = new List<CharacterToggleBehaviour>();

    private void Start()
    {
        foreach (var character in _gameStateProvider.GameState.Units.OfType<Character>())
        {
            var toogle = _toggleFactory.Create(character);
            _toggles.Add(toogle);
            toogle.transform.SetParent(transform, false);
            var image = toogle.GetComponent<Image>();
            image.color = character.Color;
        }

        _gameStateProvider.GameState.Units.CollectionChanged +=
            (_, e) =>
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Remove:
                        var id = e.OldStartingIndex;
                        foreach (var item in e.OldItems)
                        {
                            if (item is Character)
                            {
                                var behaviour = _toggles[id];
                                _toggles.Remove(behaviour);
                                Destroy(behaviour);
                            }

                            id++;
                        }

                        break;

                    case NotifyCollectionChangedAction.Add:
                    case NotifyCollectionChangedAction.Move:
                    case NotifyCollectionChangedAction.Replace:
                    case NotifyCollectionChangedAction.Reset:
                    default:
                        break;
                }
            };
    }
}