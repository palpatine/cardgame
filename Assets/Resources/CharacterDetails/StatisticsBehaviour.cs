﻿using Assets.GameLogic.Tools.Notification;

public class StatisticsBehaviour : UnitStatisticsBehaviour
{
    private void Start()
    {
        _gameStateProvider.UIState.CharacterDetails
            .OnChange(x => x.Character)
            .Or(x => x.IsOpen)
            .React(() =>
            {
                UpdateDisplay(_gameStateProvider.UIState.CharacterDetails.Character);
            });
    }
}