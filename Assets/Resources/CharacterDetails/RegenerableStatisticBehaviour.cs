﻿using System;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class RegenerableStatisticBehaviour : MonoBehaviour, IPoolable<string, RegenerableStatistic>
{
    private Text _current;
    private Text _max;
    private Text _name;
    private IDisposable _reactionsDisposer;
    private RegenerableStatistic _regenerableStatistic;
    private Text _regeneration;

    public void Awake()
    {
        _name = transform.Find(UIConstants.UI.RegenerableStatisticPrefab.Name).GetComponent<Text>();
        _current = transform.Find(UIConstants.UI.RegenerableStatisticPrefab.Current).GetComponent<Text>();
        _max = transform.Find(UIConstants.UI.RegenerableStatisticPrefab.Max).GetComponent<Text>();
        _regeneration = transform.Find(UIConstants.UI.RegenerableStatisticPrefab.Regeneration).GetComponent<Text>();
    }

    public void OnDespawned() => _reactionsDisposer.Dispose();

    public void OnSpawned(string name, RegenerableStatistic regenerableStatistic)
    {
        _regenerableStatistic = regenerableStatistic;
        _name.text = name + ":";
        _current.text = regenerableStatistic.Current.ToString();
        _max.text = regenerableStatistic.Maximum.ToString();
        _regeneration.text = regenerableStatistic.RegenerationRate.ToString();
        _reactionsDisposer = new MergingDisposer(
            regenerableStatistic.OnChange(x => x.Current).React(() =>
              _current.text = _regenerableStatistic.Current.ToString()),
            regenerableStatistic.OnChange(x => x.Maximum).React(() =>
              _max.text = _regenerableStatistic.Maximum.ToString()),
            regenerableStatistic.OnChange(x => x.RegenerationRate).React(() =>
              _regeneration.text = _regenerableStatistic.RegenerationRate.ToString()));
    }

    public class Pool : MonoPoolableMemoryPool<string, RegenerableStatistic, RegenerableStatisticBehaviour>
    {
    }
}