﻿using Assets.GameLogic.State;
using Assets.GameUI;
using Assets.GameUI.ResourcePools;
using Assets.ResourcePools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DangerCardBehaviour : DisposableMonoBehaviour, IPoolable<DangerCard, DangerCardBehaviour.Pool>
{
    private Pool _pool;
    private Text _title;
    private Text _description;

    public DangerCard DangerCard { get; private set; }

    public override void Dispose() => _pool.Despawn(this);

    public void OnDespawned() => transform.localScale = Vector3.one;

    public void OnSpawned(DangerCard dangerCard, Pool pool)
    {
        _pool = pool;
        DangerCard = dangerCard;

        if (_title == null)
        {
            _title = transform.Find(UIConstants.Cards.DangerCardPrefab.TitleComponent).GetComponent<Text>();
            _description = transform.Find(UIConstants.Cards.ActionCardPrefab.DescriptionComponent).GetComponent<Text>();
        }

        _title.text = dangerCard.Settings.DisplayName;
        _description.text = dangerCard.Settings.Description;
    }

    public class Pool : MonoPoolableMemoryPool<DangerCard, Pool, DangerCardBehaviour>, IDangerCardBehaviourPool
    {
        DisposableMonoBehaviour IDangerCardBehaviourPool.Spawn(DangerCard card) => Spawn(card, this);
    }
}