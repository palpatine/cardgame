﻿using UnityEngine;
using Zenject;

public class LevelEdgeBehaviour : MonoBehaviour, IPoolable<bool, int>
{
    public bool IsLeft { get; private set; }

    public int Level { get; private set; }

    public void OnDespawned()
    {
    }

    public void OnSpawned(bool isLeft, int level)
    {
        IsLeft = isLeft;
        Level = level;
    }

    public class Pool : MonoPoolableMemoryPool<bool, int, LevelEdgeBehaviour>
    {
    }
}