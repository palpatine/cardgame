﻿using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class RuneCardBehaviour : MonoBehaviour, IPoolable<RuneCard>
{
    public RuneCard RuneCard { get; private set; }

    public void OnDespawned()
    {
        transform.localScale = Vector3.one;
    }

    public void OnSpawned(RuneCard runeCard)
    {
        RuneCard = runeCard;
        var title = transform.Find(UIConstants.Cards.RuneCardPrefab.TitleComponent).GetComponent<Text>();
        title.text = runeCard.Settings.DisplayName;
        var description = transform.Find(UIConstants.Cards.RuneCardPrefab.DescriptionComponent).GetComponent<Text>();
        description.text = runeCard.Settings.Description;
        var cost = transform.Find(UIConstants.Cards.RuneCardPrefab.CostComponent);
        if (runeCard.Settings.Cost == 0)
        {
            cost.gameObject.SetActive(false);
        }
        else
        {
            cost.GetComponentInChildren<Text>().text = runeCard.Settings.Cost.ToString();
            cost.gameObject.SetActive(true);
        }
        var value = transform.Find(UIConstants.Cards.RuneCardPrefab.ValueComponent);
        value.GetComponentInChildren<Text>().text = runeCard.Settings.Value.ToString();

        var backgroundImage = transform.GetComponent<Image>();
        var costImage = cost.GetComponent<Image>();
        var valueImage = value.GetComponent<Image>();

        if (runeCard.Settings.Decks.Any(x => x == Constants.Decks.Might))
        {
            backgroundImage.color = new Color(230 / 255f, 200 / 255f, 200 / 255f);
            costImage.color = new Color(170 / 255f, 40 / 255f, 40 / 255f);
            valueImage.color = new Color(170 / 255f, 40 / 255f, 40 / 255f);
        }
        else if (runeCard.Settings.Decks.Any(x => x == Constants.Decks.Magic))
        {
            backgroundImage.color = new Color(200 / 255f, 200 / 255f, 230 / 255f);
            costImage.color = new Color(40 / 255f, 40 / 255f, 170 / 255f);
            valueImage.color = new Color(40 / 255f, 40 / 255f, 170 / 255f);
        }
        else
        {
            backgroundImage.color = new Color(200 / 255f, 230 / 255f, 200 / 255f);
            costImage.color = new Color(40 / 255f, 170 / 255f, 40 / 255f);
            valueImage.color = new Color(40 / 255f, 170 / 255f, 40 / 255f);
        }
    }

    public class Pool : MonoPoolableMemoryPool<RuneCard, RuneCardBehaviour>
    {
    }
}