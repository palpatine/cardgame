﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using Assets.GameUI.Tools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class PlatformCardBehaviour : MonoBehaviour, IPoolable<PlatformCard>
{
#pragma warning disable CS0649

    [Inject]
    private IPlatformFeatureBehaviourPool _platformFeatureBehaviourPool;

    [Inject]
    private IPlatformObstacleBehaviourPool _platformObstacleBehaviourPool;

    [Inject]
    private InventoryItemBehaviour.Pool _inventoryItemBehaviourPool;

#pragma warning restore CS0649
    private Image _background;
    private Text _platformKind;
    private Text _exploration;
    private Transform _featuresContainer;
    private Transform _itemsContainer;
    private RectTransform _actionArea;
    private RectTransform _charactersContainerLeft;
    private RectTransform _charactersContainerRight;
    private string _currentObstacleName;
    private RectTransform _obstacle;
    private Dictionary<Item, InventoryItemBehaviour> _items = new Dictionary<Item, InventoryItemBehaviour>();
    private ResourceIndicatorBehavoiur[] _resourceIndicators;

    public PlatformCard Card { get; private set; }

    private List<(string, Transform)> _features = new List<(string, Transform)>();

    private IDisposable _reactionsDisposer;

    public class Pool : MonoPoolableMemoryPool<PlatformCard, PlatformCardBehaviour>
    {
    }

    public void OnDespawned()
    {
        _reactionsDisposer.Dispose();
        ClearFeatures();

        foreach (var item in _items)
        {
            _inventoryItemBehaviourPool.Despawn(item.Value);
        }

        _items.Clear();
    }

    private void ClearFeatures()
    {
        foreach (var item in _features)
        {
            _platformFeatureBehaviourPool.Despawn(item.Item2, item.Item1);
        }

        _features.Clear();
    }

    public void OnSpawned(PlatformCard card)
    {
        Card = card;
        _reactionsDisposer = new MergingDisposer(
            Card.OnChange(x => x.IsExplored).React(FillExploration),
            Card.OnChange(x => x.Obstacle).React(PlaceObstacle),
            Card.OnChange(x => x.Features).React(PlaceFeatures),
            Card.TrackValue(x => x.Items, ItemsSetChanged),
            Card.TrackValue(x => x.Resources, x => UpdateResources()));

        if (_background != null)
        {
            _charactersContainerRight.SetParent(null, false);
            Render();
        }
    }

    private void ItemsSetChanged(NotifyCollectionChangedEventArgs e)
    {
        if (e.Action == NotifyCollectionChangedAction.Add)
        {
            foreach (Item item in e.NewItems)
            {
                var inventoryItem = _inventoryItemBehaviourPool.Spawn(item);
                inventoryItem.transform.SetParent(_itemsContainer);
                _items.Add(item, inventoryItem);
            }
        }
        else if (e.Action == NotifyCollectionChangedAction.Remove)
        {
            foreach (Item item in e.OldItems)
            {
                _inventoryItemBehaviourPool.Despawn(_items[item]);
                _items.Remove(item);
            }
        }
        else
        {
            throw new NotImplementedException();
        }
    }

    private void Render()
    {
        var backgroundPath = string.Format(
            UIConstants.Cards.PlatformCardPrefab.BackgroundResourcePathTemplate,
            Card.Settings.Kinds.First());

        var texture = (Texture2D)Resources.Load(
            backgroundPath);
        if (texture != null)
        {
            _background.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
        else
        {
            Debug.Log($"mising background {backgroundPath}");
        }

        _platformKind.text = Card.Settings.DisplayName;

        foreach (var item in _resourceIndicators)
        {
            item.gameObject.SetActive(false);
        }

        UpdateResources();

        PlaceFeatures();
        FillExploration();
        PlaceObstacle();
    }

    private void PlaceFeatures()
    {
        ClearFeatures();
        if (Card.Features != null)
        {
            for (var i = 0; i < Card.Features.Count; i++)
            {
                var feature = _platformFeatureBehaviourPool.Spawn(Card, Card.Features.Kind);
                feature.SetParent(_featuresContainer, false);
                _features.Add((Card.Features.Kind, feature.transform));
            }
        }
    }

    private void UpdateResources()
    {
        if (_resourceIndicators != null)
        {
            var resources = Card.Resources.GroupBy(x => x.Settings.Name)
                        .ToDictionary(x => x.Key, x => x.Count());
            var i = 0;
            foreach (var item in resources)
            {
                _resourceIndicators[i].SetResource(item.Key, item.Value);
                i++;
            }
        }
    }

    private void PlaceObstacle()
    {
        if (_charactersContainerRight == null)
        {
            return;
        }

        if (_obstacle != null)
        {
            _platformObstacleBehaviourPool.Despawn(_obstacle, _currentObstacleName);
            _obstacle = null;
            _currentObstacleName = null;
        }

        if (Card.Obstacle == null)
        {
            _charactersContainerRight.SetParent(null, false);
            _charactersContainerLeft.offsetMin = new Vector2(0, 0);
            _charactersContainerLeft.offsetMax = new Vector2(0, 0);
        }
        else
        {
            _currentObstacleName = Card.Obstacle.Settings.Name;
            _obstacle = _platformObstacleBehaviourPool.Spawn(Card, Card.Obstacle.Settings.Name);
            _obstacle.SetParent(_actionArea, false);
            _obstacle.SetSiblingIndex(1);
            _obstacle.SetMiddleCenterAnchor();
            _obstacle.anchoredPosition = new Vector2(0, 0);
            _charactersContainerRight.SetParent(_actionArea, false);
            _charactersContainerRight.SetSiblingIndex(2);
            var offset = _obstacle.rect.width / 2f;
            var actionAreaWidth = _actionArea.rect.width / 2f;
            _charactersContainerLeft.offsetMin = new Vector2(0, 0);
            _charactersContainerLeft.offsetMax = new Vector2(-offset - actionAreaWidth, 0);

            _charactersContainerRight.offsetMin = new Vector2(actionAreaWidth + offset, 0);
            _charactersContainerRight.offsetMax = new Vector2(0, 0);
        }
    }

    private void FillExploration()
    {
        if (_exploration != null)
        {
            if (Card.IsExplored)
            {
                _exploration.text = Card.Settings.Exploration?.Place;
            }
            else
            {
                _exploration.text = Card.Settings.Exploration != null ? "?" : "";
            }
        }
    }

    public void Start()
    {
        _platformKind = transform.Find(UIConstants.Cards.PlatformCardPrefab.PlatformKindText)
            .GetComponent<Text>();
        _exploration = transform.Find(UIConstants.Cards.PlatformCardPrefab.ExplorationText)
            .GetComponent<Text>();
        _featuresContainer = transform.Find(UIConstants.Cards.PlatformCardPrefab.FeaturesContainer);
        _itemsContainer = transform.Find(UIConstants.Cards.PlatformCardPrefab.ItemsContainer);
        _actionArea = (RectTransform)transform.Find(UIConstants.Cards.PlatformCardPrefab.ActionArea);
        _charactersContainerLeft = (RectTransform)_actionArea.Find(UIConstants.Cards.PlatformCardPrefab.UnitsContainerLeft);
        _charactersContainerRight = (RectTransform)_actionArea.Find(UIConstants.Cards.PlatformCardPrefab.UnitsContainerRight);
        _charactersContainerRight.SetParent(null, false);
        _background = transform.GetComponent<Image>();
        _resourceIndicators = transform.Find(UIConstants.Cards.PlatformCardPrefab.ResourcesContainer)
            .GetComponentsInChildren<ResourceIndicatorBehavoiur>();
        Render();
    }
}