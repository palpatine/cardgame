﻿using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ActionCardBehaviour : MonoBehaviour, IPoolable<ActionCard>
{
    public ActionCard ActionCard { get; private set; }

    public void OnDespawned()
    {
        transform.localScale = Vector3.one;
    }

    public void OnSpawned(ActionCard actionCard)
    {
        ActionCard = actionCard;
        var title = transform.Find(UIConstants.Cards.ActionCardPrefab.TitleComponent).GetComponent<Text>();
        title.text = actionCard.Settings.DisplayName;
        var description = transform.Find(UIConstants.Cards.ActionCardPrefab.DescriptionComponent).GetComponent<Text>();
        description.text = actionCard.Settings.Description;
        var cost = transform.Find(UIConstants.Cards.RuneCardPrefab.CostComponent).GetComponentInChildren<Text>();
        cost.text = actionCard.Settings.EnergyCost.ToString();
    }

    public class Pool : MonoPoolableMemoryPool<ActionCard, ActionCardBehaviour>
    {
    }
}