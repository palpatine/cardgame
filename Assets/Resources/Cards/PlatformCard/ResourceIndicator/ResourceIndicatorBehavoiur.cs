﻿using Assets.GameLogic;
using Assets.GameLogic.Settings;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;

public class ResourceIndicatorBehavoiur : MonoBehaviour
{
    private Image _resource;
    private Text _count;

    private void Awake()
    {
        _resource = transform.Find(UIConstants.Cards.PlatformCardResourceIndicatorPrefab.Resource).GetComponent<Image>();
        _count = transform.Find(UIConstants.Cards.PlatformCardResourceIndicatorPrefab.Count).GetComponent<Text>();
    }

    public void SetResource(string name, int count)
    {
        gameObject.SetActive(true);
        var resourcePath = string.Format(
            UIConstants.Icons.Resource,
            name);

        var texture = (Texture2D)Resources.Load(
            resourcePath);
        if (texture != null)
        {
            _resource.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
        else
        {
            Debug.Log($"mising resource icon {resourcePath}");
        }

        _count.text = count.ToString();
    }
}
