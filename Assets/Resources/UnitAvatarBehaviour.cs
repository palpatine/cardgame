﻿using System;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.State;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UnitAvatarBehaviour : MonoBehaviour, IPoolable<IUnit>
{
#pragma warning disable CS0649

    [Inject]
    private IGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    public IUnit Unit { get; private set; }

    private Image _image;
    private Text _name;
    private Transform _face;
    private Color _color;
    private bool _isDragged;

    public bool IsSelected
    {
        get => _isDragged; set
        {
            if (_isDragged == value) return;
            if (value)
            {
                _color = _image.color;
                var factor = 150 / 255f;
                _image.color = new Color(
                    Math.Max(0, _color.r - factor),
                    Math.Max(0, _color.g - factor),
                    Math.Max(0, _color.b - factor));
            }
            else
            {
                _image.color = _color;
            }
            _isDragged = value;
        }
    }

    public class Pool : MonoPoolableMemoryPool<IUnit, UnitAvatarBehaviour>
    {
    }

    public void OnDespawned()
    {
    }

    public void OnSpawned(IUnit unit)
    {
        ((RectTransform)transform).localScale = new Vector3(1, 1, 1);

        Unit = unit;
        _image = GetComponent<Image>();
        _name = transform.GetComponentInChildren<Text>();
        _face = transform.Find(UIConstants.UnitAvatarPrefab.FaceComponent);
        if (unit is Character character)
        {
            _name.text = string.Empty;
            _image.color = character.Color;
        }
        else
        {
            _image.color = Color.black;
            var foe = (Foe)Unit;
            _name.text = foe.Settings.Name.Substring(0, 1).ToUpper() + _gameStateProvider.GameState.Units.OfType<Foe>().Where(x => x.Settings.Name == foe.Settings.Name).ToList().IndexOf(foe);
        }

        SetIsFacingLeft(unit.Location.IsFacingLeft);
    }

    public void MakeEphemeral()
    {
        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 200 / 255f);
        ((RectTransform)transform).localScale = new Vector3(.4f, .4f, .4f);
    }

    internal void SetIsFacingLeft(bool isFacingLeft)
    {
        if (isFacingLeft)
        {
            _face.localPosition = new Vector3(-12, 0);
        }
        else
        {
            _face.localPosition = new Vector3(0, 0);
        }
    }
}