﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Assets.GameLogic.Activities;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class ActivityOptionsSetBehaviour : MonoBehaviour,
    IPoolable<ActivityOptionsSetBehaviour.Pool, IEnumerable<ActivityOption>, Transform>,
    IDisposable
{
#pragma warning disable CS0649

    [Inject]
    private ActivityOptionBehaviour.Pool _activityOptionBehaviourPool;

    [Inject]
    private IUpdateNotifier _updateNotifier;

#pragma warning restore CS0649
    private List<ActivityOptionBehaviour> _options = new List<ActivityOptionBehaviour>();
    private LayoutElement _layoutElement;
    private RectTransform _rectTransform;
    private RectTransform _parentRectTransform;
    private Pool _pool;
    private bool _isDisposed;
    private bool _canClose;

    public class Pool : MonoPoolableMemoryPool<Pool, IEnumerable<ActivityOption>, Transform, ActivityOptionsSetBehaviour>
    {
    }

    public void OnDespawned()
    {
        foreach (var item in _options)
        {
            _activityOptionBehaviourPool.Despawn(item);
        }

        _options.Clear();
    }

    public void Awake()
    {
        _layoutElement = GetComponent<LayoutElement>();
        _rectTransform = (RectTransform)transform;
    }

    public void OnSpawned(Pool pool, IEnumerable<ActivityOption> options, Transform parent)
    {
        _isDisposed = false;

        _pool = pool;
        transform.SetParent(parent, false);
        _parentRectTransform = (RectTransform)transform.parent;
        foreach (var option in options)
        {
            var item = _activityOptionBehaviourPool.Spawn(this, option);
            item.transform.SetParent(transform, false);
            _options.Add(item);
        }

        _updateNotifier.Await(AdjustLayout());
    }

    private async Task AdjustLayout()
    {
        await _updateNotifier.NextUpdate;
        var halfPopupWidth = _rectTransform.rect.width / 2f;
        var halfPopupHeight = _rectTransform.rect.height / 2f;

        var preferredX = Input.mousePosition.x + 200 + _rectTransform.rect.width > Screen.width ? Input.mousePosition.x - 200 : Input.mousePosition.x + 200;

        transform.position = new Vector3(
            Math.Min(Screen.width - halfPopupWidth,
                     Math.Max(halfPopupWidth, preferredX)),
            Math.Min(Screen.height - halfPopupHeight,
                     Screen.height / 2f));

        await _updateNotifier.NextUpdate;
        _canClose = true;
    }

    public void Dispose()
    {
        if (_isDisposed) return;
        _isDisposed = true;
        _canClose = false;
        _pool.Despawn(this);
    }

    public void Update()
    {
        if (_canClose && (Input.mousePosition.x < _parentRectTransform.position.x - _parentRectTransform.rect.width / 2
            || Input.mousePosition.x > _parentRectTransform.position.x + _parentRectTransform.rect.width / 2
            || Input.mousePosition.y < _parentRectTransform.position.y - _parentRectTransform.rect.height / 2
            || Input.mousePosition.y > _parentRectTransform.position.y + _parentRectTransform.rect.height / 2))
        {
            Dispose();
        }
    }
}