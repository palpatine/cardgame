﻿using Assets.GameLogic;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Zenject;

public class UnitStatisticChangeIndicator : MonoBehaviour, IPoolable<string, int, Color>
{
    public void OnDespawned()
    {
    }

    public void OnSpawned(string statisticName, int delta, Color unitColor)
    {
        var nameTransform = transform.Find(UIConstants.UnitStatisticChangeIndicatorPrefab.NameComponent);
        var nameOutline = nameTransform.GetComponent<NicerOutline>();
        var nameTextComponent = nameTransform.GetComponent<Text>();
        nameTextComponent.text = statisticName;
        var deltaTransform = transform.Find(UIConstants.UnitStatisticChangeIndicatorPrefab.DeltaComponent);
        var deltaTextComponent = deltaTransform.GetComponent<Text>();
        var deltaOutline = deltaTransform.GetComponent<NicerOutline>();
        deltaTextComponent.text = delta.ToString();

        Color color;
        if (delta < 0)
        {
            color = new Color(230 / 255f, 50 / 255f, 50 / 255f);
        }
        else
        {
            color = new Color(50 / 255f, 230 / 255f, 50 / 255f);
        }

        nameTextComponent.color = color;
        deltaTextComponent.color = color;
        nameOutline.effectColor = new Color(unitColor.r / 2f, unitColor.g / 2f, unitColor.b / 2f);
        deltaOutline.effectColor = new Color(unitColor.r / 2f, unitColor.g / 2f, unitColor.b / 2f);
    }

    public class Pool : MonoPoolableMemoryPool<string, int, Color, UnitStatisticChangeIndicator>
    {
    }
}