﻿using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ActionBehaviour : MonoBehaviour, IPoolable<IAction>
{
    public class Pool : MonoPoolableMemoryPool<IAction, ActionBehaviour>
    {
    }

    private IAction _action;
    private Button _button;
#pragma warning disable CS0649

    [Inject]
    private IUpdateNotifier _updateNotifier;
    [Inject]
    protected IGameStateProvider _gameStateProvider;



#pragma warning restore CS0649

    public void OnSpawned(IAction action)
    {
        _action = action;
        var title = transform.GetComponentInChildren<Text>();
        title.text = _action.DisplayName;
    }

    private void Awake() => _button = transform.GetComponent<Button>();

    private void Update() => _button.interactable = _action?.CanExecute() ?? false;

    public void OnExecute()
    {
        if (_gameStateProvider.GameState.CurrentUnit is Character)
        {
            _updateNotifier.Await(_action.Execute());
        }
    }

    public void OnDespawned()
    {
    }
}