﻿using System;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Activities;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ActivityOptionBehaviour : MonoBehaviour, IPoolable<IDisposable, ActivityOption>
{
    private Text _cost;
    private RectTransform _rectTransform;
    private RectTransform _text;

#pragma warning disable CS0649

    [Inject]
    private IActivitySummaryProvider _actionSummaryProvider;

    [Inject]
    private IUpdateNotifier _updateNotifier;

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private IExecuteActionActivity _executeActionAction;

#pragma warning restore CS0649
    private ActivityOption _actionOptions;
    private IDisposable _close;

    public class Pool : MonoPoolableMemoryPool<IDisposable, ActivityOption, ActivityOptionBehaviour>
    {
    }

    public void OnDespawned()
    {
    }

    public void Awake()
    {
        _cost = transform
            .Find(UIConstants.ActivityOptionPrefab.Cost)
            .GetComponent<Text>();
        _rectTransform = (RectTransform)transform;
        _text = _cost.rectTransform;
    }

    public async void OnSpawned(IDisposable close, ActivityOption option)
    {
        _actionOptions = option;
        _close = close;
        _cost.text = _actionSummaryProvider
            .CreateCostSummary(option.Cost, option.Action.DisplayName)
            + Environment.NewLine
            + string.Join(Environment.NewLine, option.Targets.Select(x => $"{GetName(x)}: L{x.Life.Current} E{x.Energy.Current} B{x.Block.Current} D{x.Defense.Current}"));

        await _updateNotifier.NextUpdate;
        var height = _text.rect.height * _text.localScale.y + 10;
        _rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }

    private string GetName(IUnit unit)
    {
        if (unit is Foe foe)
        {
            return foe.Settings.Name.Substring(0, 1).ToUpper() + _gameStateProvider.GameState.Units.OfType<Foe>().Where(x => x.Settings.Name == foe.Settings.Name).ToList().IndexOf(foe);
        }
        else
        {
            var character = (Character)unit;
            if (character.Color.r > 0) return "Red";
            if (character.Color.b > 0) return "Blue";
            return "Green";
        }
    }

    public void Execute()
    {
        _close.Dispose();

        _updateNotifier.Await(_executeActionAction.Execute(_actionOptions));
    }
}