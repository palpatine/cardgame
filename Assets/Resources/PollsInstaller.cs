﻿using Assets.GameUI;
using Assets.GameUI.ResourcePools;
using UnityEngine;
using Zenject;

internal class PollsInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container
            .BindMemoryPoolCustomInterface<DangerCardBehaviour, DangerCardBehaviour.Pool, IDangerCardBehaviourPool>()
                         .FromComponentInNewPrefab(
                            Resources.Load<GameObject>(UIConstants.Cards.DangerCardPrefab.Location));
    }
}
