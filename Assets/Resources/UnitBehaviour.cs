﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Assets.GameLogic;
using Assets.GameLogic.Messages;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UnitBehaviour : MonoBehaviour, IPoolable<IUnit>
{
#pragma warning disable CS0649

    [Inject]
    private PlatformCardsLayoutBehaviour _mapContainer;

    [Inject]
    private IGameStateProvider _gameStateProvider;

    [Inject]
    private IUpdateNotifier _updateNotifier;

    [Inject]
    private UnitStatisticChangeIndicator.Pool _unitStatisticChangeIndicatorPool;

#pragma warning restore CS0649
    private Animator _animator;
    private Text _name;
    private Transform _container;
    private Transform _face;
    private Transform _equipment;
    private RectTransform _shield;
    private RectTransform _sword;
    private IDisposable _currentUnitReactionDisposer;
    private bool _started;
    private CancellationTokenSource _cancellationTokenSource;
    private Vector2 _unitLocationOnTransferPanel;
    private Color _color;

    public IUnit Unit { get; private set; }

    public void OnDespawned()
    {
        _animator.SetBool("IsCurrentUnit", false);
        _currentUnitReactionDisposer.Dispose();
    }

    public void Awake()
    {
        _animator = transform.GetComponentInChildren<Animator>();
        _name = transform.GetComponentInChildren<Text>();
        _container = transform.Find(UIConstants.UnitPrefab.ContainerComponent);
        _face = _container.Find(UIConstants.UnitPrefab.FaceComponent);
        _equipment = _container.Find(UIConstants.UnitPrefab.EquipmentComponent);
        //this will be replaced when actual items are implemented
        _shield = (RectTransform)_equipment.Find("Shiled");
        _sword = (RectTransform)_equipment.Find("Sword");
    }

    public void OnSpawned(IUnit unit)
    {
        Unit = unit;

        _currentUnitReactionDisposer =
            new MergingDisposer(
                _gameStateProvider.GameState
                     .OnChange(x => x.CurrentUnit)
                      .React(() =>
                             _animator.SetBool("IsCurrentUnit",
                                 _gameStateProvider.GameState.CurrentUnit == Unit)),
                unit
                    .TrackValue(x => x.Location)
                    .OnChange(x => x.PlatformCard)
                    .Or(x => x.IsToTheLeftOfObstacle)
                    .Or(x => x.UnitId)
                    .Or(x => x.IsFacingLeft)
                    .ReactOneAtTime(MoveToNewLocation),
                unit
                    .TrackValue(x => x.Life)
                    .OnChange(x => x.Current)
                    .React(x => DisplayStatisticChange(x, Constants.Statisitcs.Life)),
                unit
                    .TrackValue(x => x.Energy)
                    .OnChange(x => x.Current)
                    .React(x => DisplayStatisticChange(x, Constants.Statisitcs.Energy)),
                unit
                    .TrackValue(x => x.Movement)
                    .OnChange(x => x.Current)
                    .React(x => DisplayStatisticChange(x, Constants.Statisitcs.Movement)),
                unit
                    .TrackValue(x => x.Block)
                    .OnChange(x => x.Current)
                    .React(x => DisplayStatisticChange(x, Constants.Statisitcs.Block)),
                unit
                    .TrackValue(x => x.Attack)
                    .OnChange(x => x.Current)
                    .React(x => DisplayStatisticChange(x, Constants.Statisitcs.Attack)),
                unit
                    .TrackValue(x => x.Mana)
                    .OnChange(x => x.Current)
                    .React(x => DisplayStatisticChange(x, Constants.Statisitcs.Mana)));

        this.Subscribe<ActionMessage>(AttackAnimation);
        this.Subscribe<BlockMessage>(BlockAnimation);
        SetColor(unit);

        SetFacingDirection();

        _animator.SetBool("IsCurrentUnit",
            _gameStateProvider.GameState.CurrentUnit == Unit);

        if (Unit.Location.PlatformCard != null && _started)
        {
            _updateNotifier.Await(SetLocationAsync());
        }
    }

    private async Task BlockAnimation(BlockMessage arg)
    {
        if (arg.Unit == Unit)
        {
            var start = _shield.anchoredPosition;
            var target = new Vector2(start.x + 10, start.y + 20);
            Vector2 current, _ = Vector2.zero;
            do
            {
                current = _shield.anchoredPosition;
                _shield.anchoredPosition = Vector2.SmoothDamp(_shield.anchoredPosition, target, ref _, Time.deltaTime * 50);
                await _updateNotifier.NextUpdate;
            }
            while (current != _shield.anchoredPosition);

            await _updateNotifier.NextUpdate;
            do
            {
                current = _shield.anchoredPosition;
                _shield.anchoredPosition = Vector2.SmoothDamp(_shield.anchoredPosition, start, ref _, Time.deltaTime * 50);
                await _updateNotifier.NextUpdate;
            }
            while (current != _shield.anchoredPosition);
        }
    }

    private async Task AttackAnimation(ActionMessage arg)
    {
        var speed = 300;
        if (arg.Actor == Unit)
        {
            var angleDelta = 0f;
            for (var i = 0; i < 8; i++)
            {
                var angle = -speed * Time.deltaTime;
                angleDelta += angle;
                _sword.Rotate(Vector3.forward, angle);
                await _updateNotifier.NextUpdate;
            }

            for (var i = 0; i < 8; i++)
            {
                var angle = speed * Time.deltaTime;
                angleDelta += angle;
                _sword.Rotate(Vector3.forward, angle);
                await _updateNotifier.NextUpdate;
            }

            _sword.Rotate(Vector3.forward, -angleDelta);
        }
    }

    private async Task DisplayStatisticChange(ChangeNotification change, string statistic)
    {
        _updateNotifier.Await(DisplayStatisticChangeImpl(change, statistic));
        await Task.Delay((int)(5 * Time.deltaTime * 1000));
    }

    private async Task DisplayStatisticChangeImpl(ChangeNotification change, string statistic)
    {
        if (Unit.Location.PlatformCard == null || transform.parent != GetNewLocationContainer())
            return;

        var current = (int)change.NewValue;
        var old = (int)change.OldValue;
        var delta = current - old;
        var position = new Vector2(_unitLocationOnTransferPanel.x, _unitLocationOnTransferPanel.y + 40);

        var behaviour = _unitStatisticChangeIndicatorPool.Spawn(statistic, delta, _color);
        var item = (RectTransform)behaviour.transform;
        var loop = true;
        do
        {
            await _updateNotifier.NextUpdate;
            var items = _mapContainer.transform.parent.Cast<Transform>()
              .Select(x => x.GetComponent<UnitStatisticChangeIndicator>())
              .Where(x => x != null)
              .ToArray();
            if (!items.Any()) break;
            var positions = items.Select(x => ((RectTransform)x.transform).anchoredPosition)
            .ToArray();

            loop = false;
            foreach (var x in positions)
            {
                if (!(x.x >= position.x + item.rect.width
                    || x.x + item.rect.width <= position.x
                    || x.y <= position.y - item.rect.height
                    || x.y - item.rect.height >= position.y))
                {
                    loop = true;
                }
            }
        }
        while (loop);

        item.SetParent(_mapContainer.transform.parent, false);
        item.anchoredPosition = position;

        await _updateNotifier.NextUpdate;

        var target = new Vector2(item.anchoredPosition.x, item.anchoredPosition.y + 100);
        Vector2 currentPossiton;
        do
        {
            currentPossiton = item.anchoredPosition;
            item.anchoredPosition = Vector2.MoveTowards(item.anchoredPosition, target, Time.deltaTime * 40);
            await _updateNotifier.NextUpdate;
        }
        while (currentPossiton != item.anchoredPosition);

        _unitStatisticChangeIndicatorPool.Despawn(behaviour);
    }

    private void SetColor(IUnit unit)
    {
        var image = GetComponent<Image>();
        if (unit is Character character)
        {
            _color = image.color = character.Color;
        }
        else
        {
            _color = image.color = Color.black;
            var foe = (Foe)Unit;
            _name.text = GetName(foe);
        }
    }

    private string GetName(Foe foe) => foe.Settings.Name.Substring(0, 1).ToUpper() + _gameStateProvider.GameState.Units.OfType<Foe>().Where(x => x.Settings.Name == foe.Settings.Name).ToList().IndexOf(foe);

    private void SetFacingDirection()
    {
        SetFacingDirection(Unit.Location.IsFacingLeft);
    }

    private void SetFacingDirection(bool isFacingLeft)
    {
        if (isFacingLeft)
        {
            _container.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            _container.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void Start()
    {
        _started = true;
        if (Unit.Location.PlatformCard != null)
        {
            _updateNotifier.Await(async () =>
            {
                await SetLocationAsync();
                await SetTransferPanelLocation();
            });
        }
    }

    private async Task SetTransferPanelLocation()
    {
        await _updateNotifier.NextUpdate;
        transform.SetParent(_mapContainer.transform.parent, true);
        var rectangle = (RectTransform)transform;
        await _updateNotifier.NextUpdate;
        _unitLocationOnTransferPanel = rectangle.anchoredPosition;
        await SetLocationAsync();
    }

    private async Task MoveToNewLocation(ChangeNotification data, CancellationToken token)
    {
        var rectangle = (RectTransform)transform;
        transform.SetParent(_mapContainer.transform.parent, true);
        await _updateNotifier.NextUpdate;
        var currentPosition = rectangle.anchoredPosition;
        var speed = 350;
        await SetLocationAsync();
        if (data.OldValue != null)
        {
            await _updateNotifier.NextUpdate;
            transform.SetParent(_mapContainer.transform.parent, true);
            transform.SetSiblingIndex(1);
            var newPosition = rectangle.anchoredPosition;
            SetFacingDirection(currentPosition.x > newPosition.x);
            rectangle.anchoredPosition = currentPosition;
            do
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }

                _unitLocationOnTransferPanel = rectangle.anchoredPosition;
                rectangle.anchoredPosition = Vector2.MoveTowards(rectangle.anchoredPosition, newPosition, Time.deltaTime * speed);
                await _updateNotifier.NextUpdate;
            } while (_unitLocationOnTransferPanel != rectangle.anchoredPosition);

            await SetLocationAsync();
        }
        else
        {
            await SetTransferPanelLocation();
        }
    }

    private async Task SetLocationAsync()
    {
        var container = GetNewLocationContainer();
        transform.SetParent(container, false);
        SetFacingDirection();

        do
        {
            transform.SetSiblingIndex(Unit.Location.UnitId);
            await _updateNotifier.NextUpdate;
        }
        while (transform.GetSiblingIndex() != Unit.Location.UnitId);
    }

    private Transform GetNewLocationContainer()
    {
        var currentLevelId = Unit.Location.CurrentLevelId;
        var containerName = Unit.Location.IsToTheLeftOfObstacle
            ? UIConstants.Cards.PlatformCardPrefab.UnitsContainerLeft
            : UIConstants.Cards.PlatformCardPrefab.UnitsContainerRight;

        var platforms = _gameStateProvider.GameState.Levels[currentLevelId].Platforms;
        var platformId = platforms.IndexOf(Unit.Location.PlatformCard);
        var container = _mapContainer.GetLevelContainer(currentLevelId)
            .Cast<Transform>()
            .ElementAt(platformId + 1)
            .Find(UIConstants.Cards.PlatformCardPrefab.ActionArea)
            .Find(containerName);
        return container;
    }

    public class Pool : MonoPoolableMemoryPool<IUnit, UnitBehaviour>
    {
    }
}