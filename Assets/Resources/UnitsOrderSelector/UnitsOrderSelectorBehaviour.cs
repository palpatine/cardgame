﻿using System.Collections.Generic;
using System.Linq;
using Assets.GameLogic;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class UnitsOrderSelectorBehaviour : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IUnitIncidents _unitIncidents;

    [Inject]
    private UnitAvatarBehaviour.Pool _unitsPool;

#pragma warning restore CS0649

    private UnitAvatarBehaviour _dragged;
    private bool _imDragging;
    private Dictionary<IUnit, UnitAvatarBehaviour> _units = new Dictionary<IUnit, UnitAvatarBehaviour>();
    private UnitAvatarBehaviour _selected;
    private UnitAvatarBehaviour _otherSelectd;

    private void Start()
    {
        _gameStateProvider.UIState.UnitsOrderSelection
            .OnChange(x => x.IsOpen)
            .React(x =>
                {
                    if ((bool)x.NewValue)
                    {
                        Clear();
                        Create();
                    }
                    else
                    {
                        Clear();
                    }
                });

        if (_gameStateProvider.UIState.UnitsOrderSelection.IsOpen)
        {
            Create();
        }
    }

    private void Create()
    {
        _units = _gameStateProvider.GameState.UnitsOrder
        .ToDictionary(item => item, item => _unitsPool.Spawn(item));
        foreach (var item in _units.Values)
        {
            item.transform.SetParent(transform, false);
        }
    }

    private void Clear()
    {
        foreach (var item in _units)
        {
            _unitsPool.Despawn(item.Value);
        }
        _units.Clear();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        var avatar = eventData.hovered.SingleOrDefault(x => x.CompareTag(UIConstants.UI.UnitAvatarTag));
        if (avatar != null)
        {
            _selected = avatar.GetComponent<UnitAvatarBehaviour>();
            var character = _selected.Unit as Character;

            if (character != null)
            {
                _selected.IsSelected = true;
                _dragged = _unitsPool.Spawn(character);
                _dragged.MakeEphemeral();
                _dragged.transform.SetParent(transform.parent, false);
                _dragged.transform.position = Input.mousePosition;
                _imDragging = true;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_imDragging)
        {
            _imDragging = false;
            if (_otherSelectd != null)
            {
                _otherSelectd.IsSelected = false;
                _otherSelectd = null;
            }
            _selected.IsSelected = false;

            var avatar = eventData.hovered.Where(x =>
            x != _dragged.gameObject
            && x.CompareTag(UIConstants.UI.UnitAvatarTag))
            .SingleOrDefault(x => x.GetComponent<UnitAvatarBehaviour>().Unit != _dragged.Unit);

            if (avatar != null)
            {
                var behaviour = avatar.GetComponent<UnitAvatarBehaviour>();
                var character = behaviour.Unit as Character;
                if (character != null)
                {
                    var newId = behaviour.transform.GetSiblingIndex();
                    var draggedId = _selected.transform.GetSiblingIndex();
                    behaviour.transform.SetSiblingIndex(draggedId);
                    _selected.transform.SetSiblingIndex(newId);
                }
            }

            _unitsPool.Despawn(_dragged);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!_imDragging) return;

        if (_otherSelectd != null)
        {
            _otherSelectd.IsSelected = false;
            _otherSelectd = null;
        }

        var avatar = eventData.hovered.Where(x =>
        x != _dragged.gameObject
        && x.CompareTag(UIConstants.UI.UnitAvatarTag))
        .SingleOrDefault(x => x.GetComponent<UnitAvatarBehaviour>().Unit != _dragged.Unit);
        if (avatar != null)
        {
            var behaviour = avatar.GetComponent<UnitAvatarBehaviour>();
            var character = behaviour.Unit as Character;

            if (character != null)
            {
                behaviour.IsSelected = true;
                _otherSelectd = behaviour;
            }
        }

        _dragged.transform.position = new Vector3(
            Input.mousePosition.x - 8,
            Input.mousePosition.y,
            Input.mousePosition.z);
    }

    public void OnAccept()
    {
        _unitIncidents.SetUnitsOrder(_units.OrderBy(x => x.Value.transform.GetSiblingIndex()).Select(x => x.Key));
    }
}