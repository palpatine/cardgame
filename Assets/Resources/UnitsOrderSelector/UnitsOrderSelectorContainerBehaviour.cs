﻿using Assets.GameLogic;
using Assets.GameLogic.Messages;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class UnitsOrderSelectorContainerBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    private void Start()
    {
        transform.gameObject.SetActive(_gameStateProvider.UIState.UnitsOrderSelection.IsOpen);
        _gameStateProvider.UIState.UnitsOrderSelection
            .OnChange(x => x.IsOpen)
            .React(() =>
                transform.gameObject.SetActive(_gameStateProvider.UIState.UnitsOrderSelection.IsOpen));
        this.Subscribe<SelectUnitsOrder>(_ =>
        {
            _gameStateProvider.UIState.UnitsOrderSelection.IsOpen = true;
            return _gameStateProvider.UIState.UnitsOrderSelection.AwaitChangeOnce(x => x.IsOpen);
        });
    }
}