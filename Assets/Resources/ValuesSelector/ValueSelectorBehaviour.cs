﻿using Assets.GameLogic;
using Assets.GameLogic.Messages;
using Assets.GameUI;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ValueSelectorBehaviour : MonoBehaviour, IPoolable<SelectedValue>
{
    private SelectedValue _selectedValue;
    private Slider _slider;
    private Text _title;
    private Text _value;

    public void OnDespawned()
    {
        _selectedValue = null;
    }

    public void OnSpawned(SelectedValue selectedValue)
    {
        if (_slider == null)
        {
            _slider = GetComponentInChildren<Slider>();
            _title = transform.Find(UIConstants.UI.ValueSelector.Title).GetComponent<Text>();
            _value = transform.Find(UIConstants.UI.ValueSelector.Value).GetComponent<Text>();
        }
        _selectedValue = selectedValue;
        _slider.minValue = selectedValue.Min;
        _slider.value = selectedValue.Min;
        _slider.maxValue = selectedValue.Max;
        _selectedValue.Value = selectedValue.Min;
        _value.text = ((int)_slider.value).ToString();
        _title.text = $"Select value for '{selectedValue.SelectorSetting.Name}':";
    }

    public void OnValueChange()
    {
        var direction = _selectedValue.Value - (int)_slider.value < 0;
        _selectedValue.Value = (int)_slider.value;
        if (_selectedValue.Value % _selectedValue.Increment != 0)
        {
            _selectedValue.Value = (_selectedValue.Value / _selectedValue.Increment + (direction ? 1 : 0)) * _selectedValue.Increment;
        }

        _slider.value = _selectedValue.Value;
        _value.text = _selectedValue.Value.ToString();
    }

    public class Pool : MonoPoolableMemoryPool<SelectedValue, ValueSelectorBehaviour>
    {
    }
}