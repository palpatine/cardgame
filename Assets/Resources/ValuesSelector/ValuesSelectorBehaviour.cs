﻿using System.Collections.Generic;
using Assets.GameLogic;
using Assets.GameLogic.Incidents;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class ValuesSelectorBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

    [Inject]
    private IUnitIncidents _unitIncidents;

    [Inject]
    private ValueSelectorBehaviour.Pool _valueSelectorPool;

#pragma warning restore CS0649

    private readonly List<ValueSelectorBehaviour> _items = new List<ValueSelectorBehaviour>();

    private Transform _cancelButton;

    private void Start()
    {
        _cancelButton = transform
            .parent
            .Find(UIConstants.UI.ValueSelector.CancelButton);
        _gameStateProvider.UIState.ValuesSelector
            .OnChange(x => x.IsOpen)
            .React(x =>
            {
                if ((bool)x.NewValue)
                {
                    Clear();
                    Create();
                }
                else
                {
                    Clear();
                }
            });

        if (_gameStateProvider.UIState.ValuesSelector.IsOpen)
        {
            Create();
        }
    }

    private void Create()
    {
        _cancelButton.gameObject.SetActive(_gameStateProvider.UIState.ValuesSelector.Details.CanCancel);

        foreach (var setting in _gameStateProvider.UIState.ValuesSelector.Details.Selectors)
        {
            var item = _valueSelectorPool.Spawn(setting);
            _items.Add(item);
            item.transform.SetParent(transform, false);
        }
    }

    private void Clear()
    {
        foreach (var item in _items)
        {
            _valueSelectorPool.Despawn(item);
        }

        _items.Clear();
    }

    public void OnAccept()
    {
        _gameStateProvider.UIState.ValuesSelector.IsOpen = false;
    }

    public void OnCancel()
    {
        _gameStateProvider.UIState.ValuesSelector.Details.Cancelled = true;
        _gameStateProvider.UIState.ValuesSelector.IsOpen = false;
    }
}