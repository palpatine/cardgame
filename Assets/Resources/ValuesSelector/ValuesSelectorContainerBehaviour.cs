﻿using Assets.GameLogic;
using Assets.GameLogic.Messages;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI;
using UnityEngine;
using Zenject;

public class ValuesSelectorContainerBehaviour : MonoBehaviour
{
#pragma warning disable CS0649

    [Inject]
    private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

    private void Start()
    {
        transform.gameObject.SetActive(_gameStateProvider.UIState.ValuesSelector.IsOpen);
        _gameStateProvider.UIState.ValuesSelector
            .OnChange(x => x.IsOpen)
            .React(() =>
                transform.gameObject.SetActive(_gameStateProvider.UIState.ValuesSelector.IsOpen));
        this.Subscribe<SelectValues>(message =>
        {
            _gameStateProvider.UIState.ValuesSelector.Details = message;
            _gameStateProvider.UIState.ValuesSelector.IsOpen = true;
            return _gameStateProvider.UIState.ValuesSelector.AwaitChangeOnce(x => x.IsOpen);
        });
    }
}