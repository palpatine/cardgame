﻿using System.Collections;
using System.Collections.Generic;
using Assets.GameLogic.Tools;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;
using Zenject;

public class UpdateNotifierBehaviour : MonoBehaviour
{
    [Inject]
    public IUpdateNotifier _updateNotifierAbstraction;

    private UpdateNotifier _updateNotifier;
    public void Awake()
    {
        _updateNotifier = (UpdateNotifier)_updateNotifierAbstraction;
    }

    public void LateUpdate()
    {
        _updateNotifier.LateUpdate();
    }

    public void Update()
    {
        _updateNotifier.Update();
    }
}