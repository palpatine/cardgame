﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.GameUI.Tools
{
    public static class Extenstions
    {
        public static IEnumerable<Transform> FindAllByTag(this Transform transform, string tag)
        {
            var items = new Stack<Transform>();
            items.Push(transform);
            var result = new List<Transform>();
            while (items.Any())
            {
                var item = items.Pop();
                if (item.CompareTag(tag))
                {
                    result.Add(item);
                }
                foreach (Transform child in item)
                {
                    items.Push(child);
                }
            }

            return result;
        }

        public static Transform FindByTag(this Transform transform, string tag)
        {
            var items = new Stack<Transform>();
            items.Push(transform);

            while (items.Any())
            {
                var item = items.Pop();
                if (item.CompareTag(tag))
                {
                    return item;
                }
                foreach (Transform child in item)
                {
                    items.Push(child);
                }
            }

            return null;
        }

        public static bool NearlyEqual(this Vector3 a, Vector3 b) => NearlyEqual(a.x, b.x, 0.001f) && NearlyEqual(a.y, b.y, 0.001f) && NearlyEqual(a.z, b.z, 0.001f);

        public static bool NearlyEqual(this Vector2 a, Vector2 b) => NearlyEqual(a.x, b.x, 0.001f) && NearlyEqual(a.y, b.y, 0.001f);

        public static bool NearlyEqual(float a, float b, float epsilon)
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (a == b)
            {
                return true;
            }
            else if (a == 0 || b == 0 || diff < float.Epsilon)
            {
                return diff < epsilon;
            }
            else
            {
                return diff / (absA + absB) < epsilon;
            }
        }

        public static void SetMiddleCenterAnchor(this RectTransform transform)
        {
            transform.anchorMin = new Vector2(0.5f, 0.5f);
            transform.anchorMax = new Vector2(0.5f, 0.5f);
        }

        public static void SetStrechAnchor(this RectTransform transform, bool fill = true)
        {
            transform.anchorMin = new Vector2(0, 0);
            transform.anchorMax = new Vector2(1, 1);

            if (fill)
            {
                transform.offsetMin = new Vector2(0, 0); // new Vector2(left, bottom);
                transform.offsetMax = new Vector2(0, 0); // new Vector2(-right, -top)
            }
        }

        public static void SetTopLeftAnchor(this RectTransform transform)
        {
            transform.anchorMin = new Vector2(0, 1);
            transform.anchorMax = new Vector2(0, 1);
        }

        public static void SetTopRightAnchor(this RectTransform transform)
        {
            transform.anchorMin = new Vector2(1, 1);
            transform.anchorMax = new Vector2(1, 1);
        }
    }
}
