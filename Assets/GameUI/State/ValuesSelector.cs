﻿using Assets.GameLogic.Messages;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameUI.State
{
    public class ValuesSelector : Notifier<ValuesSelector>
    {
        private bool _isOpen;

        public ValuesSelector(IUpdateNotifier updateNotifier)
            : base(updateNotifier) { }

        public SelectValues Details { get; set; }

        public bool IsOpen { get => _isOpen; set => ChangeValue(ref _isOpen, value); }
    }
}