﻿using Assets.GameLogic.Tools.Notification;

namespace Assets.GameUI.State
{
    public class UnitsOrderSelection : Notifier<UnitsOrderSelection>
    {
        private bool _isOpen;

        public UnitsOrderSelection(IUpdateNotifier updateNotifier)
            : base(updateNotifier) { }

        public bool IsOpen { get => _isOpen; set => ChangeValue(ref _isOpen, value); }
    }
}