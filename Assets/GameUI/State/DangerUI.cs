﻿using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameUI.State
{
    public class DangerUI : Notifier<DangerUI>
    {
        private bool _isOpen;
        private DangerCard _dangerCard;

        public DangerUI(IUpdateNotifier updateNotifier) : base(updateNotifier) { }

        public bool IsOpen { get => _isOpen; set => ChangeValue(ref _isOpen, value); }
        public DangerCard DangerCard { get => _dangerCard; set => ChangeValue(ref _dangerCard, value); }
    }
}