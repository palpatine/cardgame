﻿using Assets.GameLogic.Tools.Notification;
using UnityEngine;

namespace Assets.GameUI.State
{
    public class DragDropState : Notifier<DragDropState>
    {
        private GameObject _dragged;
        private GameObject _dragTarget;
        private bool _isDragging;

        public DragDropState(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public GameObject Dragged { get => _dragged; set => ChangeValue(ref _dragged, value); }

        public GameObject DragTarget { get => _dragTarget; set => ChangeValue(ref _dragTarget, value); }

        public bool IsDragging { get => _isDragging; set => ChangeValue(ref _isDragging, value); }
    }
}