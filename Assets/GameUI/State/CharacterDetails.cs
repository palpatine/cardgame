﻿using Assets.GameLogic.State;
using Assets.GameLogic.Tools.Notification;

namespace Assets.GameUI.State
{
    public class CharacterDetails : Notifier<CharacterDetails>
    {
        private Character _character;
        private bool _isOpen;

        public CharacterDetails(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
        }

        public Character Character
        {
            get => _character; set { ChangeValue(ref _character, value); }
        }

        public bool IsOpen { get => _isOpen; set => ChangeValue(ref _isOpen, value); }
    }
}