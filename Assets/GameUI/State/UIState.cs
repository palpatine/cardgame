﻿using System.Collections.Generic;
using Assets.GameLogic.Tools.Notification;
using UnityEngine.EventSystems;

namespace Assets.GameUI.State
{
    public class UIState : Notifier<UIState>
    {
        private IEnumerable<RaycastResult> _currentActonRaycast;

        public UIState(IUpdateNotifier updateNotifier) : base(updateNotifier)
        {
            CharacterDetails = new CharacterDetails(updateNotifier);
            DragDropState = new DragDropState(updateNotifier);
            UnitsOrderSelection = new UnitsOrderSelection(updateNotifier);
            ValuesSelector = new ValuesSelector(updateNotifier);
            DangerUI = new DangerUI(updateNotifier);
        }

        public CharacterDetails CharacterDetails { get; }

        public IEnumerable<RaycastResult> CurrentActonRaycast
        {
            get => _currentActonRaycast;
            set => ChangeValue(ref _currentActonRaycast, value);
        }

        public DragDropState DragDropState { get; }

        public UnitsOrderSelection UnitsOrderSelection { get; }

        public ValuesSelector ValuesSelector { get; }

        public DangerUI DangerUI { get; } 
    }
}