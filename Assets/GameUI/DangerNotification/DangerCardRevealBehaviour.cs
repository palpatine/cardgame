﻿using System;
using System.Threading.Tasks;
using Assets.GameLogic.Tools.Notification;
using Assets.GameUI.ResourcePools;
using UnityEngine;
using Zenject;

namespace Assets.GameUI.DangerNotification
{
    public class DangerCardRevealBehaviour : MonoBehaviour
    {
#pragma warning disable CS0649
        [Inject]
        private IDangerCardBehaviourPool _dangerCardPool;
        [Inject]
        private IUpdateNotifier _updateNotifier;
        [Inject]
        private IUIGameStateProvider _gameStateProvider;
#pragma warning restore CS0649

        public void Start()
        {
            gameObject.SetActive(_gameStateProvider.UIState.DangerUI.IsOpen);

            _gameStateProvider.UIState.DangerUI.OnChange(x => x.IsOpen)
                .React(() =>
                    gameObject.SetActive(_gameStateProvider.UIState.DangerUI.IsOpen));

            _gameStateProvider.UIState.DangerUI.OnChange(x => x.DangerCard)
                .React(async x =>
                    await RevealDangerCard()
                );

            if (_gameStateProvider.UIState.DangerUI.IsOpen)
            {
                _updateNotifier.Await(RevealDangerCard());
            }
        }

        private async Task RevealDangerCard()
        {
            if (!_gameStateProvider.UIState.DangerUI.IsOpen)
            {
                return;
            }

            var card = _gameStateProvider.UIState.DangerUI.DangerCard;

            using (var behaviour = _dangerCardPool.Spawn(card))
            {
                behaviour.transform.localScale = Vector3.zero;
                var rect = (RectTransform)behaviour.transform;
                behaviour.transform.SetParent(transform);
                rect.position = new Vector3(Screen.width / 2, Screen.height / 2);
                var oversize = new Vector3(2.5f, 2.5f, 2.5f);
                var _ = new Vector3();

                while (Math.Abs(behaviour.transform.localScale.x - oversize.x) > 0.1)
                {
                    behaviour.transform.localScale = Vector3.SmoothDamp(
                    behaviour.transform.localScale,
                    oversize,
                    ref _,
                    Time.deltaTime * 80);
                    await _updateNotifier.NextUpdate;
                    if (Input.GetMouseButtonDown(0))
                    {
                        behaviour.transform.localScale = oversize;
                        await _updateNotifier.NextUpdate;
                        break;
                    }
                }

                while (!Input.GetMouseButtonDown(0))
                {
                    await _updateNotifier.NextUpdate;
                }
            }

            _gameStateProvider.UIState.DangerUI.IsOpen = false;
        }
    }
}