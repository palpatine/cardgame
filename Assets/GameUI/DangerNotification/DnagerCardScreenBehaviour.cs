﻿using Assets.GameLogic.Messages;
using Assets.GameLogic.Tools.Notification;
using UnityEngine;
using Zenject;

namespace Assets.GameUI.DangerNotification
{
    public class DnagerCardScreenBehaviour : MonoBehaviour
    {
#pragma warning disable CS0649
        [Inject]
        private IUIGameStateProvider _gameStateProvider;

#pragma warning restore CS0649

        public void Start()
        {
            this.Subscribe<DangerRevealed>(x =>
            {
                _gameStateProvider.UIState.DangerUI.DangerCard = x.DangerCard;
                _gameStateProvider.UIState.DangerUI.IsOpen = true;

                return _gameStateProvider.UIState.DangerUI.AwaitChangeOnce(y => y.IsOpen);
            });
        }
    }
}