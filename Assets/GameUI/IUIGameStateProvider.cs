﻿using Assets.GameLogic;
using Assets.GameUI.State;

namespace Assets.GameUI
{

    public interface IUIGameStateProvider : IGameStateProvider
    {
        UIState UIState { get; }
    }
}
