﻿using Assets.GameLogic.State;
using Assets.ResourcePools;
using UnityEngine;
using Zenject;

namespace Assets.GameUI.ResourcePools
{
    public interface IDangerCardBehaviourPool: IMemoryPool
    {
        DisposableMonoBehaviour Spawn(DangerCard card);
    }
}
