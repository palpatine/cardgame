﻿using System;
using UnityEngine;
using Zenject;

namespace Assets.ResourcePools
{
    public abstract class DisposableMonoBehaviour : MonoBehaviour, IDisposable
    {
        public abstract void Dispose();
    }
}
