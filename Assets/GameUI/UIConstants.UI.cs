﻿namespace Assets.GameUI
{
    public static partial class UIConstants
    {
        public static class UI
        {
            public const string AnimationAreaTag = "AnimationArea";

            public const string CardsInventoryTag = "CardsInventory";

            public const string CharacterDetailsContainerTag = "CharacterDetailsContainer";

            public const string InventoryItemTag = "InventoryItem";

            public const string InventoryItemTargetTag = "InventoryItemTarget";

            public const string MapContainerTag = "MapContainer";

            public const string UnitAvatarTag = "UnitAvatar";

            public static class ActionPrefab
            {
                public const string Location = @"Action";
            }

            public static class EquipmentSlotPrefab
            {
                public const string ItemName = "ItemName";
                public const string Location = @"CharacterDetails\EquipmentSlot";
                public const string SlotName = "SlotName";
            }

            public static class InventoryItemPrefab
            {
                public const string ItemName = "ItemName";
                public const string Location = @"CharacterDetails\InventoryItem";
            }

            public static class LevelContainerPrefab
            {
                public const string Location = @"LevelContainer";
            }

            public static class LevelEdgePrefab
            {
                public const string LevelEdgeTag = "LevelEdge";
                public const string Location = @"Cards\LevelEdge";
            }

            public static class RegenerableStatisticPrefab
            {
                public const string Current = "Current";
                public const string Location = @"CharacterDetails\RegenerableStatisitc";
                public const string Max = "Max";
                public const string Name = "Name";
                public const string Regeneration = "Regeneration";
            }

            public static class ReplenishableStatisticPrefab
            {
                public const string Current = "Current";
                public const string Location = @"CharacterDetails\ReplenishableStatistic";
                public const string Max = "Max";
                public const string Name = "Name";
            }

            public static class RuneCardSelector
            {
                public const string Accept = "Accept";
                public const string Cancel = "Cancel";
            }

            public static class TogglePrefab
            {
                public const string Location = @"CharacterDetails\CharacterToggle";
            }

            public static class TooltipDisplay
            {
                public const string Container = "Canvas";
                public const string Tooltip = "Tooltip";
                public const string TooltipTag = "Tooltip";
            }

            public static class ValueSelector
            {
                public const string CancelButton = "Cancel";
                public const string Location = @"ValuesSelector\ValueSelector";
                public const string Title = "Title";
                public const string Value = "Value";
            }

            public static class ValueStatisticPrefab
            {
                public const string Current = "Current";
                public const string Location = @"CharacterDetails\ValueStatistic";
                public const string Name = "Name";
            }
        }

    }
}
