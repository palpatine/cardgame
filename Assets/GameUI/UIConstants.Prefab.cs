﻿namespace Assets.GameUI
{
    public static partial class UIConstants
    {
        public static class Icons
        {
            public const string Resource = @"Icons\Resources\{0}";
        }

        public static class ActivityOptionPrefab
        {
            public const string Cost = "Cost";
            public const string Execute = "Execute";
            public const string Location = @"ActivityOption";
        }

        public static class ActivityOptionsSetPrefab
        {
            public const string Location = @"ActivityOptionsSet";
        }

        public static class Cards
        {
            public static class ActionCardPrefab
            {
                public const string ActionCardTag = "ActionCard";
                public const string DescriptionComponent = @"Description";
                public const string Location = @"Cards\ActionCard";
                public const string TitleComponent = @"Title";
            }

            public static class DangerCardPrefab
            {
                public const string DangerCardTag = "DangerCard";
                public const string DescriptionComponent = @"Description";
                public const string Location = @"Cards\DangerCard";
                public const string TitleComponent = @"Title";
            }

            public static class PlatformCardPrefab
            {
                public const string ActionArea = "ActionArea";
                public const string Air = "Air";
                public const string ExplorationActionAreaTag = "ExplorationActionArea";
                public const string ExplorationText = "Exploration";
                public const string FeaturesContainer = "FeaturesContainer";
                public const string ItemsContainer = "Items";
                public const string Location = @"Cards\PlatformCard\PlatformCard";
                public const string PlatformFeatureTag = "PlatformFeature";
                public const string PlatformKindText = "PlatformKind";
                public const string PlatformResourcesTag = "PlatformResources";
                public const string PlatformTag = "Platform";
                public const string ResourcesContainer = "ResourcesContainer";
                public const string UnitsAreaLeftTag = "UnitsAreaLeft";
                public const string UnitsAreaRightTag = "UnitsAreaRight";
                public const string UnitsContainerLeft = "UnitsContainerLeft";
                public const string UnitsContainerRight = "UnitsContainerRight";
                public const string BackgroundResourcePathTemplate = @"Cards\PlatformCard\empty_platform_{0}";
            }

            public static class PlatformCardResourceIndicatorPrefab
            {
                public const string Resource = "Resource";
                public const string Count = "Count";
            }

            public static class RuneCardPrefab
            {
                public const string CostComponent = @"Cost";
                public const string DescriptionComponent = @"Description";
                public const string Location = @"Cards\RuneCard";
                public const string RuneCardTag = "RuneCard";
                public const string TitleComponent = @"Title";
                public const string ValueComponent = @"Value";
            }

            public static class OreNuggetPrefab
            {
                public const string Location = @"Features\OreNugget";
            }

            public static class TreePrefab
            {
                public const string Location = @"Features\Tree";
            }

            public static class BoulderPrefab
            {
                public const string Location = @"Features\Boulder";
            }
        }

        public static class UnitAvatarPrefab
        {
            public const string FaceComponent = "Face";
            public const string Location = @"UnitAvatar";
        }

        public static class UnitPrefab
        {
            public const string ContainerComponent = "Container";
            public const string EquipmentComponent = "Equipment";
            public const string FaceComponent = "Face";
            public const string Location = "Unit";
            public const string UnitTag = "Unit";
        }

        public static class UnitStatisticChangeIndicatorPrefab
        {
            public static string DeltaComponent = "Delta";
            public static string Location = "UnitStatisticChangeIndicator";
            public static string NameComponent = "Name";
        }

    }
}
